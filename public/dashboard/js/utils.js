function popupMessage(type,title,message) {

    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: title,
        // (string | mandatory) the text inside the notification
        text: message,
        class_name: 'gritter-' + type + ' gritter-light'
    });


    setTimeout(function () {
        // $.gritter.removeAll();
    }, 3000)
}

function makeDataUrl(el){

    if ( ! window.FileReader ) {
        return alert( 'FileReader API is not supported by your browser.' );
    }
    var $i = $( '#' + el ), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    console.log(input);
    if ( input.files && input.files[0] ) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function () {
            // Do stuff on onload, use fr.result for contents of file

            // alert(fr.result);
            return fr.result;
        };
        //fr.readAsText( file );
        fr.readAsDataURL( file );
    } else {
        // Handle errors here
        alert( "File not selected or browser incompatible." )
    }
}

async function getBase64Image(el) {
    var dataUrl = await makeDataUrl(el);
    return dataUrl
}

function removeMedia(e) {
    // e.preventDefault();

    let action = $(e).data('action');
    let csrf = $('input[name="_token"]').val();
    let r = confirm('Are you sure want to delete?');

    if (r){

        let form = $("#dynamicImageDeleteForm");
        $(form).attr('action', action);
        $(form).submit();
        console.log($(form)[0]);

    }
}

function ucfirst(str) {

        var text = str;

        var parts = text.split(' '),
            len = parts.length,
            i, words = [];
        for (i = 0; i < len; i++) {
            var part = parts[i];
            var first = part[0].toUpperCase();
            var rest = part.substring(1, part.length);
            var word = first + rest;
            words.push(word);

        }

        return words.join(' ');
}

