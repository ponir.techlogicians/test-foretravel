<?php

class SettingsTableDataSeeder extends CsvSeeder
{
    protected $table  = 'settings';
    protected $csv    = 'settings.csv';
    protected $lookup = 'key';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
