<?php


class SlidersTableDataSeeder extends CsvSeeder
{
    protected $table  = 'sliders';
    protected $csv    = 'sliders.csv';
    protected $lookup = 'section';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
