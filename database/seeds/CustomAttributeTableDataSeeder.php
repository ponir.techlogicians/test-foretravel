<?php

class CustomAttributeTableDataSeeder extends CsvSeeder
{
    protected $table  = 'custom_attributes';
    protected $csv    = 'custom_attributes.csv';
    protected $lookup = 'slug';

    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
