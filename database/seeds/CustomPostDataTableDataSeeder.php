<?php

class CustomPostDataTableDataSeeder extends CsvSeeder
{
    protected $table  = 'custom_post_datas';
    protected $csv    = 'custom_post_datas.csv';
    protected $lookup = 'id';

    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
