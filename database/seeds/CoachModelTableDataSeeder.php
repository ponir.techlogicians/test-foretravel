<?php

class CoachModelTableDataSeeder extends CsvSeeder
{
    protected $table  = 'coach_models';
    protected $csv    = 'coach_models.csv';
    protected $lookup = 'slug';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
