<?php

class TestimonialsTableDataSeeder extends CsvSeeder
{
    protected $table  = 'testimonials';
    protected $csv    = 'testimonials.csv';
    protected $lookup = 'name';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
