<?php

class CustomReferenceCategoryTableDataSeeder extends CsvSeeder
{
    protected $table  = 'custom_reference_categories';
    protected $csv    = 'custom_reference_categories.csv';
    protected $lookup = 'slug';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
