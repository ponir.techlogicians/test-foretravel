<?php

use Illuminate\Database\Seeder;

class ConfiguratorSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableDataSeeder::class);
        // $this->call(CustomPostTableDataSeeder::class);
        // $this->call(CustomSectionTableDataSeeder::class);
        // $this->call(CustomAttributeTableDataSeeder::class);
        // $this->call(CoachModelTableDataSeeder::class);
        // $this->call(CustomReferenceCategoryTableDataSeeder::class);
        // $this->call(PageTableDataSeeder::class);
    }
}
