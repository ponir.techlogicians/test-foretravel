<?php

class CustomSectionTableDataSeeder extends CsvSeeder
{
    protected $table  = 'custom_sections';
    protected $csv    = 'custom_sections.csv';
    protected $lookup = 'slug';

    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
