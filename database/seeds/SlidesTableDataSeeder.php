<?php

class SlidesTableDataSeeder extends CsvSeeder
{
    protected $table  = 'slides';
    protected $csv    = 'slides.csv';
    protected $lookup = 'title';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
