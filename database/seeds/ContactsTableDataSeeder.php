<?php

class ContactsTableDataSeeder extends CsvSeeder
{
    protected $table  = 'contacts';
    protected $csv    = 'contacts.csv';
    protected $lookup = 'email';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
