<?php

use League\Csv\Reader;
use Illuminate\Database\Seeder;

class CsvSeeder extends Seeder
{
    /**
     * The table we are seeding.
     *
     * @var string
     */
    protected $table;

    /**
     * The filename, in the /database/seeds/csv folder.
     *
     * @var string
     */
    protected $csv;

    /**
     * A column (or array of columns) to lookup an existing record in the
     * database. If an existing record is matched, it will be updated in place.
     * If no record is matched, a new record will be inserted. Typically used
     * for slugs.
     *
     * @var string|array
     */
    protected $lookup;

    /**
     * The CSV headers array.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Whether the seeder should auto-populate the created_at/updated_at
     * columns. Defaults to true.
     *
     * @var boolean
     */
    protected $timestamps = true;

    /**
     * Seed the table based on the provided CSV data.
     *
     * @return void
     */
    protected function seedFromCsv()
    {
        // Get the CSV file.
        $csv = $this->getCsv();

        // Get all its records.
        $records = $csv->getRecords();

        // Loop through them all.
        foreach ($records as $offset => $record) {

            // Do some data processing.
            $record = $this->formatData($record);

            // Do we have a record to update?
            $update = $this->lookup($record);

            // If we found one, we need to update it. // PLEASE COMMENT IN IF DONT WANT TO UPDATE BUT INSERT
            if ($update) {
//                $this->update($update->id, $record);
            }

            // Otherwise, just insert it.
            else {
                $this->insert($record);
            }
        }
    }

    /**
     * Insert a new record.
     *
     * @param array $data
     *
     * @return void
     */
    protected function insert($data = [])
    {
        // Add timestamps, if necessary.
        if ($this->timestamps) {
            $data = array_merge($data, [
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s'),
            ]);
        }

        // Insert the record.
        DB::table($this->table)->insert($data);
    }

    /**
     * Update an existing record.
     *
     * @param integer $id
     * @param array $data
     *
     * @return void
     */
    protected function update($id, $data = [])
    {
        // Add timestamps, if necessary.
        if ($this->timestamps) {
            $data = array_merge($data, [
                'updated_at' => date('Y-m-d h:i:s'),
            ]);
        }

        // Update the record.
        DB::table($this->table)->where('id', $id)->update($data);
    }

    /**
     * Do some data formatting/cleanup.
     *
     * @param array $row
     *
     * @return array
     */
    protected function formatData($row = [])
    {
        // Process each column.
        foreach($row as $column => $value) {

            // Nullify empty strings.
            if($column == 'password' && $value !== ''){
                $row[$column] = bcrypt($value);
            }
            else if ($value === '') {
//                $row[$column] = null;
                $row[$column] = '...';
            }
        }

        // Done.
        return $row;
    }

    /**
     * Lookup an existing record that matches the columns we want.
     *
     * @param array $data
     *
     * @return \StdClass
     */
    protected function lookup($data = [])
    {
        // Start with an empty record.
        $record = null;

        // Not necessary if we have no lookups.
        if (!$this->lookup) {
            return $record;
        }

        // Single string lookup.
        if (is_string($this->lookup)) {

            // Get the record.
            $record = DB::table($this->table)->where($this->lookup, $data[$this->lookup])->first();
        }

        // Array lookup.
        elseif (is_array($this->lookup)) {

            // Start the query.
            $query = DB::table($this->table);

            // Add each lookup.
            foreach ($this->lookup as $column) {
                $query->where($column, $data[$column]);
            }

            // Get the record.
            $record = $query->first();
        }

        // Return whatever we found.
        return $record;
    }

    /**
     * Open the CSV file for this Seeder.
     *
     * @return Reader
     */
    protected function getCsv()
    {
        // Open the CSV file.
        $csv = Reader::createFromPath(database_path('seeds/csv/' . $this->csv), 'r');

        // Set the header offset.
        $csv->setHeaderOffset(0);

        // Get the headers.
        $this->headers = $csv->getHeader();

        // Return it.
        return $csv;
    }
}
