<?php

class PageTableDataSeeder extends CsvSeeder
{
    protected $table  = 'pages';
    protected $csv    = 'pages.csv';
    protected $lookup = 'key';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
