<?php

class UsersTableDataSeeder extends CsvSeeder
{
    protected $table  = 'users';
    protected $csv    = 'users.csv';
    protected $lookup = 'email';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
