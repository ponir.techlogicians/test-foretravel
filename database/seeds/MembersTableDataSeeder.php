<?php

class MembersTableDataSeeder extends CsvSeeder
{
    protected $table  = 'users';
    protected $csv    = 'members.csv';
    protected $lookup = 'email';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
