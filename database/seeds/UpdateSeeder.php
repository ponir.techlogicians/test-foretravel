<?php

use Illuminate\Database\Seeder;

class UpdateSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UsersTableDataSeeder::class);
//        $this->call(PageTableDataSeeder::class);
        $this->call(SettingsTableDataSeeder::class);
//        $this->call(ContactsTableDataSeeder::class);
//        $this->call(SlidersTableDataSeeder::class);
//        $this->call(SlidesTableDataSeeder::class);
//        $this->call(TestimonialsTableDataSeeder::class);
        $this->call(CustomPostTableDataSeeder::class);
        $this->call(CustomSectionTableDataSeeder::class);
        $this->call(CustomAttributeTableDataSeeder::class);
        $this->call(CoachModelTableDataSeeder::class);
        $this->call(CustomReferenceCategoryTableDataSeeder::class);
    }
}
