<?php

class CustomPostTableDataSeeder extends CsvSeeder
{
    protected $table  = 'custom_posts';
    protected $csv    = 'custom_posts.csv';
    protected $lookup = 'slug';

    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        $this->seedFromCsv();
    }
}
