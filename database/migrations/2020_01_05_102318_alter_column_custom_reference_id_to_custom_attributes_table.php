<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnCustomReferenceIdToCustomAttributesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_attributes', function (Blueprint $table) {
            $table->renameColumn('reference_id','reference_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_attributes', function (Blueprint $table) {
            $table->renameColumn('reference_type','reference_id');
//            $table->integer('reference_id')->change();
        });
    }
}
