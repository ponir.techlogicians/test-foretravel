<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomPostDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_post_datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('custom_post_id');
            $table->integer('custom_attribute_id');
            $table->integer('section_id');
            $table->string('page');
            $table->text('value')->nullable();
            $table->integer('sort')->default(99);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_post_datas');
    }
}
