<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddValidateCreateOnlyToCustomAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_attributes', function (Blueprint $table) {
            $table->boolean('validate_create_only')->after('validation_rules')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_attributes', function (Blueprint $table) {
            $table->dropColumn('validate_create_only');
        });
    }
}
