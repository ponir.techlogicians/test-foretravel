<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableVirtualTours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_virtual_tour', function (Blueprint $table) {

            $table->renameColumn('image','tour_url');
            $table->rename('virtual_tours');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('virtual_tours', function (Blueprint $table) {

            $table->rename('table_virtual_tour');
            $table->renameColumn('tour_url','image');

        });
    }
}
