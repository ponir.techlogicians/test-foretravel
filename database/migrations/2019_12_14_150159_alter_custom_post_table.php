<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCustomPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_posts', function (Blueprint $table) {

            $table->integer('order')->default(99)->change();
            $table->renameColumn('order','sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_posts', function (Blueprint $table) {

            $table->renameColumn('sort','order');
            $table->integer('order')->change();
        });
    }
}
