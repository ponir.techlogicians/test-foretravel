<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCategoryDescriptionCustomReferenceCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_reference_categories', function (Blueprint $table) {
            $table->text('description')->nullable()->after('type');
            $table->text('info')->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_reference_categories', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('info');
        });
    }
}
