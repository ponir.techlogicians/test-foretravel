<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->nullable();

            $table->string('register_password')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();

            $table->string('spouse_name')->nullable();
            $table->string('register_email')->nullable();
            $table->string('address_line_1')->nullable();
            $table->string('address_line_2')->nullable();

            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();

            $table->string('phone_number')->nullable();
            $table->string('cell_number')->nullable();

            $table->date('birthday')->nullable();
            $table->date('spouse_birthday')->nullable();
            $table->date('anniversary')->nullable();

            $table->string('coach_type')->nullable();
            $table->date('purchase_date')->nullable();
            $table->string('purchase_form')->nullable();
            $table->string('year_model')->nullable();

            $table->string('last_six_number_vin')->nullable();
            $table->string('complete_model_number')->nullable();

            $table->boolean('fmca_member')->default(0);
            $table->string('fmca_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(["username", "register_password", "first_name", "last_name", "spouse_name", "register_email", "address_line_1", "address_line_2", "city", "state", "zip", "phone_number", "cell_number", "birthday", "spouse_birthday", "anniversary", "coach_type", "purchase_date", "purchase_form", "year_model", "last_six_number_vin", "complete_model_number", "fmca_member", "fmca_number"]);
        });
    }
}
