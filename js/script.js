// let show_custom_scrollbar = true;

// if(Modernizr == undefined || Modernizr.touchevents){
//     show_custom_scrollbar = false;
// }

$(document).ready(function () {

    if($('.service-special-image-show').length){
        let spe_image = $('.service-special-image-show').simpleLightbox({
            disableScroll: false,
            captionPosition: 'outside',
            heightRatio: .7
        });
    }

    /* For opening all external url in a new tab*/
    $.expr[':'].external = function(obj){
        return !obj.href.match(/^mailto\:/)
               && (obj.hostname != location.hostname)
               && !obj.href.match(/^javascript\:/)
               && !obj.href.match(/^$/)
    };
    $('a:external').attr('target', '_blank');
    /* For opening all external url in a new tab*/

    // if(show_custom_scrollbar){

    //     const mScrollbar = $(".main").mCustomScrollbar({
    //         theme:"minimal-dark",
    //         axis: 'y',
    //         scrollEasing: false,
    //         // scrollInertia: 10000,
    //         mouseWheel:{
    //             scrollAmount: 300
    //         },
    //         callbacks:{
    //             whileScrolling: function(){
    //                 stickNavbar(this);
    //                 let section_href = [];
    //                 // let fragment;
    //                 $('.menu-list a').each(function(i,anchor){

    //                     // section_href.push()
    //                     let fragment = $(anchor).attr('href');
    //                     if(fragment.indexOf('#') > -1){
    //                         data = {
    //                             fragment: fragment,
    //                             start: ($(fragment).offset().top - $(fragment).parents(".mCSB_container").offset().top),
    //                             end: ($(fragment).offset().top - $(fragment).parents(".mCSB_container").offset().top) + $(fragment).height()
    //                         };
    //                         section_href.push(data);
    //                     }
    //                 });
    //                 scrollSpy(this,section_href);


    //                 var scroller = $(".mCSB_container");
    //                 var scrollerBox = scroller.closest(".mCustomScrollBox");
    //                //  $(".lazy[data-src]").filter(function() {
    //                //      var $this = $(this);
    //                //      if($this.attr("src") == $this.data("src")) return false;
    //                //      var scrollerTop = scroller.position().top;
    //                //      var scrollerHeight = scrollerBox.height();
    //                //      var offset = $this.closest("div").position();
    //                //      return (offset.top < scrollerHeight - scrollerTop);
    //                //  }).trigger("lazyScroll");

    //                // $('.lazy').lazy({
    //                //     bind: 'lazyScroll',
    //                //     beforeLoad: function(element) {
    //                //         console.log('beforeLoad');
    //                //         // called before an elements gets handled
    //                //     },
    //                //     afterLoad: function(element) {
    //                //         console.log('afterLoad');
    //                //         // called after an element was successfully handled
    //                //     },
    //                //     onError: function(element) {
    //                //         console.log('onError');
    //                //         // called whenever an element could not be handled
    //                //     },
    //                //     onFinishedAll: function() {
    //                //         console.log('onFinishedAll');
    //                //         // called once all elements was handled
    //                //     }
    //                // });
    //             }
    //         }

    //     });

    //     if (window.location.hash){
    //         fragmentScroll(window.location.hash)
    //     }

    // } else{
        $('.sidebar .model-list').scroll(function(){
            $('.sidebar .model-list .lazy').lazy({
                onError: function(element) {
                    $(element).attr('src','/img/no-image.png');
                    $(element).addClass('not-loaded');
                }
            });
        })
        $(".main").scroll(function() {
            $('.lazy').lazy({
                beforeLoad: function(element) {
                    // console.log('beforeLoad', element);
                    // called before an elements gets handled
                },
                afterLoad: function(element) {
                    // console.log('afterLoad', element);
                    // called after an element was successfully handled
                },
                onError: function(element) {
                    // console.log('onError', element);
                    $(element).attr('src','/img/no-image.png');
                    $(element).addClass('not-loaded');
                    // called whenever an element could not be handled
                },
                onFinishedAll: function(element) {
                    // console.log('onFinishedAll', element);
                    // called once all elements was handled
                }
            });
            stickNavbar();
            let section_href = [];

            // let fragment;
            $('.menu-list a').each(function(i,anchor){

                // section_href.push()
                let fragment = $(anchor).attr('href');
                if(fragment.indexOf('#') > -1){
                    data = {
                        fragment: fragment,
                        start: ($(fragment).position().top),
                        end: ($(fragment).position().top) + $(fragment).height()
                    };
                    section_href.push(data);
                }
            });
            scrollSpy(null,section_href);

            if(Modernizr.csspositionsticky == false){
                stickModelSubmenu();
            }
        });
    // }


    // event
    $('body').on('mouseover','.color-options .model-images .model-image img',function(){
        let image_src = $(this).attr('src');
        let image_title = $(this).attr('title');
        if(!$(this).closest('.image-wrapper').hasClass('active')){
            $(this).closest('.model-images').find('.image-wrapper.active').removeClass('active');
            $(this).closest('.image-wrapper').addClass('active');
        }

        $(this).closest('.row').find('.main-image').attr('src',image_src);
        $(this).closest('.row').find('.main-image-anchor').attr('href',image_src);
        $(this).closest('.row').find('.main-title').text(image_title);
    });

    $(document).on("click","a[href^='#']",function(e){
        const href = $(this).attr("href");
        if(href != '#'){
            let extraTop = 0;
            if($('.sticky').length){
              extraTop += $('.sticky').height();
            }
            if($('.menu-list').length){
              extraTop += $('.menu-list').height();
            }

            fragmentScroll(href,e,extraTop);
        }
    });

    // owner service - information tab - vendor contact
    $('body').on('click','.load-wrapper',function(){
        let table_height = 800;
        if(window.innerWidth >= 767){
            table_height = (table_height/1920)*window.innerWidth
        } else {
            table_height = (table_height/1920)*767;
        }

        if($(this).hasClass('loaded')){
            $(this).closest('.vendor-contact').find('.table-responsive').css('max-height',table_height+'px');
            $(this).removeClass('loaded');
        } else {
            let table_total_height = $(this).closest('.vendor-contact').find('.table-responsive .table-vendor-contact').height();
            $(this).closest('.vendor-contact').find('.table-responsive').css('max-height', table_total_height + 'px');
            $(this).addClass('loaded');
        }
    });

    $('body').on('click','.answer-wrapper .answer-link',function(){
        $(this).parent().toggleClass('show');
    });

    $('body').on('click','.open-popup',function(){
        let popop_title = '';
        let popop_body = '';

        popop_title = $(this).closest('.popup-content-parent').find('.popup-content-title').text();

        $(this).closest('.popup-content-parent').find('.popup-content-image').each(function(){
            popop_body += $(this)[0].outerHTML;
        });
        popop_body += $(this).closest('.popup-content-parent').find('.popup-content-body')[0].outerHTML;

        $('.popup .modal-body > .title').text(popop_title);
        $('.popup .modal-body > .body').html(popop_body);

        $('.popup .modal-body > .body').find('font').removeAttr('size');

        $('.popup').modal('show');
    });

    $('body').on('click', '.main', function(e){
        if(!$(e.target).hasClass('sidebar-toggler') && sidebar_open){
            closeSidebar()
        }
    });


    // url

    var url = document.location.toString();
    if (url.match('#')) {
        $('.blue-tab.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.blue-tab.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
        pageTitleAsBlueTab();
    })


    $('.team-members').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      // centerMode: true,
      responsive: [
            {
                breakpoint: 1201,
                settings: {
                      centerMode: false
                }
            },{
                breakpoint: 992,
                settings: {
                      slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: 'unslick'
            },
      ]
    }).on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.team-members .lazy').lazy({
            onError: function(element) {
                $(element).attr('src','/img/no-image.png');
                $(element).addClass('not-loaded');
                $(element).addClass('not-loaded');
            }
        });
    });

    $('.service-special').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,

        responsive: [
            {
                breakpoint: 1201,
                settings: {
                      centerMode: false
                }
            },{
                breakpoint: 992,
                settings: {
                      slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: 'unslick'
            },
      ]
    })

    

    $('body').on('click', '.slick-slider-prev', function(){
        $(this).closest('.section').find('.slick-slider').slick('slickPrev')
    })
    $('body').on('click', '.slick-slider-next', function(){
        $(this).closest('.section').find('.slick-slider').slick('slickNext')
    })
    $('body').on('click', '.go-to-top', function(){
        // if(show_custom_scrollbar){
        //     $('.main').mCustomScrollbar("scrollTo", 0);
        // } else {
            $('.main').animate({
                scrollTop: 0
            }, 500);
        // }
    });

    loaded();
});

function loaded(){
    $('.main').scrollTop(5);
    // $('.main').scrollTop(0);
    pageTitleAsBlueTab();
}

function pageTitleAsBlueTab(){
    const title = $('.blue-tab .active .tab-text').text();
    $('.page-title').text(title);
}

function fragmentScroll(href,e,extraTop){
    if(e == undefined){
        e = null
    }
    if(extraTop == undefined){
        extraTop = 0;
    }

    // if(show_custom_scrollbar){
    //     const target = $(href).parents(".mCustomScrollbar");
    //     if(target.length){
    //       if(e){
    //         e.preventDefault();
    //       }

    //       target.mCustomScrollbar("scrollTo",$(href).offset().top - $(href).parents(".mCSB_container").offset().top - extraTop);
    //     }
    // } else {
        const target = $(href).parents(".main");
        if(target.length){
          if(e){
            e.preventDefault();
          }

          target.animate({
              scrollTop: $(href).offset().top + target.scrollTop() - extraTop
          }, 500);
        }
    // }

}

function scrollSpy(scrollbarInstance, section_href){
    if(scrollbarInstance == undefined){
        scrollbarInstance = null;
    }

    let extraTop = 0;
    if($('.sticky').length){
      extraTop += $('.sticky').height();
    }
    if($('.menu-list').length){
      extraTop += $('.menu-list').height();
    }

    let currentPosition = 0;

    // if(scrollbarInstance){
    //     currentPosition = Math.abs(scrollbarInstance.mcs.top) + extraTop;

    //     section_href.forEach(function(section){
    //         if(section.start - extraTop < currentPosition  && section.end - extraTop >= currentPosition){
    //             let anchor = $('[href="'+section.fragment+'"]');
    //             if(!anchor.hasClass('active')){
    //                 $('.menu-list a').removeClass('active')
    //                 $(anchor).addClass('active')
    //             }
    //         }
    //     });
    // } else {
        currentPosition = $('.main').scrollTop();

        section_href.forEach(function(section){
            if((section.start + extraTop) < currentPosition  && (section.end + extraTop) >= currentPosition){
                let anchor = $('[href="'+section.fragment+'"]');
                if(!anchor.hasClass('active')){
                    $('.menu-list a').removeClass('active')
                    $(anchor).addClass('active')
                }
            }
        });
    // }

}

/**
 * Toggle sidebar
 * @type {boolean}
 */
let sidebar_open = false;
function openSidebar() {
    let sidebarPropertyValue = 500; //the value which will implement, when sidebar will be expand
    const window_width = window.innerWidth;
    if (window_width > 991){
        sidebarPropertyValue = ((sidebarPropertyValue/1920)*window_width) + 'px';
    } else if (window_width <= 991){
        sidebarPropertyValue = '300px';
    } else {
        sidebarPropertyValue = '200px';
    }

    // CSS variable not working on IE
    // document.documentElement.style.setProperty('--left-sidebar-width', sidebarPropertyValue);
    $('.sidebar').css('width',sidebarPropertyValue);
    $('.main').css('left',sidebarPropertyValue);

    sidebar_open = true;
}
function closeSidebar() {
    // CSS variable not working on IE
    // document.documentElement.style.setProperty('--left-sidebar-width', '0px');

    let sidebarPropertyValue = '0px'; //the value which will implement, when sidebar will be expand
    $('.sidebar').css('width',sidebarPropertyValue);
    $('.main').css('left',sidebarPropertyValue);

    sidebar_open = false;
}

function toggleSidebar() {
    if(sidebar_open){
        closeSidebar();
    } else {
        openSidebar();
    }
}
function smoothModelPageRedirect(instance,event){
    event.preventDefault();
    closeSidebar();
    let url = $(instance).attr('href');
    setTimeout(function(){
        window.location.href = url;
    },1000)
}

function getScrollbarWidth(){
    return (window.innerWidth - document.querySelector('.main').clientWidth);
}

/*
 * ================================= Sticky Menu Start =================================
 */
stickNavbar();
function stickNavbar(instance){
    if(instance == undefined){
        instance = null
    }
    // if(instance){
    //     sticky = Math.abs(instance.mcs.top);
    // } else {
        sticky = $(".main").scrollTop();
    // }
    // console.log($(".main .navbar-section").height(),sticky);
    if ($(".main .navbar-section").height() <= sticky) {
        $(".main .navbar-section").addClass("sticky");
        // if(!$(".main .navbar-section").hasClass('fixed')){
        //     $(".main .navbar-section").parent().css('padding-top',$(".main .navbar-section").height()+'px')
        // }

        if($('.go-to-top').length){
            $('.go-to-top').addClass('shown');
        }
    } else {
        $(".main .navbar-section").removeClass("sticky");

        // if(!$(".main .navbar-section").hasClass('fixed')){
        //     $(".main .navbar-section").parent().css('padding-top',0)
        // }


        if($('.go-to-top').length){
            $('.go-to-top').removeClass('shown');
        }
    }
}
/*
 * ================================= Sticky Menu End =================================
 */


/*
 * ================================= Sticky Model Sub Menu Start =================================
 */

if(Modernizr.csspositionsticky == false){
    stickModelSubmenu();
}

function stickModelSubmenu(){
    if($('.scroll-area .menu').length){
        sticky = $(".main").scrollTop();
        if ($(".main .cover").height()  <= sticky) {
            $('.scroll-area .menu').addClass("fixed");
            $('.scroll-area .menu').css("width", document.querySelector('.main').clientWidth + 'px');
            $('.scroll-area').css('padding-top', $('.scroll-area .menu').height()+'px');


            if($('.scroll-area').height() + $(".main .cover").height()  <= sticky){
                $('.scroll-area .menu').addClass("hidden");
            } else {
                $('.scroll-area .menu').removeClass("hidden");
            }
        } else {
            $('.scroll-area .menu').removeClass("fixed");
            $('.scroll-area').css('padding-top',0);
        }
    }
}

/*
 * ================================= Sticky Model Sub Menu Start =================================
 */
