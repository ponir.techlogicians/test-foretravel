<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\EmailLog;

class NotificationSend extends Mailable implements  ShouldQueue
{
    use Queueable, SerializesModels;

    private $type;
    private $content;
    private $send_by;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type, $content = '',$subject = 'New Motorcade Member Registration!!', $send_by = null)
    {
        $this->type     = $type;
        $this->content  = $content;
        $this->subject  = $subject;
        $this->send_by  = $send_by;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $template = 'templates.notifications.member-registration';

        if ($this->type == 'event-payment'){
            $template = 'templates.notifications.event-payment';
        }

        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject($this->subject)
            ->markdown($template)
            ->with([
                'body'  => $this->content
            ]);
    }
}
