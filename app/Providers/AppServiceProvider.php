<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\Setting;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $pageContents = [];

        // checking if table exists.
        if (Schema::hasTable('pages')){

            $pageContents = Page::orderBy('sort')->pluck('value','key')->toArray();

        }

        $set = Setting::pluck('value','key')->toArray();

//        dd($setting);

        View::share('set',$set);

        //////////////////////
        // Blade Directives //
        //////////////////////

        /**
         * Print page content from database according to key value pair
         *
         * Usage:
         *  @text("key")
         *  @return string
         */
        Blade::directive('text', function ($keyIndex) use($pageContents) {

            if (!empty($keyIndex)){
                if (isset($pageContents[$keyIndex])){
                    return $pageContents[$keyIndex];
                }
            }

            return '';
        });

        /**
         * Print page image sources from database according to key value pair associated with url path
         *
         * Usage:
         *  @text("key")
         *  @return string
         */
        Blade::directive('img', function ($keyIndex, $options = null) use($pageContents) {

            $alt = '';
            $class = '';
            $style = '';

//            dd($options);

            if (isset($options)){
                $class  .= $options['class'] or '';
                $style  .= $options['style'] or '';
                $alt    .= $options['alt'] or '';
            }

//            dd($pageContents['history_image']);
//            dd(in_array(trim($keyIndex,"'"),$pageContents));


            if (!empty($keyIndex)){
                if (isset($pageContents[$keyIndex])){

                    return '<img
                                data-src="' . asset($pageContents[$keyIndex]) . '"
                                alt="'. $alt .'"
                                class="' . $class . ' lazy"
                                style="' . $style . '"
                                >';
                }
            }

            return '<img src="" alt="no image" class="lazy">';
        });

        Schema::defaultStringLength(191);
    }
}
