<?php

namespace App\Console\Commands;

use DB;
use League\Csv\Writer;
use Illuminate\Console\Command;

class DbGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:generate {--csv}';
//    protected $signature = 'db:generate {tables} {--trashed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate files for CSV-based seeders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the tables to be seeded.
//        $tables = explode(',', $this->argument('tables'));
        $tables = $this->getImportableTables();

        // Loop through all tables.
        foreach($tables as $table) {

            // Create the file for this table.
            $this->createTableCsv($table);
        }
    }

    /**
     * Create the CSV file for a given table.
     *
     * @param string $table
     *
     * @return Writer
     */
    protected function createTableCsv($table)
    {
        // Start a new CSV file.
        $writer = Writer::createFromPath(database_path('seeds/csv/' . $table . '.csv'), 'w+');

        // Get the query we're going to need.
        $query = $this->getTableQuery($table);

        // Get all the data.
        $data = $query->get();

        // Grab the first row so we can write the headers.
        $first = $data->first();

        // skip if no record found
        if (!$first)
            return;

        // Get the array version.
        if (!is_array($first)) {
            $first = stdClassToArray($first);
        }

        // Kill a few things that we don't want present in the output.
        foreach($this->getIgnoredColumns() as $ignore) {
            unset($first[$ignore]);
        }

        // Get its array keys.
        $headers = array_keys($first);

        // Write it into the file.
        $writer->insertOne($headers);

        // Loop through everything.
        foreach($data as $row) {

            // Get the array version
            if (!is_array($row)) {
                $row = stdClassToArray($row);
            }

            // Kill a few things that we don't want present in the output.
            foreach($this->getIgnoredColumns() as $ignore) {
                unset($row[$ignore]);
            }

            // Write it into the file.
            $writer->insertOne($row);
        }

        // Output.
        $this->info('Wrote ' . count($data) . ' lines to ' . $table . '.csv');

        // Done.
        return $writer;
    }

    /**
     * Build a new query for the CSV generator.
     *
     * @param string  $table
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTableQuery($table)
    {
        // Start the query.
        $query = DB::table($table);

        // Probably throw out soft-deleted records.
//        if (!$this->option('trashed')) {
//            $query->whereNull('deleted_at');
//        }

        // That ought to do it.
        return $query;
    }

    /**
     * Get the array of columns to be ignored when creating the file.
     *
     * @return array
     */
    protected function getIgnoredColumns()
    {
        return [
            'id'
//            'created_at',
//            'updated_at',
//            'created_by_user_id',
//            'updated_by_user_id',
//            'deleted_by_user_id',
        ];
    }

    protected function getImportableTables()
    {
        return [
            'custom_posts',
            'custom_sections',
            'custom_attributes',
            'custom_reference_categories',
            'coach_models',
            'custom_post_datas',
            'pages',
            'testimonials',
            'floor_plans',
            'floorplan_items',
            'contacts',
            'settings',
            'sliders',
            'slides',
            'users',
        ];
    }
}
