<?php

namespace App\Console\Commands;

use PDF;
use App\Models\User;
use Illuminate\Console\Command;

class CheckMembershipExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:expiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check members subscription for 31st december for expiration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dayMonth = date("m-d");

        if ($dayMonth == '01-01'){

            User::where('role','motorcade-member')->update(['is_paid' => 0]);

            $this->info('All Member are set Subscription Expired');
        }
        else
            $this->info('All Member are set Subscription Expiration will be at 01-01 but now is '. date("m-d"));


        if (!is_dir(public_path('members'))){
            mkdir(public_path('members'));
        }


        // get motorcade members
        $members = User::where('role', 'motorcade-member')->orderBy('club_number')->get();

        $pdf = PDF::loadView('admin.members.make_members_pdf', ['members' => $members]);
        $pdf->save(public_path('members/byClubNumber.pdf'));

    }
}
