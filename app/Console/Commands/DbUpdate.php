<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class DbUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records from CSV-based seeders that may have been updated/appended since the last time the database was seeded.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('db:seed', ['--class' => 'UpdateSeeder']);
    }
}
