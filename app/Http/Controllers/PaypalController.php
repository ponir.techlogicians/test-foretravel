<?php
namespace App\Http\Controllers;

use App\Mail\NotificationSend;
use App\Models\MembersPaymentHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\WebProfile;
use PayPal\Api\ItemList;
use PayPal\Api\InputFields;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;


class PaypalController extends Controller
{
    private $apiContext;

    public function __construct()
    {
        # Main configuration in constructor
        $paypalConfig = Config::get('paypal');

        $this->apiContext = new ApiContext(new OAuthTokenCredential(
                $paypalConfig['client_id'],
                $paypalConfig['secret'])
        );

        $this->apiContext->setConfig($paypalConfig['settings']);
    }

    public function index()
    {
        return view('store.index');
    }

    public function payWithPayPal(Request $request)
    {
        # We initialize the payer object and set the payment method to PayPal
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        # We insert a new order in the order table with the 'initialised' status
//        $order = new Order();
//        $order->user_id = Auth::user()->id;
//        $order->invoice_id = null;
//        $order->status = 'initialised';
//        $order->save();

        # We need to update the order if the payment is complete, so we save it to the session
//        session('orderId', $order->getKey());

        # We get all the items from the cart and parse the array into the Item object
        $items = [];

//        foreach (Cart::content() as $item) {
//            $items[] = (new Item())
//                ->setName($item->name)
//                ->setCurrency('USD')
//                ->setQuantity($item->qty)
//                ->setPrice($item->price);
//        }

        $items[] = (new Item())
            ->setName(isset($request->pay_for)? _getItemAttributeValueById($request->pay_for,'tour_name') : "Membership Payment")
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice(isset($request->amount)? $request->amount : 95);

        # We create a new item list and assign the items to it
        $itemList = new ItemList();
        $itemList->setItems($items);

        # Disable all irrelevant PayPal aspects in payment
        $inputFields = new InputFields();
        $inputFields->setAllowNote(true)
            ->setNoShipping(1)
            ->setAddressOverride(0);

        $webProfile = new WebProfile();
        $webProfile->setName(uniqid())
            ->setInputFields($inputFields)
            ->setTemporary(true);

        $createProfile = $webProfile->create($this->apiContext);

        # We get the total price of the cart
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal(isset($request->amount)? $request->amount : 95);

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($itemList)
            ->setDescription(isset($request->pay_for)? _getItemAttributeValueById($request->pay_for,'tour_name') : "Membership Payment");

        $redirectURLs = new RedirectUrls();
        $redirectURLs->setReturnUrl(url('paypal-payment-status' . (isset($request->pay_for)? '?package='. $request->pay_for . '&amount='. $request->amount : '')))
            ->setCancelUrl(url('paypal-payment-status' . (isset($request->pay_for)? '?package='. $request->pay_for : '')));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectURLs)
            ->setTransactions(array($transaction));
        $payment->setExperienceProfileId($createProfile->getId());
        $payment->create($this->apiContext);

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirectURL = $link->getHref();
                break;
            }
        }

        # We store the payment ID into the session
        session('paypalPaymentId', $payment->getId());

        if (isset($redirectURL)) {
            return redirect($redirectURL);
        }

        session('error', 'There was a problem processing your payment. Please contact support.');

        return redirect('/motorcade-club');
    }

    public function getPaymentStatus(Request $request)
    {
//        $paymentId = $request->get('paypalPaymentId');
        $paymentId = $request->get('paymentId');
        $user = Auth::user();

//        dd($paymentId);

        # We now erase the payment ID from the session to avoid fraud
        Session::forget('paypalPaymentId');

        # If the payer ID or token isn't set, there was a corrupt response and instantly abort

        if (empty($request->get('PayerID')) || empty($request->get('token'))) {
            session('paypal-error-message', 'There was a problem processing your payment. Please contact support.');
            dd(session('paypal-error-message'));
            return redirect('motorcade-club');
        }

        $payment = Payment::get($paymentId, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));

        $result = $payment->execute($execution, $this->apiContext);

        # Payment is processing but may still fail due e.g to insufficient funds

        if ($result->getState() == 'approved') {

//            $invoice = new Invoice();
//            $invoice->price = $result->transactions[0]->getAmount()->getTotal();
//            $invoice->currency = $result->transactions[0]->getAmount()->getCurrency();
//            $invoice->customer_email = $result->getPayer()->getPayerInfo()->getEmail();
//            $invoice->customer_id = $result->getPayer()->getPayerInfo()->getPayerId();
//            $invoice->country_code = $result->getPayer()->getPayerInfo()->getCountryCode();
//            $invoice->payment_id = $result->getId();

            # We update the invoice status
//            $invoice->payment_status = 'approved';
//            $invoice->save();

            # We also update the order status
//            $order->invoice_id = $invoice->getKey();
//            $order->status = 'pending';
//            $order->save();

            # We insert the suborder (products) into the table
//            foreach (Cart::content() as $item) {
//                $suborder = new Suborder();
//                $suborder->order_id = $orderId;
//                $suborder->product_id = $item->id;
//                $suborder->price = $item->price;
//                $suborder->quantity = $item->qty;
//                $suborder->save();
//            }
//
//            Cart::destroy();

            if (isset($request->package)){

                $packageTitle = _getItemAttributeValueById($request->package,'tour_name');

                $history = new MembersPaymentHistory;
                $history->user_id       = $request->user()->id;
                $history->item_id       = $request->package;
                $history->description   = $packageTitle;
                $history->amount        = $request->amount;
                $history->save();

                /***
                 *  Notification Area
                 */

                // get the subject of notification email.
                $subject        = 'New Event Payment Notification!!';

                // get the body of notification email.
                $contentText    = ['name' => $request->user()->name, 'reason' => $packageTitle, 'amount' => $request->amount];

                // send notification email to admin
                Mail::to(env('MAIL_TO_ADDRESS'))->send(new NotificationSend('event-payment', $contentText, $subject));
            }
            else {

                $user->is_paid = 1;
                $user->save();
            }

            return Redirect::to('/motorcade-club')->with('paypal-success-message','Your payment was successful. Thank you.');
        }

        session('error', 'There was a problem processing your payment. Please contact support.');

        return Redirect::to('/home');
    }
}
