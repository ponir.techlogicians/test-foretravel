<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use Symfony\Component\Console\Input\Input;

class PageController extends BaseController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 'home',$tab = null)
    {

        $contents = Page::getContents($page,null,null,$tab);


        if ($tab)
            $sections = Page::where('name',$page)->where('tab',$tab)->orderBy('sort')->select(['pages.section'])->get()->toArray();
        else
            $sections = Page::where('name',$page)->orderBy('sort')->select(['pages.section'])->get()->toArray();

        $secs = [];
        foreach ($sections as $s){
            array_push($secs,$s['section']);
        }
        $sections = array_unique($secs);


//        dd(in_array($page,['home','about','model'])? $page : 'common');
//        $blade = in_array($page,['home','about'])? $page : 'common';
        $blade = 'common';

        $pageTitle = $tab? strtoupper($tab) : ( $page == 'information'? strtoupper('Resources'): strtoupper($page));
        $pageName = $page;
        $pageTab = $tab;
        $breads = $pageTitle;

        if ($page == 'model' && !$tab){

            return redirect('admin/page/model/realm');
        }

        if ($page == 'model' && $tab){
            $breads = ['Model',$tab];
        }

        // dd($pageName);
//        dd($blade);

        // Return home page data with info.
        return view('admin.pages.' . $blade, compact('contents','sections','pageTitle','pageName','pageTab','breads'));
    }

    public function update(Request $request)
    {
        $name = $request->name;
        $section = $request->section;

        $data = $request->request;
        $memes = $request->files;

        // dd($data);

        foreach ($data as $key => $value) {


            if (!in_array($key,['_token','name','section'])){

                Page::where('name', $name)->where('section',$section)->where('key',$key)->update(['value' => _eliminateBadChars($value)]);

            }


        }


        foreach ($memes as $key => $meme) {

//            dd($meme);

//            $token = explode('|', $meme);
//
//            $key        = $token[0];
//            $fakePath   = $token[1];
//
//            $ext = pathinfo($fakePath)['extension'];
//
//            $filename = time(). '_' . $key . '.'. $ext;
//            $newPath = public_path('img/pages/photo/') . $filename;
//            move_uploaded_file($fakePath, $newPath);
//            dd($fakePath);

            if($request->hasFile($key)){

                //
                $file = $request->file($key);
                $filename = time(). '_' . $key . '.' . $file->getClientOriginalExtension();
                $updloadDirectory = 'img/pages/photo/';
                $path = public_path($updloadDirectory);
                $file->move($path, $filename);

                // creating file accessing directory
                $file_path = $updloadDirectory.$filename;

                Page::where('name', $name)->where('section',$section)->where('key',$key)->update(['value' => $file_path]);

            }

        }
//            $slide->file_path = 'img/photo/'.$filename;

        return redirect('admin/page/' . $name . ($request->tab != ""? ('/'. $request->tab) : ''))->with('success','Data updated successfully');


    }

    public function deleteContent($pageName,$key,$pageTab=null,Request $request){
        //  $name = $request->name;
        //  dd($name);
        // dd($pageName);

        $file = Page::where('key',$key)->first();

        if ($file && file_exists($file->value)){
            unlink($file->value);
        }

        if ($file && $key){
            $file->update(['value' => '']);
        }

        if($pageTab !=null){
            return redirect('admin/page/'.$pageName.'/'.$pageTab)->with('success','Data updated successfully');
        }


        return redirect('admin/page/'.$pageName)->with('success','Data updated successfully');



    }
}
