<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FloorplanItem;
use App\Models\FloorPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FloorplanItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'text'             => 'required',
            // 'order'            => 'required',
           
            

        ]);
            
        

        if (gettype($request->text) == 'string') {
            $text_array = json_decode($request->text, true);
        } elseif (gettype($request->text == 'array')) {
            $text_array = $request->text;
        }

        
        
        if(count(array_filter($text_array))==count($request->text)){
            foreach ($text_array as $texts) {

                $floorplanitems = new FloorplanItem();
                $floorplanitems->text = $texts;
                $floorplanitems->created_by = Auth::user()->id;
                $floorplanitems->floorplan_id = $request->floorplan_id;
                $floorplanitems->save();
                
            }
        }else{
            
            return redirect()->route('admin.floorplans.edit',$request->floorplan_id);
        }
        
        

        return redirect()->route('admin.floorplans.edit',$request->floorplan_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floorplanitem = FloorplanItem::find($id);
        // dd($floorplanitem);

        return view('admin.floorplans.floorplanitems.edit',compact('floorplanitem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = $request->validate([

            'text'             => 'required',
            'order'            => 'required',
           
            

        ]);

        $floorplanitem = FloorplanItem::find($id);
        $floorplanitem->text = $request->text;
        $floorplanitem->order = $request->order;
        $floorplanitem->floorplan_id = $request->floorplan_id;
        $floorplanitem->updated_by = Auth::user()->id;
        $floorplanitem->update();

        return redirect()->route('admin.floorplans.edit',$request->floorplan_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floorplanitems = FloorplanItem::find($id);
        // dd($floorplanitems);
        $floorplanitems->delete();
        if($floorplanitems){
            return redirect()->route('admin.floorplans.edit',$floorplanitems->floorplan_id);
        }
    }
}
