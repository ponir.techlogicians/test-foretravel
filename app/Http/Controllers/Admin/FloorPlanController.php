<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachModel;
use App\Models\FloorplanItem;
use Illuminate\Http\Request;
use App\Models\FloorPlan;
use Illuminate\Support\Facades\Auth;


class FloorPlanController extends Controller
{


     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // fetching all floorplan list
        if ($request->has('model')){
            $floorplans = FloorPlan::where('model', $request->model)->orderBy('sort')->get();
        }
        else {
            $floorplans = FloorPlan::orderBy('sort')->get();
        }

        return view('admin.floorplans.index',compact('floorplans'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coachModels = CoachModel::all();

        return view('admin.floorplans.create',compact('coachModels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'model'             => 'required',
            'title'             => 'required',
            'image'             => 'required|image|mimes:jpeg,png,jpg,svg|max:10480'
        ]);

        $floorplan = new FloorPlan();
        $floorplan->model = $request->model;
        $floorplan->title = $request->title;
        $floorplan->created_by = Auth::user()->id;
        $floorplan->updated_by = Auth::user()->id;

        if($request->hasFile('image')){

            $file = $request->file('image');
            $filename = time(). '.' . $file->getClientOriginalExtension();
            $path = public_path('img/photo');
            $file->move($path, $filename);
            $floorplan->image = 'img/photo/'.$filename;

        }

        $floorplan->save();

        return redirect('admin/floorplans/'. $floorplan->id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success','floorplan saved successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floorplan = FloorPlan::with('floorPlanItems')->find($id);
        $coachModels = CoachModel::all();

        return view('admin.floorplans.edit',compact('floorplan','coachModels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $request->validate([
             'model'             => 'required',
             'title'             => 'required',
             'image'             => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'
         ]);

        $floorplan =  FloorPlan::find($id);
        $floorplan->model = $request->model;
        $floorplan->title = $request->title;
        $floorplan->updated_by = Auth::user()->id;

        if($request->hasFile('image')){
            $remove_previous_file = public_path().'/'.$floorplan->image;
            if(file_exists($remove_previous_file)){
                unlink($remove_previous_file);
            }
            $file = $request->file('image');
            $filename = time(). '.' . $file->getClientOriginalExtension();
            $path = public_path('img/photo');
            $file->move($path, $filename);
            $floorplan->image = 'img/photo/'.$filename;
        }

        $floorplan->save();

        return redirect('admin/floorplans/'. $floorplan->id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->withSuccess('floorplan saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floorplan = FloorPlan::find($id);

        if($floorplan){

            FloorplanItem::where('floorplan_id', $floorplan->id)->delete();
            $floorplan->delete();

            return redirect('admin/floorplans'. (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success','floorplan deleted successfully');
        }else{
            return redirect('admin/floorplans'. (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('error','floorplan deletion failed');
        }
    }

    public function addItems(Request $request){

        $items = array_filter($request->text);

        if (count($items)){

            FloorplanItem::where('floorplan_id',$request->floorplan_id)->delete();

            foreach ($items as $item) {

                $floorplanitem = new FloorplanItem();
                $floorplanitem->text = $item;
                $floorplanitem->created_by = Auth::user()->id;
                $floorplanitem->updated_by = Auth::user()->id;
                $floorplanitem->floorplan_id = $request->floorplan_id;
                $floorplanitem->save();
            }

            return redirect('admin/floorplans/'. $request->floorplan_id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success', 'Floor Plan item has updated successfully' );
        }

        return redirect('admin/floorplans/'. $request->floorplan_id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('error', 'Minimum one item should be added' );

    }

}
