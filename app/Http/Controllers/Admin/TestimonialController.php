<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimonial;
use Illuminate\Http\Request;
//use League\Flysystem\File;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $testimonials = Testimonial::get();
        // dd($testimonials);
        return view('admin.testimonials.index')->withTestimonials($testimonials);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data for storing testimonials
        $validator = $request->validate([

            'name'              => 'required',
            'designation'       => 'required',
            'company'           =>  'required',
            'photo'             => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'           => 'required',
            'background_image'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',


        ]);

        // if validation failed


        // if validation successfull
        $testimonial = new Testimonial();
        $testimonial->name = $request->name;
        $testimonial->designation = $request->designation;
        $testimonial->company = $request->company;

        // image validation and store
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename  = time() . '.' . $photo->getClientOriginalExtension();
			$path = public_path('img/photo/' . $filename);
			Image::make($photo->getRealPath())->resize(106,106)->save($path);
			$testimonial->photo = 'img/photo/'.$filename;
			$testimonial->save();
        }

        $testimonial->content = $request->description;

        // background_image validation and store
        if ($request->hasFile('background_image')) {
            $background_image = $request->file('background_image');
            $filename  = time() . '.' . $background_image->getClientOriginalExtension();
			$path = public_path('img/background_image/' . $filename);
			Image::make($background_image->getRealPath())->resize(622, 551)->save($path);
			$testimonial->background_image = 'img/background_image/'.$filename;
			$testimonial->save();
        }

        // dd(Auth::user());
        $testimonial->created_by = Auth::user()->id;
        // $testimonial->updated_by = Auth::user()->id;

        // saving testimonials
        $testimonial->save();

        // redirected to index page with success message
        return redirect()->route('admin.testimonials.index')->with('success', 'Testimonials updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // finding single testimonials
        $testimonial = Testimonial::find($id);

        // return view
        // return view('testimonials.show')->withTestimonial($testimonial);
        return view('admin.testimonials.show', compact('testimonial'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // finding single testimonial for edit
        $testimonial = Testimonial::find($id);

        // return view for edit
        return view('admin.testimonials.edit')->withTestimonial($testimonial);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // validate the data for storing testimonials
         $validator = $request->validate([

            'name'              => 'required',
            'designation'       => 'required',
            'company'           =>  'required',
            'description'       => 'required',

        ]);

        // if validation failed


        // if validation successfull
        $testimonial = Testimonial::find($id);
        $testimonial->name = $request->name;
        $testimonial->designation = $request->designation;
        $testimonial->company = $request->company;

        // image validation and store
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename  = time() . '.' . $photo->getClientOriginalExtension();
			$path = public_path('img/photo/' . $filename);
			Image::make($photo->getRealPath())->resize(106,106)->save($path);
			$testimonial->photo = 'img/photo/'.$filename;
			$testimonial->update();
        }

        $testimonial->content = $request->description;

        // background_image validation and store
        if ($request->hasFile('background_image')) {
            $background_image = $request->file('background_image');
            $filename  = time() . '.' . $background_image->getClientOriginalExtension();
			$path = public_path('img/background_image/' . $filename);
			Image::make($background_image->getRealPath())->resize(622,551)->save($path);
			$testimonial->background_image = 'img/background_image/'.$filename;
			$testimonial->update();
        }

        $testimonial->updated_by = Auth::user()->id;

        // saving testimonials
        $testimonial->update();

        // redirected to index page with success message
        return redirect()->route('admin.testimonials.index')->with('success', 'Testimonials updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        $image_path = public_path().'/'.$testimonial->photo;
        // dd($image_path);
        $background_image_path = public_path().'/'.$testimonial->background_image;
        
        // check if the image indeed exists
        if(file_exists($image_path)){
            unlink($image_path);
        } 
        if(file_exists($background_image_path)){
            unlink($background_image_path);
        } 
       
       
        $testimonial->delete();
        if ($testimonial) {
            return redirect()->route('admin.testimonials.index')->with('success', 'testimonial deleted successfully');
        } else {
            return redirect()->route('admin.testimonials.index')->with('error', 'testimonial deletion failed');
        }
    }
}
