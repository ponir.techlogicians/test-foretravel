<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CustomReferenceCategory;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FabricationController extends Controller
{


     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // fetching all floorplan list
        if ($request->has('model')){
//            $fabrications = FloorPlan::where('model', $request->model)->orderBy('sort')->get();
        }
        else {
            $fabrications = Page::select('tab')->where('parent','fabrication')->groupBy('tab')->get();
//            $fabrications = $fabrications->groupBy('tab');

//            dd($fabrications->get());
        }

        $fabrications = CustomReferenceCategory::where('type','fabrications')->orderBy('sort','asc')->get();
//dd($fabrications);
        return view('admin.fabrications.index',compact('fabrications'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.fabrications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name'             => 'required',
        ]);

        $tab = strtolower($request->name);
        $tab = str_replace('&','', $tab);
        $tab = str_replace('?','', $tab);
        $tab = str_replace('[','', $tab);
        $tab = str_replace(']','', $tab);
        $tab = str_replace('\'','', $tab);
        $tab = str_replace(' ','-', $tab);
        $tab = str_replace('--','-', $tab);

        // adding category section item
        $category = new CustomReferenceCategory;
        $category->slug = $tab;
        $category->label = $request->name;
        $category->type = 'fabrications';
        $category->save();

        // creating pages items
        $sections = [
            [
                'label'     => 'Header Title',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_header_title',
                'value'     => $request->name,
                'section'   => 'header',
                'type'      => 'text',
                'sort'      => 1,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Header Background Image',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_header_background_image',
                'value'     => '',
                'section'   => 'header',
                'type'      => 'image',
                'sort'      => 2,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Name',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_page_section_name',
                'value'     => '',
                'section'   => 'description',
                'type'      => 'text',
                'sort'      => 1,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Phone',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_page_section_phone',
                'value'     => '',
                'section'   => 'description',
                'type'      => 'text',
                'sort'      => 2,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Email',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_page_section_email',
                'value'     => '',
                'section'   => 'description',
                'type'      => 'text',
                'sort'      => 3,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Page Description',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_page_section_description',
                'value'     => 'Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor',
                'section'   => 'description',
                'type'      => 'longtext',
                'sort'      => 4,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Video Section Title',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_video_section_title',
                'value'     => 'Video',
                'section'   => 'fabrication_video',
                'type'      => 'text',
                'sort'      => 4,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Video Section Description',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_video_section_description',
                'value'     => 'Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor',
                'section'   => 'fabrication_video',
                'type'      => 'longtext',
                'sort'      => 5,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Gallery Section Title',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_gallery_section_title',
                'value'     => 'Gallery',
                'section'   => 'fabrication_gallery',
                'type'      => 'text',
                'sort'      => 6,
                'tab'       => $tab,
            ],
            [
                'label'     => 'Gallery Section Description',
                'key'       => 'fabrication_'. str_replace('-','_',$tab) .'_gallery_section_description',
                'value'     => 'Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor',
                'section'   => 'fabrication_gallery',
                'type'      => 'longtext',
                'sort'      => 7,
                'tab'       => $tab,
            ],
        ];

        foreach ($sections as $section){

            $fabrication                 = new Page();
            $fabrication->name           = 'fabrications';
            $fabrication->label          = $section['label'];
            $fabrication->key            = $section['key'];
            $fabrication->value          = $section['value'];
            $fabrication->section        = $section['section'];
            $fabrication->type           = $section['type'];
            $fabrication->sort           = $section['sort'];
            $fabrication->tab            = $section['tab'];
            $fabrication->parent            = 'fabrication';
            $fabrication->created_by     = Auth::user()->id;
            $fabrication->updated_by     = Auth::user()->id;
            $fabrication->save();
        }

        return redirect('admin/page/fabrications/'. $fabrication->tab. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success','saved successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $fabrication
     * @return \Illuminate\Http\Response
     */
    public function edit($fabrication)
    {
//        $fabrication = Page::where('tab',$fabrication)->first();
        $fabrication = CustomReferenceCategory::where('slug',$fabrication)->first();

//        dd($fabrication);

        return view('admin.fabrications.edit',compact('fabrication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {

        $validator = $request->validate([
            'name'             => 'required',
         ]);

        $tab = strtolower($request->name);
        $tab = str_replace('&','', $tab);
        $tab = str_replace('?','', $tab);
        $tab = str_replace('[','', $tab);
        $tab = str_replace(']','', $tab);
        $tab = str_replace('\'','', $tab);
        $tab = str_replace(' ','-', $tab);
        $tab = str_replace('--','-', $tab);

//        $fabrications =  Page::where('tab', $slug)->get();

        $fabrication = CustomReferenceCategory::where('slug',$slug)->first();
        if ($fabrication){
//            $fabrication->slug = $tab;
            $fabrication->label = $request->name;
            $fabrication->save();
        }

        return redirect('admin/page/fabrications/'. $fabrication->slug. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->withSuccess('saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fabrication = Page::where('tab',$id)->first();

        if($fabrication){

            Page::where('tab', $fabrication->tab)->delete();
            CustomReferenceCategory::where('slug', $fabrication->tab)->delete();

            return redirect('admin/fabrications'. (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success','floorplan deleted successfully');
        }else{
            return redirect('admin/fabrications'. (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('error','floorplan deletion failed');
        }
    }

    public function addItems(Request $request){

        $items = array_filter($request->text);

        if (count($items)){

            FloorplanItem::where('floorplan_id',$request->floorplan_id)->delete();

            foreach ($items as $item) {

                $fabricationitem = new FloorplanItem();
                $fabricationitem->text = $item;
                $fabricationitem->created_by = Auth::user()->id;
                $fabricationitem->updated_by = Auth::user()->id;
                $fabricationitem->floorplan_id = $request->floorplan_id;
                $fabricationitem->save();
            }

            return redirect('admin/floorplans/'. $request->floorplan_id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success', 'Floor Plan item has updated successfully' );
        }

        return redirect('admin/floorplans/'. $request->floorplan_id .'/edit'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('error', 'Minimum one item should be added' );

    }

}
