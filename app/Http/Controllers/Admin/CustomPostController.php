<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CustomAttribute;
use Illuminate\Http\Request;
use App\Models\CustomPost;
use App\Models\CustomSection;
use Illuminate\Support\Str;
use Auth;
use Response;
use Illuminate\Support\Facades\DB;


class CustomPostController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // getting all custom post
        $custom_posts = CustomPost::get();

        return view('admin.custompost.index',compact('custom_posts',$custom_posts));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        return view('admin.custompost.create');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'slug' => 'required|unique:custom_posts'
        ]);


        $cutompost = new CustomPost();
        $custom_section = new CustomSection();
        $cutompost->name = $request->name;
    
        if($request->slug){
            $cutompost->slug = Str::slug($request->slug, '-');
        }else{
            $cutompost->slug = Str::slug($request->name, '-');
        }
        
        $cutompost->created_by = Auth::user()->id;
        $cutompost->save();

        // custom section data save
        $custom_section->name = $request->name;
        if($request->slug){
            $custom_section->slug = Str::slug($request->slug, '-');
        }else{
            $custom_section->slug = Str::slug($request->name, '-');
        }

        $custom_section->custom_post_id = $cutompost->id;

        $custom_section->created_by = Auth::user()->id;

        $custom_section->save();
        
        return redirect()->route('admin.customposts.index')->with('success','Custom Post Created Successfully');
    }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function edit($id){
         
        $custompost = CustomPost::find($id);

        $custom_attributes = CustomAttribute::where('custom_post_id',$id)->get();


        
        // dd($custom_attributes);
        return view('admin.custompost.edit',compact('custompost','custom_attributes',$custompost,$custom_attributes));
     }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function update( Request $request ,$id){

        $request->validate([
            'slug' => 'unique:custom_posts,slug,'.$id,
        ]);
        $custompost = CustomPost::find($id);
        $custompost->name = $request->name;

        $custompost->slug = Str::slug($request->slug, '-');

        $custompost->update();
        return redirect()->route('admin.customposts.index')->with('success','Custompost Updated Successfully');
     } 


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function delete($id){

         $custompost = CustomPost::find($id);
         $custompost->delete();

         return redirect()->route('admin.customposts.index')->with('success','Custom Posts Deleted Successfully');


     }


     public function slug(Request $request){


        // dd($request->slug);
         $slug = createSlug($request->slug,'custom_posts');
         
         return Response::json($slug);
        
     }



}
