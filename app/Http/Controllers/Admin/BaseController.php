<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    public $menuOpen;

    public function __construct()
    {
        $this->menuOpen = new \stdClass();
        $this->menuOpen->m_home = 0;
    }


}
