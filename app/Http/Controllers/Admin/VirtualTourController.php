<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachModel;
use App\Models\FloorplanItem;
use App\Models\VirtualTour;
use Illuminate\Http\Request;
use App\Models\FloorPlan;
use Illuminate\Support\Facades\Auth;


class VirtualTourController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // fetching all floorplan list
        if ($request->has('model')) {
            $virtualtours = VirtualTour::where('model', $request->model)->orderBy('sort')->get();
        } else {
            $virtualtours = VirtualTour::orderBy('sort')->get();
        }

        return view('admin.virtualtours.index', compact('virtualtours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coachModels = CoachModel::all();
        return view('admin.virtualtours.create',compact('coachModels'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'model'             => 'required',
            'tour_url'          => 'required',
            'title'             => 'required',
            'thumb_image'       => 'required|image|mimes:jpeg,png,jpg,svg|max:10480'
        ]);

        $virtualtour = new VirtualTour();
        $virtualtour->model = $request->model;
        $virtualtour->tour_url = $request->tour_url;
        $virtualtour->title = $request->title;
        $virtualtour->created_by = Auth::user()->id;


        if ($request->hasFile('thumb_image')) {

            $file = $request->file('thumb_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('img/photo');
            $file->move($path, $filename);
            $virtualtour->thumb_image = 'img/photo/' . $filename;
        }

        $virtualtour->save();

        return redirect()->route('admin.virtual-tour.index')->with('success', 'Virtual Tour created successfully');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $virtualtour = VirtualTour::find($id);

        $coachModels = CoachModel::all();
        if ($virtualtour) {
            return view('admin.virtualtours.edit', compact('virtualtour','coachModels'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'model'             => 'required',
            'tour_url'          => 'required',
            'title'             => 'required',
        ]);

        $virtualtour = VirtualTour::find($id);
        if ($virtualtour) {

            $virtualtour->model = $request->model;
            $virtualtour->tour_url = $request->tour_url;
            $virtualtour->title = $request->title;
            $virtualtour->updated_by = Auth::user()->id;

            if ($request->hasFile('thumb_image')) {

                $file = $request->file('thumb_image');
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $path = public_path('img/photo');
                $file->move($path, $filename);
                $virtualtour->thumb_image = 'img/photo/' . $filename;
            }

            $virtualtour->update();

            return redirect()->route('admin.virtual-tour.index')->with('success','virtual tour updated successfully');
        }
    }

    public function destroy($id)
    {
        $virtualTour = VirtualTour::find($id);

        if($virtualTour){

            $virtualTour->delete();

            return redirect()->route('admin.virtual-tour.index')->with('success','virtual tour deleted successfully');
        }else{
            return redirect()->route('admin.virtual-tour.index')->with('error','virtual tour deletion failed');
        }
    }
}
