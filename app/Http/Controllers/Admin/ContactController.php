<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class ContactController extends Controller
{
    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::get();

        return view('admin.contacts.index',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'name'             => 'required',
            'email'            => 'required|email',
            'interested_in'    => 'required',
            'message'          => 'required|min:10',

        ]);

        $clientip = \Request::getClientIp(true);

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->interested_in = $request->interested_in;
        $contact->message  = $request->message;
        $contact->ip_address  = $clientip;

        $contact->save();

        return redirect()->route('admin.contacts.index')->with('success', 'Contact created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        return view('admin.contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);
        return view('admin.contacts.edit')->withContact($contact);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([

            'name'             => 'required',
            'email'            => 'required|email',
            'interested_in'    => 'required',
            'message'          => 'required|min:10',

        ]);

        $clientip = \Request::getClientIp(true);

        $contact =  Contact::find($id);
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->interested_in = $request->interested_in;
        $contact->message  = $request->message;
        $contact->ip_address  = $clientip;
        $contact->updated_by  = Auth::user()->id;

        $contact->update();

        return redirect()->route('admin.contacts.index')->with('success', 'Contact updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);

        $contact->delete();

        if($contact){
            return redirect()->route('admin.contacts.index')->with('success' , 'contact deleted successfully');
        }
        else{
            return redirect()->route('admin.contacts.index')->with('error' , 'contact deletion failed');
        }
    }
}
