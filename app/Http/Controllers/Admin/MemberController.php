<?php

namespace App\Http\Controllers\Admin;


use App\Jobs\RegenerateMembersPDF;
use App\Models\MembersPaymentHistory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use PDF;

class MemberController extends Controller
{
    /**
     * List all Users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = $members = User::where('role','motorcade-member')->get();
//        dispatch(new RegenerateMembersPDF());

        return view('admin.members.index', compact('users'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // Return the view.
        return view('admin.members.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // Validate.
        $this->validate($request, [
//            'first_name'            => 'required',
//            'last_name'             => 'required',
            'email'                 => 'nullable|email',
//            'address_line_1'        => 'required',
//            'city'                  => 'required',
//            'state'                 => 'required',
//            'zip'                   => 'required',
//            'coach_type'            => 'required',
//            'year_model'            => 'required',
//            'last_six_number_vin'   => 'required',
//            'complete_model_number' => 'required',
        ]);

        // Create the new user.
        User::create([
            'name'                  => $request->last_name . ', ' . $request->first_name . (isset($request->spouse_name) && $request->spouse_name !=""? ' & ' .$request->spouse_name : ''),
            'password'              => bcrypt('SECRET'),
            'first_name'            => $request->first_name,
            'last_name'             => $request->last_name,
            'email'                 => $request->email,
            'spouse_name'           => $request->spouse_name,
            'address_line_1'        => $request->address_line_1,
            'address_line_2'        => $request->address_line_2,
            'city'                  => $request->city,
            'state'                 => $request->state,
            'zip'                   => $request->zip,
            'phone_number'          => $request->phone_number,
            'cell_number'           => $request->cell_number,
            'birthday'              => $request->birthday,
            'spouse_birthday'       => $request->spouse_birthday,
            'anniversary'           => $request->anniversary,
            'coach_type'            => $request->coach_type,
            'purchase_date'         => $request->purchase_date,
            'purchase_form'         => $request->purchase_form,
            'year_model'            => $request->year_model,
            'last_six_number_vin'   => $request->last_six_number_vin,
            'complete_model_number' => $request->complete_model_number,
            'fmca_member'           => isset($request->fmca_member)? $request->fmca_member : 0,
            'fmca_number'           => $request->fmca_number,
            'club_number'           => $request->club_number,
            'is_paid'               => 1,
            'role'                  => 'motorcade-member'
        ]);

        // regenerating members pdf
//        dispatch(new RegenerateMembersPDF());
//        _regenerateMembershipDirectory();

        // Done.
        return redirect()->route('admin.members.index')->with('success', 'The member was created successfully.');
    }

    /**
     * Display details about the specified User.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // check role
        if (Auth::user()->role == 'user'){

            return view('admin.pages.error');
        }

    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);

        // Return the view.
        return view('admin.members.edit', compact('user'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Validate.
        $this->validate($request, [
//            'first_name'            => 'required',
//            'last_name'             => 'required',
            'email'                 => 'nullable|email',
            'username'              => 'nullable|unique:users,username,'. $request->id,
            'password'              => 'nullable|min:6|confirmed',
            'password_confirmation' => 'nullable',
//            'address_line_1'        => 'required',
//            'city'                  => 'required',
//            'state'                 => 'required',
//            'zip'                   => 'required',
//            'coach_type'            => 'required',
//            'year_model'            => 'required',
//            'last_six_number_vin'   => 'required',
//            'complete_model_number' => 'required',
        ]);

        $user = User::findOrFail($request->id);

        // Also change their password, if necessary.
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        // Save.
        $user->save();

        User::where('id', $user->id)
            ->update([
                'name'                  => $request->last_name . ', ' . $request->first_name . (isset($request->spouse_name) && $request->spouse_name !=""? ' & ' .$request->spouse_name : ''),
                'username'              => $request->username,
                'first_name'            => $request->first_name,
                'last_name'             => $request->last_name,
                'email'                 => $request->email,
                'spouse_name'           => $request->spouse_name,
                'address_line_1'        => $request->address_line_1,
                'address_line_2'        => $request->address_line_2,
                'city'                  => $request->city,
                'state'                 => $request->state,
                'zip'                   => $request->zip,
                'phone_number'          => $request->phone_number,
                'cell_number'           => $request->cell_number,
                'birthday'              => $request->birthday,
                'spouse_birthday'       => $request->spouse_birthday,
                'anniversary'           => $request->anniversary,
                'coach_type'            => $request->coach_type,
                'purchase_date'         => $request->purchase_date,
                'purchase_form'         => $request->purchase_form,
                'year_model'            => $request->year_model,
                'last_six_number_vin'   => $request->last_six_number_vin,
                'complete_model_number' => $request->complete_model_number,
                'fmca_member'           => isset($request->fmca_member)? $request->fmca_member : 0,
                'fmca_number'           => $request->fmca_number,
                'club_number'           => $request->club_number,
            ]);

        // regenerating members pdf
//        dispatch(new RegenerateMembersPDF());
//        _regenerateMembershipDirectory();

        // Done.
        return redirect()->route('admin.members.index')->with('success', 'The User was updated successfully.');
    }


    /**
     * Remove the specified User from storage.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Delete the user.
        $user = User::findOrFail($id);
        $user->delete();

//        _regenerateMembershipDirectory();

        // Done.
        return redirect()->route('admin.members.index')->with('success', 'The User was deleted successfully.');
    }

    /**
     * List all Members who paid for custom events.
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentHistory(Request $request, $package = null)
    {

        $payments = MembersPaymentHistory::all();

        return view('admin.members.payments', compact('payments'));
    }


}
