<?php

namespace App\Http\Controllers\Admin;

use App\Models\CoachModel;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Slide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $sliders = Slider::get();
        return view('admin.sliders.index')->withSliders($sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'name'             => 'required|unique:sliders,name',
            'description'      => 'required',

        ]);

        $slider = New Slider();
        $slider->name = $request->name;
        $slider->section = makeSlugByName($slider->name);
        $slider->description = $request->description;
        $slider->created_by = Auth::user()->id;

        $slider->save();
        return redirect()->route('admin.sliders.index')->with('success','slider created successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $slider = Slider::with('slides')->where('section',$id)->first();

        $slide = Slide::with('coachModel')->where('slider_id',$slider->id)->get();

        $coachModels = CoachModel::all();

        $param = null;

        if($request->has('model')){
            $param = $request->get('model');

            $slider = Slider::with(['slides' => function($query) use ($param){
                $query->where('model', $param);
            }])->where('section',$id)->first();
//            dd($slide);
        }

        return view('admin.sliders.edit',compact('slider','slide','coachModels','param'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([

            'name'             => 'required|unique:sliders,name,'. $id,
            'description'      => 'required',

        ]);
        $slider = Slider::find($id);
        $slider->name = $request->name;
        $slider->section = makeSlugByName($slider->name);
        $slider->description = $request->description;
        $slider->updated_by = Auth::user()->id;

        $slider->update();
        return redirect()->route('admin.sliders.index')->with('success','slider updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $slider = Slider::find($id);
        $slides = slide::where('slider_id',$slider->id);
        $slides->delete();
        $slider->delete();
        if($slider){
            return redirect()->route('admin.sliders.index')->with('success','slider deleted successfully');

        }else{
            return redirect()->route('admin.sliders.index')->with('success','slider deletion fialed');
        }
    }

}
