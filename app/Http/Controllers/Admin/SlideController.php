<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\Slider;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'model'                 => 'required',
            'title'                 => 'required',
//            'caption'               => 'required',
//            'file_type'             => 'required',
            'file_path'             => 'required|mimetypes:image/jpg,image/jpeg,image/svg,image/png,video/avi,video/mpeg,video/mp4,video/quicktime|max:40000',
             'slider_id'            => 'required',
//             'order'                =>  'required'

        ]);

        $slide = New Slide();
        $slide->model = $request->model;
        $slide->title = $request->title;
        $slide->caption = $request->caption;


        if($request->hasFile('file_path')){

            $file               = $request->file('file_path');
            $ext                = $file->getClientOriginalExtension();
            $filename           = time(). '.' . $ext;
            $path               = public_path('img/photo');
            $file->move($path, $filename);
            $slide->file_path   = 'img/photo/'.$filename;
            $slide->file_type   = _getOriginalFileType($ext);
        }

//        $slide->order = $request->order;
        $slide->slider_id = $request->slider_id;
        $slide->created_by = Auth::user()->id;
        $slide->updated_by = Auth::user()->id;
        $slide->save();


         return redirect(
                'admin/sliders/'.$slide->slider->section .'/edit'. (isset($_GET['model'])?
                     ('?model=' . $_GET['model']) : '')
                )
                ->with('success','An Item has Successfully Added');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $slide = Slide::find($id);

        $slider = Slider::where('id',$slide->slider_id)->first();
//dd($slide);
//dd($slider);

        $breadcrumbs = _breadcrumbsGenerator(
            [
                [
                    'url'   => 'admin/page/model/' . $slide->model ,
                    'label' => makeLabelBySlug($slide->model),
                    'q'     => '?model=' . $slide->model
                ],
                [
                    'url'   => 'admin/sliders/' . $slider->section .'/edit' ,
                    'label' => $slider->name,
                    'q'     => '?model=' . $slide->model
                ],
                [
                    'url'   => '' ,
                    'label' => $slide->title,
                    'q'     => '?model=' . $slide->model
                ],
            ]
        );



        return view('admin.slide.edit')->withSlide($slide)->withSlider($slider)->withBreadcrumbs($breadcrumbs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = $request->validate([

             'title'                 => 'required',
//             'caption'               => 'required',
//             'file_type'             => 'required',
             'file_path'             => 'nullable|mimetypes:image/jpg,image/jpeg,image/svg,image/png,video/avi,video/mpeg,video/mp4,video/quicktime|max:40000',


         ]);

        // persisting to slide object
        $slide              = Slide::find($id);
        $slide->title       = $request->title;
        $slide->caption     = $request->caption;
        $slide->file_type   = $request->file_type;

        // uploading file
        if($request->hasFile('file_path')){
            $remove_previous_file = public_path().'/'.$slide->file_path;
            if(file_exists($remove_previous_file)){
                unlink($remove_previous_file);
            }

            $file               = $request->file('file_path');
            $ext                = $file->getClientOriginalExtension();
            $filename           = time(). '.' . $file->getClientOriginalExtension();
            $path               = public_path('img/photo');
            $file->move($path, $filename);
            $slide->file_path   = 'img/photo/'.$filename;
            $slide->file_type   = _getOriginalFileType($ext);
        }

        $slide->updated_by  = Auth::user()->id;
        $slide->save();

         return redirect(
                'admin/sliders/'.$slide->slider->section .'/edit?model=' . $slide->model
                )
                ->with('success','An Item has Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $slides = Slide::find($id);

        $image_path = public_path().'/'.$slides->file_path;
        if(file_exists($image_path)){
            unlink($image_path);
        }

        if($slides != null){
            $slides->delete();
            return redirect('admin/sliders/'. $slides->slider->section .'/edit' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->with('success','An Item has Successfully Deleted');
        }


    }
}
