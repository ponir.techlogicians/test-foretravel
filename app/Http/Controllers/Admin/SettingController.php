<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use DB;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::get();
        return view('admin.setting.index')->withSettings($settings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  $validator = $request->validate([
        //      'value' => 'required'
        //  ]);

        //  dd($request->all());

        $settings = Setting::get();
      

        $request->offsetUnset('_token');
        
        foreach ($request->all() as $key => $value_text) {

            if(is_string($value_text)){
                DB::table('settings')->where("settings.key", '=', $key)
                ->update(['settings.value'=> $value_text]);
            }
        
        }

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $filename = time(). '.' . $file->getClientOriginalExtension();
            $path = public_path('img/settingphoto');
            $file->move($path, $filename);
            Setting::where("key" , '=' , 'logo')
            ->update(['value' => 'img/settingphoto/'.$filename]);
            
        }

        if($request->hasFile('video')){
            // dd(1);
            $file = $request->file('video');
            $filename = time(). '.' . $file->getClientOriginalExtension();
            $path = public_path('img/settingvideo');
            $file->move($path, $filename);
            Setting::where("key" , '=' , 'video')
            ->update(['value' => 'img/settingvideo/'.$filename]);
            
        }

        return redirect()->route('admin.settings.edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::find($id);

        return view('admin.setting.edit')->withSetting($setting);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = Setting::find($id);

        $setting->delete();

        if($setting){
            return redirect()->route('settings.index')->with('success' , 'settings deleted successfully');
        }else{

            return redirect()->route('settings.index')->with('error' , 'settings deletions failed');
        }
    }
}
