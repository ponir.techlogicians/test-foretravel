<?php

namespace App\Http\Controllers\Admin;

use App\Models\CoachModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Return view.
        return view('admin.home');
    }

    public function models(Request $request){


        $models = CoachModel::orderBy('sort')->get();

        return view('admin.models.index',compact('models'));


    }

    public function editModel(Request $request, $id){

        $model = CoachModel::findOrFail($id);

        return view('admin.models.edit',compact('model'));
    }

    public function updateModel(Request $request, $id){

        $validator = $request->validate([
            'title'             => 'required',
            'image'             => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'
        ]);

        $model =  CoachModel::find($id);
        $model->title = $request->title;
        $model->created_by = Auth::user()->id;

        if($request->hasFile('image')){
            $remove_previous_file = public_path().'/'.$model->image;
            if(file_exists($remove_previous_file)){
                unlink($remove_previous_file);
            }

            $file = $request->file('image');
            $filename = time(). '.' . $file->getClientOriginalExtension();
            $path = public_path('img/model/sidebar');
            $file->move($path, $filename);
            $model->image = 'img/model/sidebar/'.$filename;
        }

        $model->save();

        return redirect('admin/models'. (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''))->withSuccess('model saved successfully');

    }

}
