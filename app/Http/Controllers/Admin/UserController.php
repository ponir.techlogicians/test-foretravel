<?php

namespace App\Http\Controllers\Admin;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * List all Users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // check role
        if (Auth::user()->role == 'user'){

            return view('admin.pages.error');
        }

        if (Auth::user()->role == 'superadmin'){

            $users = User::all();
        }
        else {
            $users = User::where('role','user')->orWhere('role','admin')->get();
        }

        // Return the view.
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // check role
        if (Auth::user()->role == 'user'){

            return view('admin.pages.error');
        }

        // Return the view.
        return view('admin.users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // Validate.
        $this->validate($request, [
            'name'         => 'required',
            'email'        => 'required|email|unique:users',
            'password'     => 'required:min:6|confirmed',
            'password_confirmation'     => 'required',
            'role'         => 'required',
        ]);

//        dd($request);
        // Create the new user.
        User::create([
            'name'         => $request->name,
            'email'        => $request->email,
            'password'     => bcrypt($request->password),
            'role'         => $request->role
        ]);

        // Done.
        return redirect()->route('admin.users.index')->with('success', 'The user was created successfully.');
    }

    /**
     * Display details about the specified User.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // check role
        if (Auth::user()->role == 'user'){

            return view('admin.pages.error');
        }

//        $this->authorize('view', $role);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        // check role
        if (Auth::user()->role == 'user'){

            return view('admin.pages.error');
        }

        // Return the view.
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        // Validate.
        $this->validate($request, [
            'name'         => 'required',
            'email'        => 'required|email|unique:users,email,'. $user->id,
            'password'     => 'nullable|min:6|confirmed',
            'password_confirmation'     => 'nullable',
//            'role'         => 'required',
        ]);

        // Update the user.
        $user->name         = $request->name;
        $user->email        = $request->email;

        if (Auth::user()->role == 'superadmin'){
            $user->role         = $request->role;
        }

        // Also change their password, if necessary.
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        // Save.
        $user->save();

        // Done.
        return redirect()->route('admin.users.index')->with('success', 'The User was updated successfully.');
    }


    /**
     * Remove the specified User from storage.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // Delete the user.
        $user->delete();

        // Done.
        return redirect()->route('admin.users.index')->with('success', 'The User was deleted successfully.');
    }
}
