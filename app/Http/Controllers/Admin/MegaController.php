<?php

namespace App\Http\Controllers\Admin;

use App\Models\CustomPost;
use App\Models\CustomPostData;
use App\Models\CustomReferenceCategory;
use App\Models\CustomSection;
use App\Models\FloorPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use PhpParser\Node\Expr\Cast\Object_;

class MegaController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$customPostSlug, $sectionSlug = null)
    {

        $customPost = CustomPost::where('slug', $customPostSlug)->first();

        if ($customPost){

            $items = DB::table('custom_post_datas')
                ->leftJoin('custom_posts', 'custom_posts.id', '=', 'custom_post_datas.custom_post_id')
                ->where('custom_posts.slug', $customPostSlug)
                ->groupBy('custom_post_datas.item_id')
                ->orderBy('custom_post_datas.item_id')
                ->select(
                    'custom_post_datas.item_id'
                )
                ->get();

            $attributes = DB::table('custom_attributes')
                ->leftJoin('custom_posts', 'custom_posts.id', '=', 'custom_attributes.custom_post_id')
                ->where('custom_posts.slug', $customPostSlug)
                ->orderBy('custom_attributes.sort')
                ->select(
                    'custom_attributes.*',
                    'custom_posts.name as pageTitle',
                    'custom_posts.slug as pageSlug'
                )
                ->get();

            $posts = DB::table('custom_post_datas')
                ->leftJoin('custom_posts', 'custom_posts.id', '=', 'custom_post_datas.custom_post_id')
                ->leftJoin('custom_sections', 'custom_sections.id', '=', 'custom_post_datas.section_id')
                ->leftJoin('custom_attributes', 'custom_attributes.id', '=', 'custom_post_datas.custom_attribute_id')
                ->leftJoin('users', 'users.id', '=', 'custom_post_datas.updated_by')
                ->where('custom_posts.slug', $customPostSlug)
//                ->orderBy('custom_post_datas.item_id','desc')
                ->orderBy('custom_post_datas.sort')
                ->select(
                    'custom_post_datas.value',
                    'custom_post_datas.item_id',
                    'custom_attributes.*',
                    'custom_posts.name as post_name',
                    'custom_sections.name as pageSection',
                    'custom_sections.slug as sectionSlug',
                    'users.name as updatedBy'
                )
                ->get();
//dd($posts);
            $categories = CustomReferenceCategory::pluck('label','id')->toArray();

//            if ($sectionSlug)
//                $posts = $posts->where('sectionSlug',$sectionSlug);

//            dd($posts);

//            $columns['section'] = "Page Section";
            $columns = $attributes->pluck('label','name')->toArray();
            if (in_array($customPostSlug, ['interior-color-options']))
                $columns['updated_by'] = 'Updated By';
//            dd($columns);

            $data = [];

            // initializing all items bucket
            foreach ($posts as $post){

                $obj            = new \stdClass();
                $obj->id        = $post->item_id;
                $obj->column    = 'Page Section';
                $obj->value     = $post->pageSection;
                $obj->type      = 'select';

                $data['items'][$post->item_id]['section_id'] = $obj;

            }

            foreach ($items as $item){
                foreach ($columns as $key => $column){
                    $data['items'][$item->item_id][$key] = null;
                }
            }


//            dd($posts);
//            dd($data['items']);


            // adding section to all item

//           dd($posts->groupBy('item_id'));

            foreach ($posts->groupBy('item_id') as $itemId => $items){

                // check if its okey to go
//dd($items);

                // here we escaped!!
                foreach ($items as $item) { //dd($item);

                    if ($item->label) {

                        $obj            = new \stdClass();
                        $obj->id        = null;
                        $obj->column    = $item->label;
                        $obj->type      = $item->type;
                        $obj->value     = $item->type == 'select'? ($categories[ $item->value ]?? '') : $item->value;

                        $data['items'][$itemId][$item->name] = $obj;
                    }
                }

                if (in_array($customPostSlug, ['interior-color-options'])){

                    // added for updated by
                    $obj            = new \stdClass();
                    $obj->id        = null;
                    $obj->column    = 'Updated By';
                    $obj->type      = 'text';
                    $obj->value     = $items->first()->updatedBy;

                    $data['items'][$itemId]['updated_by'] = $obj;
                }
            }

//            dd($categories);
//
//
//
//            dd($data['items']);

            if ($customPost->slug == 'interior-color-options'){

                $data['filters'] = _getFilters($data['items'],'model');
            }
            else if ($customPost->slug == 'service-center-locations') {

                $data['filters'] = _getFilters($data['items'],'state');
            }

//            dd($data['items']);
            $data['items']      = isset($data['items'])? (isset($request->filterBy)? _megaFilterBy($data['items'],explode('_', $request->filterBy)[0], explode('_', $request->filterBy)[1]) : $data['items']) : [];
            $data['columns']    = $columns;
            $data['pageTitle']  = $customPost->name;
            $data['pageSlug']   = $customPost->slug;
//            $data['pageSection']= $posts[0]->pageSection;

//            dd($data);

        } else {

            return view('admin.pages.error');

        }


//        dd($data);

        // Return view.
        return view('admin.mega.index',compact('data'));
    }

    public function create(Request $request,$customPostSlug){

        $customPost = CustomPost::where('slug', $customPostSlug)->first();

        if ($customPost){

            $data = [];

            $data['items'] = DB::table('custom_attributes')
                ->leftJoin('custom_posts', 'custom_posts.id', '=', 'custom_attributes.custom_post_id')
                ->where('custom_posts.slug', $customPostSlug)
                ->orderBy('custom_attributes.sort')
                ->select(
                    'custom_attributes.*',
                    'custom_posts.name as pageTitle',
                    'custom_posts.slug as pageSlug'
                )
                ->get();

            foreach ($data['items'] as &$item) {
                $item->categories = CustomReferenceCategory::where('type', $item->reference_type)->orderBy('label')->get();
            }

            $data['pageTitle'] = $customPost->name;
            $data['pageSlug'] = $pageSlug = $customPost->slug;
            $sections   = CustomSection::where('custom_post_id', $customPost->id)->get();
//            dd($data['items']);
        } else {

            return view('admin.pages.error');

        }

        // Return view.
        return view('admin.mega.create',compact('data','sections'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        // getting custom post by slug
        $customPost = CustomPost::where('slug', $request->custom_post_slug)->first();

        // getting all the attributes by custom post slug.
        $items = DB::table('custom_attributes')
            ->leftJoin('custom_posts',
                'custom_posts.id',
                '=',
                'custom_attributes.custom_post_id'
            )
            ->where('custom_posts.slug', $customPost->slug)
//            ->where('custom_sections.id', (int) $request->section_id)
            ->orderBy('custom_attributes.sort')
            ->select(
                'custom_attributes.*',
                'custom_posts.name as pageTitle',
                'custom_posts.slug as pageSlug'
            )
            ->get();

        // getting all attributes of a custom post.
        $attributes = $items->pluck('id','name')->toArray();

        // fetching rules by filtering which is non required
        $rules = array_filter($items->pluck('validation_rules','name')->toArray());

//        dd($items);

        // validating rules
        $request->validate($rules);

        // filtering non required data for inserting to db
        $data = $request->except(['_token','custom_post_slug']);

        // get the section via id
        $section = CustomSection::find($request->section_id);

//        dd($items[0]);
//        dd($attributes);
        // preparing data set for batch insert
        $dataset = [];

        foreach ($attributes as $column => $attributeId){

            $set = [];
            $set['custom_post_id']      = $customPost->id;
            $set['custom_attribute_id'] = $attributeId;
            $set['section_id']          = $section->id;
            $set['page']                = $section->page;

            // checking the file type to decide either upload or assign.
            if (isset($data[$column])){

                if (is_object($data[$column])){

                    // uploading file for getting uploaded path as value
                    $set['value']           = _uploadFileToPublic($data[$column],'images/' . $customPost->slug);
                }
                else {

                    // set the value
                    $set['value']           = $data[$column];

                }
            }
            else {

                $set['value']           = null;
            }


            $set['created_by']           = Auth::id();
            $set['updated_by']           = Auth::id();
            $set['created_at']           = Carbon::now();
            $set['item_id']              = CustomPostData::max('item_id') + 1;

            // append set to batch dataset.
            $dataset[] = $set;

        }

//        dd($dataset);
        // inserting to DB
        CustomPostData::insert($dataset);

        return redirect('admin/' . $customPost->slug)->with('success','New Item Successfully Added');
    }

    public function edit($customPostSlug,$itemId){

        $customPost = CustomPost::where('slug',$customPostSlug)->first();

        $items = DB::table('custom_attributes')
            ->leftJoin(
                'custom_posts',
                'custom_posts.id',
                '=',
                'custom_attributes.custom_post_id'
            )
            ->leftJoinSub(
                DB::table('custom_post_datas')
                    ->where('custom_post_id',$customPost->id)
                    ->where('item_id',$itemId),
                'post_data',
                function ($join) {
                    $join->on('custom_attributes.id', '=', 'post_data.custom_attribute_id');
                }
            )
            ->where('custom_posts.slug', $customPostSlug)
            ->orderBy('custom_attributes.sort')
            ->select(
                'custom_attributes.*',
                'post_data.value',
                'post_data.item_id',
                'post_data.section_id',
                'custom_posts.name as pageTitle',
                'custom_posts.slug as pageSlug'
            )
            ->get();

//        dd($items);

        if ($customPost && $items){

            $data = [];
            $data['items']      = $items;

            foreach ($data['items'] as &$item) {

                if($item->type == 'select' && $item->reference_type == "floorplans"){
                    $item->categories = FloorPlan::where('id', $item->value)->select(['id','title as label'])->get();
                }
                else if ($item->type == 'select' && $item->reference_type != "")
                    $item->categories = CustomReferenceCategory::where('type', $item->reference_type)->orderBy('label')->get();
                else if ($item->type == 'select' && $item->reference_type == ""){

                    $category = CustomReferenceCategory::findOrFail($item->value);
                    if ($category){
                        $item->categories = CustomReferenceCategory::where('type', $category->type)->orderBy('label')->get();
                    }
                }

            }
//dd($data['items']);
            $data['itemId']     = $itemId;
            $data['sectionId']  = $items[0]->section_id;
            $data['pageTitle']  = $customPost->name;
            $data['pageSlug']   = $customPost->slug;

            $sections   = CustomSection::where('custom_post_id', $customPost->id)->get();


        } else {

            return view('admin.pages.error');

        }

        // Return view.
        return view('admin.mega.edit',compact('data','sections'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$customPostSlug,$itemId){

        // getting custom post by slug
        $customPost = CustomPost::where('slug', $request->custom_post_slug)->first();

        $customSection = CustomSection::find($request->section_id);

        // getting all the attributes by custom post slug.
        $items = DB::table('custom_attributes')
            ->leftJoin('custom_posts',
                'custom_posts.id',
                '=',
                'custom_attributes.custom_post_id'
            )
//            ->joinSub(
//                DB::table('custom_post_datas')
//                    ->where('custom_post_id',$customPost->id)
//                    ->where('item_id',$itemId),
//                'post_data',
//                function ($join) {
//                    $join->on('custom_attributes.id', '=', 'post_data.custom_attribute_id');
//                }
//            )
            ->where('custom_posts.slug', $customPostSlug)
            ->orderBy('custom_attributes.sort')
            ->select(
                'custom_attributes.*',
//                'post_data.value',
                'custom_posts.name as pageTitle',
                'custom_posts.slug as pageSlug'
            )
            ->get();

        $old = DB::table('custom_attributes')
            ->leftJoin('custom_posts',
                'custom_posts.id',
                '=',
                'custom_attributes.custom_post_id'
            )
            ->leftJoinSub(
                DB::table('custom_post_datas')
                    ->where('custom_post_id',$customPost->id)
                    ->where('item_id',$itemId),
                'post_data',
                function ($join) {
                    $join->on('custom_attributes.id', '=', 'post_data.custom_attribute_id');
                }
            )
            ->where('custom_posts.slug', $customPostSlug)
            ->orderBy('custom_attributes.sort')
            ->select(
                'custom_attributes.*',
                'post_data.value',
                'custom_posts.name as pageTitle',
                'custom_posts.slug as pageSlug'
            )
            ->get();

        // getting all attributes of a custom post.
        $attributes = $items->pluck('id','name')->toArray();

        // fetching validation rules by filtering which is non required
        $rules = array_filter($items->where('validate_create_only',0)->pluck('validation_rules','name')->toArray());

        // validating rules
        $request->validate($rules);

        // getting old data of that specific item.
        $oldData = $old->pluck('value','name')->toArray();

//        dd($oldData['photo']);

        // filtering non required data for inserting to db
        $sectionId = (int) $request->section_id;
        $data = $request->except(['_token','custom_post_slug','_method','section_id']);

//        dd($data);

//        dd($attributes);

        // preparing data set for batch insert
        $dataset = [];

        foreach ($data as $column => $value) {

            $set = [];
//            $set['custom_post_id']      = $customPost->id;
            $set['custom_attribute_id'] = $attributes[$column];
            $set['section_id']          = $sectionId;
//            $set['page']                = 'department';

            // checking the file type to decide either upload or assign.
            if (is_object($value)){

                // uploading file for getting uploaded path as value
                $set['value']           = _uploadFileToPublic($value,'images/' . $customPost->slug, $oldData[$column]);
            }
            else {

                // set the value
                $set['value']           = $value;

            }

            $set['updated_by']           = Auth::id();
            $set['updated_at']           = Carbon::now();

            // append set to batch dataset.
            $dataset[$column] = $set;
        }

//        dd($oldData);
//        dd($dataset);

        if (count($dataset)){
            foreach ($dataset as $column => $set) {

                $isExist = CustomPostData::where('custom_attribute_id',$set['custom_attribute_id'])
                    ->where('custom_post_id', $customPost->id)
                    ->where('item_id', $itemId)
                    ->first();

                if ($isExist){

                    // updated
                    CustomPostData::where('custom_attribute_id',$set['custom_attribute_id'])
                        ->where('custom_post_id', $customPost->id)
                        ->where('item_id', $itemId)
                        ->update([
                            'value'         => $set['value'],
                            'section_id'    => $set['section_id'],
                            'updated_by'    => Auth::id(),
                        ]);
                }
                else {

                    CustomPostData::create([
                        'custom_attribute_id'   => $set['custom_attribute_id'],
                        'section_id'            => $set['section_id'],
                        'custom_post_id'        => $customPost->id,
                        'item_id'               => $itemId,
                        'value'                 => $set['value'],
                        'created_by'            => Auth::id(),
                        'updated_by'            => Auth::id(),
                        'created_at'            => Carbon::now(),
                        'page'                  => $customSection->page
                    ]);

                }

            }
        }

        // filling up if any attibute missed

//        if (count($attributes)){
//            foreach ($attributes as $column => $attributeId) {
//
//                if (!isset($dataset[$column])){
////
////                    dd($column);
//
////                  // first delete
//                    CustomPostData::where('custom_attribute_id',$attributeId)
//                        ->where('custom_post_id', $customPost->id)
//                        ->where('item_id', $itemId)
//                        ->delete();
//
//                    // then create
//                    CustomPostData::create([
//                        'custom_attribute_id'   => $attributeId,
//                        'section_id'            => 1,
//                        'custom_post_id'        => $customPost->id,
//                        'item_id'               => $itemId,
//                        'value'                 => null,
//                        'created_by'            => Auth::id(),
//                        'created_at'            => Carbon::now(),
//                        'page'                  => 'department'
//                    ]);
//                }
//            }
//        }

        return redirect('admin/' . $customPost->slug)->with('success','Item Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String $customPostSlug
     * @param  String $itemId
     * @return \Illuminate\Http\Response
     */
    public function delete($customPostSlug,$itemId)
    {

        $customPost = CustomPost::where('slug',$customPostSlug)->first();

        $data = CustomPostData::where('custom_post_id',$customPost->id)->where('item_id', $itemId)->delete();

        if($data){
            return redirect('admin/' . $customPost->slug  . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))->with('success','Item deleted successfully');
        }else{
            return redirect('admin/' . $customPost->slug . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))->with('error','Item deletion failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String $customPostSlug
     * @param  String $itemId
     * @return \Illuminate\Http\Response
     */
    public function deleteDocument($customPostSlug,$itemId,$sectionId,$attributeId)
    {

        $customPost = CustomPost::where('slug',$customPostSlug)->first();

        $data = CustomPostData::where('custom_post_id',$customPost->id)->where('item_id', $itemId)->where('section_id', $sectionId)->where('custom_attribute_id', $attributeId)->first();


        if($data){

            if (file_exists($data->value)){
                unlink($data->value);
            }

            $data->update(['value' => '']);

//            dd($data);
            return redirect('admin/'. $customPost->slug .'/'. $itemId .'/edit'. (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))->with('success','Deleted successfully');

        }else{

            return redirect('admin/'. $customPost->slug .'/'. $itemId .'/edit'.  (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))->with('error','Deletion failed');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String $selectedSlug
     *
     * @return \Illuminate\Http\Response
     */
    public function getReferenceData($selectedId, $tableReferencedBy = null, $foreignKey = null)
    {
//        die($selectedId);
//        dd(Schema::hasTable($tableReferencedBy));

        if ($foreignKey == 'model'){
            $category = CustomReferenceCategory::findOrFail($selectedId);

            if ($category)
                $selectedId = $category->slug;
        }

        if ($tableReferencedBy && Schema::hasTable($tableReferencedBy) && isset($foreignKey)){
            return DB::table($tableReferencedBy)->where($foreignKey,$selectedId)->select(['id','title as label'])->get();
        }

        $category = CustomReferenceCategory::findOrFail($selectedId);

        if($category){

            return CustomReferenceCategory::where('type', $category->slug)->get();

        }else{

            return null;

        }
    }

    /**
     *  Remove the specified resource from storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     *
     */

    public function updateSortOrder(Request $request){

        if (isset($request->items)){

            $offset = $request->per_page && $request->page_no? (((int) $request->page_no - 1) * ((int) $request->per_page)) + 1 : 1;

            $table = 'custom_post_datas';

            if (isset($request->section) && $request->section == 'floor_plans'){

                // change table
                $table = 'floor_plans';

                foreach ($request->items as $order => $item) {

                    // update each item id to update sort.
                    DB::table($table)->where('id', $item)->update(['sort' => $order + $offset]);
                }
            }
            else if(isset($request->section) && in_array($request->section, ['fabrications'])){

                // change table
                $table = 'custom_reference_categories';

                foreach ($request->items as $order => $item) {

                    // update each item id to update sort.
                    DB::table($table)->where('id', $item)->update(['sort' => $order + $offset]);
                }
            }
            else if(isset($request->section) && in_array($request->section, ['interior-gallery','exterior-gallery','color-options'])){

                // change table
                $table = 'slides';

                foreach ($request->items as $order => $item) {

                    // update each item id to update sort.
                    DB::table($table)->where('id', $item)->update(['order' => $order + $offset]);
                }
            }
            else {

                foreach ($request->items as $order => $item){

                    // update each item id to update sort.
                    DB::table($table)->where('item_id', $item)->update(['sort' => $order + $offset]);
                }
            }



            echo json_encode( [
                'status'    => 'success',
                'message'   => 'Item Reorganised'
            ] );
        }
    }


    /**
     *  Remove the specified resource from storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     *
     */

     public function editCustomReferenceCategory($id){

        $custom_reference_category =  DB::table('custom_reference_categories')->where('id',$id)->where("type","chapters")->first();

        $data['pageTitle']  = 'Chapters';
        $data['pageSlug']   = 'Chapters';

        return view('admin.mega.edit-category',compact('custom_reference_category','data'));

     }


     /**
     *  Remove the specified resource from storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     *
     */

     public function updateCustomReferenceCategory(Request $request){

         $request->validate([
            'info' => 'required',
            'label' => 'required',
            'description' => 'required'
         ]);

          $custom_reference_category = CustomReferenceCategory::where('slug',$request->slug)->first();

          if($custom_reference_category){

            $custom_reference_category->info = $request->info;

            $custom_reference_category->label = $request->label;

            $custom_reference_category->description = $request->description;

            $custom_reference_category->update();
          }


          return redirect()->route('admin.custom-reference-attribute.index')->with('success', 'Modified Successfully');


     }

     /**
     *  Remove the specified resource from storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     *
     */

     public function customReferenceCategory(Request $request){

          $customReferenceCategories = CustomReferenceCategory::where('type','chapters')->get();

         $data['pageTitle']  = 'Chapters';
         $data['pageSlug']   = 'Chapters';

         return view('admin.mega.list-category',compact('customReferenceCategories','data'));


     }


}
