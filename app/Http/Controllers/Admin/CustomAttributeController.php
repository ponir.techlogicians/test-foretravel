<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomAttribute;
use Auth;
use Response;

class CustomAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
       $request->validate([

            'name.*'                    => 'required',
            'label.*'                   => 'required',
            'type.*'                    => 'required',
            'slug'                      => 'required|unique:custom_attributes',
 
       ]);

       
        if (gettype($request->name) == 'name') {
            $name_array = json_decode($request->name, true);
        } elseif (gettype($request->name == 'array')) {
            $name_array = $request->name;
        }
        
        $custompost_id = $request->custom_post_id;
        
        // label input check if array or string
        if (gettype($request->label) == 'label') {
            $label_array = json_decode($request->label, true);
        } elseif (gettype($request->label == 'array')) {
            $label_array = $request->label;
        }


        // type input check if array or string
        if (gettype($request->type) == 'type') {
            $type_array = json_decode($request->type, true);
        } elseif (gettype($request->type == 'array')) {
            $type_array = $request->type;
        }

        // validation rules check if array or string
        if (gettype($request->validation_rules) == 'validation_rules') {
            $validation_rules_array = json_decode($request->validation_rules, true);
        } elseif (gettype($request->validation_rules == 'array')) {
            $validation_rules_array = $request->validation_rules;
        }


        // validation created only check if array or string
        if (gettype($request->validation_created_only) == 'validation_created_only') {
            $validation_created_only_array = json_decode($request->validation_created_only, true);
        } elseif (gettype($request->validation_created_only == 'array')) {
            $validation_created_only_array = $request->validation_created_only;
        }

        
        // slug check if array or string
        if (gettype($request->slug) == 'slug') {
            $slug_array = json_decode($request->slug, true);
        } elseif (gettype($request->slug == 'array')) {
            $slug_array = $request->slug;
        }

        $row_loop = count($name_array);
        
        $test = 0;
   
        // dd($request->custom_post_id);
          
        foreach ($name_array as $key => $name_array_value) {
            $custom_attributes = new CustomAttribute();
            $custom_attributes->name = $name_array_value;
            
            $custom_attributes->label = $label_array[$key];
            
            $custom_attributes->slug = $slug_array[$key];
            
            $custom_attributes->custom_post_id = $custompost_id;


            $custom_attributes->validation_rules = implode("|",$validation_rules_array);
            $custom_attributes->validate_create_only = $validation_created_only_array;
            $custom_attributes->type= implode("|",$type_array);
            $custom_attributes->created_by = Auth::user()->id;
            $custom_attributes->save();
        }

        $data = [
            'data'  => $custom_attributes,
            'status' => 'success',
            'message' => 'custom attribute created successfully',
        ];

            
       return Response::json($data);
     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        // dd($request->all());
        

        $custom_attribute = CustomAttribute::find($request->custom_attribute_id);
        if($custom_attribute){

            $custom_attribute->name = $request->name;
            $custom_attribute->label = $request->label;
            $custom_attribute->type = $request->type;

            if($request->validation_rules != null){
                $custom_attribute->validation_rules = implode("|",$request->validation_rules);
            }
            
            $custom_attribute->validate_create_only = $request->validation_created_only;

            $custom_attribute->update();
            if($custom_attribute->update()){
                $data = [
                    'status' => 'success',
                    'message' => 'custom attribute updated successfully',
                ];
            }else{
                $data = [
                    'status' => 'error',
                    'message' => 'custom attribute update failed',
                ];
            }
        }
        
        return Response::json($data);
     
        
    }

   

    public function destroy($id,Request $request)
    
    {
        
        $custom_attribute = CustomAttribute::find($id);

        if($custom_attribute){

            $custom_attribute->delete();


            $data = [
                'status' => 'success',
                'message' => 'Custom attribute deleted successfully'
            ];
            return Response::json($data);

        }


    }
}
