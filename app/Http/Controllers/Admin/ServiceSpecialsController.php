<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachModel;
use App\Models\FloorplanItem;
use App\Models\ServiceSpecial;
use Illuminate\Http\Request;
use App\Models\FloorPlan;
use Illuminate\Support\Facades\Auth;


class ServiceSpecialsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // fetching all floorplan list
        if ($request->has('model')) {
            $servicespecials = ServiceSpecial::where('model', $request->model)->orderBy('sort')->get();
        } else {
            $servicespecials = ServiceSpecial::orderBy('sort')->get();
        }

        return view('admin.servicespecials.index', compact('servicespecials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coachModels = CoachModel::all();
        return view('admin.servicespecials.create',compact('coachModels'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'model'             => 'required',
            'tour_url'          => 'required',
            'title'             => 'required',
            'thumb_image'       => 'required|image|mimes:jpeg,png,jpg,svg|max:10480'
        ]);

        $servicespecial = new ServiceSpecial();
        $servicespecial->model = $request->model;
        $servicespecial->tour_url = $request->tour_url;
        $servicespecial->title = $request->title;
        $servicespecial->created_by = Auth::user()->id;


        if ($request->hasFile('thumb_image')) {

            $file = $request->file('thumb_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('img/photo');
            $file->move($path, $filename);
            $servicespecial->thumb_image = 'img/photo/' . $filename;
        }

        $servicespecial->save();

        return redirect()->route('admin.servicespecial.index')->with('success', 'Virtual Tour created successfully');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicespecial = ServiceSpecial::find($id);

        $coachModels = CoachModel::all();
        if ($servicespecial) {
            return view('admin.servicespecial.edit', compact('virtualtour','coachModels'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'model'             => 'required',
            'tour_url'          => 'required',
            'title'             => 'required',
        ]);

        $servicespecial = ServiceSpecial::find($id);
        if ($servicespecial) {

            $servicespecial->model = $request->model;
            $servicespecial->tour_url = $request->tour_url;
            $servicespecial->title = $request->title;
            $servicespecial->updated_by = Auth::user()->id;

            if ($request->hasFile('thumb_image')) {

                $file = $request->file('thumb_image');
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $path = public_path('img/photo');
                $file->move($path, $filename);
                $servicespecial->thumb_image = 'img/photo/' . $filename;
            }

            $servicespecial->update();

            return redirect()->route('admin.servicespecial.index')->with('success','virtual tour updated successfully');
        }
    }



    public function destroy($id)
    {
        $serviceSpecial = ServiceSpecial::find($id);

        if($serviceSpecial){

            $serviceSpecial->delete();

            return redirect()->route('admin.servicespecial.index')->with('success','virtual tour deleted successfully');
        }else{
            return redirect()->route('admin.servicespecial.index')->with('error','virtual tour deletion failed');
        }
    }
}
