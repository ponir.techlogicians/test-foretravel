<?php

namespace App\Http\Controllers;

use App\Models\CoachModel;
use App\Models\Contact;
use App\Models\CustomReferenceCategory;
use App\Models\FloorPlan;
use App\Models\Page;
use App\Models\Slider;
use App\Models\Slide;
use App\Models\Testimonial;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // fetching home page contents by key pair value.
        $contents = Page::getContents('home',null,true);
        $videoSlides = Slider::with('slides')->where('section','home-slider')->first();
        $luxuryCoaches = Slider::with('slides')->where('section','luxury-coach')->first();


        return view('pages.home',compact('contents','videoSlides','luxuryCoaches'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        // fetching home page contents by key pair value.
        $testimonials = Testimonial::all();

        return view('pages.about',compact('testimonials'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function models($coach = 'realm')
    {

//        dd($sl->slides);
        // fetching home page contents by key pair value.
        $model = CoachModel::where('slug', $coach)->first();
        $categoryModel = CustomReferenceCategory::where('slug', $coach)->first();

        if ($model){

            $floorplan = FloorPlan::where('model',$coach)->orderBy('sort')->get();

            $colorOptions = Slider::with(['slides' => function ($query) use ($coach) {
                                $query->where('model', $coach)->orderBy('order');
                            }])->where('section','color-options')->first();
            $keyFeatures = Slider::with(['slides' => function ($query) use ($coach) {
                                $query->where('model', $coach);
                            }])->where('section','key-features')->first();

            $interiorGallery = Slider::with(['slides' => function ($query) use ($coach) {
                                    $query->where('model', $coach)->orderBy('order');
                                }])->where('section','interior-gallery')->first();

            $exteriorGallery = Slider::with(['slides' => function ($query) use ($coach) {
                                    $query->where('model', $coach)->orderBy('order');
                                }])->where('section','exterior-gallery')->first();

            $coachSlug = $coach != 'realm'? $coach.'_' : '';

//            dd('model_' . $coachSlug . 'floorplan_title');
//            dd(__t('model_' . $coachSlug . 'floorplan_title'));

            $interiorColorOptions = _makeGroupByObject(
                _getSectionsData(
                    'model',
                    'interior-color-options',
                    null,
                    [
                        'model' => $categoryModel->id
                    ],
                    null,
                    null,
                    0,
                    'data_sort'
                ),
                'type'
            );

            $floorplanVideos = _getSectionsData(
                    'model',
                    'floorplan-videos',
                    null,
                    [
                        'model' => $categoryModel->id
                    ]);

            $virtualTours = _getSectionsData(
                    'model',
                    'virtual-tours',
                    null,
                    [
                        'model' => $categoryModel->id
                    ],
                    null,
                    null,
                    0,
                    'data_sort'
                );

//            dd($virtualTours);

            return view('pages.models',compact('floorplan','colorOptions','keyFeatures','interiorGallery','exteriorGallery','coachSlug','interiorColorOptions','floorplanVideos','virtualTours'));
        }

        return view('pages.error');

    }

    /**
     * Show the inventory page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function inventory()
    {
        return view('pages.inventory');
    }

    /**
     * Show the motorcade club page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function sectionLoadMore(Request $request)
    {

        $postSlug       = $request->post_slug;
        $pageSlug       = $request->page_slug;
        $section        = $request->section?? null;
        $filterBy       = $request->filter_by?? null;
        $categoryBy     = $request->category_by?? null;
        $limit          = $request->limit?? null;
        $offset         = $request->offset?? 0;

        $data = _getSectionsData(
            $postSlug,
            $pageSlug,
            $section,
            $filterBy,
            $categoryBy,
            $limit,
            $offset
        );


//        dd($chapters);
        return view('pages.motorcade-club',compact('magazines','newses','events','california_chapters','mid_atlantic_chapters','board_of_directors','club_managers','chapters'));
    }

    /**
     * Show the motorcade club page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function motorcadeClub()
    {
        $magazines              = _getSectionsData('magazine','magazines','our_magazine',null,null,null,0,'data_sort');
        $newses                 = _getSectionsData('news-and-events','news','latest_news',null,null,null,0,'data_sort','asc');
        $events                 = _getSectionsData('news-and-events','events','motorcade_events',null,null,null,0,'data_sort','asc');
//        dd($newses);
//        $california_chapters = _getSectionsData('chapters','chapters','california_chapter');
//        $mid_atlantic_chapters = _getSectionsData('chapters','chapters','mid_atlantic_chapter');
        $presidentAndFirstLady  = _getSectionsData('officers','officers','international_president_first_lady');
        $board_of_directors     = _getSectionsData('officers','officers','board_of_directors');
        $club_managers          = _getSectionsData('officers','officers','club_manager');
        $tours                  = _getSectionsData('motorcade-club','tour-event','tour_event',null,null,null,0,'item_id','desc');

//        dd($presidentAndFirstLady);
        $chapters = _getSectionsData(
            'chapters',
            'chapters',
            null,
            null,
            'chapter',
            null,
            0,
            'data_sort',
            'asc',
            1
        );


//        dd($tours);
        return view('pages.motorcade-club',compact(
            'magazines',
            'newses',
            'events',
            'board_of_directors',
            'club_managers',
            'chapters',
            'presidentAndFirstLady',
            'tours'
            )
        );
    }

     /**
     * Show the recall-information page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function recallInformation(){

         $notices   = _getSectionsData('notices','notices','recall_notice');
            //dd($notices);
         return view('pages.recall-information', compact('notices'));
     }


       /**
     * Show the owner-service page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function ownerService(){

        $serviceLocationLatLongs       = _getSectionsData('owner-service','service-center-locations',null,null,'state',null,0,'lat','asc',1);

        $coordinates = [];
        foreach ($serviceLocationLatLongs as $loc => $coords) {

            $obj = new \stdClass();
            $obj->state = $loc;

            $coordsArray = [];
            foreach ($coords as $coord){
                $latLong = new \stdClass();
                $latLong->lat  = (float) $coord->lat;
                $latLong->lng  = (float) $coord->long;
                $latLong->info = $coord->description;
                $coordsArray[] = $latLong;
            }
            $obj->location_cordinates = $coordsArray;

            $coordinates[] = $obj;

        }
        $coordinates = json_encode($coordinates);
         return view('pages.owner-service',compact('coordinates'));
     }



     public function technicalSupport(Request $request){

         $category  = isset($request->category)? $request->category : null;
         $section   = isset($request->section)? $request->section : null;

         if ($category){

             $categoryBy = [
                 'category' => $category
             ];

             if ($section)
                 $categoryBy['section'] = $section;

             $supportQA = _makeGroupByObject(
                 _getSectionsData(
                     'technical-support',
                     'technical-supports',
                     null,
                      $categoryBy,
                     null
                 ),
                 [
                     'category',
                     'section'
                 ]
             );

             if ($section){

                 $sec = CustomReferenceCategory::findOrFail($section);
                 $sections = CustomReferenceCategory::where('type',$sec->type)->get();

             } else {

                 $sec = CustomReferenceCategory::findOrFail($category);
                 $sections = CustomReferenceCategory::where('type',$sec->slug)->get();


             }

         }
         else {

             $supportQA = _makeGroupByObject(
                 _getSectionsData(
                     'technical-support',
                     'technical-supports',
                     null,
                     null,
                     null
                 ),
                 [
                     'category',
                     'section'
                 ]
             );

             $sections = [];
             //         dd($supportQA);

         }

//         dd($sections);

         $categories = CustomReferenceCategory::where('type', 'category')->get();

         return view('pages.technical-support',compact('supportQA','categories','sections'));
     }

     public function information(){

         $warrantyInformation           = _getSectionsData('information','warranty-informations','warranty_information',null,null,null,0,'data_sort');
         $helpfullLinks                 = _getSectionsData('information','helpful-service-links','helpful_service_links');
         $vendorContactInfos            = _getSectionsData('information','vendor-contact-infos','vendor_contact_info');

         $brochure_information          =  _getSectionsData('information','model-brochure','model_brochure',null,null,null,0,'data_sort');
//         dd($serviceLocationLatLongs);


         return view('pages.information',compact('warrantyInformation','helpfullLinks','vendorContactInfos','brochure_information'));
     }


     public function partsDepartment(){

         $partsDepartment = _getSectionsData('parts-department','parts-department-members','parts_department');
        //  dd($partsDepartment);
         return view('pages.parts-department',compact('partsDepartment'));
     }


     public function remodelDepartment(){

         $remodelDepartment = _getSectionsData('remodel-department','remodel-department-members','remodel_department');

         return view('pages.remodel-department',compact('remodelDepartment'));
     }



     public function serviceDepartment(){

         $serviceDepartment     = _getSectionsData('service-department','service-department-members','service_department');
         $serviceSpecials       = _getSectionsData(
                                     'service-department',
                                     'service-specials',
                                     'service-specials',
                                     null,
                                     null,
                                     null,
                                     0,
                                     'data_sort',
                                     'asc'
                                 );
         $appointmentMembers    = _getSectionsData('service-department','service-appointment-members','appointment_department');

//         dd($serviceSpecials);
         return view('pages.service-department',compact('serviceDepartment','appointmentMembers','serviceSpecials'));
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Mixed $selectedId
     *
     * @param  String $tableReferencedBy
     *
     * @param  String $foreignKey
     *
     * @return mixed
     */

    public function getReferenceData($selectedId)
    {

        $category = CustomReferenceCategory::findOrFail($selectedId);

        if($category){

            return CustomReferenceCategory::where('type', $category->slug)->get();

        }else{

            return null;

        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contactSubmit(Request $request)
    {
        $validator = $request->validate([

            'name'             => 'required',
            'email'            => 'required|email',
            'interested_in'    => 'required',
            'message'          => 'required|min:10',

        ]);

        $clientip = \Request::getClientIp(true);

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->interested_in = $request->interested_in;
        $contact->message  = $request->message;
        $contact->ip_address  = $clientip;

        $contact->save();
//        dd($testimonials);

        return redirect('/')->with('success','Your Query Submitted Successfully');
    }


    /**
    * career page
    *
    */

    public function career(){
        return view('pages.career');
    }


    /**
     * Show a new motorcade member instance with preloaded value.
     *
     * @param  Request $request
     * @return \App\Models\User
     */
    public function motorcadeMemberProfile(Request $request)
    {
//        dd(Auth::user());

        if (Auth::user() && Auth::user()->role == 'motorcade-member'){

        $member = $request->user();
        return view('pages.motorcade-profile', compact('member'));
        }

//        return redirect('/motorcade-club');

    }

    /**
     * Create a new motorcade member instance after a valid registration.
     *
     * @param  Request $request
     * @return \App\Models\User
     */
    public function motorcadeMemberUpdate(Request $request)
    {

        $request->validate([
            'username' => 'required|unique:users,username,'. $request->user()->id .'|max:36',
            'password' => 'nullable|min:6',

//            'first_name' => 'required',
//            'last_name' => 'required',
//
            'register_email' => 'nullable|email',
//
//            'address_line_1' => 'required',
//            'city' => 'required',
//            'state' => 'required',
//            'zip' => 'required',
//
//            'coach_type' => 'required',
//            'year_model' => 'required',
//
//            'last_six_number_vin' => 'required',
//            'complete_model_number' => 'required',
        ]);

        $user = User::findOrFail($request->user()->id);
//        dd(Hash::make($request->password));
        $user->name = '';
        $user->role = 'motorcade-member';
        $user->username = $request->username;
        $user->password = bcrypt($request->password);


        if($request->has('first_name')) {

            $user->first_name = $request->first_name;
            $user->name = $request->first_name;
        }

        if($request->has('last_name')){

            $user->last_name = $request->last_name;
            $user->name .= ' '.$request->last_name;
        }

        if($request->has('register_email'))
            $user->email = $request->register_email;

        if($request->has('spouse_name')){
            $user->spouse_name = $request->spouse_name;
        }

        if($request->has('phone_number')){
            $user->phone_number = $request->phone_number;
        }

        if($request->has('cell_number')){
            $user->cell_number = $request->cell_number;
        }

        if($request->has('birthday')){
            $user->birthday = date("Y-m-d", strtotime($request->birthday));
        }

        if($request->has('spouse_birthday')){
            $user->spouse_birthday = date("Y-m-d", strtotime($request->spouse_birthday));
        }

        if($request->has('anniversary')){
            $user->anniversary = date("Y-m-d", strtotime($request->anniversary));
        }

        if($request->has('address_line_2'))
            $user->address_line_1 = $request->address_line_1;

        if($request->has('address_line_2')){
            $user->address_line_2 = $request->address_line_2;
        }

        if($request->has('city'))
            $user->city = $request->city;

        if($request->has('state'))
            $user->state = $request->state;

        if($request->has('zip'))
            $user->zip = $request->zip;

        if($request->has('coach_type'))
            $user->coach_type = $request->coach_type;

        if($request->has('purchase_date')){
            $user->purchase_date = date("Y-m-d", strtotime($request->purchase_date));
        }

        if($request->has('purchase_form')){
            $user->purchase_form = $request->purchase_form;
        }

        if($request->has('year_model'))
            $user->year_model = $request->year_model;

        if($request->has('last_six_number_vin'))
            $user->last_six_number_vin = $request->last_six_number_vin;

        if($request->has('complete_model_number'))
            $user->complete_model_number = $request->complete_model_number;


        if($request->has('fmca_member')){
            $user->fmca_member = $request->fmca_member;

        }

        if($request->has('fmca_number') && $request->fmca_member){
            $user->fmca_number = $request->fmca_number;
        }

        $user->save();

        return redirect(url('/motorcade-member-profile'))->with('update-message', 'Profile Update Successfull!');
    }

    public function motorcadeMemberDirectory(Request $request, $type = 'by-name'){

        return redirect(url(_regenerateMembershipDirectory($type)));

    }

    public function getFabrication(Request $request, $page){

        $categoryModel = CustomReferenceCategory::where('slug', $page)->first();

        $images = _getSectionsData(
            'fabrications',
            'fabrication-gallery',
            null,
            [
                'fabrication' => $categoryModel->id
            ],
            null,
            null,
            0,
            'data_sort'
        );

        $videos = _getSectionsData(
            'fabrications',
            'fabrication-videos',
            null,
            [
                'fabrication' => $categoryModel->id
            ],
            null,
            null,
            0,
            'data_sort'
        );

        $slug = str_replace('-','_',$page);


//            dd($floorplanVideos);

        return view('pages.fabrication',compact('videos','images','slug'));

    }

}
