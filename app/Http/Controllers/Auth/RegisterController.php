<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\RegenerateMembersPDF;
use App\Mail\NotificationSend;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Create a new motorcade member instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function motorcadeMemberRegister(Request $request)
    {

        $request->validate([
            'username' => 'required|unique:users|max:36',
            'register_password' => 'required|min:6',
            'register_email' => 'nullable|email',

            'first_name' => 'required',
            'last_name' => 'required',

            'address_line_1' => 'required',
//            'address_line_2' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone_number' => 'required',
            'cell_number' => 'required',
            'coach_type' => 'required',
            'purchase_date' => 'required',
            'purchase_form' => 'required',
            'year_model' => 'required',

            'last_six_number_vin' => 'required',
            'complete_model_number' => 'required',
        ]);

        if ((int) $request->fmca_member){
            $request->validate([
                'fmca_number' => 'required',
            ]);
        }

        $user = new User();

        $user->name = '';
        $user->role = 'motorcade-member';
        $user->username = $request->username;
        $user->password = Hash::make($request->register_password);

        if($request->has('last_name')){

            $user->last_name = $request->last_name;
            $user->name .= $request->last_name;
        }

        if($request->has('first_name')) {

            $user->first_name = $request->first_name;

            if (isset($request->last_name) && $request->last_name != "")
                $user->name .=  ', '.$request->first_name;
            else
                $user->name .=  ' '.$request->first_name;
        }


        if($request->has('register_email'))
            $user->email = $request->register_email;

        if($request->has('spouse_name')){
            $user->spouse_name = $request->spouse_name;
            if ($request->spouse_name != "")
                $user->name .= ' & '.$request->spouse_name;
        }

        if($request->has('phone_number')){
            $user->phone_number = $request->phone_number;
        }

        if($request->has('cell_number')){
            $user->cell_number = $request->cell_number;
        }

        if($request->has('birthday')){
            $user->birthday = date("Y-m-d", strtotime($request->birthday));
        }

        if($request->has('spouse_birthday')){
            $user->spouse_birthday = date("Y-m-d", strtotime($request->spouse_birthday));
        }

        if($request->has('anniversary')){
            $user->anniversary = date("Y-m-d", strtotime($request->anniversary));
        }

        if($request->has('address_line_2'))
            $user->address_line_1 = $request->address_line_1;

        if($request->has('address_line_2')){
            $user->address_line_2 = $request->address_line_2;
        }

        if($request->has('city'))
            $user->city = $request->city;

        if($request->has('state'))
            $user->state = $request->state;

        if($request->has('zip'))
            $user->zip = $request->zip;

        if($request->has('coach_type'))
            $user->coach_type = $request->coach_type;

        if($request->has('purchase_date')){
            $user->purchase_date = date("Y-m-d", strtotime($request->purchase_date));
        }

        if($request->has('purchase_form')){
            $user->purchase_form = $request->purchase_form;
        }

        if($request->has('year_model'))
            $user->year_model = $request->year_model;

        if($request->has('last_six_number_vin'))
            $user->last_six_number_vin = $request->last_six_number_vin;

        if($request->has('complete_model_number'))
            $user->complete_model_number = $request->complete_model_number;


        if($request->has('fmca_member')){
            $user->fmca_member = $request->fmca_member;

        }

        if($request->has('fmca_number') && $request->fmca_member){
            $user->fmca_number = $request->fmca_number;
        }

        $user->save();

        /***
         *  Notification Area
         */

        // get the subject of notification email.
        $subject        = 'New Motorcade Member Registration!!';

        // get the body of notification email.
        $contentText    = $user;

        // send notification email to admin
        Mail::to(env('MAIL_TO_ADDRESS'))->send(new NotificationSend('member-registration',$contentText, $subject));

        // regenerating members pdf
//        _regenerateMembershipDirectory();

        Auth::login($user);

        return redirect(url('/motorcade-club'))->with('registeration-message', 'Registration Successfull!');
    }

}
