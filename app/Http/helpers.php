<?php


use App\Models\Page;

// use PDF;
use App\Models\User;
use Illuminate\Support\Facades\DB;



if (!function_exists('_FTDateFormat')) {


	function _FTDateFormat ($date = null) {


		if($date){

		    return date("F j, Y", strtotime($date));
        }

        return $date;
    }
}

if (!function_exists('createSlug')) {


	function createSlug ($name='', $table_name='', $slug_column_name = 'slug') {


		if($name && $table_name){


			$table_exist = Schema::hasTable($table_name);

			if($table_exist){


				$slug_column_exist = Schema::hasColumn($table_name,$slug_column_name);

				if($slug_column_exist){

				    $slug = trim($name);

                    $slug = strtolower($slug);
				    $slug = str_replace(' ','-',$slug);
                    $slug = str_replace('&','', $slug);
                    $slug = str_replace('?','', $slug);
                    $slug = str_replace('[','', $slug);
                    $slug = str_replace(']','', $slug);
                    $slug = str_replace('\'','', $slug);
                    $slug = str_replace('--','-', $slug);

				    $count_slug = $table_name != ''? DB::table($table_name)->where($slug_column_name,'like','%'.$slug.'%')->count() : 0;

                    $slug = $count_slug > 0 ? $slug.'-'.$count_slug : $slug;

				    return $slug;
				}else{
					return false;
				}

			} else {

				return false;

			}

		} else {

			return false;

		}

	}
}

if (!function_exists('makeLabelBySlug')) {

    /**
     * Dump/die a variable to the browser/console, prepended by the file and
     * line number of the ddl() call.
     *
     * @return string
     */

    function makeLabelBySlug($slug){

        if ($slug == 'color_options')
            $slug = 'exterior_options';
        else if ($slug == 'interior_color_options')
            $slug = 'interior_options';

        $delimiters = ['-','_'];

        foreach ($delimiters as $delimiter) {

            $token = explode($delimiter, $slug);

            foreach ($token as &$t){
                $t = ucfirst($t);
            }

            $slug = implode(' ', $token);
        }


        return $slug;
    }
}


if (!function_exists('makeSlugByName')) {

    /**
     * Dump/die a variable to the browser/console, prepended by the file and
     * line number of the ddl() call.
     *
     * @return string
     */

    function makeSlugByName($name){


        $token = explode(' ', strtolower($name));

        return implode('-', $token);
    }
}

if (!function_exists('stdClassToArray')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \stdClass $class
     *
     * @return array
     */
    function stdClassToArray(\stdClass $class)
    {
        return json_decode(json_encode($class), true);
    }
}

if (!function_exists('__t')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \stdClass $class
     *
     * @return string
     */
    function __t($key = null)
    {
        $pageContents = Page::orderBy('sort')->pluck('value','key')->toArray();

//        $contents = array_merge([],$homeContent,$aboutContent);
        $contents = array_merge([],$pageContents);

        if (key_exists($key,$contents)){
            return $contents[$key];
        }

        return "";
    }
}

if (!function_exists('_breadcrumbs')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $page
     *
     * @return string
     */
    function _breadcrumbs($pages)
    {

        $tree = '';
        if (is_array($pages)){
            foreach ($pages as $key => $page) {


                if ($key == 1 && count($pages) - 1 !== $key){
                    $tree .= '<li>
                                <a href="' . url('admin') . '/' . strtolower(makeSlugByName($page)) . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : ''). '">' . makeLabelBySlug($page) .'</a>
                            </li>';
                }
                else if ($key == 0){

                    $tree .= '<li>
                                <a href="' . (!isset($_GET['ref'])? url('admin') : url('admin/page')) . '/' . strtolower(makeSlugByName($page)) . (isset($_GET['model'])? ('/' . $_GET['model']) : '') .'">' . makeLabelBySlug($page).'</a>
                            </li>';
                }
                else {
                    $tree .= '<li class="active">' . makeLabelBySlug($page) .'</a></li>';
                }
            }
        }
        else {
            $tree .= '<li class="active">' . makeLabelBySlug($pages) .'</a></li>';
        }

        return '<div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="' . url('admin') . '">Dashboard</a>
                    </li>
                    ' . $tree .'
                </ul><!-- /.breadcrumb -->
            </div>';

    }
}

if (!function_exists('_breadcrumbsGenerator')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \Mixed $breads
     *
     * @return array
     */
    function _breadcrumbsGenerator($breads)
    {

        $tree = '';
        if (is_array($breads)){
            foreach ($breads as $key => $bread) {

                if (count($breads) - 1 !== $key){
                    $tree .= '<li>
                                <a href="' . url($bread['url']) . $bread['q'] .'">' . makeLabelBySlug($bread['label']) .'</a>
                            </li>';
                }
                else {
                    $tree .= '<li class="active">' . makeLabelBySlug($bread['label']) .'</li>';
                }
            }
        }

        return '<div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="' . url('admin') . '">Dashboard</a>
                        </li>
                        ' . $tree .'
                    </ul><!-- /.breadcrumb -->
                </div>';

    }
}

if (!function_exists('_getPageLocatorBySection')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $section
     *
     * @return string
     */
    function _getPageLocatorBySection($section)
    {
        switch ($section){
            case 'luxury-coach':
            case 'home-slider':
                return 'Home';
                break;
            default:
                return 'Model';
                    break;
        }

    }
}

if (!function_exists('_getGallery')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $index
     *
     * @return integer
     */
    function _getGallery($index, $gallery = [4,4,4,8,4,3,3,6]){

        $index++;

        if($index > count($gallery)){
            $index = $index % count($gallery);
        }

        $index--;

        if($index == -1){
            $index = count($gallery) - 1;
        }

        return $gallery[$index];
    }
}

if (!function_exists('_uploadFileToPublic')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $fileToBeUploaded
     * @param \String $oldFilePathToDelete
     *
     * @return string
     */
    function _uploadFileToPublic(&$fileToBeUploaded, $uploadPath = '', $oldFilePathToDelete = null)
    {
        // checking old file exists or not to be deleted
        if ($oldFilePathToDelete){
            $remove_previous_file = public_path().'/'.$oldFilePathToDelete;
            if(file_exists($remove_previous_file)){
                unlink($remove_previous_file);
            }
        }

        // uploading to directory
        $filename = time(). rand(1111,9999). '.' . $fileToBeUploaded->getClientOriginalExtension();
        $path = empty($uploadPath)? public_path('img/photo') : public_path($uploadPath);
        $fileToBeUploaded->move($path, $filename);
        return (empty($uploadPath)? 'img/photo/' : $uploadPath . '/').$filename;

    }
}

if (!function_exists('_isInPage')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $page
     * @param \String $tab
     *
     * @return string
     */
    function _isInPage($page = null,$menu = null, $tab = null, $pass = false)
    {

        if ($page || is_array($page)) {

            if (is_array($page))
            foreach ($page as $p)
                if(Request::segment(2) == $p) $pass = true; else continue;

//            (is_array($page) && in_array('fabrications', $page) && Request::segment(2) == $page[ array_keys($page,'fabrications')[0] ])
            if (Request::segment(2) == $page || $pass){

                if ($menu){

                    if (Request::segment(3) == $menu){

                        if ($tab){

                            if (Request::segment(4) == $tab)
                                return true;

                            return false;
                        }

                        return true;
                    }
                    else if ($menu == 'only' && !Request::segment(3))
                        return true;
                    else if ($menu == 'alter' && !Request::segment(3))
                        return true;
                    else if ($menu == 'alter' && Request::segment(3) && Request::segment(3) == 'fabrications')
                        return true;
                    else if ($menu == 'alter' && Request::segment(2) && Request::segment(2) == 'fabrications')
                        return true;

                    return false;
                }

                return true;

            }
        }

        return false;
    }
}

if (!function_exists('_getOriginalExtension')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $path
     *
     * @return string
     */
    function _getOriginalExtension($path)
    {

        if ($path) {
            return pathinfo($path, PATHINFO_EXTENSION);
//            if (file_exists($path)){
//            }
        }

        return null;
    }
}

if (!function_exists('_getOriginalFileType')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $ext
     *
     * @return string
     */
    function _getOriginalFileType($ext)
    {

        if (in_array($ext,['jpg','jpeg','svg','png'])) {
            return 'image';
        } else if (in_array($ext,['mp4','mov','avi','mpeg'])){
            return 'video';
        }

        return 'image';
    }
}

if (!function_exists('_getSectionsData')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $path
     *
     * @return string
     */
    function _getSectionsData($page, $post, $section = null, $filterBy = null, $categoryBy = null, $limit = null, $offset = 0, $orderBy = null, $direction = 'asc', $depth = 0)
    {

        if ($page && $post) {

            $data = DB::table('custom_attributes')
                ->join('custom_posts', 'custom_posts.id', '=', 'custom_attributes.custom_post_id')
                ->leftJoin('custom_post_datas', 'custom_attributes.id', '=', 'custom_post_datas.custom_attribute_id')
                ->join('custom_sections', 'custom_sections.id', '=', 'custom_post_datas.section_id')
                ->leftJoin('custom_reference_categories', 'custom_post_datas.value', '=', 'custom_reference_categories.id')
                ->where('custom_posts.slug', $post)
//                ->orderBy('custom_post_datas.item_id','desc')
                ->orderBy('custom_attributes.sort')
                ->select(
                    'custom_post_datas.value',
                    'custom_sections.slug                       as section_slug',
                    'custom_sections.page                       as section_page',
                    'custom_post_datas.item_id                  as item_id',
                    'custom_attributes.*',
                    'custom_reference_categories.slug           as reference_slug',
                    'custom_reference_categories.label          as reference_label',
                    'custom_reference_categories.info           as reference_info',
                    'custom_reference_categories.description    as reference_description',
                    'custom_posts.name                          as post_name',
                    'custom_post_datas.sort                     as data_sort',
                    'custom_post_datas.created_at               as date_sort',
                    'custom_attributes.sort                     as attr_sort',
                    'custom_sections.sort                       as section_sort'
                )
                ->get();

//            return $data;

            if ($section){
                $data = $data->where('section_slug', $section)
                    ->where('section_page', $page);

                $items = $data->groupBy('item_id');

                $attributes = $data->groupBy('name')->toArray();

                $allItems = [];


                foreach ($items as $itemId => $item) {

                    // adding extra key value for help sorting
                    $sortableField = $orderBy? _getItemValues($item, [$orderBy,'reference_info','reference_description','item_id']) : [];

                    $allItems[] = _convertToPluckObject($item->pluck('value','name')->toArray(),$attributes, $sortableField);
                }
            }
            else {

                if ($categoryBy){

                    $items = $data->groupBy('item_id');

                    // dd($items);

                    $attributes = $data->groupBy('name')->toArray();
//                    $attributes[] = 'data_sort';

                    foreach ($items as $item) {

//                        dd($item->toArray());

                        $pluckArray = $item->pluck('value','name')->toArray();

                        // adding extra key value for help sorting
//                        $sortableField = $orderBy? [$orderBy => _getItemValue($item, $orderBy)] : [];
                        $sortableField = $orderBy? _getItemValues($item, [$orderBy,'reference_info','reference_description']): [];

                        $allItems[] = _convertToPluckObject($pluckArray,$attributes,$sortableField);

                    }


                    $allItems = _makeGroupByObject($allItems,$categoryBy);
//                    dd($allItems);

                }
                else {

                    if ($filterBy && is_array($filterBy) && count($filterBy)){

                        $items = $data->groupBy('item_id');

                        $attributes = $data->groupBy('name')->toArray();
                        $attributes[] = 'data_sort';

                        foreach ($items as $item) {

                            $pluckArray = $item->pluck('value','name')->toArray();

                            // adding extra key value for help sorting
                            $sortableField = $orderBy? _getItemValues($item, [$orderBy,'reference_info','reference_description']) : [];

                            $goodToSave = true;
                            foreach ($filterBy as $term => $value){
                                if ($pluckArray[$term] != $value){
                                    $goodToSave = false;
                                }
                            }

                            if ($goodToSave)
                                $allItems[] = _convertToPluckObject($pluckArray,$attributes,$sortableField);

                        }

                    }
                    else {

                        $items = $data->groupBy('item_id');

                        $attributes = $data->groupBy('name')->toArray();



                        $allItems = [];
                        foreach ($items as $itemId => $item) {

//                            dd($items);

                            // adding extra key value for help sorting
                            $sortableField = $orderBy? _getItemValues($item, [$orderBy,'reference_info','reference_description']) : [];

                            $allItems[$itemId] = _convertToPluckObject($item->pluck('value','name')->toArray(),$attributes,$sortableField);

                        }
                    }

                }
            }



            if (!isset($allItems) || !count($allItems))
                return [];
            //dd($allItems);
            //dd($orderBy);
            if ($orderBy)
            $allItems = _treeBasedSorting($allItems,$orderBy,$direction, $depth);

            // return data limit and offset wise
            return _getLimitedData($allItems, $limit, $offset);

        }

        return null;
    }
}

if (!function_exists('_getItemValue')){

    function _getItemValue($items, $key){

        if (count($items)){

            if (isset($items->first()->$key)){

                return $items->first()->$key;
            }
            else
                foreach ($items as $item) {

                    if ($item->name == $key)
                        return $item->value;
                }
        }

        return null;
    }
}

if (!function_exists('_getItemValues')){

    function _getItemValues($items, $keys = []){

        if (count($items) && count($keys)){

            $extras = [];

            foreach ($keys as $key){

                $extras[$key] = _getItemValue($items,$key);

            }

            return $extras;
        }

        return [];
    }
}

if (!function_exists('_swapItemOrderBy')){

    /**
     * Get the array representation of a stdClass object.
     *
     * @param array $allItems
     *
     * @param \String $keyName
     *
     * @param \String $direction
     *
     * @return array
     */
    function _swapItemOrderBy($allItems, $keyName , $direction = 'asc'){

        // loop through all the item to control sort
        for ($outerIndex = 0; $outerIndex < count($allItems); $outerIndex++){

            // again nested loop manipulate
            for ($innerIndex = $outerIndex + 1; $innerIndex < count($allItems); $innerIndex++){

                // check key name and value exists and
                switch ($direction){
                    case 'desc':

                        // checking unicode characters by making lower
                        if (mb_strtolower($allItems[$innerIndex]->$keyName) > mb_strtolower($allItems[$outerIndex]->$keyName)){

                            // swapping
                            $temp                   = $allItems[$innerIndex];
                            $allItems[$innerIndex]  = $allItems[$outerIndex];
                            $allItems[$outerIndex]  = $temp;

                        }

                        break;

                    case 'asc':
                    default:

                        // checking unicode characters by making lower
                        if (mb_strtolower($allItems[$innerIndex]->$keyName) < mb_strtolower($allItems[$outerIndex]->$keyName)){

                            // swapping
                            $temp                   = $allItems[$innerIndex];
                            $allItems[$innerIndex]  = $allItems[$outerIndex];
                            $allItems[$outerIndex]  = $temp;

                        }

                        break;

                }


            }
        }

        return $allItems;
    }
}

if (!function_exists('_treeBasedSorting')){

    /**
     * Get the array representation of a stdClass object.
     *
     * @param array $allItems
     *
     * @param \String $keyName
     *
     * @param \String $direction
     *
     * @param \Integer $depth
     *
     * @return array
     */

    function _treeBasedSorting($allItems, $keyName , $direction = 'asc', $depth = 0){

        // check items exists or not
        if ($allItems && count($allItems)){

            // if sorting starts from the second level
            if ($depth){

                // loop through all the child nodes
                foreach ($allItems as &$item){

                    // make sorting between child items
                    $item = _swapItemOrderBy($item, $keyName, $direction);

                }

                // sorting through parent items
                ksort($allItems);

                // returning sorted array
                return $allItems;

            }

            // root items will be sorting because depth is 0.
            else {

                // make sorting between items and return
                return _swapItemOrderBy($allItems, $keyName, $direction);
            }

        }

    }
}

if (!function_exists('_convertToPluckObject')) {

    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $path
     *
     * @return string
     */
    function _convertToPluckObject($array,$attr,$extra = [])
    {

        if (is_array($array) && count($array) && is_array($attr) && count($attr)){

            $obj = new stdClass();
            foreach ($attr as $column => $at){

                if (count($array) && isset($array[$column])) {

                    if ($at[0]->type == 'select'){

                        foreach ($at as $a){

                            if ($a->value == $array[$column]){

                                $obj->$column = isset($a->reference_label)? $a->reference_label : null;
                            }
                        }
                    }
                    else {

                        $obj->$column = $array[$column];
                    }
                }
                else
                    $obj->$column = null;
            }

            // adding extra key value with the returning object
            if (count($extra)){
                foreach ($extra as $column => $x)
                    $obj->$column = $x;
            }

            return $obj;
        }

        return null;
    }
}

if (!function_exists('_makeGroupByObject')) {


    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $path
     *
     * @return array
     */
    function _makeGroupByObject($array,$columns = [])
    {

        if (is_array($array) && count($array) && ((is_array($columns) && count($columns)) || $columns != "")){

            $bucket = [];
            foreach ($array as $object){

                if (!is_array($columns)){

                    if (isset($object->$columns)){

                        $bucket[$object->$columns][] = $object;
                    }
                }
                else {

                    $goodToPick = true;
                    $groupKeys = [];

                    foreach ($columns as $column) {
                        if (isset($object->$column)){
                            array_push($groupKeys,$object->$column);
                        }
                    }

                    if ($goodToPick){

                        $bucket[implode('-', $groupKeys)][] = $object;
                    }
                }


            }

//            dd($bucket);

            return $bucket;
        }

        return null;
    }
}

if (!function_exists('_getLimitedData')) {


    /**
     * Get the array representation of a stdClass object.
     *
     * @param mixed $array
     *
     * @param \String $limit
     *
     * @param \String $offset
     *
     * @return mixed
     */
    function _getLimitedData($array,$limit,$offset)
    {

        if (is_array($array)){

            if (++$offset && $limit){

                $iteration = 0;
                $allItems = [];
                foreach ($array as $index => $item){

                    // step ahead to counting iteration
                    $iteration++;

                    if ($offset >= $iteration && $iteration <= $limit){
                        $allItems[$index] = $item;
                    }

                }

                return $allItems;
            }

            return $array;
        }

        return $array;
    }
}

if (!function_exists('_eliminateBadChars')) {


    /**
     * Get the array representation of a stdClass object.
     *
     * @param \String $text
     *
     * @return string
     */
    function _eliminateBadChars($text)
    {

        $eliminateTokens = [
            '<span lang="NL">',
            '<span lang="IT">',
            '<span lang="DE">',
            '<span lang="ES-TRAD">'
        ];

        if ($text != ""){

            foreach ($eliminateTokens as $token)
                $text = str_replace($token,'',$text);
        }

        return $text;
    }
}

if (!function_exists('_convertToYoutubeVideoUrl')) {


    /**
     * Convert Youtube url to embed url to adjust in youtube player.
     *
     * @param \String $url
     *
     * @return mixed
     */
    function _convertToYoutubeVideoUrl($url)
    {

        if ($url != ""){


            $url    = strpos($url,'&') !== false? explode('&',$url)[0] : $url;
            $url    = strpos($url,'?') !== false? explode('?',$url)[1] : null;
            $code   = strpos($url,'=') !== false? explode('=',$url)[1] : null;

                return 'https://www.youtube.com/embed/' . $code;
        }

        return $url;
    }
}

if (!function_exists('_filterBy')) {

    /**
     * Mega Filter by column and value.
     *
     * @param array $items
     *
     * @return mixed
     */
    function _megaFilterBy($items, $column, $value)
    {

        $allItems = [];
        foreach ($items as $itemId => $item) {

            $goodToSave = true;
            if ($value != "" && isset($item[$column]) && strtolower($value) !== strtolower($item[$column]->value))
                $goodToSave = false;

            if ($goodToSave)
                $allItems[$itemId] = $item;

        }

        return $allItems;

    }
}

if (!function_exists('_getFilters')) {

    function _getFilters($items, $column)
    {

        $allItems = [];
        foreach ($items as $itemId => $item) {

            $allItems[ strtolower($item[$column]->value) ] = $column . '_' . strtolower($item[$column]->value);

        }

        return $allItems;

    }
}

if (!function_exists('_getItemAttributeValueById')) {

    function _getItemAttributeValueById($itemId, $attr)
    {

        $data = \App\Models\CustomPostData::where('item_id', $itemId)->with('customAttribute')->get();

        foreach ($data as $d){
            if (isset($d->customAttribute->name) && $d->customAttribute->name == $attr){
                return $d->value;
            }
        }

        return null;

    }
}


if (!function_exists('_regenerateMembershipDirectory')) {

    function _regenerateMembershipDirectory($type = 'by-number')
    {

        if ($type == 'by-number'){
            //get motorcade members
            $members = User::where('role', 'motorcade-member')->orderBy('name')->get();
            $pdf = PDF::loadView('admin.members.make_members_pdf', ['members' => $members]);
            $pdf->save(public_path('members/byClubName.pdf'));

            return 'members/byClubNumber.pdf';
        }

        $members = User::where('role', 'motorcade-member')->orderBy('club_number')->get();
        $pdf = PDF::loadView('admin.members.make_members_pdf', ['members' => $members]);
        $pdf->save(public_path('members/byClubNumber.pdf'));

        return 'members/byClubName.pdf';


    }
}

