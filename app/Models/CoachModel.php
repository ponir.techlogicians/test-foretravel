<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoachModel extends Model
{
//    public function Floorplans(){
//        return $this->belongsTo(CustomPost::class,'custom_post_id');
//    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
