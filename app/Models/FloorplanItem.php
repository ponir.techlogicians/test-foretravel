<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FloorplanItem extends Model
{
    
    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
    public function floorPlan(){
        return $this->belongsTo(FloorPlan::class,'floorplan_id');
    }
}
