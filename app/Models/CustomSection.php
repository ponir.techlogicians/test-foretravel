<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomSection extends Model
{
    public function customDatas(){
        return $this->HasMany(CustomPostData::class,'section_id');
    }

    public function customAttributes(){
        return $this->HasMany(CustomAttribute::class,'custom_attribute_id');
    }

    public function customPost(){
        return $this->belongsTo(CustomPost::class,'custom_post_id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
