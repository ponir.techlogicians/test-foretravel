<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['value'];

    public static function getContents($page = 'home', $key = null, $pluck = null,$tab = null){

        if ($key){
            return Page::where('name',$page)->where('key')->first();
        }

        if ($pluck){

            if ($tab)
                return Page::where('name',$page)->where('tab',$tab)->orderBy('sort')->pluck('value','key');

            return Page::where('name',$page)->orderBy('sort')->pluck('value','key');
        }

        if ($tab)
            return Page::where('name',$page)->where('tab',$tab)->orderBy('sort')->get();

        return Page::where('name',$page)->orderBy('sort')->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'created_by');
    }
}
