<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembersPaymentHistory extends Model
{
    protected $table = 'members_payment_history';

    public function package(){
        return $this->hasMany(CustomPostData::class,'item_id','item_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
