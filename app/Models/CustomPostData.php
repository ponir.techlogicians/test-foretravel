<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomPostData extends Model
{
    protected $table = 'custom_post_datas';

    protected $fillable = [

        'custom_post_id',
        'custom_attribute_id',
        'section_id',
        'page',
        'value',
        'item_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'

    ];

    public function customPost(){
        return $this->belongsTo(CustomPost::class,'custom_post_id');
    }

    public function customSection(){
        return $this->belongsTo(CustomSection::class,'section_id');
    }

    public function customAttribute(){
        return $this->belongsTo(CustomAttribute::class,'custom_attribute_id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
