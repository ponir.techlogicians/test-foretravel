<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
