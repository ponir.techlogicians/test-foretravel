<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    public function slides()
    {
    	return $this->hasMany(Slide::class)->orderBy('order');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }


    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
