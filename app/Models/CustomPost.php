<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomPost extends Model
{
    public function customPostDatas(){
        return $this->hasMany(CustomPostData::class,'custom_post_id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
