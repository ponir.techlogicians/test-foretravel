<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomReferenceCategory extends Model
{
    protected $table = 'custom_reference_categories';

    public function customAttribute(){

        return $this->belongsTo(CustomAttribute::class,'custom_reference_id','type');

    }

    

}
