<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'first_name',
        'last_name',
        'email',
        'spouse_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zip',
        'phone_number',
        'cell_number',
        'birthday',
        'spouse_birthday',
        'anniversary',
        'coach_type',
        'purchase_date',
        'purchase_form',
        'year_model',
        'last_six_number_vin',
        'complete_model_number',
        'fmca_member',
        'fmca_number',
        'club_number',
        'role',
        'is_paid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function slider(){
        return $this->hasMany(Slider::class,'created_by');
    }

    public function contacts(){
        return $this->hasMany(Contact::class);
    }

    public function testimonals(){
        return $this->hasMany(Testimonial::class);
    }

    public function floorplans(){
        return $this->hasMany(FloorPlan::class);
    }

    // public function createdBy(){
    //     return $this->hasMany(Slide::class,'created_by');
    // }

    // // public function slideUpdatedBy(){
    // //     return $this->hasMany(Sl);
    // // }
}
