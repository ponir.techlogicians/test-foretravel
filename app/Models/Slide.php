<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    public function slider()
    {
    	return $this->belongsTo(Slider::class);
    }

    public function coachModel()
    {
    	return $this->belongsTo(CoachModel::class,'model','slug');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }
}
