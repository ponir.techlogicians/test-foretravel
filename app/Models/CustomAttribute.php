<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomAttribute extends Model
{
    public function customPost(){
        return $this->belongsTo(CustomPost::class,'custom_post_id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }

    public function customReferenceCategory(){
        return $this->hasMany(CustomReferenceCategory::class,'custom_reference_id','type');
    }
}
