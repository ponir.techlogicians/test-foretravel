<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceSpecial extends Model
{
    protected $fillable = [
        'image',
        'description',
        'model_id',
        'sort'
    ];

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class,'updated_by');
    }

    public function model(){
        return $this->belongsTo(Model::class,'model_id');
    }

}
