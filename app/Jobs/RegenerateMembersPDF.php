<?php

namespace App\Jobs;

use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
//use PDF;

class RegenerateMembersPDF implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!is_dir(public_path('members'))){
            mkdir(public_path('members'));
        }


        // get motorcade members
        $members = \App\Models\User::where('role', 'motorcade-member')->orderBy('club_number')->get();

        $pdf = PDF::loadView('admin.members.make_members_pdf', ['members' => $members]);
        $pdf->save(public_path('members/byClubNumber.pdf'));

        // get motorcade members
//        $members = \App\Models\User::where('role', 'motorcade-member')->orderBy('name')->get();
//        $pdf = PDF::loadView('admin.members.make_members_pdf', ['members' => $members]);
//        $pdf->save(public_path('members/byClubName.pdf'));


    }
}
