<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

use App\Mail\NotificationSend;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/model/{coach?}', 'HomeController@models')->name('coach-model');
Route::get('/inventory', 'HomeController@inventory')->name('inventory');
Route::get('/motorcade-club', 'HomeController@motorcadeClub')->name('motorcade-club');
Route::get('/recall-information' , 'HomeController@recallInformation')->name('recall-information');
Route::get('/owner-service' , 'HomeController@ownerService')->name('owner-service');
Route::get('/technical-support', 'HomeController@technicalSupport')->name('technical-support');
//Route::get('/technical-support/search', 'HomeController@technicalSupportSearch')->name('technical-support-search');
Route::get('/information', 'HomeController@information')->name('information');
Route::get('/parts-department', 'HomeController@partsDepartment')->name('parts-department');
Route::get('/remodel-department', 'HomeController@remodelDepartment')->name('remodel-department');
Route::get('/service-department','HomeController@serviceDepartment')->name('service-department');
Route::get('/career','HomeController@career')->name('career');
Route::get('reference-data-by-selection/{selectedId}', 'HomeController@getReferenceData');
Route::get('fabrication/{page}', 'HomeController@getFabrication');

Route::post('/contact', 'HomeController@contactSubmit')->name('contact');

Route::post('/motorcade-member-register', 'Auth\RegisterController@motorcadeMemberRegister')->name('motorcade-member-register')->middleware('guest');
Route::get('/motorcade-member-profile', 'HomeController@motorcadeMemberProfile')->name('motorcade-member-profile');
Route::post('/motorcade-member-profile', 'HomeController@motorcadeMemberUpdate')->name('motorcade-member-update');
Route::get('/motorcade-member-directory/{type}', 'HomeController@motorcadeMemberDirectory')->name('motorcade-member-directory');

# PayPal checkout
Route::get('checkout', 'PaypalController@payWithPayPal')->name('checkout');

# PayPal status callback
Route::get('paypal-payment-status', 'PaypalController@getPaymentStatus')->name('payment-status');


Route::get('send-mail', function (){

    // get the subject of notification email.
    $subject        = 'New Motorcade Member Registration!!';

    // get the body of notification email.
    $contentText    = Auth::user();

    Mail::to(env('MAIL_TO_ADDRESS'))->send(new NotificationSend('member-registration',$contentText, $subject));

});
