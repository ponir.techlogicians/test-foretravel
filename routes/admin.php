<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin-level routes. Routes in this file
| are protected by auth/login requirements.
|
*/

Route::middleware('admin')->namespace('Admin')->group(function () {

    /**
     * Admin-level homepage/dashboard.
     */
    Route::get('/', 'HomeController@index')->name('home');

    // Testimonials Route
    Route::resource('testimonials', 'TestimonialController');


    // Contact Route
    Route::resource('contacts', 'ContactController');

// setting route
    Route::get('settings' , 'SettingController@index')->name('settings.edit');

    Route::post('settings', 'SettingController@store')->name('settings.store');

    // new fabrication pages
//    Route::get('page/fabrication/list' , 'PageController@listFabrication')->name('fabrication.list');

    Route::resource('fabrications', 'FabricationController');

// slider Route
    Route::resource('sliders', 'SliderController');


    // slide Route
    Route::resource('slides', 'SlideController');

    // floorplan route
    Route::resource('floorplans', 'FloorPlanController');

    Route::post('floorplans/add-items', 'FloorPlanController@addItems');

    // coach model route
    Route::get('models', 'HomeController@models');
    Route::get('models/edit/{id}', 'HomeController@editModel');
    Route::post('models/update/{id}', 'HomeController@updateModel');

    // page route
    Route::get('page/{page}/{tab?}', 'PageController@index');

    Route::post('page/update', 'PageController@update');

    Route::post('page/content/{page}/{key}/delete', 'PageController@deleteContent');

    Route::post('page/content/{page}/{key}/{tab?}/delete', 'PageController@deleteContent');

    // custom reference categories routes
    Route:Route::resource('custom-reference-categories', 'CustomReferenceCategoryController');
    // custom post routes
    Route::get('/custom-posts','CustomPostController@index')->name('customposts.index');

    Route::Post('/custom-posts/slug','CustomPostController@slug')->name('customposts.slug');
    Route::get('/custom-post/create','CustomPostController@create')->name('customposts.create');

    Route::post('/custom-post/store','CustomPostController@store')->name('customposts.store');

    Route::get('/cutom-post/{id}/edit','CustomPostController@edit')->name('customposts.edit');

    Route::post('/custom-post/{id}/update','CustomPostController@update')->name('customposts.update');

    Route::delete('/custom-post/{id}/delete','CustomPostController@delete')->name('customposts.delete');

    Route::get('/custom-reference-category','MegaController@customReferenceCategory')->name('custom-reference-attribute.index');

    Route::get('/custom-reference-category/{id}/edit','MegaController@editCustomReferenceCategory')->name('custom-reference-attribute.edit');

    Route::post('/custom-reference-category/{id}/update','MegaController@updateCustomReferenceCategory')->name('custom-reference-attribute.update');

    /**
     * Users
     */
    //    Route::get('users/{user}/delete', 'UserController@delete')->name('users.delete');
    Route::resource('users', 'UserController');
    Route::resource('members', 'MemberController');
    Route::resource('virtual-tour', 'VirtualTourController'); // ***
//    Route::resource('service-specials', 'ServiceSpecialsController'); // ***
    Route::get('tour-event/payments', 'MemberController@paymentHistory');

    Route::get('{customPostSlug}', 'MegaController@index');
    Route::get('{customPostSlug}/create', 'MegaController@create');
    Route::post('{customPostSlug}/store', 'MegaController@store');
    Route::get('{customPostSlug}/{itemId}/edit', 'MegaController@edit');
    Route::put('{customPostSlug}/{itemId}/update', 'MegaController@update');
    Route::delete('{customPostId}/{itemId}/destroy', 'MegaController@delete');
    Route::post('{customPostId}/{itemId}/{sectionId}/{attributeId}/delete', 'MegaController@deleteDocument');
    Route::get('reference-data-by-selection/{selectedId}/{tableReferencedBy?}/{foreignKey?}', 'MegaController@getReferenceData');
    Route::post('update-sorting-order', 'MegaController@updateSortOrder');

    Route::resource('custom-attributes', 'CustomAttributeController');

});

