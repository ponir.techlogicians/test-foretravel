@component('mail::message')
	@slot('slot')

        <h3>Payment made:</h3>
        <p><strong>Reason:</strong> {{ $body['reason'] }}</p>
        <p><strong>Amount:</strong> {{ $body['amount'] }}</p>
        <p><strong>Paid By:</strong> {{ $body['name'] }}</p>

	@endslot
@endcomponent
