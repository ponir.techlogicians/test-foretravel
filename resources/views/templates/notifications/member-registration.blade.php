@component('mail::message')
    @slot('slot')

        <h3>New user registered:</h3>
        <p><strong>Name:</strong> {{ $body->name }}</p>
        <p><strong>Username:</strong> {{ $body->username }}</p>
        <p><strong>Email:</strong> {{ $body->email }}</p>
        <p><strong>Spouse Name:</strong> {{ $body->spouse_name }}</p>
        <p><strong>Phone Number:</strong> {{ $body->phone_number }}</p>
        <p><strong>Cell Number:</strong> {{ $body->cell_number }}</p>
        <p><strong>Birthday:</strong> {{ $body->birthday }}</p>
        <p><strong>Spouse Birthday:</strong> {{ $body->spouse_birthday }}</p>
        <p><strong>Anniversary:</strong> {{ $body->anniversary }}</p>
        <p><strong>Address:</strong> {{ $body->address_line_1 }}</p>
        <p><strong>Address 2:</strong> {{ $body->address_line_2 }}</p>
        <p><strong>City:</strong> {{ $body->city }}</p>
        <p><strong>State:</strong> {{ $body->state }}</p>
        <p><strong>Zip:</strong> {{ $body->zip }}</p>
        <p><strong>Coach Type:</strong> {{ $body->coach_type }}</p>
        <p><strong>Purchase Date:</strong> {{ $body->purchase_date }}</p>
        <p><strong>Purchase From:</strong> {{ $body->purchase_form }}</p>
        <p><strong>Year Model:</strong> {{ $body->year_model }}</p>
        <p><strong>Last Six Number of VIN:</strong> {{ $body->last_six_number_vin }}</p>
        <p><strong>Complete Model Number:</strong> {{ $body->complete_model_number }}</p>
        <p><strong>FMCA Member:</strong> {{ $body->fmca_member == 1? 'Yes' : 'No'  }}</p>
        <p><strong>FMCA Number:</strong> {{ $body->fmca_number }}</p>

    @endslot
@endcomponent
