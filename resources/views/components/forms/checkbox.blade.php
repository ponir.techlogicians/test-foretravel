<div class="form-group {{ $formGroupClass ?? null }}">
    <div class="custom-control custom-checkbox {{ $controlClass ?? null }}">

        {{-- The checkbox itself --}}
        {!! Form::checkbox(
            $name, 
            ($value ?? 1), 
            old($name, $checked ?? null), 
            [
                'class' => 'custom-control-input' . ($errors->has($name) ? ' is-invalid' : ''), 
                'id' => ($id ?? Format::alphanumeric($name, false))
            ]
        ) !!}

        {{-- Label & tooltip --}}
        @if (!empty($label) || !empty($tooltip))
            <label class="custom-control-label" for="{{ ($id ?? $name) }}">
                {{ $label }}
                @if (!empty($tooltip))
                    @tooltip($tooltip)
                @endif
            </label>
        @endif

        {{-- Validation message --}}
        @if ($errors->has($name))
            <div class="invalid-feedback">
                {{ $errors->first($name) }}
            </div>
        @endif

        {{-- Help text --}}
        @if (!empty($help))
            <small class="form-text text-muted">
                {{ $help }}
            </small>
        @endif
    </div>
</div>