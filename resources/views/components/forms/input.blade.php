<div class="form-group {{ $formGroupClass ?? null }}">

    {{-- Show the label --}}
    {!! Form::label($name, $label, ['class' => 'form-label' . (!empty($labelClass) ? ' ' . $labelClass : '')]) !!}

    {{-- Show the tooltip --}}
    @if (!empty($tooltip))
        @tooltip($tooltip)
    @endif

    {{-- Start the input-group if any of the children are present --}}
    @if (!empty($prepend) || !empty($append) || !empty($help) || $errors->has($name))
        <div class="input-group">
    @endif

    {{-- Prepend icon(s) --}}
    @if(!empty($prepend))
        <div class="input-group-prepend">
            @if (is_array($prepend))
                @foreach($prepend as $icon)
                    <div class="input-group-text">@icon($icon)</div>
                @endforeach
            @elseif (is_string($prepend))
                <div class="input-group-text">@icon($prepend)</div>
            @endif
        </div>
    @endif

    {{-- The input itself --}}
    {{ $input }}

    {{-- Append icon(s) --}}
    @if (!empty($append))
        <div class="input-group-append">
            @if (is_array($append))
                @foreach($append as $icon)
                    <div class="input-group-text">@icon($icon)</div>
                @endforeach
            @else
                <div class="input-group-text">@icon($append)</div>
            @endif
        </div>
    @endif

    {{-- Help text --}}
    @if (!empty($help))
        <small class="form-text text-muted">
            {{ $help }}
        </small>
    @endif

    {{-- Validation message --}}
    @if ($errors->has($name))
        <div class="invalid-feedback">
            {{ $errors->first($name) }}
        </div>
    @endif

    {{-- Close the input-group, if necessary --}}
    @if (!empty($prepend) || !empty($append) || !empty($help) || $errors->has($name))
        </div>
    @endif

</div>