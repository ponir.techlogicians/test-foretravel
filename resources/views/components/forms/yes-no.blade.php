<div class="text-right">
    <button class="btn btn-success" type="submit">
        {{ __('Yes') }}
        @icon('submit')
    </button>
    <a class="btn btn-danger" href="{{ $back }}">
        {{ __('No') }}
        @icon('cancel')
    </a>
</div>