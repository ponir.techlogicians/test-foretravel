@component('components.forms.input', [
    'input'          => Form::email(
        $name, 
        ($value ?? old($name)),
        array_merge(
            [
                'id'    => $id ?? Format::alphanumeric($name, false),
                'class' => 'form-control' . ($errors->has($name) ? ' is-invalid' : '') . (!empty($class) ? ' ' . $class : '')
            ],
            (!empty($extra) ? $extra : [])
        )
    ),
    'name'           => $name ?? null,
    'label'          => $label ?? null,
    'value'          => $value ?? null,
    'help'           => $help ?? null,
    'prepend'        => $prepend ?? null,
    'append'         => $append ?? null,
    'tooltip'        => $tooltip ?? null,
    'formGroupClass' => $formGroupClass ?? null,
])@endcomponent