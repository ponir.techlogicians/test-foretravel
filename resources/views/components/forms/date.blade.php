@component('components.forms.input', [
    'input'           => Form::text(
        $name, 
        ($value ?? old($name)),
        array_merge(
            [
                'id'          => $id ?? Format::alphanumeric($name, false),
                'class'       => 'form-control' . ($errors->has($name) ? ' is-invalid' : '') . (!empty($class) ? ' ' . $class : ''),
                'data-toggle' => 'datepicker',
            ],
            (!empty($extra) ? $extra : [])
        )
    ),
    'name'           => $name ?? null,
    'label'          => $label ?? null,
    'value'          => $value ?? null,
    'help'           => $help ?? null,
    'prepend'        => $prepend ?? null,
    'append'         => (!empty($append) ? (is_array($append) ? array_merge($append, ['date']) : [$append, 'date']) : 'date'),
    'tooltip'        => $tooltip ?? null,
    'formGroupClass' => $formGroupClass ?? null,
])@endcomponent