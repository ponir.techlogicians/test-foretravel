<div class="text-right">
    <button class="btn btn-success" type="submit">
        {{ __('Save') }}
        @icon('save')
    </button>
    <a class="btn btn-danger" href="{{ $back }}">
        {{  __('Cancel')  }}
        @icon('cancel')
    </a>
</div>