@component('components.forms.input', [
    'input'          => Form::select(
        $name, 
        $options, 
        ($value ?? old($name)),
        array_merge(
            [
                'id'          => $id ?? Format::alphanumeric($name, false),
                'class'       => 'form-control' . ($errors->has($name) ? ' is-invalid' : '') . (!empty($class) ? ' ' . $class : ''),
                'placeholder' => $placeholder ?? null,
                'multiple'    => (!empty($multiple) ? 'multiple' : null),
                'size'        => (!empty($multiple) ? ($size ?? 3) : null),
            ],
            (!empty($extra) ? $extra : [])
        )
    ),
    'name'           => $name ?? null,
    'label'          => $label ?? null,
    'value'          => $value ?? null,
    'help'           => $help ?? null,
    'prepend'        => $prepend ?? null,
    'append'         => $append ?? null,
    'tooltip'        => $tooltip ?? null,
    'formGroupClass' => $formGroupClass ?? null,
])@endcomponent