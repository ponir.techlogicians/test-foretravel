@if (!empty($form))
    {!! $form !!}
@endif
    <div class="card {{ $class ?? null }}">
        <h5 class="card-header {{ $classHeader ?? null }}">
            {!! $icon !!}
            {{ $title }}
            @if (!empty($actions))
                <div class="card-header-actions">
                    {!! $actions !!}
                </div>
            @endif
        </h5>
        <div class="card-body {{ $classBody ?? null }}">
            {{ $slot }}
        </div>
        @if (!empty($footer))
            <div class="card-footer">
                {!! $footer !!}
            </div>
        @endif
    </div>
@if (!empty($form))
    {!! Form::close() !!}
@endif