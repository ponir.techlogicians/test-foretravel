@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Members')  !!}
    <div class="">
        <div class="row">
            <div class="col-12">

                <div class="page-header">
                    <h1>
                        Fabrication Services
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All Pages
                        </small>
                    </h1>
                </div>

                <a href="javascript:;" onclick="updateSortOrder()" data-model="fabrications" id="sortButton" style="display: none;position: absolute;top: 56px;right: 397px;">
                    <span class="btn btn-warning">Save Order</span>
                </a>

                <a href="javascript:;" onclick="cancelSortOrder()" id="cancelSortButton" style="display: none;position: absolute;top: 56px;right: 320px;">
                    <span class="btn btn-danger">Clear</span>
                </a>

                <a href="{{ url('admin/fabrications/create' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '')) }}">
                    <span class="btn btn-success create-button">Create</span>
                </a>

                @if (session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Success!
                        </strong>
                        {{ session('success') }}
                        <br>
                    </div>
                @endif


                <table id="d" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>SN #</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody class="sortable">
                    @php $count = 0 @endphp
                    @foreach($fabrications as $page)

                        <tr class="items" data-item_id="{{ $page->id }}">

                            <td>{{ ++$count }}</td>
                            <td>{{ $page->label }}</td>
                            <td>
                                <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                    <a class="btn btn-xs btn-success" href="{{route('admin.fabrications.edit', $page->slug)}}">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    </a>

                                    <form method="post" class="delete_form" action="{{route('admin.fabrications.destroy', $page->slug)}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                        <span class="red">
                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>


@endsection
