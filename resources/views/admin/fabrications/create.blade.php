@extends('layouts.admin')
@section('content')
    <div class="container background-color">
        <div class="row">
            <div class="col-12">
                <div class="page-header">
                    <h1>
                        Fabrication Pages
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Create Page
                        </small>
                    </h1>
                </div>

                <form class="form-horizontal" action="{{ route('admin.fabrications.store')}}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ old('name') }}" id="form-field-1" placeholder="Name" name="name" required class="col-xs-10 col-sm-5" />
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Create
                            </button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

@endsection

