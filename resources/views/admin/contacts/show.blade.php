@extends('layouts.admin')
        @section('content')

            {!!  _breadcrumbs(['Contacts','Details'])  !!}
        <div class="background-color">
            <div class="row">
                <div class="col-12">
                    <div class="page-header">
                        <h1>
                            Contacts
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                Details
                            </small>
                        </h1>
                    </div>

                    <table id="simple-table" class="table  table-bordered table-hover">

                        <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{$contact->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$contact->email}}</td>
                            </tr>
                            <tr>
                                <th>Interested In</th>
                                <td>{{$contact->interested_in}}</td>
                            </tr>
                            <tr>
                                <th>Message</th>
                                <td>{{$contact->message}}</td>
                            </tr>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>

        @endsection

