@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Contacts')  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
                <div class="page-header">
                        <h1>
                            Contacts
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                All Contacts
                            </small>
                        </h1>
                    </div>
            <div class="all-contacts">
                <table  id="dynamic-table" class="table  table-bordered table-hover">
                        <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Interested In</th>
                                    <th>Message</th>
                                    <th>Actions</th>
                                </tr>
                        </thead>

                        <tbody>
                            @foreach ($contacts as $contact)
                            <tr>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->interested_in}}</td>
                            <td>{{$contact->message}}</td>
                            <td>
                                <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                    {{-- <button class="btn btn-xs btn-success" class="tooltip-info" data-rel="tooltip" title="View"> --}}
                                        <a class="btn btn-xs btn-success" href="{{route('admin.contacts.show', $contact->id)}}" >
                                        <i class="ace-icon fa fa-eye bigger-120"></i>
                                        </a>
                                    {{-- </button> --}}

                                    {{-- <button class="btn btn-xs btn-info" class="tooltip-info" data-rel="tooltip" title="Edit">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                        <a href="{{route('contacts.edit', $contact->id)}}"></a>
                                    </button> --}}


                                    <form method="post" class="delete_form" action="{{route('admin.contacts.destroy', $contact->id)}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                                <span class="red">
                                                    <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </span>
                                        </button>
                                    </form>
                                </div>
                            </td>

                        </tr>
                            @endforeach

                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
