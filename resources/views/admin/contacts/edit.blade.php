@extends('layouts.admin')
        @section('content')
        <div class="container background-color">
            <div class="row">
                <div class="col-12">
                        <div class="page-header">
                                <h1>
                                    Contacts
                                    <small>
                                        <i class="ace-icon fa fa-angle-double-right"></i>
                                        Edit Contacts
                                    </small>
                                </h1>
                        </div>

                    <form class="form-horizontal" action="{{ route('admin.contacts.update', $contact->id)}}" method="post">
                            {{ csrf_field() }}
                            @method('PATCH')
                            <div class="form-group @error('name') has-error   @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="name"> Name </label>
        
                                    <div class="col-sm-9">
                                        <div class="clearfix">  
                                        <input type="text" id="name" value="{{$contact->name}}" placeholder="Name" name="name" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('name')
                                        <div id="name-error" class="help-block">{{ $message }}</div>
                                        @enderror 
                                    </div>
                            </div>

                            <div class="form-group @error('email') has-error   @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="name"> Email </label>
        
                                    <div class="col-sm-9">
                                        <div class="clearfix">  
                                        <input type="text" id="name" value="{{$contact->email}}" placeholder="Name" name="email" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('email')
                                        <div id="name-error" class="help-block">{{ $message }}</div>
                                        @enderror 
                                    </div>
                            </div>

                            <div class="space-4"></div>

                                <div class="form-group @error('email') has-error   @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Intersted In </label>
                                    
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="form-field-1-1" value="{{$contact->interested_in}}" placeholder="Interested in" name="interested_in" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('interested_in')
                                        <div id="name-error" class="help-block">{{ $message }}</div>
                                        @enderror 
                                    </div> 
                                </div>

                                <div class="form-group @error('message') has-error @enderror">
                                        <label class="col-sm-3 control-label no-padding-right" for="message">Message</label>
                                        
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                            <textarea class="col-xs-10  col-sm-5" rows="5" id="form-field-9"  name="message">{{$contact->message}}</textarea>
                                            </div>
                                            @error('message')
                                                <div  class="help-block">{{ $message }}</div>
                                            @enderror
                                           
                                        </div>
                                       
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                    </form>


                </div>
            </div>
        </div>

        @endsection

