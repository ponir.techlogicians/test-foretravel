@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['CustomPost','Create'])  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    Floor Plans
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Create Floor Plan
                    </small>
                </h1>
            </div>

            <div class="form">
                <form id="validation-form" class="form-horizontal" action="{{ route('admin.floorplans.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group @error('model') has-error   @enderror">
                                <label class="col-sm-3 control-label no-padding-right" for="model"> Model </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="model" name="model" class="col-xs-10 col-sm-5">
                                            @foreach($coachModels as $model)
                                                <option value="{{ $model->slug }}">{{ $model->title }}</option>
                                            @endforeach
                                        </select>
                                        {{--<input type="text" id="model" placeholder="Model" name="model" class="col-xs-10 col-sm-5" />--}}
                                    </div>
                                    @error('model')
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                    @enderror
                                </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group  @error('title') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" id="title" placeholder="Title" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('title')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group @error('image') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image</label>
                            <div class="col-sm-4">
                            <div class="clearfix">
                                <input class="col-xs-10 col-sm-5 id-input-file-3"  type="file" name="image"/>
                            </div>
                            @error('image')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror

                            </div>
                        </div>


                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Create
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

