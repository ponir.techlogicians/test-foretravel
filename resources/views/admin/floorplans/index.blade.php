@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['Model', 'Floorplans'])  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    FloorPlans
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        All FloorPlans
                    </small>
                </h1>


                <a href="javascript:;" onclick="updateSortOrder()" data-model="floor_plans" id="sortButton" style="display: none;position: absolute;top: 56px;right: 397px;">
                    <span class="btn btn-warning">Save Order</span>
                </a>

                <a href="javascript:;" onclick="cancelSortOrder()" id="cancelSortButton" style="display: none;position: absolute;top: 56px;right: 320px;">
                    <span class="btn btn-danger">Clear</span>
                </a>

                <a href="{{ url('admin/floorplans/create' . (isset($_GET['model'])? ('?model=' . $_GET['model']) : '') ) }}">
                    <span class="btn btn-success create-button">Create</span>
                </a>

            </div>

            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif


            @if(isset($data['filters']) && count($data['filters']))
                <form action="">
                    <select name="filterBy" onchange='this.form.submit()' class="chosen-select form-control filter-select" id="form-field-select-3" data-placeholder="Filter" style="width: 200px;float: right;position: absolute;top: 59px;right: 110px;">
                        <option value="">Select to Filter</option>
                        @foreach($data['filters'] as $index => $filter)
                            <option {{ Request::get('filterBy') == $filter? 'selected' : ''}} value="{{ $filter }}"> {{ ucfirst($index) }}</option>
                        @endforeach
                    </select>

                    <input type="hidden" name="model" value="{{ Request::get('model') }}">
                    <input type="hidden" name="ref" value="{{ Request::get('ref') }}">
                </form>
            @endif


            <div class="all-contacts">
                <table  id="dynamic-table" class="table  table-bordered table-hover">
                        <thead>
                                <tr>
                                    <th>Model</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Updated By</th>
                                    <th>Actions</th>
                                </tr>
                        </thead>

                        <tbody class="sortable">
                            @foreach ($floorplans as $floorplan)
                            <tr class="items" data-item_id="{{ $floorplan->id }}">
                                <td>{{ $floorplan->model }}</td>
                                <td>{{ $floorplan->title }}</td>
                                <td><img style="width:60px;height:60px" src="{{ asset($floorplan->image) }}" alt=""></td>

                                <td>{{ $floorplan->updatedBy->name }}</td>
                                <td>
                                    <div class="hidden-sm hidden-xs btn-group" style="display:flex">

                                        <a class="btn btn-xs btn-success" href="{{route('admin.floorplans.edit', $floorplan->id) . (isset($_GET['model'])? ('?model=' . $_GET['model']) : '') }}" >
                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                        </a>

                                        <form method="post" class="delete_form" action="{{route('admin.floorplans.destroy', $floorplan->id) . (isset($_GET['model'])? ('?model=' . $_GET['model']) : '')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                                    <span class="red">
                                                        <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </span>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
