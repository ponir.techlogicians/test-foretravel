@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['Model', 'Floorplans','Edit'])  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    Floor plans
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit Floor plan
                    </small>
                </h1>
            </div>


            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Error !!
                    </strong>
                    {{ session('error') }}
                    <br>
                </div>
            @endif

            <div class="form">
                <form id="validation-form" class="form-horizontal" action="{{ route('admin.floorplans.update',$floorplan->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method('PATCH')
                        <div class="form-group @error('model') has-error   @enderror">
                                <label class="col-sm-3 control-label no-padding-right" for="model"> Model </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="model" name="model" class="col-xs-10 col-sm-5">
                                            @foreach($coachModels as $model)
                                                <option {{$floorplan->model == $model->slug ? 'selected' : ''}} value="{{ $model->slug }}">{{ $model->title }}</option>
                                            @endforeach
                                        </select>
                                        {{--<input type="text" value="{{$floorplan->model}}" id="model" placeholder="Model" name="model" class="col-xs-10 col-sm-5" />--}}
                                    </div>
                                    @error('model')
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                    @enderror
                                </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group  @error('title') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                <input type="text" value="{{$floorplan->title}}" id="title" placeholder="Title" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('title')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group @error('image') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image</label>
                            <div class="col-sm-4">
                            <div class="clearfix">
                                <input class="col-xs-10 col-sm-5 id-input-file-3"  type="file" name="image"/>
                            </div>
                            @error('image')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror

                            </div>
                            <div class="col-sm-3">
                            @if($floorplan->image != '')
                            <div class="col-xs-4">
                                <ul class="ace-thumbnails clearfix">
                                    <li>
                                        <a href="{{ asset($floorplan->image) }}" data-rel="colorbox" class="cboxElement">
                                            <img width="150" height="150" alt="150x150" src="{{ asset($floorplan->image) }}">
                                        </a>

                                        {{--<div class="tools tools-right">--}}
                                            {{--<a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/' . $content->key .'/delete') }}">--}}
                                                {{--<i class="ace-icon fa fa-times red"></i>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            </div>
                        </div>


                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>
                            </div>
                        </div>
                </form>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            {{-- create floorplan iteams --}}

            <div class="space-4"></div>

            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        Update Floor Plan Items

                    </h1>
                </div>
                <div class="form">
                    <form id="validation-form" class="form-horizontal" action="{{ url('admin/floorplans/add-items')}}" method="POST">
                            {{ csrf_field() }}


                        <div class="input-container">

                            @if(!count($floorplan->floorplanitems))

                                <div class="form-group after-add-more-text @error('text') has-error @enderror">
                                    <label class="col-sm-2 control-label no-padding-right" for="model"> Text </label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="model" placeholder="Type your text here" name="text[]" value="" class="col-xs-10 col-sm-9" />

                                            <div class="col-sm-2">

                                                <button class="btn  btn-success add-more-text" type="button">
                                                    <i class="ace-icon fa fa-plus bigger-110"></i>
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @foreach ($floorplan->floorplanitems as $item)
                                <div class="form-group @if($loop->first) after-add-more-text @else control-group-text @endif @error('text') has-error   @enderror">
                                    <label class="col-sm-2 control-label no-padding-right" for="model"> Text </label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="model" placeholder="Type your text here" name="text[]" value="{{ $item->text }}" class="col-xs-10 col-sm-9" />

                                            <div class="col-sm-2">

                                                @if($loop->first)
                                                    <button class="btn  btn-success add-more-text" type="button">
                                                        <i class="ace-icon fa fa-plus bigger-110"></i>
                                                    </button>
                                                @else
                                                    <button class="btn btn-danger remove-more-text" type="button">
                                                        <i class="ace-icon fa fa-trash bigger-110"></i>
                                                    </button>
                                                @endif

                                            </div>
                                        </div>
                                        @error('text')
                                        <div id="name-error" class="help-block">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                {{--<div class="space-4"></div>--}}
                            @endforeach
                        </div>

                        <input type="hidden" value="{{ $floorplan->id }}" id="title" placeholder="Type your text here" name="floorplan_id" class="col-xs-10 col-sm-9" />

                        <div class="clearfix">
                                <div class="col-md-offset-5 col-md-7">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Submit
                                    </button>
                                </div>
                            </div>

                    </form>


                    {{-- copy file design --}}

                    {{-- text field --}}
                    <div class="copy-text hide">
                        <div class="form-group control-group-text    @error('text') has-error   @enderror" style="margin-top:10px;">
                            <label class="col-sm-2 control-label no-padding-right" for="model"> Text </label>

                            <div class="col-sm-10">
                                <div class="clearfix">
                                    <input type="text" id="model" placeholder="Type your text" name="text[]" class="col-xs-10 col-sm-9" />
                                    <div class="col-sm-2">
                                        <button class="btn btn-danger remove-more-text" type="button">
                                            <i class="ace-icon fa fa-trash bigger-110"></i>
                                        </button>
                                    </div>
                                </div>
                                @error('text')
                                <div id="name-error" class="help-block">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                    </div>

             </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function() {


        $(".add-more-text").click(function(){
            var html = $(".copy-text").html();
            $(".input-container").append(html);

        });


        $(document).on("click",".remove-more-text",function(){

            $(this).parents(".control-group-text").remove();

        });

        $("input[name='text[]']").each(function(){
            var text = $("input[name='text[]']").val();
            if(text==null){
                $('.error-floorplan').removeClass("")
            }else{
                $('.error-floorplan').addClass("hide")
            }
        })
    });

    </script>
</div>
@endsection
