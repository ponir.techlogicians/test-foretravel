@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    FloorPlanItems
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit  FloorPlanItems
                    </small>
                </h1>
            </div>

            <div class="form">
                <form id="validation-form" class="form-horizontal" action="{{ route('admin.floorplanitems.update',$floorplanitem->id)}}" method="POST">
                        {{ csrf_field() }}
                        @method('PATCH')

                        <div class="form-group @error('text') has-error   @enderror">
                                <label class="col-sm-3 control-label no-padding-right" for="model"> Text </label>
    
                                <div class="col-sm-9">
                                    <div class="clearfix">  
                                    <input type="text" value="{{$floorplanitem->text}}" id="model" placeholder="text" name="text" class="col-xs-10 col-sm-5" />
                                    </div>
                                    @error('text')
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                    @enderror 
                                </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group  @error('order') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="order"> Order </label>
    
                            <div class="col-sm-9">
                                <div class="clearfix">
                                <input type="text" value="{{$floorplanitem->order}}" id="order" placeholder="order" name="order" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('title')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="space-4"></div>
    
                        <div class="form-group hidden  @error('floorplan_id') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="title"> FloorplanId </label>
    
                            <div class="col-sm-9">
                                <div class="clearfix">
                                <input type="text" value="{{$floorplanitem->floorplan_id}}" id="title" placeholder="Title" name="floorplan_id" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('floorplan_id')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
    
                        
                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>
                            </div>
                        </div>
                </form>
                </div>
        </div>
    </div>
</div>
@endsection