@extends('layouts.admin')
@section('content')
    <div class="">
        <div class="row">

            <div class="col-lg-12">
                {!! $breadcrumbs !!}

                <div class="page-header">
                    <h1>
                        {{ $slider->name }}
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Edit item
                        </small>
                    </h1>
                </div>

                @if (session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Success!
                        </strong>
                        {{ session('success') }}
                        <br>
                    </div>
                @endif

            </div>

            <div class="">
                <div class="">
                    <form class="form-horizontal" action="{{ route('admin.slides.update',$slide->id)}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field()}}
                            @method('PATCH')

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1"  placeholder="Name" value="{{$slide->title}}" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                            </div>


                            <div class="form-group @error('caption') has-error @enderror">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Caption </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                            <input type="text" id="form-field-1"  placeholder="Name" value="{{$slide->caption}}" name="caption" class="col-xs-10 col-sm-5" />
                                    </div>
                                    @error('caption')
                                        <div  class="help-block">{{ $message }}</div>
                                    @enderror

                                </div>
                            </div>

                            <div class="form-group @error('file_path') has-error @enderror">
                                <label class="col-xs-3 col-sm-3 control-label no-padding-right" for="form-field-1">Upload File Video or Image

                                </label>

                                <div class="col-xs-4 col-sm-4">
                                    <div class="clearfix">
                                        <input  type="file" name="file_path"  class="id-input-file-3" />
                                    </div>
                                    @error('file_path')
                                        <div  class="help-block">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-sm-4">
                                    <img style="width:100px;height:100px" src="{{asset($slide->file_path)}}" alt="">
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i> Submit
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
