@extends('layouts.admin')
@section('content')
    @php $bread = isset($_GET['ref'])? [$_GET['ref'],$data['pageTitle']] : $data['pageTitle']  @endphp
    {!!  _breadcrumbs($bread)  !!}

    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        Categories
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All Categories
                        </small>
                    </h1>


                </div>

                @if (session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Success!
                        </strong>
                        {{ session('success') }}
                        <br>
                    </div>
                @endif


                <div class="all-contacts">
                    <table  id="dynamic-table" class="table  table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#SN</th>
                            <th>Type</th>
                            <th>Label</th>
                            <th>Info</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody class="">

                        @php $count = 0 @endphp

                        @if(isset($customReferenceCategories))
                            @foreach ($customReferenceCategories as $key => $category)
                                <tr class="items" data-item_id="{{ $key }}">
                                    <td>{{ ++$count }}</td>
                                    <td>{{ $category->type }}</td>
                                    <td>{{ $category->label }}</td>
                                    <td>{{ $category->info }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs btn-group" style="display:flex">

                                            <a class="btn btn-xs btn-success" href="{{url('admin/custom-reference-category/'. $category->id .'/edit' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))}}" >
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

<script type="text/javascript">


</script>



@endsection
