@extends('layouts.admin')
@section('content')
    @php $bread = isset($_GET['ref'])? [$_GET['ref'],$data['pageTitle']] : $data['pageTitle']  @endphp
    {!!  _breadcrumbs($bread)  !!}

    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        Custom Reference

                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Update Custom Reference Category
                        </small>
                    </h1>
                </div>

                <div class="form">
                <form id="validation-form" class="form-horizontal" action="{{route('admin.custom-reference-attribute.update',$custom_reference_category->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="slug" value="{{$custom_reference_category->slug}}">
                        <div class="form-group @if($errors->has('info')) has-error @endif">
                            <label class="col-sm-3 control-label no-padding-right" for="section_id">Information</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text"  placeholder="Provide a info" name="info" value="{{ $custom_reference_category->info }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @if($errors->has('info'))
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('label')) has-error @endif">
                            <label class="col-sm-3 control-label no-padding-right" for="section_id">Label</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text"  placeholder="Provide a Label" name="label" value="{{ $custom_reference_category->label }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @if($errors->has('label'))
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('type')) has-error @endif">
                            <label class="col-sm-3 control-label no-padding-right" for="section_id">Type</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" disabled  placeholder="Provide a Label" name="label" value="{{ $custom_reference_category->type }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @if($errors->has('type'))
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('description')) has-error @endif">
                            <label class="col-sm-3 control-label no-padding-right" for="section_id">Description</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <textarea name="description"  placeholder="Provide a description"  class="" cols="60"
                                        rows="10">{{ $custom_reference_category->description}}</textarea>
                                </div>
                                @if($errors->has('description'))
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="space-4"></div>

                            <div class="clearfix">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="button" data-form_id="validation-form">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Update
                                    </button>
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script type="text/javascript">



    $(function(){

        $('button').on('click',function (e) {
            let formId = $(this).data('form_id');

            console.log(formId);

            if (formId !== '' && $('form#' + formId).valid()){

                $.each($(".wysiwyg"), function(key, el){

                    var id = $(el).data('key');

                    // set newly data to hidden textarea
                    $("#" + id).html($(el).html())
                });


                $('form#' + formId).submit();

                var fd = new FormData();
                fd.append( 'image', $('form#' + formId + ' input[type="file"]')[0].files[0] );

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('#' + formId +' input[name="_token"]').val()
                    }
                });

            }

            e.stopPropagation();
        });




    });
</script>



@endsection
