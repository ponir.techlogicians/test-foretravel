@extends('layouts.admin')
@section('content')
    @php $bread = isset($_GET['ref'])? [$_GET['ref'],$data['pageSlug'],'Create'] : [$data['pageSlug'],'Create']  @endphp
    {!!  _breadcrumbs($bread)  !!}
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        {{ $data['pageTitle'] }}
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Create {{ $data['pageTitle'] }}
                        </small>
                    </h1>
                </div>

                <div class="form">
                    <form id="validation-form" class="form-horizontal" action="{{ url('admin/'. $data['pageSlug'] .'/store') }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <input type="hidden" name="custom_post_slug" value="{{$data['pageSlug']}}">

                        <div class="form-group @if($errors->has('section_id')) has-error @endif">
{{--                            <label class="col-sm-3 control-label no-padding-right" for="section">{{ $data['pageSlug'] == 'service-center-locations'? 'Location' : 'Page Section' }}</label>--}}
                            <label class="col-sm-3 control-label no-padding-right" for="section">Page Section</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select name="section_id" id="section_id" class="col-xs-10 col-sm-5">
                                        @foreach($sections as $section)
                                            <option value="{{ $section->id }}"> {{ $section->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('section_id'))
                                    <div id="name-error" class="help-block">{{ $message }}</div>
                                @endif
                            </div>
                        </div>

                        @foreach($data['items'] as $item)
                            @if($item->type == 'text')
                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}"> {{ $item->label }} </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="{{ $item->name }}" placeholder="Provide a {{ $item->label }}" name="{{ $item->name }}" value="{{ old($item->name) }}" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @if($errors->has($item->name))
                                        <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                        @endif
                                    </div>
                                </div>

                            @elseif($item->type=='date')
                            <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}"> {{ $item->label }} </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="{{ $item->name }}"  placeholder="Provide a {{ $item->label }}" name="{{ $item->name }}" class="datepicker col-xs-10 col-sm-5" autocomplete="off"/>
                                    </div>
                                    @if($errors->has($item->name))
                                    <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                    @endif
                                </div>
                            </div>
                            @elseif($item->type == 'longtext')
                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}"> {{ $item->label }} </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <textarea name="{{ $item->name }}" id="{{ $item->name }}" placeholder="Provide a {{ $item->label }}" data-key="{{ $item->name }}" class="" cols="60"
                                                      rows="10">{{ old($item->name) }}</textarea>
                                        </div>
                                        @if($errors->has($item->name))
                                        <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                        @endif
                                    </div>
                                </div>
                            @elseif($item->type == 'longtext-rich')
                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}"> {{ $item->label }} </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            {{--<div class="wysiwyg" id="editor1" data-key="{{ $item->name }}"></div>--}}

                                            {{--<textarea name="{{ $item->name }}" id="{{ $item->name }}" class="hiddenEditor" style="visibility: hidden;"></textarea>--}}
                                            {{----}}
                                            <textarea name="{{ $item->name }}" id="{{ $item->name }}" class="hiddenEditor full-tiny"></textarea>

                                        </div>
                                        @if($errors->has($item->name))
                                        <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                        @endif
                                    </div>
                                </div>
                            @elseif($item->type == 'image')
                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-xs-3 control-label no-padding-right" for="{{ $item->name }}">{{ $item->label }}</label>

                                    <div class="col-xs-4">
                                        <div class="clearfix">
                                            <input  type="file" name="{{ $item->name }}" data-type="image" data-key="{{ $item->name }}" id="{{ $item->name }}"  class="id-input-file-3" />
                                        </div>
                                        @if($errors->has($item->name))
                                        <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                        @endif
                                    </div>

                                </div>
                            @elseif($item->type == 'select')
                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}">{{ $item->label }}</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select name="{{ $item->name  }}" id="{{ $item->name  }}" data-depending-by="{{ $item->depends_on_me }}" {{ $item->reference_type == ''? 'disabled' : 'data-table-referenced-by=floor_plans data-table-foreign-key=model'}}  class="col-xs-10 col-sm-5">
                                                <option value="">Select {{ $item->label }}</option>
                                                @if(count($item->categories))
                                                    @foreach($item->categories as $category)
                                                        <option {{ old($item->name) == $category->id? 'selected' : ''}} value="{{ $category->id }}"> {{ $category->label }} </option>
                                                    @endforeach
                                                @endif

                                            </select>
                                        </div>
                                        @if($errors->has($item->name))
                                            <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                        @endif
                                    </div>
                                </div>
                            @elseif($item->type == 'switch')

                                <div class="form-group @if($errors->has($item->name)) has-error @endif">
                                    <label class="col-sm-3 control-label no-padding-right" for="{{ $item->name }}">{{ $item->label }}</label>

                                    <div class="col-sm-5">
                                        <div class="clearfix">
                                            <div class="tabbable tabs-right">
                                                <ul class="nav nav-tabs" id="myTab3">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#home3" aria-expanded="true">
                                                            <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                                            Youtube
                                                        </a>
                                                    </li>

                                                    <li class="">
                                                        <a data-toggle="tab" href="#profile3" aria-expanded="false">
                                                            <i class="blue ace-icon fa fa-user bigger-110"></i>
                                                            Manual
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">

                                                    <div id="home3" class="tab-pane active">
                                                        <div class="col-sm-12">
                                                            <div class="clearfix">
                                                                <input type="text" id="{{ $item->name }}" placeholder="Provide a {{ $item->label }}" name="{{ $item->name }}" value="{{ old($item->name) }}" class="col-xs-12 col-sm-12" />
                                                            </div>
                                                            @if($errors->has($item->name))
                                                                <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div id="profile3" class="tab-pane">
                                                        <div class="">
                                                            <div class="clearfix">
                                                                <input  type="file" name="{{ $item->name }}" data-type="image" data-key="{{ $item->name }}" id="{{ $item->name }}"  class="id-input-file-3" />
                                                            </div>
                                                            @if($errors->has($item->name))
                                                                <div id="name-error" class="help-block">{{ $errors->first($item->name) }}</div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @else

                            @endif
                        @endforeach

                            <div class="space-4"></div>

                            <div class="clearfix">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="button" data-form_id="validation-form">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Create
                                    </button>
                                </div>
                            </div>
                    </form>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script type="text/javascript">

    $(function(){

        $('button').on('click',function (e) {
            let formId = $(this).data('form_id');

            console.log(formId);

            if (formId !== '' && $('form#' + formId).valid()){

                $.each($(".wysiwyg"), function(key, el){

                    var id = $(el).data('key');

                    // set newly data to hidden textarea
                    $("#" + id).html($(el).html())
                });


                $('form#' + formId).submit();

                var fd = new FormData();
                fd.append( 'image', $('form#' + formId + ' input[type="file"]')[0].files[0] );

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('#' + formId +' input[name="_token"]').val()
                    }
                });

            }

            e.stopPropagation();
        })



    });
</script>

@endsection

