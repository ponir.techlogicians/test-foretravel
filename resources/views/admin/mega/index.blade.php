@extends('layouts.admin')
@section('content')
    @php $bread = isset($_GET['ref'])? [$_GET['ref'],$data['pageTitle']] : $data['pageTitle']  @endphp
    {!!  _breadcrumbs($bread)  !!}
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        {{ $data['pageTitle'] }}
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All {{ $data['pageTitle'] }}
                        </small>
                    </h1>

                    <a href="javascript:;" onclick="updateSortOrder()" data-model="{{ $data['pageSlug'] }}" id="sortButton" style="display: none;position: absolute;top: 56px;right: 397px;">
                        <span class="btn btn-warning">Save Order</span>
                    </a>

                    <a href="javascript:;" onclick="cancelSortOrder()" id="cancelSortButton" style="display: none;position: absolute;top: 56px;right: 320px;">
                        <span class="btn btn-danger">Clear</span>
                    </a>

                    <a href="{{ url('admin/'. $data['pageSlug'] .'/create' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '')) }}">
                        <span class="btn btn-success create-button">Create</span>
                    </a>

                </div>

                @if (session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Success!
                        </strong>
                        {{ session('success') }}
                        <br>
                    </div>
                @endif

                @if(isset($data['filters']) && count($data['filters']))
                    <form action="">
                        <select name="filterBy" onchange='this.form.submit()' class="chosen-select form-control filter-select" id="form-field-select-3" data-placeholder="Filter" style="width: 200px;float: right;position: absolute;top: 59px;right: 110px;">
                            <option value="">Select to Filter</option>
                            @foreach($data['filters'] as $index => $filter)
                                <option {{ Request::get('filterBy') == $filter? 'selected' : ''}} value="{{ $filter }}"> {{ ucfirst($index) }}</option>
                            @endforeach
                        </select>

                        <input type="hidden" name="model" value="{{ Request::get('model') }}">
                        <input type="hidden" name="ref" value="{{ Request::get('ref') }}">
                    </form>
                @endif



                <div class="all-contacts">
                    <table  id="dynamic-table" class="table  table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#SN</th>
                            <th>Section</th>
                            @foreach ($data['columns'] as $column)
                                <th>{{ $column }}</th>
                            @endforeach
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody class="{{ in_array($data['pageSlug'], ['chapters','interior-color-options','warranty-informations','model-brochure','magazines','news','events','fabrication-gallery','fabrication-videos','virtual-tours','service-specials'])? 'sortable' : '' }}">
                        @php $count = 0 @endphp
                        @if(isset($data['items']))
                            @foreach ($data['items'] as $key => $iteration)
                            <tr class="items" data-item_id="{{ $key }}">
                                <td>{{ ++$count }}</td>
                                @foreach($iteration as $item)
                                    @if($item)
                                        @if($item->type == 'image')
                                            <td>
                                                @if(!empty($item->value))
                                                    @if(_getOriginalExtension($item->value) == 'pdf')
                                                        <a href="{{ url($item->value) }}" class="" target="_blank">
                                                            <img width="30" height="30" alt="30x30" src="{{ asset('img/icon/pdf.png') }}">
                                                        </a>
                                                    @else
                                                        <img style="width:60px;height:60px" src="{{ asset($item->value) }}" alt="">
                                                    @endif
                                                @else
                                                <img style="width:60px;height:60px" src="{{ asset('img/no-image-available.jpg') }}" alt="">
                                                @endif
                                            </td>
                                        @elseif($item->type == 'longtext')
                                            <td>{!! substr($item->value,0,200) . ((strlen($item->value) > 200)? '...' : '')  !!}</td>
                                        @elseif($item->type == 'select')
                                            <td>{{ $item->value }}</td>
                                        @elseif($item->type == 'date')
                                            <td>{{ date("F j, Y", strtotime($item->value)) }}</td>
                                        @else
                                            <td>{{ $item->value }}</td>
                                        @endif
                                    @else
                                            <td>&nbsp;</td>
                                    @endif
                                @endforeach
                                <td>
                                    <div class="hidden-sm hidden-xs btn-group" style="display:flex">

                                        <a class="btn btn-xs btn-success" href="{{url('admin/'. $data['pageSlug'] .'/'. $key .'/edit' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))}}" >
                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                        </a>

                                        <form method="post" class="delete_form" action="{{url('admin/'. $data['pageSlug'] .'/'. $key .'/destroy' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : ''))}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                                <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
