@extends('layouts.admin')
@section('content')

{!!  _breadcrumbs(['Custom-reference-categories','Edit'])  !!}


<div class="page-header">
    <h1>
        Custom Reference Category
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Edit
        </small>
    </h1>
</div><!-- /.page-header -->

<div class="form">
    <form id="validation-form" class="form-horizontal" action="{{ route('admin.custom-reference-categories.update',$custom_reference_category->id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PATCH')
            
           
            <div class="form-group  @error('label') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="label"> Label </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text" value="{{$custom_reference_category->lebel}}" id="label" placeholder="Title" name="label" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('label')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>
            <div class="form-group  @error('type') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="label"> Type </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text" value="{{$custom_reference_category->type}}" id="label" placeholder="type" name="type" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('type')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>
            <div class="form-group  @error('info') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="info"> Info </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text" value="{{$custom_reference_category->info}}" id="info" placeholder="Title" name="info" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('info')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>
            <div class="form-group @error('description') has-error   @enderror ">
                <label class="col-sm-3 control-label no-padding-right" for="description"> Description </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                        <textarea name="description" id="description" placeholder="Provide a Description" data-key="description" class="" cols="60" rows="10" style="margin: 0px; height: 241px; width: 448px;">{{$custom_reference_category->description}} </textarea>
                    </div>
                    @error('description')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>

            <div class="clearfix">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>
                </div>
            </div>
    </form>
    </div>
    
@endsection