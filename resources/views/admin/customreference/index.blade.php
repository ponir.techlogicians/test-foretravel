@extends('layouts.admin')
@section('content')
{!! _breadcrumbs(['custom-reference-category']) !!}
<div class="page-header">
    <h1>
        Custom Reference Category
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Page data
        </small>
    </h1>
    <a href="{{route('admin.custom-reference-categories.create')}}">
        <span class="btn btn-success create-button">Create</span>
    </a>
</div><!-- /.page-header -->

<div class="all-contacts">
    <table  id="dynamic-table" class="table  table-bordered table-hover">
            <thead>
                    <tr>
                        <th>Label</th>
                        <th>Type</th>
                        <th>Info</th>
                        <th>Description</th>
                        
                        <th>Action</th>
                    </tr>
            </thead>

            <tbody>
               @foreach ($custom_references as $custom_reference)
                    <tr>
                        <td>{{$custom_reference->label}}</td>
                        <td>{{$custom_reference->type}}</td>
                        <td>{{$custom_reference->info}}</td>
                        <td>{{$custom_reference->description}}</td>
                        <td>
                            <div class="hidden-sm hidden-xs btn-group" style="display:flex">

                            <a class="btn btn-xs btn-success" href="{{route('admin.custom-reference-categories.edit',$custom_reference->id)}}" >
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>

                            <form method="post" class="delete_form" action="{{route('admin.custom-reference-categories.destroy',$custom_reference->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </button>
                                </form>
                                    </div>
                        </td>
                   
                        
                    </tr>
               @endforeach

            </tbody>
    </table>
</div>
@endsection