@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Members')  !!}
<div class="">
    <div class="row">
        <div class="col-12">

            <div class="page-header">
                    <h1>
                        Members Payment History
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All Payments
                        </small>
                    </h1>
            </div>

            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif


            <table id="dynamic-table" class="table table-striped table-bordered table-hover dynamic-table">
                <thead>
                    <tr>
                        <th>Event Name</th>
                        <th>Member</th>
                        <th>Amount ($)</th>
                        <th>Paid At</th>
                        {{--<th>Action</th>--}}
                    </tr>
                </thead>

                <tbody>

                    @foreach($payments as $payment)

                    <tr>

                        <td>
                            <a href="{{route('admin.members.edit', $payment->user_id)}}">
                                {{ $payment->description }}
                            </a>
                        </td>
                        <td>{{ $payment->user->name }}</td>
                        <td>{{ $payment->amount }}</td>
                        <td>{{ date("d. M. Y H:i", strtotime($payment->created_at)) }}</td>
                        {{--<td>--}}
                            {{--<div class="hidden-sm hidden-xs btn-group" style="display:flex">--}}

                                {{--<form method="post" class="delete_form" action="{{route('admin.members.destroy', $user->id)}}">--}}
                                    {{--{{csrf_field()}}--}}
                                    {{--<input type="hidden" name="_method" value="DELETE" />--}}
                                    {{--<button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">--}}
                                        {{--<span class="red">--}}
                                            {{--<i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>--}}
                                        {{--</span>--}}
                                    {{--</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</td>--}}
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</div>


@endsection
