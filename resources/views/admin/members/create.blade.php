@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['Members','Create'])  !!}
<div class="">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="page-header">
                <h1>
                    Members
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Create New User
                    </small>
                </h1>
            </div>

            {{--@if ($errors->any())--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}

            <div class="form">
                <form class="form-horizontal" method="POST" action="{{ route('admin.members.store') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="form-group @error('first_name') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">First Name </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="first_name" placeholder="Type First Name" name="first_name" value="{{ old('first_name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('first_name')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('last_name') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Last Name </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="last_name" placeholder="Type Last Name" name="last_name" value="{{ old('last_name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('last_name')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('spouse_name') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Spouse Name </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="spouse_name" placeholder="Type Spouse Name" name="spouse_name" value="{{ old('spouse_name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('spouse_name')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group @error('email') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="email" id="email" placeholder="Type Email" name="email" value="{{ old('email') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('email')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group @error('address_line_1') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address Line 1 </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="address_line_1" placeholder="Type Address Line 1" name="address_line_1" value="{{ old('address_line_1') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('address_line_1')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('address_line_2') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address Line 2 </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="address_line_2" placeholder="Type Address Line 2" name="address_line_2" value="{{ old('address_line_2') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('address_line_2')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('city') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> City </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="city" placeholder="Type City" name="city" value="{{ old('city') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('city')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('state') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> State </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="state" placeholder="Type State" name="state" value="{{ old('state') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('state')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('zip') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Zip </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="zip" placeholder="Type Zip" name="zip" value="{{ old('zip') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('zip')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('phone_number') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone Number </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="phone_number" placeholder="Type Phone Number" name="phone_number" value="{{ old('phone_number') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('phone_number')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('cell_number') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cell Number </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="cell_number" placeholder="Type Cell Number" name="cell_number" value="{{ old('cell_number') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('cell_number')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('birthday') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Birthday </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="date" id="birthday" placeholder="" name="birthday" value="{{ old('birthday') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('birthday')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('spouse_birthday') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Spouse Birthday </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="date" id="spouse_birthday" placeholder="" name="spouse_birthday" value="{{ old('spouse_birthday') }}" class=" col-xs-10 col-sm-5" />
                            </div>
                            @error('spouse_birthday')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group @error('anniversary') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Anniversary </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="date" id="anniversary" placeholder="" name="anniversary" value="{{ old('anniversary') }}" class=" col-xs-10 col-sm-5" />
                            </div>
                            @error('anniversary')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('coach_type') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Coach Type </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="coach_type" placeholder="Type Name" name="coach_type" value="{{ old('coach_type') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('coach_type')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('purchase_date') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Purchase Date </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="date" id="purchase_date" placeholder="Type Name" name="purchase_date" value="{{ old('purchase_date') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('purchase_date')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('purchase_form') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Purchase From </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="purchase_form" placeholder="" name="purchase_form" value="{{ old('purchase_form') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('purchase_form')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('year_model') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Year Model </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="year_model" placeholder="Type Year Model" name="year_model" value="{{ old('year_model') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('year_model')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('last_six_number_vin') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Six Number of VIN </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="last_six_number_vin" placeholder="Type Last Six Number of VIN" name="last_six_number_vin" value="{{ old('last_six_number_vin') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('last_six_number_vin')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group @error('complete_model_number') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Complete Model Number * (located on the sidewall I.D. plate next to the drivers chair) </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="complete_model_number" placeholder="Type Complete Model Number" name="complete_model_number" value="{{ old('complete_model_number') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('complete_model_number')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group @error('fmca_member') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> FMCA Member </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <div class="btn-toolbar inline middle no-margin">
                                    <div data-toggle="buttons" class="btn-group no-margin">
                                        <label class="btn btn-sm btn-yellow active">
                                            <span class="bigger-110">Yes</span>
                                            <input type="radio" value="1" name="fmca_member" id="fmca_member" />
                                        </label>

                                        <label class="btn btn-sm btn-yellow">
                                            <span class="bigger-110">No</span>
                                            <input type="radio" value="0" name="fmca_member" id="fmca_member" />
                                        </label>

                                    </div>
                                </div>

                            </div>
                            @error('fmca_member')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group @error('fmca_member') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> FMCA Number</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="fmca_number" placeholder="Type FMCA Number" name="fmca_number" value="{{ old('fmca_number') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('fmca_number')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group @error('club_number') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Club Number</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="club_number" placeholder="Type Club Number" name="club_number" value="{{ old('club_number') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            @error('club_number')
                            <div  class="help-block">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>


                    {{--<input type="hidden" name="role" value="motorcade-member">--}}

                    <div class="clearfix">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@endsection
