@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Members')  !!}
<div class="">
    <div class="row">
        <div class="col-12">

            <div class="page-header">
                    <h1>
                        Members
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All Members
                        </small>
                    </h1>
            </div>

            <a href="{{ url('admin/members/create' . (isset($_GET['ref'])? ('?ref=' . $_GET['ref']) : '')) }}">
                <span class="btn btn-success create-button">Create</span>
            </a>

            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif


            <table id="dynamic-table" class="table table-striped table-bordered table-hover dynamic-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Coach Type</th>
                        <th>Year Model</th>
                        {{--<th>Last Six Number of VIN</th>--}}
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($users as $user)

                    <tr>

                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone_number }}</td>
                        <td>{{ $user->coach_type }}</td>
                        <td>{{ $user->year_model }}</td>
                        <td>
                            <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                <a class="btn btn-xs btn-success" href="{{route('admin.members.edit', $user->id)}}">
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>

                                <form method="post" class="delete_form" action="{{route('admin.members.destroy', $user->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                        <span class="red">
                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                        </span>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</div>


@endsection
