<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Members Directory</title>
</head>
<body>
{{--<h1>Members Directory</h1>--}}

<table width="100%" style="width:100%;border-spacing: 0;font-size: 10px" border="0">
    <tr style="background-color: #7a9700">
        <th>Club #</th>
        <th>Name</th>
        <th>Address</th>
        <th>City</th>
        <th>St</th>
        <th>Zip</th>
        <th>Phone</th>
        <th>Email</th>
    </tr>


    @foreach($members as $member)
        <tr style="background-color: #9affa3">
            <td>{{ $member->club_number }}</td>
            <td>{{ $member->name }}</td>
            <td>{{ $member->address_line_1 }}</td>
            <td>{{ $member->city }}</td>
            <td>{{ $member->state }}</td>
            <td>{{ $member->zip }}</td>
            <td>{{ $member->phone_number }}</td>
            <td>{{ $member->email }}</td>
        </tr>
    @endforeach

</table>
</body>
</html>
