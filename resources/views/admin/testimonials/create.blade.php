@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['About', 'Testimonials','Create'])  !!}
    <div class="">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="page-header">
                    <h1>
                        Testimonial
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Create Testimonials
                        </small>
                    </h1>
            </div>
            <div class="form">
                <form class="form-horizontal" method="POST" action="{{ route('admin.testimonials.store') }}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <div class="form-group @error('name') has-error @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="form-field-1" placeholder="Name" name="name" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('name')
                                            <div  class="help-block">{{ $message }}</div>
                                        @enderror
                                    </div>


                            </div>
                            <div class="form-group @error('designation') has-error @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Designation </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="form-field-1" placeholder="Name" name="designation" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('designation')
                                            <div  class="help-block">{{ $message }}</div>
                                        @enderror
                                    </div>


                            </div>
                            <div class="form-group @error('company') has-error @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Company </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="form-field-1" placeholder="Name" name="company" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('company')
                                            <div  class="help-block">{{ $message }}</div>
                                        @enderror
                                    </div>


                            </div>
                            <div class="form-group @error('photo') has-error @enderror">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Upload  Image</label>
                                    <div class="col-sm-4">
                                    <div class="clearfix">
                                        <input class="col-xs-10 col-sm-5 id-input-file-3"  type="file" name="photo"/>
                                    </div>
                                    @error('photo')
                                        <div  class="help-block">{{ $message }}</div>
                                    @enderror

                                    </div>
                                </div>
                                <div class="form-group @error('description') has-error @enderror">
                                        <label class="col-sm-3 control-label no-padding-right" for="message">Content</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                            <textarea class="col-xs-10  col-sm-5" rows="5" id="form-field-9" placeholder="Description"  name="description"></textarea>
                                            </div>
                                            @error('description')
                                                <div  class="help-block">{{ $message }}</div>
                                            @enderror

                                        </div>

                                </div>
                                <div class="form-group @error('background_image') has-error @enderror">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Upload Background Image</label>
                                        <div class="col-sm-4">
                                        <div class="clearfix">
                                            <input class="col-xs-10 col-sm-5 id-input-file-3"  type="file" name="background_image"/>
                                        </div>
                                        @error('background_image')
                                            <div  class="help-block">{{ $message }}</div>
                                        @enderror

                                        </div>
                                    </div>


                                    <div class="clearfix">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button class="btn btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Submit
                                                </button>
                                            </div>
                                    </div>


                    </form>
            </div>

        </div>
    </div>
</div>

@endsection
