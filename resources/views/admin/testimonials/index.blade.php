@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['About', 'Testimonials'])  !!}
    <div class="">
    <div class="row">
        <div class="col-12">


                <div class="page-header">
                    <h1>
                        Testimonial
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            All Testimonials
                        </small>
                    </h1>

                    <a href="{{ url('admin/testimonials/create') }}">
                        <span class="btn btn-success create-button">Create</span>
                    </a>

                </div>

                <table id="dynamic-table" class="table  table-bordered table-hover">
                        <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Company</th>
                                    <th>Photo</th>
                                    <th>Background Image</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                        </thead>

                        <tbody>
                            @foreach ($testimonials as $testimonial)
                            <tr>
                            <td>{{$testimonial->name}}</td>
                            <td>{{$testimonial->designation}}</td>
                            <td>{{$testimonial->company}}</td>
                            <td><img style="height: 60px;width:60px" src="{{asset($testimonial->photo)}}" alt=""></td>
                            <td><img style="height: 60px;width:60px" src="{{asset($testimonial->background_image)}}" alt=""></td>
                            <td>{{$testimonial->createdBy->name}}</td>
                            <td>
                                    <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                            {{-- <button class="btn btn-xs btn-success" class="tooltip-info" data-rel="tooltip" title="View"> --}}
                                                <a class="btn btn-xs btn-success" href="{{route('admin.testimonials.edit', $testimonial->id)}}">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </a>
                                            {{-- </button> --}}

                                            <form method="post" class="delete_form" action="{{route('admin.testimonials.destroy', $testimonial->id)}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                                        <span class="red">
                                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </span>
                                                </button>
                                            </form>
                                    </div>
                            </td>


                        </tr>
                            @endforeach

                        </tbody>
                </table>
        </div>
    </div>

</div>


@endsection
