@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Settings')  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
                <div class="page-header">
                        <h1>
                            Settings
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                All Settings
                            </small>
                        </h1>
                    </div>



            <div class="form">
            <form  method="POST"  class="form-horizontal" id="setting-form-submit" action="{{route('admin.settings.store')}}"  enctype="multipart/form-data">
                    {{ csrf_field()}}

                    @foreach ($settings as $setting)
                    @if (strtolower($setting->type)=='text')
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{$setting->label}} </label>

                    <div class="col-sm-9">
                        <input type="{{$setting->type}}"  value="{{$setting->value}}" id="form-field-1" placeholder="Name" name="{{$setting->key}}" class="col-xs-10 col-sm-5" />
                        {{-- <input  value="{{$setting->id}}"  name="setting_id[]" class="col-xs-10 col-sm-5" /> --}}

                    </div>
                    </div>
                    @elseif(strtolower($setting->type)=='image')
                        {{-- input file template for uploading file  --}}
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label no-padding-right" for="form-field-1"> {{$setting->label}} </label>
                                        <div class="col-xs-4">
                                        <input  type="file" name="{{$setting->key}}"  class="id-input-file-3" />
                                        {{-- <input  value="{{$setting->id}}"  name="setting_id[]" class="col-xs-10 col-sm-5" /> --}}
                                        </div>
                                        <div class="col-xs-2">
                                        <img style="width: 70px; height:70px" src="{{asset($setting->value)}}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                    @elseif(strtolower($setting->type)=='video')
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label no-padding-right" for="form-field-1"> {{$setting->label}} </label>
                                    <div class="col-xs-4">
                                    <input  type="file" name="{{$setting->key}}"  class="id-input-file-3" />
                                    {{-- <input  value="{{$setting->id}}"  name="setting_id[]" class="col-xs-10 col-sm-5" /> --}}
                                    </div>
                                    <div class="col-xs-2">
                                        <video style="width:200px;height:150px;" src="{{asset($setting->value)}}"></video>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endif

                    @endforeach

                    <div class="clearfix">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Submit
                            </button>
                        </div>
                </div>

                </form>

            </div>


        </div>
    </div>
</div>
<script>


</script>
@endsection
