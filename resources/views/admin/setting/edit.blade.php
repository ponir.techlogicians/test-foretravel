@extends('layouts.admin')
@section('content')
<div class="container background-color">
    <div class="row">
        <div class="col-12">
            <div class="page-header">
                <h1>
                    Settings
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit Settings
                    </small>
                </h1>
            </div>

            <form action="{{ route('admin.settings.update', $setting->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('PATCH')
                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Key </label>

                            <div class="col-sm-9">
                            <input type="text" value="{{$setting->key}}" id="form-field-1" placeholder="Name" name="key" class="col-xs-10 col-sm-5" />
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Value </label>

                            <div class="col-sm-9">
                            <input type="text" id="form-field-1-1" value="{{$setting->value}}" placeholder="Email" name="value" class="form-control" />
                            </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block">Update</button>
            </form>

        </div>
    </div>
</div>

@endsection

