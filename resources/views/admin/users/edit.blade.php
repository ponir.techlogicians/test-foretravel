@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['Users','Edit'])  !!}
<div class="background-color">
    <div class="row">
        <div class="col-12">
            <div class="page-header">
                    <h1>
                        User
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Edit User Info
                        </small>
                    </h1>
            </div>
            <div class="form">
                    <form class="form-horizontal" method="post" action="{{ route('admin.users.update',$user->id) }}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            @method('PATCH')
                        <div class="form-group @error('name') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" id="name" placeholder="Type Name" name="name" value="{{ old('name', $user->name) }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('name')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="form-group @error('email') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="email" id="email" placeholder="Type Email" name="email" value="{{ old('email', $user->email) }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('email')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>
                        <div class="form-group @error('password') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="password" id="password" placeholder="Type a password" name="password" value="{{ old('password') }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('password')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>
                        <div class="form-group @error('password_confirmation') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Confirm Password</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="password" id="password_confirmation" placeholder="Type again to confirm" name="password_confirmation" value="{{ old('password_confirmation') }}" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('password_confirmation')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        @if(Auth::user()->role == 'superadmin')
                        <div class="form-group @error('role') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Role</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select id="role" name="role" class="col-xs-10 col-sm-5" >

                                        <option value="admin" {{'admin' == $user->role || old('role') == 'admin'? 'selected' : '' }}>Admin</option>
                                        <option value="user" {{ 'user' == $user->role || old('role') == 'user'? 'selected' : '' }}>User</option>

                                    </select>

                                </div>

                                @error('role')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        @endif

                        <div class="clearfix">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Update
                                    </button>
                                </div>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>

@endsection

