@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs(['Virtual Tour','Update'])  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    Virtual Tour
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Update Virtual Tours
                    </small>
                </h1>
            </div>

            <div class="form">
                <form id="validation-form" class="form-horizontal" action="{{ route('admin.virtual-tour.update',$virtualtour->id)}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @method('PATCH')
                    <div class="form-group @error('model') has-error   @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="model"> Model </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <select id="model" name="model" class="col-xs-10 col-sm-5">
                                    @foreach($coachModels as $model)
                                        <option {{$virtualtour->model == $model->slug ? 'selected' : ''}} value="{{ $model->slug }}">{{ $model->title }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" value="{{$floorplan->model}}" id="model" placeholder="Model" name="model" class="col-xs-10 col-sm-5" />--}}
                            </div>
                            @error('model')
                            <div id="name-error" class="help-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                        <div class="form-group  @error('tour_url') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="tour_url"> Tour Url </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                <input type="text" id="tour_url" value="{{$virtualtour->tour_url}}" placeholder="Tour Url" name="tour_url" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('tour_url')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="space-4"></div>
                        <div class="form-group  @error('title') has-error   @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                <input type="text" id="title" value="{{$virtualtour->title}}" placeholder="Title" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('title')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group @error('thumb_image') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Thumb Image</label>
                            <div class="col-sm-4">
                            <div class="clearfix">
                                <input class="col-xs-10 col-sm-5 id-input-file-3"  type="file" name="thumb_image"/>
                            </div>
                            @error('thumb_image')
                                <div  class="help-block">{{ $message }}</div>
                            @enderror

                            </div>
                            <div class="col-sm-3">
                            @if($virtualtour->thumb_image != '')
                            <div class="col-xs-4">
                                <ul class="ace-thumbnails clearfix">
                                    <li>
                                        <a href="{{ asset($virtualtour->thumb_image) }}" data-rel="colorbox" class="cboxElement">
                                            <img width="150" height="150" alt="150x150" src="{{ asset($virtualtour->thumb_image) }}">
                                        </a>

                                        {{--<div class="tools tools-right">--}}
                                            {{--<a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/' . $content->key .'/delete') }}">--}}
                                                {{--<i class="ace-icon fa fa-times red"></i>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            </div>
                        </div>


                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Update
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

