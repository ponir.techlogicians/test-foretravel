@extends('layouts.admin')
@section('content')


    {!!  _breadcrumbs($breads)  !!}

    <div class="page-header">
        <h1>
            {{ makeLabelBySlug($pageTitle) }}
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Page data
            </small>
        </h1>
    </div><!-- /.page-header -->

    @if (session('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>

            <strong>
                <i class="ace-icon fa fa-check"></i>
                Success!
            </strong>
            {{ session('success') }}
            <br>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">

                <div class="col-sm-12">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs" id="myTab3">
                            @foreach($sections as $key => $section)
                                <li class="{{ $key == 0 ? 'active' : ''}}">
                                    <a data-toggle="tab" href="#{{$section. '_' . ($key + 1) }}" data-tab_key="{{$section. '_' . ($key + 1) }}">
                                        <i class="ace-icon fa fa-rocket"></i>
                                        {{ makeLabelBySlug($section) }}
                                    </a>
                                </li>
                            @endforeach

                        </ul>

                        <div class="tab-content">
                            @foreach($sections as $key => $section)
                                <div id="{{$section. '_' . ($key + 1)}}" class="tab-pane in {{ $key == 0 ? 'active' : ''}}">

                                    @if($section == 'main_slider')
                                        <a href="{{ url('admin/sliders/home-slider/edit') }}"><span class="btn btn-success crud-reference-button">All Slides</span></a>
                                    @elseif($section == 'coach_slider')
                                        <a href="{{ url('admin/sliders/luxury-coach/edit') }}"><span class="btn btn-success crud-reference-button">All Slides</span></a>
                                    @endif

                                    @if($section == 'floorplan')
                                        <a href="{{ url('admin/floorplans?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Floorplans</span></a>
                                    @elseif($section == 'floorplan_videos')
                                        <a href="{{ url('admin/floorplan-videos?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Floorplan Videos</span></a>
                                    @elseif($section == 'testimonials')
                                        <a href="{{ url('admin/testimonials') }}"><span class="btn btn-success crud-reference-button">All Testimonials</span></a>
                                    @elseif($section == 'color_options')
                                        <a href="{{ url('admin/sliders/color-options/edit?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Exterior Options</span></a>
                                    @elseif($section == 'interior_color_options')
                                        <a href="{{ url('admin/interior-color-options?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Interior Options</span></a>
                                    @elseif($section == 'key_features')
                                        <a href="{{ url('admin/sliders/key-features/edit?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Key Features</span></a>
                                    @elseif($section == 'virtual_tour')
                                        <a href="{{ url('admin/virtual-tours?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Virtual Tours</span></a>
                                    @elseif($section == 'model')
                                        <a href="{{ url('admin/models?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">All Models</span></a>
                                    @elseif($section == 'gallery')
                                        <a href="{{ url('admin/sliders/interior-gallery/edit?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">Interior Gallery Images</span></a>
                                        <a href="{{ url('admin/sliders/exterior-gallery/edit?model=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button" style="top: 70px;">Exterior Gallery Images</span></a>
                                    @elseif($section == 'fabrication_gallery')
                                        <a href="{{ url('admin/fabrication-gallery?fabrication=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">Fabrication Gallery</span></a>
                                    @elseif($section == 'fabrication_video')
                                        <a href="{{ url('admin/fabrication-videos?fabrication=' . Request::segment(4)) }}"><span class="btn btn-success crud-reference-button">Fabrication Videos</span></a>
                                    @elseif($section == 'our_team')
                                        <a href="{{ url('admin/service-department-members?ref=service-department') }}"><span class="btn btn-success crud-reference-button">All Members</span></a>
                                    @elseif($section == 'appointment')
                                        <a href="{{ url('admin/service-appointment-members?ref=service-department') }}"><span class="btn btn-success crud-reference-button">All Members</span></a>
                                    @elseif($section == 'parts_department')
                                        <a href="{{ url('admin/parts-department-members?ref=parts-department') }}"><span class="btn btn-success crud-reference-button">All Members</span></a>
                                    @elseif($section == 'service_specials')
                                        <a href="{{ url('admin/service-specials?ref=service-department') }}"><span class="btn btn-success crud-reference-button">All Specials</span></a>
                                    @elseif($section == 'warranty_information')
                                        <a href="{{ url('admin/warranty-informations?ref=information') }}"><span class="btn btn-success crud-reference-button">All Infos</span></a>
                                    @elseif($section == 'technical-support')
                                        <a href="{{ url('admin/technical-supports?ref=technical-support') }}"><span class="btn btn-success crud-reference-button">All Ques/Ans</span></a>
                                    @elseif($section == 'service_links')
                                        <a href="{{ url('admin/helpful-service-links?ref=information') }}"><span class="btn btn-success crud-reference-button">All Links</span></a>
                                    @elseif($section == 'service_locations')
                                        <a href="{{ url('admin/service-center-locations?ref=owner-service') }}"><span class="btn btn-success crud-reference-button">All Locations</span></a>
                                    @elseif($section == 'vendor')
                                        <a href="{{ url('admin/vendor-contact-infos?ref=information') }}"><span class="btn btn-success crud-reference-button">All Contacts</span></a>
                                    @elseif($section == 'model_brochure')
                                        <a href="{{ url('admin/model-brochure?ref=information') }}"><span class="btn btn-success crud-reference-button">All Brochures</span></a>
                                    @elseif($section == 'remodel_department')
                                        <a href="{{ url('admin/remodel-department-members?ref=remodel-department') }}"><span class="btn btn-success crud-reference-button">All Members</span></a>
                                    @elseif($section == 'recall-notice')
                                        <a href="{{ url('admin/notices?ref=notices') }}"><span class="btn btn-success crud-reference-button">All Notices</span></a>
                                    @elseif($section == 'chapter')
                                        <a href="{{ url('admin/chapters?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button">All Chapters</span></a>
                                        <a href="{{ url('admin/custom-reference-category?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button" style="top: 70px;">All Categories</span></a>
                                    @elseif($section == 'officers')
                                        <a href="{{ url('admin/officers?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button">All Officers</span></a>
                                    @elseif($section == 'magazine')
                                        <a href="{{ url('admin/magazines?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button">All Magazines</span></a>
                                    @elseif($section == 'news')
                                        <a href="{{ url('admin/news?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button">All News</span></a>
                                    @elseif($section == 'events')
                                        <a href="{{ url('admin/events?ref=motorcade-club') }}"><span class="btn btn-success crud-reference-button">All Events</span></a>
                                    @endif

                                    <form id="{{$section. '_' . ($key + 1)}}" action="{{ url('admin/page/update') }}" class="form-horizontal validation" method="post" role="form" enctype="multipart/form-data">

                                        @csrf
                                        @foreach($contents as $content)
                                            @if($content->section == $section)



                                                @if($content->type == 'image')
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label no-padding-right" for="form-field-1">{{ $content->label }}</label>

                                                        <div class="col-xs-4">
                                                            <input  type="file" name="{{ $content->key }}" data-type="image" data-key="{{ $content->key }}" id="{{ $content->key }}"  class="id-input-file-3" />
                                                        </div>
                                                        @if(_getOriginalExtension($content->value) == 'pdf')
                                                            <div class="col-xs-4">
                                                                <ul class="ace-thumbnails clearfix">
                                                                    <li>
                                                                        <a href="{{ url($content->value) }}" class="" target="_blank">
                                                                            <img width="150" height="150" alt="150x150" src="{{ asset('img/icon/pdf.png') }}">
                                                                        </a>

                                                                        <div class="tools tools-right">
                                                                            <a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/' .$pageName. '/'. $content->key. ($pageTab? ('/'.$pageTab) : "") .'/delete') }}">
                                                                                <i class="ace-icon fa fa-times red"></i>
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                        @elseif(_getOriginalExtension($content->value) != '')
                                                            <div class="col-xs-4">
                                                                <ul class="ace-thumbnails clearfix">
                                                                    <li>
                                                                        <a href="{{ url($content->value) }}" data-rel="colorbox" class="cboxElement">
                                                                            <img width="150" height="150" alt="150x150" src="{{ url($content->value) }}">
                                                                        </a>

                                                                        <div class="tools tools-right">
                                                                            <a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/'.$pageName. '/' . $content->key. ($pageTab? ('/'.$pageTab) : "") .'/delete') }}">
                                                                                <i class="ace-icon fa fa-times red"></i>
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        @endif
                                                    </div>

                                                @elseif($content->type == 'switch')
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label no-padding-right" for="form-field-1">{{ $content->label }}</label>
                                                    <div class="col-xs-4">
                                                        <input name = "" id="switch_on_off" @if($content->value == '1') checked  @else   @endif    class="ace ace-switch ace-switch-3" type="checkbox">
                                                        <span class="lbl"></span>
                                                        <input type="hidden" name="{{ $content->key }}" data-key="{{ $content->key }}" id="switch_value" type=""  value="{{ $content->value }}">
                                                    </div>

                                                </div>

                                                @elseif($content->type == 'video')
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label no-padding-right" for="form-field-1">{{ $content->label }}</label>

                                                        <div class="col-xs-4">
                                                            <input  type="file" name="{{ $content->key }}" data-type="image" data-key="{{ $content->key }}" id="{{ $content->key }}"  class="id-input-file-3" />
                                                        </div>
                                                        @if(_getOriginalExtension($content->value) != '')
                                                            <div class="col-xs-4">
                                                                <video width="100%" controls>
                                                                    <source src="{{ url($content->value) }}" type="video/mp4">
                                                                    Your browser does not support the video tag.
                                                                </video>
                                                                {{--<ul class="ace-thumbnails clearfix">--}}
                                                                {{--<li>--}}
                                                                {{--<a href="{{ url($content->value) }}" data-rel="colorbox" class="cboxElement">--}}
                                                                {{--<img width="150" height="150" alt="150x150" src="{{ url($content->value) }}">--}}
                                                                {{--</a>--}}

                                                                {{--<div class="tools tools-right">--}}
                                                                {{--<a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/' . $content->key .'/delete') }}">--}}
                                                                {{--<i class="ace-icon fa fa-times red"></i>--}}
                                                                {{--</a>--}}
                                                                {{--</div>--}}
                                                                {{--</li>--}}
                                                                {{--</ul>--}}
                                                            </div>
                                                        @endif
                                                    </div>
                                                @elseif($content->type == 'map')
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                            <input type="text" name="{{ $content->key }}" value="{{ $content->value }}" data-key="{{ $content->key }}" id="{{ $content->key }}" placeholder="" class="col-xs-10 col-sm-5" />
                                                        </div>
                                                    </div>
                                                @elseif($content->type == 'longtext')
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                                <textarea name="{{ $content->key }}" id="{{ $content->key }}" data-key="{{ $content->key }}" class="" cols="60"
                                                                          rows="10">{{ $content->value }}</textarea>
                                                        </div>
                                                    </div>
                                                @elseif($content->type == 'longtext-rich')
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">

                                                            {{--<div class="wysiwyg" id="editor1" data-key="{{ $content->key }}">{!! $content->value !!}</div>--}}

                                                            {{--<textarea name="{{ $content->key }}" id="{{ $content->key }}" class="hiddenEditor" style="visibility: hidden;">{{ $content->value }}</textarea>--}}
                                                            <textarea name="{{ $content->key }}" id="{{ $content->key }}" class="hiddenEditor full-tiny">{{ $content->value }}</textarea>

                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                            <input type="{{ $content->type }}" name="{{ $content->key }}" value="{{ $content->value }}" data-key="{{ $content->key }}" id="{{ $content->key }}" placeholder="" class="col-xs-10 col-sm-5" />
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif
                                        @endforeach

                                        <input type="hidden" name="section" value="{{$section}}">
                                        <input type="hidden" name="name" value="{{ $pageName }}">
                                        <input type="hidden" name="tab" value="{{ $pageTab }}">

                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button class="btn btn-info" type="button" data-form_id="{{$section. '_' . ($key + 1)}}">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Update
                                                </button>

                                            </div>
                                        </div>

                                    </form>

                                </div>
                            @endforeach

                            {{--<div id="profile3" class="tab-pane">--}}
                            {{--<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}

                            {{--<div id="dropdown13" class="tab-pane">--}}
                            {{--<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}

                            {{--<div id="dropdown14" class="tab-pane">--}}
                            {{--<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div><!-- /.col -->

            </div><!-- /.row -->

        </div>
    </div>


@endsection

@section('script')

    <script type="text/javascript">

        $(function(){

            $("input[type='checkbox']").on('change', function() {

                console.log($(this).parent().find('#switch_value'));
                if($(this).parent().find('#switch_on_off').prop('checked') == true){
                    $(this).parent().find('#switch_value').val("1");

                }else if($(this).parent().find('#switch_on_off').prop('checked') == false){
                    $(this).parent().find('#switch_value').val("0");
                }
            });
            $('#switch_value').val(" ");


            $('button').on('click',function (e) {
                let formId = $(this).data('form_id');

                console.log(formId);

                if (formId !== '' && $('form#' + formId).valid()){

                    $.each($(".wysiwyg"), function(key, el){

                        var id = $(el).data('key');

                        // set newly data to hidden textarea
                        $("#" + id).html($(el).html())
                    });


                    $('form#' + formId).submit();

                    // preparing the data set
                    // var formData = new FormData(); // Currently empty
                    // let images = [];
                    //
                    // let allData = $('form#' + formId).serializeArray();
                    // let allFiles = $('form#' + formId + ' input[type="file"]');
                    //
                    // $.each(allData, function (key,val) {
                    //
                    //     // yo.append(val.name,val.value)
                    //
                    // });
                    //
                    // $.each(allFiles, function (key,val) {
                    //
                    //     // formData.append(val.name,val.value)
                    //
                    //     let image = [];
                    //     image.push( val.name );
                    //     image.push( getBase64Image(val.name) );
                    //
                    //     console.log(image);
                    //
                    //     images.push(image.join('|'))
                    //
                    // });


                    // return false;

                    var fd = new FormData();
                    fd.append( 'image', $('form#' + formId + ' input[type="file"]')[0].files[0] );



                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('#' + formId +' input[name="_token"]').val()
                        }
                    });

                    // video on/off switch










                    {{--$.ajax({--}}
                    {{--url: "{{ url('admin/page/update') }}",--}}
                    {{--data: fd,--}}
                    {{--processData: false,--}}
                    {{--contentType: 'multipart/form-data',--}}
                    {{--type: 'PUT',--}}
                    {{--success: function(data){--}}
                    {{--alert(data);--}}
                    {{--}--}}
                    {{--});--}}

                    {{--$.ajax({--}}
                    {{--method: "PUT",--}}
                    {{--url: "{{ url('admin/page/update') }}",--}}
                    {{--data: {'data': allData, 'memes' : images}--}}
                    {{--// ,--}}
                    {{--// processData: false,--}}
                    {{--// contentType: false--}}
                    {{--})--}}
                    {{--.done(function( response ) {--}}

                    {{--response = $.parseJSON(response);--}}



                    // popupMessage(response.status? 'success': 'danger','Success', response.message);

                    {{--});--}}
                }

                e.stopPropagation();
            })




        });
    </script>

@endsection

