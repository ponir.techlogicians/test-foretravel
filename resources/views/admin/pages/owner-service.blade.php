@extends('layouts.admin')
@section('content')

    {!!  _breadcrumbs('Home')  !!}

    <div class="page-header">
        <h1>
            Owner Service
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Page data
            </small>
        </h1>
    </div><!-- /.page-header -->

    @if (session('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>

            <strong>
                <i class="ace-icon fa fa-check"></i>
                Success!
            </strong>
            {{ session('success') }}
            <br>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">

                <div class="col-sm-12">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs" id="myTab3">
                            @foreach($sections as $key => $section)
                                <li class="{{ $key == 0 ? 'active' : ''}}">
                                    <a data-toggle="tab" href="#{{$section. '_' . ($key + 1) }}" data-tab_key="{{$section. '_' . ($key + 1) }}">
                                        <i class="ace-icon fa fa-rocket"></i>
                                        {{ makeLabelBySlug($section) }}
                                    </a>
                                </li>
                            @endforeach

                        </ul>

                        <div class="tab-content">
                            @foreach($sections as $key => $section)


                                <div id="{{$section. '_' . ($key + 1)}}" class="tab-pane in {{ $key == 0 ? 'active' : ''}}">

                                    @if($section == 'main_slider')
                                        <a href="{{ url('admin/sliders/home-slider/edit') }}"><span class="btn btn-success crud-reference-button">All Slides</span></a>
                                    @elseif($section == 'coach_slider')
                                        <a href="{{ url('admin/sliders/luxury-coach/edit') }}"><span class="btn btn-success crud-reference-button">All Slides</span></a>
                                    @endif

                                    <form id="{{$section. '_' . ($key + 1)}}" action="{{ url('admin/page/update') }}" class="form-horizontal validation" method="post" role="form" enctype="multipart/form-data">

                                        @csrf
                                        @foreach($contents as $content)
                                            @if($content->section == $section)

                                                @if($content->type == 'image')
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label no-padding-right" for="form-field-1">{{ $content->label }}</label>

                                                        <div class="col-xs-4">
                                                            <input  type="file" name="{{ $content->key }}" data-type="image" data-key="{{ $content->key }}" id="{{ $content->key }}"  class="id-input-file-3" />
                                                        </div>
                                                        @if($content->value != '')
                                                            <div class="col-xs-4">
                                                                <ul class="ace-thumbnails clearfix">
                                                                    <li>
                                                                        <a href="{{ url($content->value) }}" data-rel="colorbox" class="cboxElement">
                                                                            <img width="150" height="150" alt="150x150" src="{{ url($content->value) }}">
                                                                        </a>

                                                                        <div class="tools tools-right">
                                                                            <a href="#" onclick="removeMedia(this)" data-key="{{ $content->key }}" data-action="{{ url('admin/page/content/' . $content->key .'/delete') }}">
                                                                                <i class="ace-icon fa fa-times red"></i>
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @elseif($content->type == 'map')
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                            <input type="text" name="{{ $content->key }}" value="{{ $content->value }}" data-key="{{ $content->key }}" id="{{ $content->key }}" placeholder="" class="col-xs-10 col-sm-5" />
                                                        </div>
                                                    </div>
                                                @elseif($content->type == 'longtext')
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                                <textarea name="{{ $content->key }}" id="{{ $content->key }}" data-key="{{ $content->key }}" class="" cols="60"
                                                                          rows="10">{{ $content->value }}</textarea>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ $content->label }} </label>

                                                        <div class="col-sm-9">
                                                            <input type="{{ $content->type }}" name="{{ $content->key }}" value="{{ $content->value }}" data-key="{{ $content->key }}" id="{{ $content->key }}" placeholder="" class="col-xs-10 col-sm-5" />
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif
                                        @endforeach

                                        <input type="hidden" name="section" value="{{$section}}">
                                        <input type="hidden" name="name" value="home">

                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button class="btn btn-info" type="button" data-form_id="{{$section. '_' . ($key + 1)}}">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Update
                                                </button>

                                            </div>
                                        </div>

                                    </form>

                                </div>
                            @endforeach

                            {{--<div id="profile3" class="tab-pane">--}}
                            {{--<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}

                            {{--<div id="dropdown13" class="tab-pane">--}}
                            {{--<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}

                            {{--<div id="dropdown14" class="tab-pane">--}}
                            {{--<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>--}}
                            {{--<p>Raw denim you probably haven't heard of them jean shorts Austin.</p>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div><!-- /.col -->

            </div><!-- /.row -->

        </div>
    </div>


@endsection

@section('script')

    <script type="text/javascript">

        $(function(){

            $('button').on('click',function (e) {
                let formId = $(this).data('form_id');

                console.log(formId);

                if (formId !== '' && $('form#' + formId).valid()){

                    $('form#' + formId).submit();

                    // preparing the data set
                    // var formData = new FormData(); // Currently empty
                    // let images = [];
                    //
                    // let allData = $('form#' + formId).serializeArray();
                    // let allFiles = $('form#' + formId + ' input[type="file"]');
                    //
                    // $.each(allData, function (key,val) {
                    //
                    //     // yo.append(val.name,val.value)
                    //
                    // });
                    //
                    // $.each(allFiles, function (key,val) {
                    //
                    //     // formData.append(val.name,val.value)
                    //
                    //     let image = [];
                    //     image.push( val.name );
                    //     image.push( getBase64Image(val.name) );
                    //
                    //     console.log(image);
                    //
                    //     images.push(image.join('|'))
                    //
                    // });


                    // return false;

                    var fd = new FormData();
                    fd.append( 'image', $('form#' + formId + ' input[type="file"]')[0].files[0] );

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('#' + formId +' input[name="_token"]').val()
                        }
                    });

                    {{--$.ajax({--}}
                    {{--url: "{{ url('admin/page/update') }}",--}}
                    {{--data: fd,--}}
                    {{--processData: false,--}}
                    {{--contentType: 'multipart/form-data',--}}
                    {{--type: 'PUT',--}}
                    {{--success: function(data){--}}
                    {{--alert(data);--}}
                    {{--}--}}
                    {{--});--}}

                    {{--$.ajax({--}}
                    {{--method: "PUT",--}}
                    {{--url: "{{ url('admin/page/update') }}",--}}
                    {{--data: {'data': allData, 'memes' : images}--}}
                    {{--// ,--}}
                    {{--// processData: false,--}}
                    {{--// contentType: false--}}
                    {{--})--}}
                    {{--.done(function( response ) {--}}

                    {{--response = $.parseJSON(response);--}}

                    {{--// alert( "Data Saved: " + response );--}}

                    // popupMessage(response.status? 'success': 'danger','Success', response.message);

                    {{--});--}}
                }

                e.stopPropagation();
            })



        });
    </script>

@endsection

