@extends('layouts.admin')
@section('content')
    {!!  _breadcrumbs('Model')  !!}
    <div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    Models
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        All Models
                    </small>
                </h1>
            </div>


            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif


            <div class="all-contacts">

                <table id="simple-table" class="table  table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($models as $model)
                        <tr>
                            <td><img src="{{ url($model->image)}}" alt="" style="width: 100px;height: 100px"></td>
                            <td>{{$model->title}}</td>
                            <td>
                                <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                    {{-- <button class="btn btn-xs btn-success" class="tooltip-info" data-rel="tooltip" title="View"> --}}
                                    <a class="btn btn-xs btn-success" href="{{ url('/admin/models/edit', $model->id) }}">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    </a>
                                    {{-- </button> --}}

                                    {{--<form method="post" class="delete_form" action="{{route('admin.models.destroy', $model->id)}}">--}}
                                        {{--{{csrf_field()}}--}}
                                        {{--<input type="hidden" name="_method" value="DELETE" />--}}
                                        {{--<button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">--}}
                                                    {{--<span class="red">--}}
                                                        {{--<i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>--}}
                                                    {{--</span>--}}
                                        {{--</button>--}}
                                    {{--</form>--}}
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>


        </div>
    </div>
</div>
<script>


</script>
@endsection
