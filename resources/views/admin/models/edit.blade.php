@extends('layouts.admin')
        @section('content')
            {!!  _breadcrumbs(['Model','Edit'])  !!}
        <div class="container background-color">
            <div class="row">
                <div class="col-12">
                    <div class="page-header">
                        <h1>
                            Models
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                Edit Model
                            </small>
                        </h1>
                    </div>

                    <form class="form-horizontal" action="{{ url('/admin/models/update', $model->id)}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field()}}

                        <div class="form-group @error('title') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>

                            <div class="col-sm-9">

                                <div class="clearfix">
                                    <input type="text" id="form-field-1"  placeholder="Name" value="{{ old('title', $model->title) }}" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                                @error('title')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group @error('image') has-error @enderror">
                            <label class="col-xs-3 col-sm-3 control-label no-padding-right" for="form-field-1">Upload Image

                            </label>

                            <div class="col-xs-4 col-sm-4">
                                <div class="clearfix">
                                    <input  type="file" name="image"  class="id-input-file-3" />
                                </div>
                                @error('image')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-sm-4">
                                <img style="width:100px;height:100px" src="{{asset($model->image)}}" alt="">
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @endsection

