@extends('layouts.admin')
@section('content')
{!!  _breadcrumbs(['Custom-posts','Edit'])  !!}
<div class="page-header">
    <h1>
        Custom Posts
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Edit
        </small>
    </h1>
</div><!-- /.page-header -->


<div class="form">
<form id="validation-form" class="form-horizontal" action="{{route('admin.customposts.update',$custompost->id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
           
            
            <div class="form-group  @error('name') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="title"> Name </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text" id="name" value="{{$custompost->name}}" placeholder="Title" name="name" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('name')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>

            <div class="form-group  @error('slug') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="title"> slug </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text" id="slug" value="{{$custompost->slug}}" placeholder="Title" name="slug" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('slug')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>


            <div class="clearfix">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Update
                    </button>
                </div>
            </div>
    </form>
</div>

<div class="col-lg-12">
    <div class="page-header">
        <h1>
            Custom Attributes 

        </h1>
        <a id="create_custom_attribute" class="btn btn-success create-button">
            Create
        </a>
    </div>
    <div class="form">
        @foreach ($custom_attributes as $custom_attribute)
        <form id="update_c_attr_data"> 
            
        <input type="hidden" id="custom_attribute_id_{{$custom_attribute->id}}" name="custom_attribute_id" value="{{$custom_attribute->id}}">
                <div class="container">
                    <div class="after-add-more-text custom-attributes-field row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="my-input">Name</label>
                            <input id="u_name_{{$custom_attribute->id}}" class="form-control" value="{{$custom_attribute->name}}" type="text" name="name[]">
                            </div>
                        </div>
                   

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="my-input">Label</label>
                                <input id="u_label_{{$custom_attribute->id}}" class="form-control" value="{{$custom_attribute->label}}" type="text" name="label[]">
                            </div>
                        </div>
                        <div class="col-sm-2" >
                            <div>
                                <label for="form-field-select-1">Type</label>

                                <select id="u_type_{{$custom_attribute->id}}" name="type[]" class="form-control" id="form-field-select-1">
                                <option  value="{{$custom_attribute->type}}">{{$custom_attribute->type}}</option>
                                    <option value="text">text</option>
                                    <option value="image">image</option>
                                    <option value="longtext">longtext</option>
                                    <option value="select">select</option>
                                    <option value="date">date</option>
                                    <option value="longtext-rich">longtext-rich</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="" for="food">Validation Rules</label>
                                @php
                                    $others = "required|nullable|image|mimes:jpeg,png,jpg,svg|max:20480|mimes:pdf";
                                    $selected = explode("|", $custom_attribute->validation_rules);

                                    $options = explode("|" , $others);

                                    $unique_options = array_diff($options,$selected)
                                @endphp
                               
                                <div class="form-controle">
                                    <select id="u_validation_rules_{{$custom_attribute->id}}" name="validation_rules" id="food" class="multiselect" multiple="">
                                        @foreach ($selected as $select)
                                            <option selected value="{{$select}}">{{$select}}</option>
                                            
                                        @endforeach
                                        @foreach ($unique_options as $unique_option)
                                            <option  value="{{$unique_option}}">{{$unique_option}}</option>  
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="checkbox" style="padding-top: 25px">
                                <label class="block">
                                <input name="form-field-checkbox" id="u_created_only{{$custom_attribute->id}}" value="{{$custom_attribute->validate_create_only}}" type="checkbox" @if($custom_attribute->validate_create_only == "1") checked @else unchecked @endif  class="ace input-lg">
                                    <span class="lbl bigger-120">Created Only</span>
                                </label>
                            </div>
                            <input type="hidden" id="u_created_only_value" value="">
                            
                        </div>
                        
                        <div class="col-sm-2" style="margin-top:25px">
                            <button  style="font-size:13px;border:1px" data-id="{{$custom_attribute->id}}"  class="btn btn-info _update" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Update
                            </button>

                        <button  style="border:1px"   data-id="{{$custom_attribute->id}}" class="btn btn-danger delete_c_attribute" data-rel="tooltip" title="Delete">
                                <span class="red">
                                <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                    
                  
                   
                </div>
    
            </form>
        @endforeach
    
    </div>

    <div id="custom_attribute_form" style="display:none">
        <form id="c_att_data" action="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="my-input">Name</label>
                                <input id="name" class="form-control"  type="text" name="name[]">
                        </div>
                    </div>
                <input type="hidden" name="custom_post_id" value="{{$custompost->id}}" >
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="my-input">Label</label>
                        <input id="label" class="form-control"  type="text" name="label[]">
                        </div>
                    </div>
                    <div class="col-sm-2" >
                        <div>
                            <label for="form-field-select-1">Type</label>

                            <select  name="type[]" class="form-control" id="form-field-select-1 type">
                                <option value="text">text</option>
                                <option value="image">image</option>
                                <option value="longtext">longtext</option>
                                <option value="select">select</option>
                                <option value="date">date</option>
                                <option value="longtext-rich">longtext-rich</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="" for="food">Validation Rules</label>

                            <div class="form-controle">
                                <select name="validation_rules[]" id="validation_rules" class="multiselect" multiple="">

                                    <option value="required">required</option>
                                    <option value="nullabe">nullable</option>
                                    <option value="image">image</option>
                                    <option value="mimes:jpeg">mimes:jpeg</option>
                                    <option value="mimes:png">mimes:png</option>
                                    <option value="mimes:svg">mimes:svg</option>
                                    <option value="mimes:jpg">mimes:jpg</option>
                                    <option value="mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime">mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="checkbox" style="padding-top: 25px">
                            <label class="block">
                                <input id="validation_created_only_create" name="form-field-checkbox" type="checkbox"  class="ace input-lg">
                                <span class="lbl bigger-120">Created Only</span>
                            </label>
                        </div>
                        <input type="hidden" name="validation_created_only" value="">
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="my-input">slug</label>
                        <input id="slug" class="form-control"  type="text" name="slug">
                        </div>
                    </div>

                    <div class="col-sm-2" style="margin-top:25px;display:flex;flex-direction:row">
                        <button id="cr_custom_attribute" style="font-size:13px;border:1px"  class="btn btn-info" type="submit">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            create
                        </button>
                        <button style="border:1px;margin-left:10px" id="hide_create_custom_attribute_form" class="btn btn-danger">
                            <i class="ace-icon fa fa-times red2"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {



    

$('body').on('click','.delete_c_attribute',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var token = $("meta[name='csrf-token']").attr("content");
    var url = "{{route('admin.custom-attributes.destroy',":id")}}".replace(':id', id);
    var re = $(this);
    
    if(confirm('Are you sure?')==true){
        $.ajax({
        type: "DELETE",
        url:  url,
        dataType: 'json',
        success: function (response) {
            re.parent().parent().remove();
            Swal.fire(
                response.status,
                response.message,
                'success'
            )
        }
    });

    }
    
   
})


$('body').on('click','._update',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    console.log(id);
    var url = "{{route('admin.custom-attributes.update',":id")}}".replace(':id', id);
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    if($('input#u_created_only'+id).prop('checked') == true){
        $('#u_created_only_value').val("1");
    }else{
        $('#u_created_only_value').val("0");
    }
    
    console.log($('#u_created_only_value').val());

    var name = $('#u_name_'+id).val();
    var label = $('#u_label_'+id).val();
    var type = $('#u_type_'+id).val();
    var validation_rules =  $('#u_validation_rules_'+id).val();
    var validation_created_only = $('#u_created_only_value').val();
    var custom_attribute_id = $('#custom_attribute_id_'+id).val();
    
    
   
    $.ajax({
        type: "PUT",
        url:  url,
        data: {
            name : name,
            label : label,
            type : type,
            validation_rules : validation_rules,
            validation_created_only : validation_created_only,
            custom_attribute_id: custom_attribute_id,

        },
        success: function (response) {
            if(response.status.toLowerCase()=='success'){
                Swal.fire(
                response.status,
                response.message,
                'success'
            )
            }
           
        },
        
    });


})

$('body').on('click','#cr_custom_attribute',function(e){
    e.preventDefault();
   

    if($('#validation_created_only_create').prop('checked') == true){
        $('input[name="validation_created_only"]').val("1");
    }else{
        $('input[name="validation_created_only"]').val("0");
    }
    let error = [];
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "{{route('admin.custom-attributes.store')}}",
        method: 'POST',
        data: $('#c_att_data').serialize(),
        success: function (response) {
            
            Swal.fire(
                response.status,
                response.message,
                'success'
            )
           
            if(response.status.toLowerCase()=='success'){
                window.location.reload();
                $('#custom_attribute_form').hide('slow');
            } 
        },

        error : function(xhr){
        
            $.each(xhr.responseJSON.errors, function(key,value) {
               error.push(value);
            }); 

            Swal.fire({

                icon: 'error',
                title: 'Invalide',
                text: error
                
            }
               
            )
        }

       
    });

})




$('body').on('click','#create_custom_attribute',function(e){
   
    e.preventDefault();
    
    $('#custom_attribute_form').show('slow');
})


$('body').on('click','#hide_create_custom_attribute_form',function(e){
    e.preventDefault();
    $('#custom_attribute_form').hide('slow');
})





$(".add-more-text").click(function(){
    var html = $(".copy-text").html();
    $(".input-container").append(html);

    
    $('.select2').css('width','200px').select2({allowClear:true})
				$('#select2-multiple-style .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('.select2').addClass('tag-input-style');
					 else $('.select2').removeClass('tag-input-style');
		});
        

});


$(document).on("click",".remove-more-text",function(){

    $(this).parents(".control-group-text").remove();

});

$("input[name='text[]']").each(function(){
    var text = $("input[name='text[]']").val();
    if(text==null){
        $('.error-floorplan').removeClass("")
    }else{
        $('.error-floorplan').addClass("hide")
    }
})
});


</script>
@endsection