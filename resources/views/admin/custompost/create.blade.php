@extends('layouts.admin')
@section('content')
{!!  _breadcrumbs(['Custom-posts','Create'])  !!}
<div class="page-header">
    <h1>
        Custom Posts
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Create
        </small>
    </h1>
    
</div><!-- /.page-header -->


<div class="form">
<form id="validation-form" class="form-horizontal" action="{{route('admin.customposts.store')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
        
            <div class="form-group  @error('name') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="title"> Name </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                        <input type="text" id="name" placeholder="Title" name="name" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('name')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="space-4"></div>

            <div class="form-group  @error('slug') has-error   @enderror">
                <label class="col-sm-3 control-label no-padding-right" for="title"> slug </label>

                <div class="col-sm-9">
                    <div class="clearfix">
                    <input type="text"  id="slug" placeholder="Title" name="slug" class="col-xs-10 col-sm-5" />
                    </div>
                    @error('slug')
                    <div  class="help-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>


            <div class="clearfix">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Create
                    </button>
                </div>
            </div>
    </form>
</div>
<script type="text/javascript">

$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#name').blur(function(){

        $.ajax({
            url: '/admin/custom-posts/slug',
            type: 'POST',
            data: {
               
               'slug':$('#name').val()
                
            },
            success:function(data){
                $('#slug').val(data);
            }
            
        })
            
    })
});


    // slug create function 
    function convertToSlug(name)
        {
            return name
                .toLowerCase()
                .replace(/[^\w ]+/g,'')
                .replace(/ +/g,'-');
        }

       
</script>
@endsection

