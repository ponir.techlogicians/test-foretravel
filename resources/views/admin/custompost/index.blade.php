@extends('layouts.admin')
@section('content')
{!! _breadcrumbs(['Custom-posts']) !!}
<div class="page-header">
    <h1>
        Custom Posts
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Page data
        </small>
    </h1>
    <a href="{{route('admin.customposts.create')}}">
        <span class="btn btn-success create-button">Create</span>
    </a>
</div><!-- /.page-header -->

<div class="all-contacts">
    <table  id="dynamic-table" class="table  table-bordered table-hover">
            <thead>
                    <tr>
                        <th>Name</th>
                        <th>slug</th>
                        <th>Created By</th>
                        <th>Actions</th>
                    </tr>
            </thead>

            <tbody>
               @foreach ($custom_posts as $custom_post)
                    <tr>
                        <td>{{ $custom_post->name }}</td>
                        <td>{{ $custom_post->slug }}</td>
                        <td>{{ $custom_post->createdBy->name }}</td>
                        <td>
                            <div class="hidden-sm hidden-xs btn-group" style="display:flex">

                            <a class="btn btn-xs btn-success" href="{{route('admin.customposts.edit',$custom_post->id)}}" >
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>

                            <form method="post" class="delete_form" action="{{route('admin.customposts.delete',$custom_post->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </button>
                                </form>
                                    </div>
                                </td>
                    </tr>
               @endforeach

            </tbody>
    </table>
</div>
@endsection