@extends('layouts.admin')
@section('content')
    <div class="">
        <div class="row">
            <div class="col-lg-12">

                {!! _breadcrumbs([_getPageLocatorBySection($slider->section),$slider->name]) !!}

                <div class="page-header">
                    <h1>
                        Slider
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Edit Slider
                        </small>
                    </h1>



                </div>

                @if (session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Success!
                        </strong>
                        {{ session('success') }}
                        <br>
                    </div>
                @endif

                @if(Auth::user()->role == 'superadmin')
                <div class="form">
                    <form class="form-horizontal" action="{{ route('admin.sliders.update',$slider->id)}}" method="post">
                        {{ csrf_field()}}
                        @method('PATCH')

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                            <div class="col-sm-9">
                                <input type="text" value="{{$slider->name}}" id="form-field-1" placeholder="Name"
                                       name="name" class="col-xs-10 col-sm-5"/>
                            </div>
                        </div>

                        <div class="form-group @error('message') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="message">Description</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <textarea class="col-xs-10  col-sm-5" rows="5" id="form-field-9"
                                              placeholder="Description"
                                              name="description">{{$slider->description}}</textarea>
                                </div>
                                @error('message')
                                <div class="help-block">{{ $message }}</div>
                                @enderror

                            </div>

                        </div>

                        <div class="clearfix">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif

            </div>

            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1>
                            All Items - {{ $slider->name }}
                        </h1>

                        <a href="javascript:;" onclick="updateSortOrder()" data-model="{{ $slider->section }}" id="sortButton" style="display: none;position: absolute;top: 56px;right: 90px;">
                            <span class="btn btn-warning">Save Order</span>
                        </a>

                        <a href="javascript:;" onclick="cancelSortOrder()" id="cancelSortButton" style="display: none;position: absolute;top: 56px;right: 13px;">
                            <span class="btn btn-danger">Clear</span>
                        </a>

                    </div>
                    <div class="all-contacts">
                        <table id="simple-table" class="table  table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>Name</th>
                                <th>Caption</th>
                                <th>File Type</th>
                                <th>File</th>
                                <th>Updated By</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody class="{{ in_array($slider->section, ['interior-gallery','exterior-gallery','color-options'])? 'sortable' : '' }}">
                            @foreach ($slider->slides as $slides)
                                <tr class="items" data-item_id="{{ $slides->id }}">
                                    <td>{{$slides->coachModel->title}}</td>
                                    <td>{{$slides->title}}</td>
                                    <td>{{$slides->caption}}</td>
                                    <td>{{$slides->file_type}}</td>
                                    <td><img style="width:80px; height:60px;" src="{{asset($slides->file_path) }}"/>
                                    </td>
                                    <td>{{$slides->updatedBy->name}}</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <a class="btn btn-xs btn-success"
                                               href="{{route('admin.slides.edit', $slides->id)}}">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </a>

                                            <button class="btn btn-xs btn-danger"
                                                    onclick="deletefromsubmit('slider{{$slides->id}}')">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </button>



                                            <form id="slider{{$slides->id}}" class="hiden" method="post"
                                                  class="delete_form"
                                                  action="{{route('admin.slides.destroy', $slides->id). (isset($_GET['model'])? ('?model=' . $_GET['model']) : '')}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE"/>
                                                {{-- <button type="submit" onclick="return confirm('Are you sure')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                                        <span class="red">
                                                            <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </span>
                                                </button> --}}
                                            </form>
                                        </div>
                                    </td>


                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        Add New Item
                    </h1>
                </div>
                <div>

                </div>
                <form class="form-horizontal" action="{{ route('admin.slides.store') . (isset($_GET['model'])? ('?model=' . $_GET['model']) : '')}}" method="POST"
                      enctype="multipart/form-data" role="form">
                    {{ csrf_field()}}

                    <div class="form-group @error('model') has-error   @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="model"> Model </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <select id="model" name="model" class="col-xs-10 col-sm-5">

                                    @foreach($coachModels as $model)
                                        @if(isset($param))
                                            @if($model->slug == $param)
                                                <option value="{{ $model->slug }}">{{ $model->title }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $model->slug }}">{{ $model->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('model')
                            <div id="name-error" class="help-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group @error('title') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="form-field-1" placeholder="Name" name="title"
                                       class="col-xs-10 col-sm-5" value="{{ old('title') }}"/>
                            </div>
                            @error('title')
                            <div class="help-block">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>


                    <div class="form-group  @error('caption') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Caption </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="form-field-1" placeholder="Input Caption" name="caption"
                                       class="col-xs-10 col-sm-5" {{ old('caption') }}/>
                            </div>
                            @error('caption')
                            <div class="help-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>


                    {{--<div class="form-group @error('message') has-error @enderror">--}}

                        {{--<label class="col-sm-3 control-label no-padding-right" for="form-field-select-1">File--}}
                            {{--Type</label>--}}
                        {{--<div class="col-sm-9">--}}
                            {{--<div class="clearfix">--}}
                                {{--<select name="file_type" class="col-xs-10 col-sm-5" id="form-field-select-1">--}}
                                    {{--<option value="image">Image</option>--}}
                                    {{--<option value="video">Video</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--@error('file_type')--}}
                            {{--<div class="help-block">{{ $message }}</div>--}}
                            {{--@enderror--}}

                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group @error('file_path') has-error @enderror">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Upload File Video or
                            Image</label>
                        <div class="col-sm-4">
                            <div class="clearfix">
                                <input class="col-xs-10 col-sm-5 id-input-file-3" type="file" name="file_path"/>
                            </div>
                            @error('file_path')
                            <div class="help-block">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>


                    {{--<div class="form-group @error('order') has-error @enderror">--}}
                        {{--<label class="col-xs-3 control-label no-padding-right" for="form-field-1"> Order </label>--}}

                        {{--<div class="col-xs-9">--}}
                            {{--<div class="clearfix">--}}
                                {{--<input type="text" id="form-field-1" placeholder="Input Slide order" name="order"--}}
                                       {{--class="col-xs-10 col-sm-5"/>--}}
                            {{--</div>--}}
                            {{--@error('order')--}}
                            {{--<div class="help-block">{{ $message }}</div>--}}
                            {{--@enderror--}}

                        {{--</div>--}}
                    {{--</div>--}}

                    <input type="hidden" name="slider_id" value="{{$slider->id}}">

                    <div class="clearfix">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
                {{-- <div class="form">
                <form class="form-horizontal" action="{{ route('admin.slides.store')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field()}}

                        <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>

                                <div class="col-sm-9">
                                <input type="text" id="form-field-1"  placeholder="Name" name="title" class="col-xs-10 col-sm-5" />
                                </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Caption </label>

                            <div class="col-sm-9">
                            <input type="text" id="form-field-1"  placeholder="Name" name="caption" class="" />
                            </div>
                        </div>
                        <div class="widget-main" style="padding-left:200px; padding-right:100px">
                                <label for="form-field-select-1">File Type</label>

                                <select name="file_type" class="form-control" id="form-field-select-1">
                                    <option value="image">Image</option>
                                    <option value="video">Video</option>
                                </select>
                        </div>
                        <div class="widget-body">
                                <div class="widget-main" style="padding-right:100px">
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label no-padding-right" for="form-field-1">Upload File Video or Image</label>
                                        <div class="col-xs-9">
                                        <input multiple="" type="file" name="file_path"  class="id-input-file-3" />
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> order </label>

                                <div class="col-sm-9">
                                <input type="text" id="form-field-1"  placeholder="Input Slide order" name="order" class="" />
                                </div>
                        </div>

                        <div class="widget-main" style="padding-left:200px; padding-right:100px">
                                <label for="form-field-select-1">Slider Name</label>

                                <select name="slider_id" class="form-control" id="form-field-select-1">
                                <option value="{{$slider->id}}">{{$slider->name}}</option>
                                </select>
                        </div>


                        <button type="submit"  class="btn btn-primary btn-block">Create</button>
                </form>
                </div> --}}

            </div>
        </div>
    </div>

    <script>
        function deletefromsubmit(slider_id) {
            if (confirm('Are you sure')) {
                $('#' + slider_id).submit();
            }

        }
    </script>
@endsection
