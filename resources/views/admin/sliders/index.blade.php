@extends('layouts.admin')
@section('content')

    {!!  _breadcrumbs('Sliders')  !!}

<div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    Sliders
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        All Slider
                    </small>
                </h1>
            </div>

            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-check"></i>
                        Success!
                    </strong>
                    {{ session('success') }}
                    <br>
                </div>
            @endif

            <div class="all-contacts">
                <table id="simple-table" class="table  table-bordered table-hover">
                        <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>

                                    <th>Updated By</th>
                                    <th>Actions</th>
                                </tr>
                        </thead>

                        <tbody>
                            @foreach ($sliders as $slider)
                                <tr>
                                    <td>{{$slider->name}}</td>
                                    <td>{{$slider->description}}</td>
                                    <td>{{$slider->updatedBy->name}}</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs btn-group" style="display:flex">
                                            {{-- <button class="btn btn-xs btn-success" class="tooltip-info" data-rel="tooltip" title="View"> --}}
                                                <a class="btn btn-xs btn-success" href="{{route('admin.sliders.edit', $slider->section)}}">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </a>
                                            {{-- </button> --}}

                                            <form method="post" class="delete_form" action="{{route('admin.sliders.destroy', $slider->id)}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-xs btn-danger" class="tooltip-info" data-rel="tooltip" title="Delete">
                                                    <span class="red">
                                                        <i style="background-color:white" class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </span>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
