@extends('layouts.admin')
@section('content')
<div class="">
    <div class="row">
            <div class="col-12">
                    <div class="page-header">
                            <h1>
                                Slider
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    create Slider
                                </small>
                            </h1>
                        </div>

               <div class="form">
               <form class="form-horizontal" action="{{ route('admin.sliders.store')}}" method="post">
                       {{ csrf_field()}}
                       <div class="form-group @error('name') has-error @enderror">
                               <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="form-field-1" placeholder="Name" name="name" class="col-xs-10 col-sm-5" />
                                        </div>
                                        @error('name')
                                            <div  class="help-block">{{ $message }}</div>
                                        @enderror
                                    </div>


                       </div>
                       <div class="form-group @error('description') has-error @enderror">
                            <label class="col-sm-3 control-label no-padding-right" for="message">Description</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <textarea class="col-xs-10  col-sm-5" rows="5" id="form-field-9" placeholder="Description"  name="description"></textarea>
                                </div>
                                @error('description')
                                    <div  class="help-block">{{ $message }}</div>
                                @enderror

                            </div>

                        </div>
                        <div class="clearfix">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Submit
                                    </button>
                                </div>
                        </div>

               </form>
               </div>
            </div>
    </div>
</div>
@endsection
