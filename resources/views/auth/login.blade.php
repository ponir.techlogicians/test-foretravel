@extends('layouts.guest')

@section('content')
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('login') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

                                {{--@error('email')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                {{--@enderror--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

                                {{--@error('password')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                {{--@enderror--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--{{ __('Remember Me') }}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-8 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Login') }}--}}
                                {{--</button>--}}

                                {{--@if (Route::has('password.request'))--}}
                                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                        {{--{{ __('Forgot Your Password?') }}--}}
                                    {{--</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div id="login-box" class="login-box visible widget-box no-border">
    <div class="widget-body">
        <div class="widget-main">
            <h4 class="header blue lighter bigger">
                <i class="ace-icon fa fa-coffee green"></i>
                Please Enter Your Information
            </h4>

            <div class="space-6"></div>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <fieldset>
                    <label class="block clearfix @error('email') has-error @enderror">
                        <span class="block input-icon input-icon-right">
                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />
                            <i class="ace-icon fa fa-user"></i>
                        </span>


                    </label>

                    <label class="block clearfix @error('email') has-error @enderror">
                        <span class="block input-icon input-icon-right">
                            <input type="password" class="form-control @error('email') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password" />
                            <i class="ace-icon fa fa-lock"></i>
                        </span>


                    </label>

                    <div class="space"></div>


                    <div class="clearfix">
                        <label class="inline">
                            <input type="checkbox" class="ace" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                            <span class="lbl"> {{ __('Remember Me') }}</span>
                        </label>

                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                            <i class="ace-icon fa fa-key"></i>
                            <span class="bigger-110">{{ __('Login') }}</span>
                        </button>
                    </div>

                    <div class="space-4"></div>

                    @error('password')
                    <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    @error('email')
                    <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    
                </fieldset>
            </form>

            {{--<div class="social-or-login center">--}}
                {{--<span class="bigger-110">Or Login Using</span>--}}
            {{--</div>--}}

            {{--<div class="space-6"></div>--}}

            {{--<div class="social-login center">--}}
                {{--<a class="btn btn-primary">--}}
                    {{--<i class="ace-icon fa fa-facebook"></i>--}}
                {{--</a>--}}

                {{--<a class="btn btn-info">--}}
                    {{--<i class="ace-icon fa fa-twitter"></i>--}}
                {{--</a>--}}

                {{--<a class="btn btn-danger">--}}
                    {{--<i class="ace-icon fa fa-google-plus"></i>--}}
                {{--</a>--}}
            {{--</div>--}}
        </div><!-- /.widget-main -->

        {{--<div class="toolbar clearfix">--}}
            {{--<div>--}}
                {{--<a href="#" data-target="#forgot-box" class="forgot-password-link">--}}
                    {{--<i class="ace-icon fa fa-arrow-left"></i>--}}
                    {{--I forgot my password--}}
                {{--</a>--}}
            {{--</div>--}}

            {{--<div>--}}
                {{--<a href="#" data-target="#signup-box" class="user-signup-link">--}}
                    {{--I want to register--}}
                    {{--<i class="ace-icon fa fa-arrow-right"></i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div><!-- /.widget-body -->
</div><!-- /.login-box -->
@endsection
