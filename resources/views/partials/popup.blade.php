<!-- popup start -->
    <div class="modal fade popup" id="common_popup" tabindex="-1" role="dialog" aria-labelledby="common_popup" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="logo">
                <img class="main-logo" src="{{ asset('/img/main_logo.svg') }}" alt="">
                <div class="title">
                    <span>FORETRAVEL</span>
                    <hr>
                    <span>MOTORCOACH</span>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <img src="{{ asset('img/icon/remove.svg') }}">
            </button>
          </div>
          <div class="modal-body">
            <div class="title">Title</div>
            <div class="body">Body</div>
          </div>
        </div>
      </div>
    </div>
<!-- popup end -->