<div class="sidebar">
    <div class="title">
        CHOOSE YOUR FORETRAVEL
    </div>

    <span class="toggle-sidebar" onclick="toggleSidebar()">
        <span class="arrow">
            <img src="{{ asset('img/icon/service/arrow-bottom.svg') }}">
        </span>
    </span>
    <ul class="model-list">
        @foreach(\App\Models\CoachModel::where('is_active',1)->get()->sortBy('sort') as $model)
            @if($model->slug != 'prevost')
            <li class="model">
                <a href="{{ url('model/' . $model->slug) }}" onclick="smoothModelPageRedirect(this,event)" class="{{URL::to('/model').'/'.$model->slug == URL::current()?'active':''}}">
                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($model->image) }}" class="lazy">
                    <span class="model-title">{{ $model->title }}</span>
                </a>
            </li>
            @endif
        @endforeach
    </ul>
</div>
