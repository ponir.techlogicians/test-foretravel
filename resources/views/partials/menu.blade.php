<!--navbar section-->
<div class="navbar-section {{(Request::route()->getName() == 'home')?'':'enable-bg'}}">
    <nav class="navbar navbar-expand-lg navbar-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse collapsed-mobile-menu" id="navbarSupportedContent">
            <ul class="navbar-nav" >
                @auth
                    @if(Auth::user()->role == 'motorcade-member')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/></svg>
                            </a>
                            <div class="dropdown-menu user-dropdown" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item user-name" href="javascript:;">{{Auth::user()->name}}</a>
                                <a class="dropdown-item" href="{{ route('motorcade-member-profile') }}">Profile</a>
                                <a class="dropdown-item" href="javascript:;" onclick="event.preventDefault();document.getElementById('logout_form').submit();">Log Out</a>
                            </div>
                        </li>
                    @endif
                @endauth
                <li class="nav-item">
                    <a class="nav-link sidebar-toggler" href="javascript:;" onclick="toggleSidebar()">Current Models</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About / Career</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('inventory') }}">Dealer Inventory</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="owner_service_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Owner Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="owner_service_dropdown">
                    <a class="dropdown-item" href="{{route('owner-service')}}">Owner Service</a>
                    <a class="dropdown-item" href="{{route('technical-support')}}">Technical Support</a>
                    <a class="dropdown-item" href="{{route('information')}}">Resources</a>
                        <a class="dropdown-item" href="{{ route('motorcade-club') }}">Motorcade club</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Foretravel Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('service-department')}}">Service Department</a>
                    <a class="dropdown-item" href="{{route('remodel-department')}}">Remodel Department</a>
                    <a class="dropdown-item" href="{{route('parts-department')}}">Parts Department</a>
                        <a class="dropdown-item" href="{{route('recall-information')}}">Recall information</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Fabrication Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach(\App\Models\CustomReferenceCategory::where('type','fabrications')->orderBy('sort')->get() as $page)
                        <a class="dropdown-item" href="{{ url($page->slug) }}">{{ $page->label }}</a>
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav" >

                @auth
                    @if(Auth::user()->role == 'motorcade-member')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/></svg>
                            </a>
                            <div class="dropdown-menu user-dropdown" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item user-name" href="javascript:;">{{Auth::user()->name}}</a>
                                <a class="dropdown-item" href="{{ route('motorcade-member-profile') }}">Profile</a>
                                <a class="dropdown-item" href="javascript:;" onclick="event.preventDefault();document.getElementById('logout_form').submit();">Log Out</a>
                            </div>
                        </li>
                    @endif
                @endauth
                <li class="nav-item">
                    <a class="nav-link sidebar-toggler" href="javascript:;" onclick="toggleSidebar()">Current<br>Models</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About /<br>Career</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('inventory') }}">Dealer<br>Inventory</a>
                </li>
            </ul>
        </div>

        <a class="navbar-brand" href="{{ route('home') }}">
{{--            <img class="main-logo" src="{{ asset('img/main_logo.svg') }}" alt="">--}}
            <img class="main-logo" src="{{ asset('img/main-logo.png') }}" alt="">
            <div class="title">
                <span>FORETRAVEL</span>
                <hr>
                <span>MOTORCOACH</span>
            </div>
        </a>

        <div class="navbar-collapse collapse">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="owner_service_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Owner<br>Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="owner_service_dropdown">
                    <a class="dropdown-item" href="{{route('owner-service')}}">Owner Service</a>
                    <a class="dropdown-item" href="{{route('technical-support')}}">Technical Support</a>
                    <a class="dropdown-item" href="{{route('information')}}">Resources</a>
                        <a class="dropdown-item" href="{{ route('motorcade-club') }}">Motorcade club</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Foretravel<br>Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('service-department')}}">Service Department</a>
                    <a class="dropdown-item" href="{{route('remodel-department')}}">Remodel Department</a>
                    <a class="dropdown-item" href="{{route('parts-department')}}">Parts Department</a>
                        <a class="dropdown-item" href="{{route('recall-information')}}">Recall information</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Fabrication<br>Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach(\App\Models\CustomReferenceCategory::where('type','fabrications')->orderBy('sort')->get() as $page)
                            <a class="dropdown-item" href="{{ url('fabrication/'.$page->slug) }}">{{ $page->label }}</a>
                        @endforeach
                    </div>
                </li>


            </ul>
        </div>
    </nav>
</div>
@auth
    @if(Auth::user()->role == 'motorcade-member')
        <form id="logout_form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    @endif
@endauth
