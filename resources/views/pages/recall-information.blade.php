@extends('layouts.app')
@section('content')

 <!-- header section -->
 <div class="header-sections" style="background-image: url({{asset(__t('recall_information_header_image'))}})!important;">
    <div class="header-section-layer">
        <h1 class="d-flex justify-content-center">{{__t('recall_information_header_title')}} </h1>
    </div>
</div>

<div class="section-wrapper bg-layer">

    <section class="section recall-information">
        <h1 class="section-headline">{{__t('recall_information_recall_notice_headline')}}</h1>

        <div class="recall-information-body">
            <div class="container">
                <div class="row">
                    @foreach($notices as $notice)
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="recall-notice-body middle-padding-right">
                            <div class="notice-body">
                                {{ $notice->details }}
                            </div>
                            <a href="{{ url($notice->file_one) }}" class="body-pdf d-flex" target="_blank">
                                <div class="pdf d-flex justify-content-center">
                                    <img src="{{ url('img/recall-information/pdf.svg') }}" alt="">
                                </div>

                                    <div class="text d-flex justify-content-center">
                                        {{ $notice->file_one_title }}
                                    </div>

                            </a>
                            @if(isset($notice->file_two) && isset($notice->file_two_title))
                            <a href="{{ url($notice->file_two) }}" class="body-pdf d-flex" target="_blank">
                                <div class="pdf d-flex justify-content-center">
                                    <img src=" {{ url('img/recall-information/pdf.svg') }}" alt="">
                                </div>

                                <div class="text d-flex justify-content-center">
                                    {{ $notice->file_two_title }}
                                </div>

                            </a>
                            @endif
                        </div>

                        {{--<div class="recall-notice-body middle-padding-right">--}}
                            {{--<div class="notice-body">--}}
                                {{--{{__t('recall_information_recall_notice_two_body')}}--}}
                            {{--</div>--}}
                            {{--<div class="body-pdf d-flex">--}}
                                {{--<div class="pdf d-flex justify-content-center">--}}
                                    {{--<img src="../img/recall-information/pdf.svg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="text d-flex justify-content-center">--}}
                                    {{--{{__t('recall_information_recall_notice_two_body_text_one')}}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    @endforeach

                    {{--<div class="col-lg-6 col-md-12 col-sm-12">--}}
                        {{--<div class="recall-notice-body middle-padding-left">--}}
                            {{--<div class="notice-body">--}}
                                {{--{{__t('recall_information_recall_notice_three_body')}}--}}
                            {{--</div>--}}
                            {{--<div class="body-pdf d-flex">--}}
                                {{--<div class="pdf d-flex justify-content-center">--}}
                                    {{--<img src="../img/recall-information/pdf.svg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="text d-flex justify-content-center">--}}
                                    {{--{{__t('recall_information_recall_notice_three_body_text_one')}}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="recall-notice-body middle-padding-left">--}}
                            {{--<div class="notice-body">--}}
                                {{--{{__t('recall_information_recall_notice_four_body')}}--}}
                            {{--</div>--}}
                            {{--<div class="body-pdf d-flex">--}}
                                {{--<div class="pdf d-flex justify-content-center">--}}
                                    {{--<img src="../img/recall-information/pdf.svg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="text d-flex justify-content-center">--}}
                                   {{--{{__t('recall_information_recall_notice_four_body_text_one')}}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>

</div>



@endsection
