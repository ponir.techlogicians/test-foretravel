@extends('layouts.app')

@section('content')

    <!-- header section -->
    <div class="header-sections" style="background-image: url({{asset(__t('motorcade_club_header_image'))}})!important;">
        <div class="header-section-layer">
            <h1 class="d-flex justify-content-center page-title">  </h1>

        </div>
    </div>

    <!-- tab section -->
    <ul class="nav nav-tabs blue-tab" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="club-tab" data-toggle="tab" href="#club" role="tab"
                aria-controls="club" aria-selected="true">
                <div class="icon-wrapper">
                    <div class="icon">
                        <img src="{{asset(__t('motorcade_club_tab_nav_club_icon'))}}">
                    </div>
                </div>
                <span class="tab-text">{{__t('motorcade_club_tab_nav_club_title')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="chapter-tab" data-toggle="tab" href="#chapter" role="tab" aria-controls="chapter"
                aria-selected="false">
                <div class="icon-wrapper">
                    <div class="icon">
                        <img src="{{asset(__t('motorcade_club_tab_nav_chapter_icon'))}}">
                    </div>
                </div>
                <span class="tab-text">{{__t('motorcade_club_tab_nav_chapter_title')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="officer-tab" data-toggle="tab" href="#officer" role="tab"
                aria-controls="officer" aria-selected="false">
                <div class="icon-wrapper">
                    <div class="icon">
                        <img src="{{asset(__t('motorcade_club_tab_nav_officers_icon'))}}">
                    </div>
                </div>
                <span class="tab-text">{{__t('motorcade_club_tab_nav_officers_title')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="magazine-tab" data-toggle="tab" href="#magazine" role="tab"
                aria-controls="magazine" aria-selected="false">
                <div class="icon-wrapper">
                    <div class="icon">
                        <img src="{{asset(__t('motorcade_club_tab_nav_magazine_icon'))}}">
                    </div>
                </div>
                <span class="tab-text">{{__t('motorcade_club_tab_nav_magazine_title')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="news-events-tab" data-toggle="tab" href="#news-events" role="tab"
                aria-controls="news-events" aria-selected="false">
                <div class="icon-wrapper">
                    <div class="icon">
                        <img src="{{asset(__t('motorcade_club_tab_nav_news_events_icon'))}}">
                    </div>
                </div>
                <span class="tab-text">{{__t('motorcade_club_tab_nav_news_events_title')}}</span>
            </a>
        </li>
    </ul>
    <div class="tab-content blue-tab" id="myTabContent">

        <!-- club tab -->
        <div class="tab-pane active" id="club" role="tabpanel" aria-labelledby="club-tab">

            @if (session('registeration-message'))
                <section class="section">
                    <div class="container">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <strong>{{ session('registeration-message') }}</strong>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    </div>
                </section>
            @endif

            @if (session('paypal-success-message'))
                <section class="section">
                    <div class="container">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <strong>{{ session('paypal-success-message') }}</strong>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    </div>
                </section>
            @endif

            @if (session('paypal-error-message'))
                <section class="section">
                    <div class="container">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ session('paypal-error-message') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </section>
            @endif

            @auth
                @if(Auth::user()->role == 'motorcade-member' && Auth::user()->is_paid == 0)
                    <section class="section payment">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="section-headline">Do the payment</h1>
                                <p class="section-info">You need pay the registration fee</p>

                                <!-- Proceed to checkout button -->
                                <div class="d-flex justify-content-center">
                                    <a class="btn btn-payment" href="{{ route('checkout') }}">
                                        <span>Pay the fee</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @endif
                @if(Auth::user()->role == 'motorcade-member')
                    <section class="section payment">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="section-headline">Make Payment for an Event</h1>
                                <p class="section-info">You can pay any amount</p>

                                <form method="GET" id="profile_update_form" action="{{ route('checkout') }}" style="text-align: center">


                                    @csrf

                                    <div class="form-row justify-content-center">
                                        <div class="form-group col-md-6">
                                            <label for="username" class="text-white">Tour Name<span class="text-danger">*</span></label>
                                            <select name="pay_for" id="pay_for" class="form-control" required="" >
                                                @foreach($tours as $tour)
                                                    <option value="{{ $tour->item_id }}"> {{ $tour->tour_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- lookup attribute for checking column value--}}
                                            <input type="hidden" name="loopup_attr" value="tour_name">
{{--                                            <input type="hidden" name="tour_name" value="{{ $tour->tour_name }}">--}}
                                        </div>
                                    </div>
                                    <div class="form-row justify-content-center">

                                        <div class="form-group col-md-6">
                                            <label for="register_password" class="text-white">Amount <span class="text-danger">*</span> (number only)</label>
                                            <input type="text" id="amount" name="amount" class="form-control" required>
                                        </div>
                                    </div>

                                    <!-- Proceed to checkout button -->
                                    <div class="d-flex justify-content-center">
                                        <button class="btn btn-payment" type="submit">
                                            <span>Make Payment</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                @endif
            @endauth


            <section class="section motorcade-club">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="section-headline">{{__t('motorcade_club_club_tab_club_title')}}</h1>
                            <p class="section-info">{{__t('motorcade_club_club_tab_club_info')}}</p>

                            @if(!Auth::user())
                                {{--||  Auth::user()->role != 'motorcade-member'--}}
                                <a class="btn btn-join-club" href="{{__t('motorcade_club_club_tab_club_join_button_link')}}" data-toggle="modal" data-target="#register_popup">
                                    <span>{{__t('motorcade_club_club_tab_club_join_button_title')}}</span>
                                </a>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="frame">
                                <img src="/img/loading.gif" data-src="{{asset(__t('motorcade_club_club_tab_club_image'))}}" class="rv-image lazy">
                                <img src="{{asset('img/motorcade-club/image/frame.png')}}" class="frame-image">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            @if(!Auth::user())
                    {{--||  Auth::user()->role != 'motorcade-member'--}}
                <section class="section join-us">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 info-wrapper" >
                                <div class="info">
                                    <div class="join-us-text">{{__t('motorcade_club_club_tab_join_us_text')}}</div>
                                    <h1 class="club-text">{{__t('motorcade_club_club_tab_join_us_middle_text')}}</h1>
                                    <div class="action-buttons">
                                        <a href="{{__t('motorcade_club_club_tab_join_us_login_button_link')}}" data-toggle="modal" data-target="#login_popup" class="btn">{{__t('motorcade_club_club_tab_join_us_login_button_text')}}</a>
                                        <a href="{{__t('motorcade_club_club_tab_join_us_register_button_link')}}" class="btn" data-toggle="modal" data-target="#register_popup">{{__t('motorcade_club_club_tab_join_us_register_button_text')}}</a>
                                    </div>
                                </div>
                            </div>
                            <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{asset(__t('motorcade_club_club_tab_join_us_image'))}}" class="col-md-7 image lazy">
                        </div>
                    </div>
                </section>
            @endif

            @auth
                @if(Auth::user()->role == 'motorcade-member' && Auth::user()->is_paid == 1)
                    <section class="section membership-directory">
                        <h1 class="section-headline">{{__t('motorcade_club_club_tab_club_member_member_section_title')}}</h1>
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="text-center">{{__t('motorcade_club_club_tab_club_member_member_section_subtitle')}}</h2>
                                    <p class="text-center">{{__t('motorcade_club_club_tab_club_member_member_section_description')}}</p>



                                    <div class="documents">
                                        <div class="document-wrapper">
                                            <div class="text"></div>
                                            {{--{{asset(__t('motorcade_club_club_tab_club_member_member_section_pdf'))}}--}}
                                            <a href="{{ route('motorcade-member-directory', 'by-number') }}" target="_blank" class="document">
                                                <span class="title">By Club Number</span>
                                                <img src="{{ url('img/icon/service/pdf.svg') }}">
                                                <span class="date"></span>
                                            </a>
                                        </div>
                                        <div class="document-wrapper">
                                            <div class="text"></div>
                                            {{--{{asset(__t('motorcade_club_club_tab_club_member_member_section_pdf_two'))}}--}}
                                            <a href="{{ route('motorcade-member-directory', 'by-name') }}" target="_blank" class="document">
                                                <span class="title">By Club Name</span>
                                                <img src="{{ url('img/icon/service/pdf.svg') }}">
                                                <span class="date"></span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                @endif
            @endauth


            <section class="section contact-info">
                <h1 class="section-headline">{{__t('motorcade_club_club_tab_contact_info_title')}}</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">{!!__t('motorcade_club_club_tab_contact_info_first_block')!!}</div>
                        <div class="col-md-4">{!!__t('motorcade_club_club_tab_contact_info_second_block')!!}</div>
                        <div class="col-md-4">{!!__t('motorcade_club_club_tab_contact_info_third_block')!!}</div>
                    </div>
                </div>
            </section>
        </div>

        {{-- magazine tab --}}
        <div class="tab-pane" id="magazine" role="tabpanel" aria-labelledby="magazine-tab">
            <section class="section our-magazine">
            <h1 class="section-headline">{{__t('motorcade_club_magazine_tab_magazine_title')}}</h1>
            <p class="section-info">{{__t('motorcade_club_magazine_tab_magazine_title_info')}}</p>

                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="magazine-list">
                               @foreach ($magazines as $magazine)
                                <a class="magazine" href="{{ $magazine->file_path != ''? url($magazine->file_path) : '' }}" target="_blank">
                                    <span class="overlay">
                                    <img src="{{asset('img/icon/club/link.svg')}}">
                                    <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_one_title')}}</span>
                                    </span>
                                <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{asset($magazine->image)}}" class="lazy">
                                </a>
                               @endforeach
                                {{-- <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                    <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_one_title')}}</span>
                                    </span>
                                <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_one_image'))}}">
                                </a> --}}
                                {{-- <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_two_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_two_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_three_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_three_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_four_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_four_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_five_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_five_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_six_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_six_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_seven_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_seven_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_eight_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_eight_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_nine_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_nine_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_ten_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_ten_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">
                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_eleven_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_eleven_image'))}}">
                                </a>
                                <a class="magazine" href="javascript:;">

                                    <span class="overlay">
                                        <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_icon'))}}">
                                        <span class="magazine-title">{{__t('motorcade_club_magazine_tab_magazine_twelve_title')}}</span>
                                    </span>
                                    <img src="{{asset(__t('motorcade_club_magazine_tab_magazine_twelve_image'))}}">
                                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        {{-- news and event --}}
        <div class="tab-pane" id="news-events" role="tabpanel" aria-labelledby="news-events-tab">
            <section class="section latest-news">
            <h1 class="section-headline">{{__t('motorcade_club_news_and_event_tab_news_and_event__title')}}</h1>
                <p class="section-info">
                    {{__t('motorcade_club_news_and_event_tab_news_and_event__title_info')}}
                </p>

                <div class="container">
                    <div class="row">
                        @foreach ($newses as $news)
                        <div class="col-lg-4 col-md-6 news-item">
                            <div class="card ">
                                <div class="card-body popup-content-parent">
                                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{asset($news->image)}}" class="lazy  popup-content-image">

                                        <span class="date">{{ _FTDateFormat($news->date) }}</span>

                                    <h5 class="card-title popup-content-title">{{$news->title}}</h5>

                                    <p class="card-text popup-content-body pre-line">{{$news->body}}</p>

                                    <a href="javascript:;" class="btn btn-read open-popup"><span>read more</span></a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </section>

            <section class="section motorcade-events">
            <h1 class="section-headline">{{__t('motorcade_club_news_and_event_tab_motorcade_event__title')}}</h1>
                <p class="section-info">
                   {{__t('motorcade_club_news_and_event_tab_motorcade_event__title_info')}}
                </p>
                <div class="container">
                    <div class="row">
                        @foreach ($events as $event)
                        <div class="col-lg-6 event-item">

                            <div class="card popup-content-parent">
                                <div class="card-image">
                                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{asset($event->image)}}" class="popup-content-image lazy">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{$event->title}}</h5>

                                    <span class="date">{{ _FTDateFormat($event->date_from)}} - {{ _FTDateFormat($event->date_to)}}</span>

                                    <span class="location">
                                        <div class="icon">
                                            <img src="../img/icon/club/map-marker.svg">
                                        </div>
                                        <span class="text">{{$event->location}}</span>
                                    </span>

                                    <p class="card-text popup-content-body pre-line">{{$event->description}}</p>

                                    <a href="javascript:;" class="btn btn-read  open-popup"><span>read more</span></a>
                                </div>
                            </div>

                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>

          <!-- chapter tab -->
          <div class="tab-pane" id="chapter" role="tabpanel" aria-labelledby="chapter-tab">
            <section class="section chapter">
            <h1 class="section-headline">{{__t('motorcade_club_chapter_tab_chapter_event__title')}}</h1>
            <p class="section-info">{{__t('motorcade_club_chapter_tab_chapter_event__title_info')}}</p>
            </section>

            @foreach ($chapters as $title => $chapter)
                <section class="section chapter">
                <h1 class="section-headline">{{ $title }}</h1>
                    <h1 class="section-headline">{{ $chapter[0]->reference_description }}</h1>
                    <p class="section-info">{{ $chapter[0]->reference_info }}</p>

                    <div class="container">
                        <div class="row">
                            @foreach($chapter as $info )
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="profile-information">
                                <div class="position">{{$info->designation}}</div>
                                    <div class="info name">
                                        <div class="icon">
                                            <img src="../img/icon/club/chapter/manager.svg"
                                                alt="">
                                        </div>
                                        <div class="text">
                                        <span><span class="name">{{$info->name}}</span>
                                            {{--<span class="id">#16681</span>--}}
                                        </span>
                                        </div>
                                    </div>
                                    <div class="info location">
                                        <div class="icon">
                                            <img src="../img/icon/club/chapter/location.svg"
                                                alt="">
                                        </div>
                                        <div class="text">
                                        <span>
                                            @if($info->post_office_box)
                                                P O Box {{$info->post_office_box}}<br>
                                            @endif

                                            @if($info->building_number && $info->street)
                                                {{$info->building_number}}, {{$info->street}}<br>
                                            @endif

                                            @if($info->apartment_room)
                                                {{$info->apartment_room}}<br>
                                            @endif

                                            @if($info->city)
                                                {{$info->city}},
                                            @endif

                                            @if($info->state)
                                                {{$info->state}}
                                            @endif

                                            @if($info->zip_code)
                                                {{$info->zip_code}}<br>
                                            @endif
                                        </span>
                                        </div>
                                    </div>
                                    <div class="info phone">
                                        <div class="icon">
                                            <img src="../img/icon/club/chapter/call.svg"
                                                alt="">
                                        </div>
                                        <div class="text">
                                            <span>
                                                <span class="bold"></span>{{$info->phone}}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="info email">
                                        <div class="icon">
                                            <img src="../img/icon/club/chapter/gmail.svg"
                                                alt="">
                                        </div>
                                        <div class="text">
                                            <div class="email">
                                                <a style="font-size:12px" href="mailto:{{ $info->email }}?Subject=Hello%20again" target="_top">
                                                    {{ $info->email}}
                                                </a>
                                            </div>
                                            {{-- <span class="bold">{{$info->email}}</span> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </section>
              @endforeach

            {{--<section class="section chapter">--}}
            {{--<h1 class="section-headline">{{__t('motorcade_club_chapter_tab_mid_atlantic_chapter_event__title_one')}}</h1>--}}
            {{--<h1 class="section-headline">{{__t('motorcade_club_chapter_tab_mid_atlantic_chapter_event__title_two')}}</h1>--}}
            {{--<p class="section-info">{{__t('motorcade_club_chapter_tab_mid_atlantic_chapter__title_info')}}</p>--}}

                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--@foreach ($mid_atlantic_chapters as $mid_atlantic_chapter)--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6">--}}
                            {{--<div class="profile-information">--}}
                                {{--@if(isset($mid_atlantic_chapter->designation))<div class="position">{{$mid_atlantic_chapter->designation}}</div>@endif--}}
                                {{--<div class="info name">--}}
                                    {{--<div class="icon">--}}
                                        {{--<img src="../img/icon/club/chapter/manager.svg"--}}
                                            {{--alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="text">--}}
                                    {{--<span>--}}
                                        {{--@if(isset($mid_atlantic_chapter->name))<span class="name">{{$mid_atlantic_chapter->name}}</span>@endif--}}
                                        {{--<span class="id">#16681</span></span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="info location">--}}
                                    {{--<div class="icon">--}}
                                        {{--<img src="../img/icon/club/chapter/location.svg"--}}
                                            {{--alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="text">--}}
                                        {{--@if(isset($mid_atlantic_chapter->location))<span>{{$mid_atlantic_chapter->location}}</span>@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="info phone">--}}
                                    {{--<div class="icon">--}}
                                        {{--<img src="../img/icon/club/chapter/call.svg"--}}
                                            {{--alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="text">--}}
                                        {{--<span>--}}
                                        {{--@if(isset($mid_atlantic_chapter->phone))<span class="bold">{{$mid_atlantic_chapter->phone}}</span>@endif--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="info email">--}}
                                    {{--<div class="icon">--}}
                                        {{--<img src="../img/icon/club/chapter/gmail.svg"--}}
                                            {{--alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="text">--}}
                                        {{--@if(isset($mid_atlantic_chapter->email))<span class="bold">{{$mid_atlantic_chapter->email}}</span>@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}

            {{--<section class="section load-more">--}}
                {{--<div class="load-more-trigger">load more</div>--}}
            {{--</section>--}}
        </div>


         <!-- officer tab -->
         <div class="tab-pane" id="officer" role="tabpanel" aria-labelledby="officer-tab">
            <section class="section president">
            <h1 class="section-headline">{{__t('motorcade_club_officers_tab_officers_event__title')}}</h1>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="presedent-profile-info-section">
                                @foreach ($presidentAndFirstLady as $officer)
                                    @if($loop->first)
                                        <div class="presedent-profile-info">
                                            <div class="left-side">
                                                <div class="profile-image">
                                                    <img src="{{ asset(isset($officer->image)? $officer->image : 'img/icon/club/manager.png') }}">
                                                </div>
                                            </div>
                                            <div class="blue-border"></div>
                                            <div class="right-side">
                                                <div class="profile-information">
                                                    <div class="p-info location-one d-flex">
                                                        <div class="icon">
                                                            <img src="../img/icon/club/manager.png"
                                                                alt="">
                                                        </div>
                                                        <div class="text">
                                                            <span class="name">{{ $officer->name }}</span>
                                                            <span class="id">{{ $officer->extention }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="p-info location-two d-flex">
                                                        <div class="icon">
                                                            <img src="../img/icon/club/location.png" alt="">
                                                        </div>
                                                        <div class="text-unique">
                                                            <span class="bold">
                                                                @if($officer->post_office_box)
                                                                    P O Box {{$officer->post_office_box}}<br>
                                                                @endif

                                                                @if($officer->building_number && $officer->street)
                                                                    {{$officer->building_number}}, {{$officer->street}}<br>
                                                                @endif

                                                                @if($officer->apartment_room)
                                                                    {{$officer->apartment_room}}<br>
                                                                @endif

                                                                @if($officer->city)
                                                                    {{$officer->city}},
                                                                @endif

                                                                @if($officer->state)
                                                                    {{$officer->state}}
                                                                @endif

                                                                @if($officer->zip_code)
                                                                    {{$officer->zip_code}}<br>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="p-info gmail d-flex">
                                                        <div class="icon">
                                                            <img src="../img/icon/club/call.png"
                                                                alt="">
                                                        </div>
                                                        <div class="text-unique">
                                                            <span class="bold">{{ $officer->phone }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="p-info phone d-flex">
                                                        <div class="icon">
                                                            <img src="../img/icon/club/gmail.png"
                                                                alt="">
                                                        </div>
                                                        <div class="text-unique">
                                                            <div class="email president">
                                                                <a  href="mailto:{{ $officer->email }}?Subject=Hello%20again" target="_top" style="color: #698DA4">
                                                                    {{$officer->email}}
                                                                </a>
                                                            </div>
                                                            {{-- <span class="bold">{{ $officer->email }}</span> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="section board-director">
            <h1 class="section-headline">{{__t('motorcade_club_officers_tab_board_of_director_event__title')}}</h1>

                <div class="container">
                    <div class="row contact-person-list">
                        @foreach ($board_of_directors as $board_of_director)
                        <div class="col-lg-3 col-sm-6 contact-person">
                            <div class="image">
                                <img src="{{ asset(isset($board_of_director->image)? $board_of_director->image : 'img/icon/user.png') }}" class="lazy">
                            </div>
                            <div class="info">
                                @if(isset($board_of_director->name))<div class="name">{{$board_of_director->name}}</div>@endif
                                @if(isset($board_of_director->extention))<div class="username">{{$board_of_director->extention}}</div>@endif
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </section>


            <section class="section club-manager">
            <h1 class="section-headline">{{__t('motorcade_club_officers_tab_club_manager_event__title')}}</h1>

                <div class="container">
                    <div class="row contact-person-list">
                        @foreach ($club_managers as $club_manager)
                        <div class="col-lg-3 col-sm-6 contact-person">
                            <div class="image">
                                <img src="{{ asset(isset($club_manager->image)? $club_manager->image : 'img/icon/user.png') }}" class="lazy">
                            </div>
                            <div class="info">
                            @if(isset($club_manager->name))
                                <div class="name">{{$club_manager->name}}</div>
                            @endif
                            @if(isset($club_manager->extention))
                                <div class="username">{{$club_manager->extention}}</div>
                            @endif
                            <div class="more-info">
                                <span class="address">
                                    @if($club_manager->post_office_box)
                                        P O Box {{$club_manager->post_office_box}}<br>
                                    @endif

                                    @if($club_manager->building_number && $club_manager->street)
                                        {{$club_manager->building_number}}, {{$club_manager->street}}<br>
                                    @endif

                                    @if($club_manager->apartment_room)
                                        {{$club_manager->apartment_room}}<br>
                                    @endif

                                    @if($club_manager->city)
                                        {{$club_manager->city}},
                                    @endif

                                    @if($club_manager->state)
                                        {{$club_manager->state}}
                                    @endif

                                    @if($club_manager->zip_code)
                                        {{$club_manager->zip_code}}<br>
                                    @endif
                                </span>
                                @if(isset($club_manager->phone))
                                    <span class="phone">
                                        <span class="bold">Phone: </span>{{$club_manager->phone}}
                                    </span>
                                @endif
                                @if(isset($club_manager->fax))
                                    <span class="fax">
                                        <span class="bold">FAX: </span>{{$club_manager->fax}}
                                    </span>
                                @endif
                                @if(isset($club_manager->email))
                                    <span class="email">
                                        <span class="bold">Email: </span>

                                        {{-- <div class="email"> --}}
                                            <a  href="mailto:{{$club_manager->email}}?Subject=Hello%20again" target="_top">
                                                {{$club_manager->email}}
                                            </a>
                                        {{-- </div> --}}

                                    </span>
                                @endif
                            </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </section>

        </div>
	</div>
@endsection


@section('extra-content')
    @guest
        {{-- Register Popup --}}
        <div class="modal fade common-popup" data-backdrop="static" id="register_popup" tabindex="-1" role="dialog" aria-labelledby="register_popup" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="logo">
                    <img class="main-logo" src="{{ asset('/img/main_logo.svg') }}" alt="">
                    <div class="title">
                        <span>FORETRAVEL</span>
                        <hr>
                        <span>MOTORCOACH</span>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <img src="{{ asset('img/icon/remove.svg') }}">
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Registration</div>
                <div class="alert alert-warning">
                    Annual Club Dues are <strong>$95</strong>. You will be promted to pay after filling out the information form.
                </div>
                <div class="alert alert-warning">
                    Fill out the following to send us an email. The fields marked with <strong class="text-danger">*</strong> are required.
                </div>
                <form method="POST" id="register_form" action="{{ route('motorcade-member-register') }}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="username">User Name (16 letter/number max)<span class="text-danger">*</span></label>
                          <input type="text" name="username" class="form-control" id="username" maxlength="16" required value="{{ old('username') }}">

                          @error('username')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="register_password">Password (minimum 6 letter/number)<span class="text-danger">*</span></label>
                          <input type="password" id="register_password" name="register_password" class="form-control" minlength="6" required>

                          @error('register_password')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="first_name">First Name <span class="text-danger">*</span></label>
                          <input type="text" name="first_name" class="form-control" id="first_name" required  value="{{ old('first_name') }}">

                          @error('first_name')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="last_name">Last Name <span class="text-danger">*</span></label>
                          <input type="text" name="last_name" class="form-control" id="last_name" required  value="{{ old('last_name') }}">

                          @error('last_name')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="spouse_name">Spouse Name<span class="text-danger">*</span></label>
                          <input type="text" name="spouse_name" class="form-control" id="spouse_name" required value="{{ old('spouse_name') }}">

                          @error('spouse_name')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="email">Email <span class="text-danger">*</span></label>
                          <input type="text" name="register_email" class="form-control" id="email" required  value="{{ old('register_email') }}">

                          @error('register_email')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="address_line_1">Address Line 1 <span class="text-danger">*</span></label>
                          <input type="text" name="address_line_1" class="form-control" id="address_line_1" required  value="{{ old('address_line_1') }}">

                          @error('address_line_1')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="address_line_2">Address Line 2</label>
                          <input type="text" name="address_line_2" class="form-control" id="address_line_2" value="{{ old('address_line_2') }}">

                          @error('address_line_2')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="city">City <span class="text-danger">*</span></label>
                          <input type="text" name="city" class="form-control" id="city" required  value="{{ old('city') }}">

                          @error('city')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="state">State <span class="text-danger">*</span></label>
                          <input type="text" name="state" class="form-control" id="state" required  value="{{ old('state') }}">

                          @error('state')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="zip">Zip <span class="text-danger">*</span></label>
                          <input type="text" name="zip" class="form-control" id="zip" required  value="{{ old('zip') }}">

                          @error('zip')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="phone_number">Phone<span class="text-danger">*</span></label>
                          <input type="tel" name="phone_number" class="form-control" id="phone_number" required value="{{ old('phone_number') }}">

                          @error('phone_number')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="cell_number">Cell<span class="text-danger">*</span></label>
                          <input type="text" name="cell_number" class="form-control" id="cell_number" required value="{{ old('cell_number') }}">

                          @error('cell_number')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="birthday">Birthday</label>
                          <input type="date" name="birthday" class="form-control" id="birthday" value="{{ old('birthday') }}">

                          @error('birthday')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="spouse_birthday">Spouse Birthday</label>
                          <input type="date" name="spouse_birthday" class="form-control" id="spouse_birthday" value="{{ old('spouse_birthday') }}">

                          @error('spouse_birthday')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="anniversary">Anniversary</label>
                          <input type="date" name="anniversary" class="form-control" id="anniversary" value="{{ old('anniversary') }}">

                          @error('anniversary')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="alert alert-success">General Info</div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="coach_type">Coach Type <span class="text-danger">*</span></label>
                          <input type="text" name="coach_type" class="form-control" id="coach_type" required  value="{{ old('coach_type') }}">

                          @error('coach_type')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="purchase_date">Purchase Date<span class="text-danger">*</span></label>
                          <input type="date" name="purchase_date" class="form-control" id="purchase_date" required value="{{ old('purchase_date') }}">

                          @error('purchase_date')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="purchase_form">Purchase From<span class="text-danger">*</span></label>
                          <input type="text" name="purchase_form" class="form-control" id="purchase_form" required value="{{ old('purchase_form') }}">

                          @error('purchase_form')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="year_model">Year Model <span class="text-danger">*</span></label>
                          <input type="text" name="year_model" class="form-control" id="year_model" required  value="{{ old('year_model') }}">

                          @error('year_model')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="last_six_number_vin">Last Six Numbers of VIN <span class="text-danger">*</span></label>
                          <input type="text" name="last_six_number_vin" class="form-control" id="last_six_number_vin" required  value="{{ old('last_six_number_vin') }}">

                          @error('last_six_number_vin')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="complete_model_number">Complete Model Number  <em>(located on the sidewall I.D. plate next to the drivers chair)</em><span class="text-danger">*</span></label>
                          <input type="text" name="complete_model_number" class="form-control" id="complete_model_number" required  value="{{ old('complete_model_number') }}">

                          @error('complete_model_number')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label>FMCA Member<span class="text-danger">*</span></label>
                        <div class="form-group">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="fmca_member" required id="fmca_member_yes" value="1"
                                @if(old('fmca_member') == 1)
                                    checked
                                @endif
                            >
                            <label class="custom-control-label" for="fmca_member_yes">Yes</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="fmca_member" id="fmca_member_no" value="0"
                                @if(old('fmca_member') == 0)
                                    checked
                                @endif
                            >
                            <label class="custom-control-label" for="fmca_member_no">No</label>
                          </div>
                        </div>

                          @error('fmca_member')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>

                        <div class="form-group col-md-6">
                          <label for="fmca_number">FMCA Number<span class="text-danger">* (required if FMCA Member)</span></label>
                          <input type="text" name="fmca_number" class="form-control" id="fmca_number" value="{{ old('fmca_number') }}">

                          @error('fmca_number')
                          <div  class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="form-group d-flex justify-content-center">
                        <button class="btn btn-foretravel form-control" type="submit">Submit</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        {{-- Login Popup --}}
        <div class="modal fade common-popup" data-backdrop="static" id="login_popup" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="logo">
                    <img class="main-logo" src="{{ asset('/img/main_logo.svg') }}" alt="">
                    <div class="title">
                        <span>FORETRAVEL</span>
                        <hr>
                        <span>MOTORCOACH</span>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <img src="{{ asset('img/icon/remove.svg') }}">
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Login</div>
                <div class="body">
                    <form method="POST" id="login_form" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username" value="{{ old('email') }}" required>

                            @error('email')
                            <div  class="help-block text-danger">{{ $message }}</div>
                            @enderror

                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                            <input type="hidden" name="type" value="member">

                            @error('password')
                            <div  class="help-block text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group d-flex justify-content-center">
                            <button class="btn btn-foretravel form-control" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    @endguest
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            @if ($errors->any())
                @if($errors->has('email') || $errors->has('password'))
                    $('#login_popup').modal('show')
                @else
                    $('#register_popup').modal('show')
                @endif
            @endif
        });
    </script>
@endsection
