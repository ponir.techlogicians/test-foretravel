@extends('layouts.app')
@section('content')

<div class="header-sections"  style="background-image: url({{asset(__t('remodel_department_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{__t('remodel_department_header_title')}}</h1>
    </div>
</div>


{{-- page body --}}
<div class="page-body">
    <!-- remodel department -->
    <section class="section remodel-department">
    <h1 class="section-headline">{{__t('remodel_department_parts_department_title')}}</h1>
    <p class="section-info">{{__t('remodel_department_parts_department_info')}}</p>
        <div class="container team-text">
            <div class="row">
                <div class="col-md-6 image">
                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('remodel_department_team_image'))}}" class="team-image lazy">
                </div>
                <div class="col-md-6 info">
                    <div class="details">
                        <h3 class="header">{{ __t('remodel_department_team_info') }}</h3>
                        <p class="body">
                            {{ __t('remodel_department_team_appointment') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container team-slider contact-person-list">
            <div class="row">
                <div class="col-12">
                    <div class="team-members">
                        @foreach($remodelDepartment as $member)
                        <div class="member">
                            <div class="image">
                                <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($member->photo) }}" class="lazy">
                            </div>
                            <div class="info">
                                <h3 class="name">{{ $member->name }}</h3>
                                <span class="position">{{ $member->designation }}</span><br>
                                {{-- <span class="position">{{ $member->fax }}</span><br> --}}
                                <span class="position">{{ $member->fax }}</span>
                                <div class="email">
                                    <a style="color: #003470;text-decoration:none;font-size:10px" href="mailto:{{ $member->email }}?Subject=Hello%20again" target="_top">
                                        {{ $member->email }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <button class="slick-slider-prev"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-left" class="svg-inline--fa fa-chevron-circle-left fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zM142.1 273l135.5 135.5c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L226.9 256l101.6-101.6c9.4-9.4 9.4-24.6 0-33.9l-17-17c-9.4-9.4-24.6-9.4-33.9 0L142.1 239c-9.4 9.4-9.4 24.6 0 34z"/></svg></button>
                    <button class="slick-slider-next"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"/></svg></button>
                </div>
            </div>
        </div>
    </section>


    <!-- our work -->
    <section class="section our-work">
    <h1 class="section-headline">{{__t('remodel_department_our_work_title')}}</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6 work-image">
                <img src="{{ asset(__t('remodel_department_our_work_one'))}}" class="lazy">
            </div>
            <div class="col-md-6 work-image">
                <img src="{{ asset(__t('remodel_department_our_work_two'))}}" class="lazy">
            </div>
            <div class="col-md-6 work-image">
                <img src="{{ asset(__t('remodel_department_our_work_three'))}}" class="lazy">
            </div>
            <div class="col-md-6 work-image">
                <img src="{{ asset(__t('remodel_department_our_work_four'))}}" class="lazy">
            </div>
        </div>
    </div>
    </section>
</div>

@endsection
