@extends('layouts.app')
@section('content')
    {{-- owner-service-header --}}
    <div class="header-sections" style="background-image: url({{asset(__t('owner_service_hader_image'))}})!important;">
        <div class="header-section-layer">
        <h1 class="d-flex justify-content-center">{{__t('owner_service_header_title')}}</h1>

        </div>
    </div>

    <div class="page-body">
        <!-- owner service -->
        <section class="section owner-service">
        <h1 class="section-headline">{{__t('owner_service_owner_service_title')}}</h1>
            <div class="container">
                <div class="row">
                    {{-- <div class="col-md-6 one">
                        <img class="img-fluid" src="{{asset(__t('owner_service_owner_service_left_image'))}}">
                    </div> --}}
                    <div class="col-md-6 offset-md-3 two d-flex justify-content-center">
                        <a target="_blank" href="https://www.spartanrvchassis.com/owners/service-support/" class="owner-service-image">
                            <img class="img-fluid lazy" src="{{url('/img/loading.svg')}}?v=1" data-src="{{asset(__t('owner_service_owner_service_right_image'))}}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="info">
            <p>{!! __t('owner_service_owner_service_office_timing') !!}</p>
            </div>
        </section>

        <!-- Owner Recommended Service Centers -->
        <section class="section recommend-service">
        <h1 class="section-headline">{{__t('owner_service_recommended_service_title')}}</h1>
        <p class="section-info">{!! __t('owner_service_recommended_service_description') !!}</p>
            <div class="container">
                <div class="row">
                    <div class="col-12 documents">
                        <a href="{{__t('owner_service_recommended_service_referral_list_file_path')}}" class="document" target="_blank">
                            <span class="title">{{__t('owner_service_recommended_service_referral_list_file_title')}}</span>
                            <img src="img/icon/service/pdf.svg">
                            <span class="date">{{__t('owner_service_recommended_service_referral_list_file_date')}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- service center location -->
        <section class="section service-center-location">
        <h1 class="section-headline">{{__t('information_service_center_location_title')}}</h1>
        <p class="section-info">{{__t('information_service_center_location_info')}}</p>


            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-of-location">

                            @foreach(json_decode($coordinates) as $coord)
                                <li class="@if($loop->first) active @endif">
                                    <a href="javascript:;" class="map-location" data-location="{{ $coord->state }}">{{ ucfirst($coord->state) }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3397.6912771050615!2d-94.68719918438829!3d31.61491814962955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86378b9e6058f039%3A0xd692c85886058acc!2sForetravel%20Motorcoach!5e0!3m2!1sen!2sbd!4v1575897600174!5m2!1sen!2sbd" frameborder="0" style="border:0;" allowfullscreen="" class="map"></iframe> --}}
                        <div id="map" class="map">

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


@section('script')
    <script type="text/javascript">

        /** map start **/

            // map object will be assigned into this variable
            let map;

            // console.log(<?= $coordinates ?>);
            let cordinates = <?= $coordinates ?>;

            // console.log(cordinates);

            // cordiante array
            // let cordinates = [
            //     {
            //         state:'california',
            //         location_cordinates: [
            //             {
            //                 lat: 32.7980328,
            //                 lng: -116.9738635
            //             },
            //             {
            //                 lat: 33.6991392,
            //                 lng: -117.8414823
            //             },
            //             {
            //                 lat: 33.5432998,
            //                 lng: -117.1745623
            //             },
            //             {
            //                 lat: 34.2563135,
            //                 lng: -119.244891
            //             }
            //         ]
            //     },
            //     {
            //         state:'florida',
            //         location_cordinates: [
            //             {
            //                 lat: 27.8951707,
            //                 lng: -82.6965572
            //             },
            //             {
            //                 lat: 28.7715815,
            //                 lng: -81.8825622
            //             }
            //         ]
            //
            //     },
            //     {
            //         state:'georgia',
            //         location_cordinates: [
            //             {
            //                 lat: 33.6155784,
            //                 lng: -84.4080457
            //             },
            //             {
            //                 lat: 33.9930698,
            //                 lng: -83.3836734
            //             }
            //         ]
            //     }
            // ];

            // array for marker object
            let markers = [];

            // initialize the function for Gmap
            function initMap() {

                function dropMarker(location){
                    let location_cordinates = [];
                    const popup = new google.maps.InfoWindow();
                    const bounds = new google.maps.LatLngBounds();

                    // find lat-lng from big array
                    for(i=0;i<cordinates.length;i++){
                        if(cordinates[i].state == location){
                            location_cordinates = cordinates[i].location_cordinates;
                            break;
                        }
                    }

                    // remove old marker from map
                    for (var i = markers.length - 1; i >= 0; i--) {
                        markers[i].setMap(null);
                    }
                    markers = [];

                    // put the markers in the map
                    for (var i = location_cordinates.length - 1; i >= 0; i--) {
                        let cordinate = location_cordinates[i];
                        // let latLng = { lat: cordinate.lat, lng: cordinate.lng };
                        let latLng = new google.maps.LatLng(cordinate.lat, cordinate.lng);
                        let marker = new google.maps.Marker({
                            position: latLng,
                            title: location,
                            map: map
                        });
                        // map.setCenter(latLng);
                        bounds.extend(latLng);
                        markers.push(marker);
                        google.maps.event.addListener(marker, 'click', function(e) {
                            popup.setContent(cordinate.info);
                            popup.open(map, this);
                            setTimeout(function(){
                                $('a:external').attr('target', '_blank')
                            },10);
                        });
                        google.maps.event.addListener(map, 'zoom_changed', function() {
                            zoomChangeBoundsListener =
                                google.maps.event.addListener(map, 'bounds_changed', function(event) {
                                    if (this.getZoom() > 16 && this.initialZoom == true) {
                                        // Change max/min zoom here
                                        this.setZoom(16);
                                        this.initialZoom = false;
                                    }
                                google.maps.event.removeListener(zoomChangeBoundsListener);
                            });
                        });
                    }
                    map.initialZoom = true;

                    map.fitBounds(bounds);
                }

                let el = document.getElementById('map');

                if (el){

                    map = new google.maps.Map(el, {zoom: 8,scrollwheel:  false});

                    // dropMarker('california');
                    dropMarker($($('.map-location')[0]).data('location'))

                }

                // cordinate for selected location
                $('body').on('click','.map-location',function(){
                    if(!$(this).closest('li').hasClass('active')){
                        // remove active from other location nav
                        $(this).closest('.list-of-location').find('.active').removeClass('active');

                        let location =$(this).attr('data-location');
                        dropMarker(location);

                        // add active in current location nav
                        $(this).closest('li').addClass('active');
                    }
                });
            }

        /** map end **/

    </script>
@endsection
