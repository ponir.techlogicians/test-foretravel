@extends('layouts.app')

@section('content')

    <!-- header section -->
    <div class="header-sections" style="background-image: url({{ asset(__t('about_header_image')) }}) !important;">
        <div class="header-section-layer">
            <h1 class="d-flex justify-content-center">{{ __t('header_title') }}</h1>
            {{--<h1 class="d-flex justify-content-center"> @text(header_title) </h1>--}}

        </div>
    </div>

    <!-- after header parent section -->
    <div class="after-header-parents-section">

        <!-- history of foretravel sections -->
        <div class="history-of-foretravel d-flex popup-content-parent">
            <div class="history-image d-flex justify-content-start">
                <img  src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('history_image')) }}" alt="loading" class="lazy popup-content-image">
            </div>
            <div class="history-text">
                <div class="text-header">
                    <h1 class="popup-content-title">{{ __t('history_title') }}</h1>
                </div>
                <div class="text-body">
                    <h1>{{ __t('history_year') }}</h1>
                    <span class="popup-content-body">{{ __t('history_description') }}</span>
                </div>
                <div class="read-more">
                    <a href="javascript:;" class="more open-popup">Read More</a>
                </div>
                {{--<div class="history-option d-flex">--}}
                    {{--<div class="models d-flex section-gap">--}}
                        {{--<div class="icon inner-flex-section-gap">--}}
                            {{--<img class="lazy" src="{{ asset('img/icon/bus.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="text">--}}
                            {{--<div class="text-header">All Models</div>--}}

                            {{-- <div class="span-text">Loren ispump</div> --}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="support d-flex section-gap">--}}
                        {{--<div class="icon inner-flex-section-gap">--}}
                            {{--<img class="lazy" src="{{ asset('img/icon/24-hours-support.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="text">--}}
                            {{--<div class="text-header">Free Support</div>--}}

                            {{-- <div class="span-text">Loren ispump</div> --}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="appliance d-flex section-gap">--}}
                        {{--<div class="icon inner-flex-section-gap">--}}
                            {{--<img class="lazy" src="{{ asset('img/icon/appliase.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="text">--}}
                            {{--<div class="text-header">Free Support</div>--}}

                            {{-- <div class="span-text">Loren ispump</div> --}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>

        <!-- foretravel location section -->
        <div class="foretravel-location-sections d-flex popup-content-parent">
            <div class="left-side">
                <div class="header popup-content-title">
                    {{ __t('location_title') }}
                </div>
                {{-- <div class="map">
                    <div class="icon">
                        <img class="lazy" src="{{ asset('img/icon/placeholder-on-map-paper-in-perspective.png') }}" alt="">
                    </div>
                    <div class="text">
                        Map
                    </div>
                </div> --}}
                <div class="body popup-content-body">
                    {{ __t('location_description') }}
                </div>
                <div class="read-more">
                    <a href="javascript:;" class="open-popup">Read More</a>
                </div>
            </div>
            <div class="right-side">
                <div class="right-side-image">
                    <img class="lazy popup-content-image" src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('location_image')) }}" alt="">
                </div>
            </div>
        </div>

        <!-- foretravel factory tour sections -->
        <div class="foretravel-factory-tour-section d-flex">
            <div class="left">
                <div class="factory-tour-left-image">
                    <img class="lazy" src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('factory_tour_image_left')) }}" alt="">
                </div>
            </div>

            <div class="middle">
                <div class="descriptions">
                    <div class="header">
                        {{ __t('factory_tour_title') }}
                    </div>
                    <div class="body">
                        {{ __t('factory_tour_description') }}
                    </div>
                </div>

            </div>

            <div class="right">
                <div class="factory-tour-right-image">
                    <img class="lazy" src="{{url('/img/loading.svg')}}?v=1" data-src="{{  asset(__t('factory_tour_image_right')) }}" alt="">
                </div>
            </div>
        </div>

        <!-- join invitation sections -->
        <div class="join-team-sections d-flex">
            <div class="left">
                <div class="header">
                    {{ __t('marketing_left_panel_title') }}
                </div>
                <div class="body">
                    {{ __t('marketing_left_panel_sub_title') }}
                </div>
                <div class="button">
                    <a href="{{ url('/inventory') }}"><img class="lazy" src="{{ asset('img/icon/magnifying-glass.png') }}" alt=""></a>
                </div>
            </div>
            <div class="right">
                <div class="header">
                    {{ __t('marketing_right_panel_title') }}
                </div>
                <div class="body">
                    {{ __t('marketing_right_panel_sub_title') }}
                </div>
                <div class="button">
                    <a href="{{ url('/career') }}">{{ __t('marketing_right_panel_button_text') }}</a>
                </div>
            </div>
        </div>

        <!-- Testimonial sections -->
        {{-- <div class="testimonial-sections">
            <div class="main-test-header d-flex justify-content-center">
                {{ __t('testimonials_title') }}
            </div>
            <div class="sub-test-header d-flex justify-content-center">
                {{ __t('testimonials_sub_title') }}
            </div>

            <div class="slider-testimonial">
                <div class="variable-width slick-track">

                    @foreach($testimonials as $item)
                    <div class="slick-slide d-flex">
                        <div class="left-side-slick">
                            <img src="{{ asset($item->background_image) }}" alt="">
                        </div>
                        <div class="right-side-slick">
                            <div class="small-image-profile">
                                <div class="bg-small-image">
                                    <img src="{{ asset($item->photo) }}" alt="">
                                </div>
                            </div>
                            <div class="testimonial">
                                <div class="header">
                                    <div class="main-header">
                                        {{ $item->name }}
                                    </div>
                                    <div class="sub-header">
                                        {{ $item->designation }}
                                    </div>
                                    <div class="s-sub-header">
                                        {{ $item->company }}
                                    </div>
                                </div>
                                <div class="body">
                                    {{ $item->content }}
                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>

            </div>

        </div> --}}

    </div>

@endsection
