@extends('layouts.app')

@section('content')

    <!-- header section -->
    {{-- <div class="header-sections" style="background-image: url({{asset(__t('inventory_header_image'))}})!important;">
        <div class="header-section-layer">
            <h1 class="d-flex justify-content-center">{{ __t('header_title') }}</h1>
            <h1 class="d-flex justify-content-center"> {{__t('inventory_header_title')}} </h1>

        </div>
    </div> --}}

    <div class="section-wrapper bg-layer">

        <section class="section available-inventory">
            <h1 class="section-headline">{{__t('inventory_available_inventory_headline')}}</h1>
            <p class="section-info">{{__t('inventory_available_inventory_info')}}</p>

            <div class="after-available-inventory d-flex">

                <a href="https://www.mhsrv.com/foretravel-rv" target="_blank" class="left-side side" style="background-image: linear-gradient(rgba(0, 0, 0, 0.51),rgba(0,0,0, 0.51)), url({{asset(__t('inventory_available_inventory_first_image'))}});">
                    <div class="elements">

                        <h1 class="headline">{{__t('inventory_available_inventory_first_title')}}</h1>

                        <div class="image d-flex justify-content-center">
                            <img src="{{ asset(__t('inventory_available_inventory_left_icon'))}}" class="lazy">
                        </div>

                    </div>

                </a>

                <a href="https://rvone.com/new-inventory/?q=page_1--sortby_price-asc--make_foretravel" class="right-side side"  target="_blank"
                style="background-image: linear-gradient(rgba(0, 0, 0, 0.51),rgba(0,0,0, 0.51)),
                url({{asset(__t('inventory_available_inventory_second_image'))}});">
                    <div class="elements">
                        <h1 class="headline">{{__t('inventory_available_inventory_second_title')}}</h1>

                        <div class="image d-flex justify-content-center">
                            <img src="{{ asset(__t('inventory_available_inventory_right_icon'))}}" class="lazy">
                        </div>



                    </div>

                </a>
                {{--<a href="https://www.foretraveloftexas.com/" target="_blank" class="left-side side"--}}
                   {{--style="background-image: linear-gradient(rgba(0, 0, 0, 0.51),rgba(0,0,0, 0.51)),--}}
                       {{--url({{asset(__t('inventory_available_inventory_third_image'))}});">--}}
                    {{--<div class="elements">--}}
                        {{--<h1 class="headline">{{__t('inventory_available_inventory_third_title')}}</h1>--}}

                        {{--<div class="image d-flex justify-content-center">--}}
                            {{--<img src="{{ asset(__t('inventory_available_inventory_third_icon'))}}" class="lazy">--}}
                        {{--</div>--}}



                    {{--</div>--}}

                {{--</a>--}}

            </div>

        </section>

    </div>

@endsection
