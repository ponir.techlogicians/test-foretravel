@extends('layouts.app')
@section('content')

<div class="header-sections"  style="background-image: url({{asset(__t('technical_support_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{__t('technical_support_header_title')}}</h1>
    </div>
</div>


<div class="page-body">
    <section class="section technical-support">
    <h1 class="section-headline">{{__t('technical_support_technical_support_title')}}</h1>
    <p class="section-info">{{__t('technical_support_technical_support_info')}}
        <a href="mailto:{{__t('technical_support_technical_support_info_email')}}?Subject=Hello%20again" target="_top" tabindex="-1">
            {{__t('technical_support_technical_support_info_email')}}
        </a>
    </p>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div id="technical-support" class="technical-qa section-headline">
                        {{__t('technical_support_technical_support_technical_qa_title')}}
                    </div>
                    <div class="technical-qa-form">
                        <form id="technicalSupportSearchForm" action="{{ url('technical-support') }}#technical-support" method="get">
                            <div class="form-elements d-flex">
                                <div class="select-options padding-gap">
                                    <select name="category" id="category" data-depending-by="section" class="categories input-section-padding font-size">
                                        {{--<option value="">Select A Category</option>--}}
                                        @foreach($categories as $category)
                                        <option {{ Request::get('category') == $category->id? 'selected' : '' }} value="{{ $category->id }}">{{ $category->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="select-sections padding-gap">
                                    <select name="section" id="section" class="sections input-section-padding font-size">
                                        <option value="">Select A Section</option>
                                        @foreach($sections as $section)
                                            <option {{ Request::get('section') == $section->id? 'selected' : '' }} value="{{ $section->id }}">{{ $section->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="submit-button ">
                                    <button type="submit">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tichnical-qa-data">

                        @if($supportQA)
                            @foreach($supportQA as $catSection => $topics)

                                <div class="t-header text-padding-bottom">
                                    Category: <span class="qa-category">{{ explode('-', $catSection)[0] }}</span> @if(isset(explode('-', $catSection)[1])) / Section: <span class="qa-section">{{ explode('-', $catSection)[1] }}</span> @endif
                                </div>
                                <div class="t-header text-padding-bottom">
                                    Available Topics
                                </div>

                                @foreach($topics as $topic)
                                <div class="topic">
                                    {{ $topic->question }}
                                </div>

                                <div class="answer-wrapper">
                                    <div class="answer-link">Answer</div>
                                    <div class="answer">
                                        {!! $topic->answer !!}
                                    </div>
                                </div>
                                @endforeach

                            @endforeach
                        @else

                            <div class="t-header text-padding-bottom">
                                Sorry !! No Question/Answer is found for the Category & Section you Searched. :(
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
