<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>404 - Fore Travel</title>
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css?v=0.26') }}">
</head>
<body class="unknown">
<header>
    <a class="logo" href="/">
        <img class="main-logo" src="{{ asset('img/main_logo.svg') }}" alt="">
        <div class="title">
            <span>FORETRAVEL</span>
            <hr>
            <span>MOTORCOACH</span>
        </div>
    </a>
</header>
<main>
    <h2>404 - page not found</h2>
</main>
</body>
<body>
