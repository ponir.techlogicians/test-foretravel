@extends('layouts.app')

@section('content')




    <div class="section-wrapper bg-layer">

        <section class="section available-inventory">
            <h1 class="section-headline">Member Profile</h1>
            <p class="section-info">You can change your personal info</p>

            @if (session('update-message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ session('update-message') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">

                        <form method="POST" id="profile_update_form" class="text-white" action="{{ route('motorcade-member-update') }}">


                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="username">User Name <span class="text-danger">*</span> (16 letter/number max)</label>
                                    <input type="text" name="username" class="form-control" id="username" maxlength="16" required="" value="{{ old('username', $member->username) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="register_password">Password (minimum 6 letter/number)</label>
                                    <input type="password" id="register_password" name="password" class="form-control" minlength="6">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">First Name </label>
                                    <input type="text" name="first_name" class="form-control" id="first_name"  value="{{ old('first_name', $member->first_name) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name"  value="{{ old('last_name', $member->last_name) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="spouse_name">Spouse Name</label>
                                    <input type="text" name="spouse_name" class="form-control" id="spouse_name" value="{{ old('spouse_name', $member->spouse_name) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">Email </label>
                                    <input type="text" name="register_email" class="form-control" id="email"  value="{{ old('email', $member->email) }}">

                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="address_line_1">Address Line 1 </label>
                                    <input type="text" name="address_line_1" class="form-control" id="address_line_1"  value="{{ old('address_line_1', $member->address_line_1) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="address_line_2">Address Line 2</label>
                                    <input type="text" name="address_line_2" class="form-control" id="address_line_2" value="{{ old('address_line_2', $member->address_line_2) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="city">City </label>
                                    <input type="text" name="city" class="form-control" id="city"  value="{{ old('city', $member->city) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="state">State </label>
                                    <input type="text" name="state" class="form-control" id="state"  value="{{ old('state', $member->state) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="zip">Zip </label>
                                    <input type="text" name="zip" class="form-control" id="zip"  value="{{ old('zip', $member->zip) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="phone_number">Phone</label>
                                    <input type="tel" name="phone_number" class="form-control" id="phone_number" value="{{ old('phone_number', $member->phone_number) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="cell_number">Cell</label>
                                    <input type="text" name="cell_number" class="form-control" id="cell_number" value="{{ old('cell_number', $member->cell_number) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="birthday">Birthday</label>
                                    <input type="date" name="birthday" class="form-control" id="birthday" value="{{ old('birthday', $member->birthday) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="spouse_birthday">Spouse Birthday</label>
                                    <input type="date" name="spouse_birthday" class="form-control" id="spouse_birthday" value="{{ old('spouse_birthday', $member->spouse_birthday) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="anniversary">Anniversary</label>
                                    <input type="date" name="anniversary" class="form-control" id="anniversary" value="{{ old('anniversary', $member->anniversary) }}">

                                </div>
                            </div>

                            <div class="alert alert-success">General Info</div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="coach_type">Coach Type </label>
                                    <input type="text" name="coach_type" class="form-control" id="coach_type"  value="{{ old('coach_type', $member->coach_type) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="purchase_date">Purchase Date</label>
                                    <input type="date" name="purchase_date" class="form-control" id="purchase_date" value="{{ old('purchase_date', $member->purchase_date) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="purchase_form">Purchase From</label>
                                    <input type="text" name="purchase_form" class="form-control" id="purchase_form" value="{{ old('purchase_form', $member->purchase_form) }}">

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="year_model">Year Model </label>
                                    <input type="text" name="year_model" class="form-control" id="year_model"  value="{{ old('year_model', $member->year_model) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="last_six_number_vin">Last Six Numbers of VIN </label>
                                    <input type="text" name="last_six_number_vin" class="form-control" id="last_six_number_vin"  value="{{ old('last_six_number_vin', $member->last_six_number_vin) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="complete_model_number">Complete Model Number  <em>(located on the sidewall I.D. plate next to the drivers chair)</em></label>
                                    <input type="text" name="complete_model_number" class="form-control" id="complete_model_number"  value="{{ old('complete_model_number', $member->complete_model_number) }}">

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label style="margin-bottom: 1em;">FMCA Member</label>
                                    <div class="form-group" style="display: flex">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="fmca_member" id="fmca_member_yes" value="1" {{ $member->fmca_member == 1? 'checked': '' }}>
                                            <label class="custom-control-label" for="fmca_member_yes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="fmca_member" id="fmca_member_no" value="0" {{ $member->fmca_member == 0? 'checked': '' }}>
                                            <label class="custom-control-label" for="fmca_member_no">No</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="fmca_number">FMCA Number</label>
                                    <input type="text" name="fmca_number" class="form-control" id="fmca_number" value="">

                                </div>
                            </div>

                            <div class="form-group d-flex justify-content-center">
                                <button class="btn btn-foretravel form-control" type="submit">Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


        </section>

    </div>

@endsection
