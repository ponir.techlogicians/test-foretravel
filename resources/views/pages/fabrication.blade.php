@extends('layouts.app')

@section('content')
	<div class="header-sections" style="background-image: url({{asset(__t('fabrication_' . $slug .'_header_background_image'))}})!important;">
	    <div class="header-section-layer">
	        <h1 class="d-flex justify-content-center">{{__t('fabrication_' . $slug .'_header_title')}}</h1>
	    </div>
	</div>
	<div class="page-body">
		<!-- Febrics -->
		<section class="section fabrications">
            <h1 class="section-headline">About</h1>
            <p class="section-info fabrication-section-info">
                {{__t('fabrication_' . $slug .'_page_section_description')}}
            </p>
            <p class="section-info fabrication-section-info">
                Name: {{__t('fabrication_' . $slug .'_page_section_name')}}
                Phone: {{__t('fabrication_' . $slug .'_page_section_phone')}}
                <a href="mailto:{{__t('fabrication_' . $slug .'_page_section_email')}}">
                    Email: {{__t('fabrication_' . $slug .'_page_section_email')}}
                </a>
            </p>
		</section>

		<!-- Gallary Section -->
		<section class="section fabrication-gallary">
            <h1 class="section-headline">Gallery</h1>
            <p class="section-info">{{__t('fabrication_' . $slug .'_gallery_section_description')}}</p>
            <div class="container">
                <div class="row">
                	<!-- 6,6,3,3,3,3 -->

                    @foreach($images as $key => $gallery)
                    <a href="{{ asset($gallery->gallery_image) }}" data-rel="gallery" class="fab-img col-{{ _getGallery($key, [6,6,3,3,3,3]) }}">
                        <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($gallery->gallery_image) }}" class="lazy fb-gallary" title="{{ $gallery->image_caption }}">
                    </a>
                    @endforeach
                </div>
            </div>

        </section>

        <!-- video section -->
        <section class="section fabrication-video">
            <h1 class="section-headline">Video</h1>
            <p class="section-info">{{__t('fabrication_' . $slug .'_video_section_description')}}</p>

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="video-gallery">
                            <div class="preview">

                            </div>
                            <ul class="thumbnails">
                                @foreach($videos as $key => $video)

                                    @if($video->gallery_video != "")
                                        <li class="thumbnail" data-src="{{ url($video->gallery_video) }}">
                                            <img src="{{ $video->video_thumb != ''? url($video->video_thumb) : url('img/febric/image 4.png') }}" alt="">
                                            <span class="caption">{{ $video->video_caption }}</span>
                                        </li>
                                    @else
                                        <li class="thumbnail" data-src="{{ _convertToYoutubeVideoUrl($video->youtube_video) }}">
                                            <img src="{{ $video->video_thumb != ''? url($video->video_thumb) : url('img/febric/image 4.png') }}" alt="">
                                            <span class="caption">{{ $video->video_caption }}</span>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</div>
@endsection

@section('script')
	<script>
	    $(document).ready(function () {
	    	if($('.fab-img').length){
	    		let interior = $('.fab-img').simpleLightbox({
	    		    disableScroll: false,
	    		    captionPosition: 'outside',
	    		    heightRatio: .7
	    		});
	    	}


	        $('body').on('click', '.video-gallery .thumbnails .thumbnail', function(){
	            const isActive = $(this).hasClass('active');

	            if(!isActive){
	                $('.video-gallery .preview').html('');
	                $('.video-gallery .preview').addClass('loading');

	                let url = $(this).attr('data-src');
	                let poster = $(this).find('img').attr('src');
	                let html;
	                let video = videoFinder(url);

	                if(video.video){
	                    html = `<video autobuffer autoloop loop controls poster="${poster}" src="${url}"></video>`;
	                } else if(video.image){
	                    html = `<img src="${url}">`;
	                } else {
	                    html = `<iframe src="${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
	                }

	                $('.video-gallery .preview').html(html);
	                $('.video-gallery .preview').removeClass('loading');

	                $('.video-gallery .thumbnails .thumbnail.active').removeClass('active');
	                $(this).addClass('active');
	            }

	        });

	        if($('.video-gallery .thumbnails .thumbnail').length){
	        	$('.video-gallery .thumbnails .thumbnail')[0].click();
	        }
	    });
	    function videoFinder(url){
	        let result = {
	            video : false,
	            image : false,
	            url : url
	        };

	        if(url.toLowerCase().match(/\.(mp4|m4a|m4v|f4v|f4a|m4b|m4r|f4b|mov|3gp|3gp2|3g2|3gpp|3gpp2|ogg|oga|ogv|ogx|wmv|wma|asf|webm|flv|avi|hdv)$/) != null){
	            result.video = true;
	        } else if(url.toLowerCase().match(/\.(jpeg|jpg|gif|png)$/) != null){
	            result.image = true;
	        }

	        return result;
	    }
	</script>
@endsection
