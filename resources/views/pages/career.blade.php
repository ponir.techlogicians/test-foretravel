@extends('layouts.app')
@section('content')
<div class="header-sections" style="background-image: url({{asset(__t('parts_department_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{'Work at Foretravel'}}</h1>
    </div>
</div>


{{-- page body --}}
<div class="page-body">
    <!-- parts department -->
    <section class="section">
	    <h1 class="section-headline">{{'Available Careers'}}</h1>
	    {{-- <p class="section-info">{{__t('parts_department_parts_department_info')}}</p> --}}
	    {{--<script id="gnewtonjs" type="text/javascript" src="//newton.newtonsoftware.com/career/iframe.action?clientId=8a7883d06c2c821b016c49d639490e44"></script>--}}
        <div style="height: 800px; max-width: calc(100% - 25px); padding-left: 20px; width: 650px; overflow: auto; -webkit-overflow-scrolling:touch;"><iframe src="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=7D4A773CE8FDA251042540DC35418ECB" style="width: 100%; height: 99%; border: none;"></iframe></div>
    </section>
</div>
@endsection
