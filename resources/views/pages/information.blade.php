@extends('layouts.app')
@section('content')

 <!-- header section -->
 <div class="header-sections" style="background-image: url({{asset(__t('information_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{__t('information_header_title')}}</h1>
    </div>
</div>


{{-- page body --}}
<div class="page-body">
    <!-- warranty information -->
    <section class="section warranty-information">
    <h1 class="section-headline">{{__t('information_warenty_information_title')}}</h1>
        <p class="section-info">{{__t('information_warenty_information_info')}}</p>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="documents">
                        @foreach($warrantyInformation as $data)
                        <div class="document-wrapper">
                            <div class="text">{{ $data->description }}</div>
                            <a href="{{ $data->file_path }}" target="_blank" class="document">
                                <span class="title">{{ $data->title }}</span>
                                <img src="{{ url('img/icon/service/pdf.svg') }}">
                                <span class="date">{{ $data->date }}</span>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section warranty-information">
        <h1 class="section-headline">{{__t('brochuries_information_title')}}</h1>
        <p class="section-info">{{__t('brochuries_information_info')}}</p>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="documents">
                            @foreach($brochure_information as $data)
                            <div class="document-wrapper">
                                <div class="text">{{ $data->title }}</div>
                                <a href="{{ $data->brochure_file }}" target="_blank" class="document">
                                    <span class="title">{{ $data->title }}</span>
                                    <img src="{{ url('img/icon/service/pdf.svg') }}">
                                    <span class="date">{{ $data->date }}</span>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <!-- helpful service -->
    <section class="section helpful-service">
    <h1 class="section-headline">{{__t('information_helpful_service_link_title')}}</h1>
    <p class="section-info">{{__t('information_helpful_service_link_info')}}</p>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="documents">
                        @foreach($helpfullLinks as $link)
                        <a href="{{ $link->url }}" class="document">
                            <span class="icon"><img src="{{ url('img/icon/service/quality.svg') }}"></span>
                            <span class="title">{{ $link->title }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- vendor contact -->
    <section class="section vendor-contact">
    <h1 class="section-headline">{{__t('information_vendor_contact_information_title')}}</h1>
    <p class="section-info">{{__t('information_vendor_contact_information_info')}}</p>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-vendor-contact">
                            <thead>
                            <tr>
                                <th scope="col">Component or Systems</th>
                                <th scope="col">Manufacturer</th>
                                <th scope="col">Warranty</th>
                                <th scope="col">Contact Number</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendorContactInfos as $info)
                            <tr>
                                <th scope="row">{{ $info->type }}</th>
                                <td>{{ $info->manufacturer }}</td>
                                <td>{{ $info->warranty }}</td>
                                <td>{{ $info->contact }}</td>
                            </tr>
                            @endforeach

                            {{--<tr>--}}
                                {{--<th scope="row">Air Conditioners</th>--}}
                                {{--<td>--}}
                                {{--Coleman/RV Products--}}
                                {{--Dometic--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--3 Years Parts & Labor *--}}
                                {{--3 Years Parts & Labor *--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--800-535-5560--}}
                                    {{--800-535-5560--}}
                                {{--</td>--}}
                            {{--</tr>--}}

                            {{--<tr>--}}
                                {{--<th scope="row">Components or Systems</th>--}}
                                {{--<td>Carefree of Colorado</td>--}}
                                {{--<td>3 Years Parts & Labor *</td>--}}
                                {{--<td>800-535-5560</td>--}}
                            {{--</tr>--}}

                            {{--<tr>--}}
                                {{--<th scope="row">Awnings</th>--}}
                                {{--<td>Meritor/Wabco</td>--}}
                                {{--<td>3 Years Parts & Labor *</td>--}}
                                {{--<td>800-535-5560</td>--}}
                            {{--</tr>--}}

                            {{--<tr>--}}
                                {{--<th scope="row">Components or Systems</th>--}}
                                {{--<td>Meritor/Wabco</td>--}}
                                {{--<td>3 Years Parts & Labor *</td>--}}
                                {{--<td>800-535-5560</td>--}}
                            {{--</tr>--}}

                            </tbody>
                        </table>
                    </div>

                    <div class="load-wrapper">
                        <span class="load-more-contact">
                            <img src="{{ url('img/icon/service/arrow-bottom.svg') }}">
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>


@endsection
<script>



</script>

