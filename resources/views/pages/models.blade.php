@extends('layouts.app')

@section('content')

    <!-- Cover -->
    <section class="cover" style="background-image: url({{ asset(__t('model_' . $coachSlug . 'header_background_image')) }});">
        <span class="title">{{ __t('model_' . $coachSlug . 'header_title') }}</span>
        <img src="{{ asset(__t('model_' . $coachSlug . 'header_image')) }}" class="lazy">
    </section>


    <!--wrapper -->
    <section class="wrapper bg-layer">

        <div class="scroll-area">

            <!-- sticky sub menu -->
            <div class="menu">
                <div class="container">
                    <nav id="model_page">
                        <ul class="menu-list">
                            @if(count($floorplan))
                                <li class="nav-item">
                                    <a class="nav-link active" href="#floorplans"><span>Floorplans</span></a>
                                </li>
                            @endif
                            @if(count($floorplanVideos) && __t('model_' . $coachSlug . 'floorplan_videos_active'))
                                <li class="nav-item">
                                    <a class="nav-link" href="#videos"><span>Videos</span></a>
                                </li>
                            @endif
                            @if(count($colorOptions->slides))
                            <li class="nav-item">
                                <a class="nav-link" href="#color_options"><span>Exterior Options</span></a>
                            </li>
                            @endif
                            @if(isset($interiorColorOptions) && count($interiorColorOptions))
                                    {{--&& ($coachSlug == '' || $coachSlug == 'presidential_')--}}
                            <li class="nav-item">
                                <a class="nav-link" href="#interior_color_options"><span>Interior Options</span></a>
                            </li>
                            @endif
                            @if(count($virtualTours) && __t('model_' . $coachSlug . 'virtual_tour_active'))
                                <li class="nav-item">
                                    <a class="nav-link" href="#virtual_tour"><span>Virtual Tour</span></a>
                                </li>
                            @endif

                            <li class="nav-item">
                                <a class="nav-link" href="#key_features"><span>Safety Features</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#specifications"><span>Specifications</span></a>
                            </li>

                            @if(count($interiorGallery->slides) || count($exteriorGallery->slides))
                            <li class="nav-item">
                                <a class="nav-link" href="#gallery"><span>Gallery</span></a>
                            </li>
                            @endif
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="#videos"><span>Videos</span></a>--}}
                            {{--</li>--}}
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url(__t('model_' . $coachSlug . 'brochure_path')) }}" target="_blank"><span>Brochure</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Floorplan -->
            @if(isset($floorplan) && count($floorplan))
            <section class="section floorplans" id="floorplans">
                <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'floorplan_title') }}</h1>
                <p class="section-info">{{ __t('model_' . $coachSlug . 'floorplan_description') }}</p>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="slider">
                                <div id="floorplansIndicators" class="carousel slide" data-ride="carousel">
                                    @if(count($floorplan) > 1)
                                     <ol class="carousel-indicators">
                                        @foreach($floorplan as $item)
                                         <li data-target="#floorplansIndicators" rel="prettyPhoto[gallery1]"  data-slide-to="{{$loop->index}}" class="@if($loop->first) active @endif">{{ $item->title }}</li>
                                        @endforeach
                                     </ol>
                                     @endif

                                    <div class="carousel-inner">

                                        @foreach($floorplan as $item)
                                            <div class="carousel-item @if($loop->first) active @endif floorplan popup-content-parent">
                                                <div class="row">
                                                    <div class="col-md-6 one floorplan-items">
                                                        <a href="{{ url($item->image) }}">
                                                            <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ url($item->image) }}" class="floorplan-image lazy popup-content-image" title="{{ $item->title }}">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6 two">
                                                        <h3 class="floorplan-title popup-content-title">{{ $item->title }}</h3>
                                                        @if(count($item->floorPlanItems))
                                                        <ul class="floorplan-spec popup-content-body">
                                                            @foreach($item->floorPlanItems as $i)
                                                            <li>{{ $i->text }}</li>
                                                            @endforeach
                                                        </ul>
                                                        <a href="javascript:;" class="more open-popup">More</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <a class="carousel-control-prev" href="#floorplansIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#floorplansIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif

            {{-- videos --}}
            @if(isset($floorplanVideos) && count($floorplanVideos) && __t('model_' . $coachSlug . 'floorplan_videos_active'))
            <section class="section slider-video" id="videos">
                <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'floorplan_videos_title') }}</h1>
                <p class="section-info">{{ __t('model_' . $coachSlug . 'floorplan_videos_description') }}</p>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="display:none;margin:0 auto;" class="html5gallery" data-width="1100" data-height="400" data-skin="gallery" data-responsive="true"  data-resizemode="fill">

                                @foreach($floorplanVideos as $videos)
                                <!-- Add images to Gallery -->
                                    @if($videos->youtube != "")
                                         <a href="{{ _convertToYoutubeVideoUrl($videos->youtube) }}"><img src="{{ url('img/photo/1575809990.png') }}" alt=""></a>
                                    @elseif($videos->upload != "")
                                        <a href="{{ asset($videos->upload) }}"><img src="{{ url('img/photo/1575809990.png') }}" alt=""></a>
                                    @endif
                                @endforeach

                                <!-- Add videos to Gallery -->
                                <!-- Big Buck Bunny Copyright 2008, Blender Foundation http://www.bigbuckbunny.org -->
                                {{--<a href="http://www.youtube.com/embed/YE7VzlLtp-4"><img src="/img/photo/1575809990.png" alt="ForeTravel Videos"></a>--}}


                                {{--<!-- Add Youtube video to Gallery -->--}}
                                {{--<a href="http://www.youtube.com/embed/YE7VzlLtp-4"><img src="/img/photo/1574859042.png" alt="ForeTravel Videos"></a>--}}


                                {{--<!-- Add Vimeo video to Gallery -->--}}
                                {{--<a href="/img/photo/Big_Buck_Bunny.mp4"><img src="/img/photo/1574859042.png" alt="ForeTravel Videos"></a>--}}


                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif

            <!-- Color Options -->
            @if(count($colorOptions->slides))
            <section class="section color-options" id="color_options">
                <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'color_options_title') }}</h1>
                <p class="section-info">{{ __t('model_' . $coachSlug . 'color_options_description') }}</p>
                <div class="container">
                    <div class="row">
                        @if(isset($colorOptions->slides[0]))
                                <div class="@if(count($colorOptions->slides) > 1) col-md-6 @else col-md-12 @endif one">


                                <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($colorOptions->slides[0]->file_path) }}" class="main-image lazy">
                                <h1 class="section-headline color-option-headline main-title">{{ $colorOptions->slides[0]->title }}</h1>
                            </div>
                            @if(count($colorOptions->slides) > 1)
                            <div class="col-md-6 two">
                                <ul class="model-images">
                                    @foreach($colorOptions->slides as $slide)
                                    <li class="model-image">
                                        <div  class=" {{$loop->first?'active':''}}">
                                            <a href="{{ asset($slide->file_path) }}" data-rel="interior" class="image-show image-wrapper">
                                                <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($slide->file_path) }}" class="lazy" title="{{ $slide->title }}">
                                            </a>
                                            {{-- <img src="{{ asset($slide->file_path) }}" title="{{ $slide->title }}" class="lazy"> --}}
                                        </div>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                            @endif
                        @endif
                    </div>
                </div>
            </section>
            @endif


            <!-- Interior Color Options -->
            @if(isset($interiorColorOptions) && count($interiorColorOptions))
                {{--&& ($coachSlug == '' || $coachSlug == 'presidential_')--}}
                <section class="section floorplans color-options interior-color-options" id="interior_color_options">
                    <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'interior_color_options_title') }}</h1>
                    <p class="section-info">{{ __t('model_' . $coachSlug . 'interior_color_options_description') }}</p>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider">
                                    <div id="floorplansIndicatorss" class="carousel slide" data-ride="carousel" data-interval="false">
                                        @if(count($interiorColorOptions) > 1)
                                            <ol class="carousel-indicators">
                                                @foreach($interiorColorOptions as $category => $option)
                                                    <li data-target="#floorplansIndicatorss" data-slide-to="{{$loop->index}}" class="@if($loop->first) active @endif">{{ $category }}</li>
                                                @endforeach
                                            </ol>
                                        @endif

                                        <div class="carousel-inner">

                                            @foreach($interiorColorOptions as $type => $items)
                                                <div class="carousel-item @if($loop->first) active @endif floorplan popup-content-parent">
                                                    <div class="row">
                                                        @if(isset($items[0]))
                                                                <div class="@if(count($items) > 1) col-md-6 @else col-md-12 @endif one  interior-color-options-items">


                                                                    <a href="{{ asset($items[0]->file_path) }}" class="main-image-anchor">
                                                                        <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($items[0]->file_path) }}" class="main-image lazy">
                                                                    </a>
                                                                    <h1 class="section-headline  color-option-headline main-title">{{ $items[0]->name }}</h1>
                                                                </div>
                                                            @if(count($items) > 1)
                                                                <div class="col-md-6 two">
                                                                    <ul class="model-images">
                                                                        @foreach($items as $item)
                                                                            <li class="model-image">
                                                                                <div class="{{$loop->first?'active':''}} interior-color-options-items">
                                                                                    <a class="image-show image-wrapper" href="{{ asset($item->file_path) }}">
                                                                                        <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($item->file_path) }}" title="{{ $item->name }}" class="lazy">
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif

            <!-- Virtual Tour -->
            @if(count($virtualTours) && __t('model_' . $coachSlug . 'virtual_tour_active'))
                <section class="section virtual-tour-section" id="virtual_tour">
                    <h1 class="section-headline popup-content-title">{{ __t('model_' . $coachSlug . 'virtual_tour_title') }}</h1>
                    <p class="section-info">{{ __t('model_' . $coachSlug . 'virtual_tour_description') }}</p>
                    <div class="video-gallery">
                        <div class="preview">
                            <iframe
                                width='853'
                                height='480'
                                src='{{ $virtualTours[0]->tour_url }}'
                                frameborder='0'
                                allowfullscreen
                                allow='xr-spatial-tracking'>
                            </iframe>
                            <p class="description">{{$virtualTours[0]->description}}</p>
                        </div>
                        <ul class="thumbnails">
                            @foreach($virtualTours as $virtualTour)
                                <li class="thumbnail"
                                    data-src="{{ $virtualTour->tour_url }}">
                                    <img src="{{ url($virtualTour->image_thumb != ""? $virtualTour->image_thumb : 'img/no-image.png') }}" alt="">
                                    <span class="caption">{{ substr($virtualTour->title,0,32) }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </section>
            @endif


            <!-- Key Features -->
            <section class="section key-features popup-content-parent" id="key_features">
                <h1 class="section-headline popup-content-title">{{ __t('model_' . $coachSlug . 'key_features_title') }}</h1>
                <p class="section-info">{{ __t('model_' . $coachSlug . 'key_features_description') }}</p>
                <div class="container">
                    <div class="row">
                        <div class="col-6 one">
                            {{--<ul class="key-features-list">--}}
                                {{--<li>12 volt accessory receptacle</li>--}}
                                {{--<li>Dual 1 amp USB charge receptacle</li>--}}
                                {{--<li>Battery boost switch / auto-charge</li>--}}
                                {{--<li>Dual 1 amp USB charge receptacle</li>--}}
                            {{--</ul>--}}
                            <div class="popup-content-body">
                                {!! __t('model_' . $coachSlug . 'key_features_items')  !!}
                            </div>
                            <a href="javascript:;" class="more open-popup">More</a>
                        </div>
                        <div class="col-6 two">
                            <div class="images">
                                <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('model_' . $coachSlug . 'key_features_image')) }}" class="lazy popup-content-image">
                                {{--<img src="{{ asset('img/model/collage.png') }}">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <!-- Specifications -->
            <section class="section specifications popup-content-parent" id="specifications">
                <h1 class="section-headline popup-content-title">{{ __t('model_' . $coachSlug . 'specifications_title') }}</h1>
                <p class="section-info">{{ __t('model_' . $coachSlug . 'specifications_description')  }}</p>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 specifications-details popup-content-body">{!! __t('model_' . $coachSlug . 'specifications_column_description')  !!}</div>
                        <div class="col-12 text-center "><a href="javascript:;" class="more open-popup">More</a></div>
                    </div>
                </div>
            </section>

            <!-- Gallery -->
            @if(count($interiorGallery->slides) || count($exteriorGallery->slides))
            <section class="section gallery" id="gallery">
                <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'gallery_title') }}</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 interior">
                            <h2 class="sub-headline">{{ __t('model_' . $coachSlug . 'gallery_type_left') }}</h2>
                            <!-- 4,4,4,8,4,3,3,6 -->
                            <div class="gallery-images row">
                                @foreach($interiorGallery->slides as $key => $gallery)
                                <a href="{{ asset($gallery->file_path) }}" data-rel="interior" class="col-{{ _getGallery($key) }}">
                                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($gallery->file_path) }}" class="lazy" title="{{ $gallery->caption }}">
                                </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6 exterior">
                            <h2 class="sub-headline">{{ __t('model_' . $coachSlug . 'gallery_type_right') }}</h2>
                            <!-- 4,4,4,8,4,3,3,6 -->
                            <div class="gallery-images row">
                                @foreach($exteriorGallery->slides as $key => $gallery)
                                    <a  href="{{ asset($gallery->file_path) }}" data-rel="exterior" class="col-{{ _getGallery($key) }}">
                                        <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset($gallery->file_path) }}" class="lazy" title="{{ $gallery->caption }}">
                                    </a>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif
            <!-- Video -->
            {{--<section class="section videos" id="videos">--}}
                {{--<h1 class="section-headline">{{ __t('model_' . $coachSlug . 'video_title') }}</h1>--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12 video-wrapper">--}}
                            {{--<video name="Video Name" loop src="{{ asset(__t('model_' . $coachSlug . 'video_path')) }}#t=4" id="model_video">--}}
                            {{--</video>--}}
                            {{--<div class="control pause">--}}
                                {{--<img src="{{ asset('img/icon/002-youtube.svg') }}" class="play-icon" onclick="modelVideoControl()">--}}
                                {{--<img src="{{ asset('img/icon/youtube-white.png') }}" class="pause-icon" onclick="modelVideoControl()">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}
        </div>

        <!-- Model -->
        <section class="section models" id="models">
            {{-- <h1 class="section-headline">{{ __t('model_' . $coachSlug . 'title') }}</h1>
            <p class="section-info">
                <span>{{ __t('model_' . $coachSlug . 'short_description') }}</span>
                {{--<a class="btn btn-inform"  href="javascript:;">Keep me inform</a>--}}
            {{-- </p> --}}
            {{-- <div class="container">
                <div class="row">
                    {{--<div class="col-6 model-wrapper">--}}
                        {{--<div class="model">--}}
                            {{--<div class="model-title">{{ __t('model_' . $coachSlug . 'coach_left_title') }}</div>--}}
                            {{--<div class="model-image"><img src="{{ asset(__t('model_' . $coachSlug . 'coach_left_image')) }}" class="lazy" alt=""></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-6 model-wrapper">--}}
                        {{--<div class="model mirror">--}}
                            {{--<div class="model-title">{{ __t('model_' . $coachSlug . 'coach_right_title') }}</div>--}}
                            {{--<div class="model-image"><img src="{{ asset(__t('model_' . $coachSlug . 'coach_right_image')) }}" class="lazy" alt=""></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{-- </div> --}}
            {{-- </div> --}}

            {{--<div class="section contact-us">--}}
                {{--<h1 class="section-headline">{{ __t('contact_us_title') }}</h1>--}}
                {{--<p class="section-info">{{ __t('contact_us_address') }}</p>--}}
                {{--<div class="container">--}}
                    {{--<div class="">--}}
                        {{--<div class="col-12">--}}
                            {{--<div class="map">--}}
                                {{--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d395398.3605075727!2d90.39151019708369!3d22.969212893344615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1573476066601!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>--}}
                                {{--<div id ="map" ></div>--}}
                                {{--<input type="hidden" id="mapCoordinates" value="{{ __t('contact_us_map_coordinates') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="contact-info-text">{{ __t('contact_us_description') }}</div>--}}
                                {{--<ul class="contact-info-list">--}}
                                    {{--<li class="contact-info">--}}
                                    {{--<span class="icon">--}}
                                        {{--<img src="{{ asset('img/icon/gmail.svg') }}">--}}
                                    {{--</span>--}}
                                        {{--<span class="text">{{ __t('contact_us_email') }}</span>--}}
                                    {{--</li>--}}
                                    {{--<li class="contact-info">--}}
                                    {{--<span class="icon">--}}
                                        {{--<img src="{{ asset('img/icon/call-answer.svg') }}">--}}
                                    {{--</span>--}}
                                        {{--<span class="text">{{ __t('contact_us_phone') }}</span>--}}
                                    {{--</li>--}}
                                    {{--<li class="contact-info">--}}
                                    {{--<span class="icon">--}}
                                        {{--<img src="{{ asset('img/icon/website.svg') }}">--}}
                                    {{--</span>--}}
                                        {{--<span class="text">{{ __t('contact_us_website') }}</span>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<form class="contact-form" id="contactFrom" method="post" action="{{ url('contact') }}">--}}

                                    {{--@csrf--}}
                                    {{--<div class="form-group">--}}
                                        {{--<input type="text" name="name" class="form-control" placeholder="Name">--}}

                                        {{--@error('name')--}}
                                        {{--<div  class="help-block">{{ $message }}</div>--}}
                                        {{--@enderror--}}

                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<input type="email" name="email" class="form-control" placeholder="Email">--}}

                                        {{--@error('email')--}}
                                        {{--<div  class="help-block">{{ $message }}</div>--}}
                                        {{--@enderror--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<input type="text" name="interested_in" class="form-control" placeholder="Which unit are you interested in?">--}}

                                        {{--@error('interested_in')--}}
                                        {{--<div  class="help-block">{{ $message }}</div>--}}
                                        {{--@enderror--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<textarea class="form-control" name="message" placeholder="Message" rows="3"></textarea>--}}

                                        {{--@error('message')--}}
                                        {{--<div  class="help-block">{{ $message }}</div>--}}
                                        {{--@enderror--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group d-flex justify-content-center">--}}
                                        {{--<button class="btn btn-primary" type="submit">Submit</button>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


            <div class="container">
                <div class="row">
                    <div class="col-12 model-description">
                           <span>{{ __t('model_' . $coachSlug . 'long_description') }}</span>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <div class="go-to-top">
        <img src="{{asset('img/icon/service/arrow-bottom.svg')}}">
    </div>

    <script type="text/javascript">
        function modelVideoControl() {
            // Get the video
            var video = document.getElementById("model_video");

            // Get the button
            var btn = document.querySelector('.video-wrapper .control');

            console.log(video);

            if (video.paused) {
                video.play();
                // video.controls = true;
                btn.classList.add("play");
                btn.classList.remove("pause");
            } else {
                video.pause();
                // video.controls = false;
                btn.classList.add("pause");
                btn.classList.remove("play");
            }
        }

    </script>

@endsection



@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            if($('.interior .gallery-images a').length){
                let interior = $('.interior .gallery-images a').simpleLightbox({
                    disableScroll: false,
                    captionPosition: 'outside',
                    heightRatio: .7
                });
            }

            // color option image show
            if($('.image-show').length){
                let interior = $('.image-show').simpleLightbox({
                    disableScroll: false,
                    captionPosition: 'outside',
                    heightRatio: .7
                });
            }

            if($('.exterior .gallery-images a').length){
                let exterior = $('.exterior .gallery-images a').simpleLightbox({
                    disableScroll: false,
                    captionPosition: 'outside',
                    heightRatio: .7
                });
            }

            // if floorplan item exists.
            if ($('.floorplan-items a').length){
                let floorplan = $('.floorplan-items a').simpleLightbox({
                    disableScroll: false,
                    captionPosition: 'outside',
                    heightRatio: .7
                });
            }

            // if interior color option item exists.
            if ($('.interior-color-options-items a').length){
                let intColorOptions = $('.interior-color-options-items a').simpleLightbox({
                    disableScroll: false,
                    captionPosition: 'outside',
                    heightRatio: .7
                });
            }

            $('body').on('click', '.video-gallery .thumbnails .thumbnail', function(){
                const isActive = $(this).hasClass('active');

                if(!isActive){
                    $('.video-gallery .preview').html('');
                    $('.video-gallery .preview').addClass('loading');

                    let url = $(this).attr('data-src');
                    let description = $(this).attr('data-description');
                    let poster = $(this).find('img').attr('src');
                    let html;
                    let video = videoFinder(url);

                    if(video.video){
                        html = `<video autobuffer autoloop loop controls poster="${poster}" src="${url}"></video>`;
                    } else if(video.image){
                        html = `<img src="${url}">`;
                    } else {
                        html = `<iframe src="${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;

                        if(description){
                            html += `<p class="description">${description}</p>`;
                        }
                    }

                    $('.video-gallery .preview').html(html);
                    $('.video-gallery .preview').removeClass('loading');

                    $('.video-gallery .thumbnails .thumbnail.active').removeClass('active');
                    $(this).addClass('active');
                }

            });

            if($('.video-gallery .thumbnails .thumbnail').length){
                $('.video-gallery .thumbnails .thumbnail')[0].click();
            }
        });

        $(window).on('load',function(){
            fixCoverImageOverlap();
        });

        $(window).on('resize',function(){
            fixCoverImageOverlap();
        });

        let positionChangeCounter = 0;
        function fixCoverImageOverlap (){
            positionChangeCounter++;
            coverInterval = setInterval(function(){
                title_margin_top = $('.cover .title').css('margin-top')
                title_margin_top = Number(title_margin_top.slice(0,title_margin_top.indexOf('px')))
                title_area = $('.cover .title').height() + title_margin_top;
                image_position = $('.cover img').position().top
                if(title_area >= image_position-10){
                    top_css = $('.cover img').css('top')
                    top_css = Number(top_css.slice(0,top_css.indexOf('px')))
                    $('.cover img').css('top', top_css + 5 + 'px')
                } else {
                    clearInterval(coverInterval);
                    if(positionChangeCounter > 1){
                        makeCoverImageTitleCloser();
                    }
                }
            },100)
        }

        function makeCoverImageTitleCloser (){

            if(window.innerWidth > 991) {
                coverInterval = setInterval(function(){
                    title_margin_top = $('.cover .title').css('margin-top')
                    title_margin_top = Number(title_margin_top.slice(0,title_margin_top.indexOf('px')))
                    title_area = $('.cover .title').height() + title_margin_top;
                    image_position = $('.cover img').position().top
                    if(title_area + 50 < image_position){
                        top_css = $('.cover img').css('top')
                        top_css = Number(top_css.slice(0,top_css.indexOf('px')))
                        $('.cover img').css('top', top_css - 5 + 'px')
                    } else {
                        clearInterval(coverInterval)
                    }
                },100)
            }

        }

        function videoFinder(url){
            let result = {
                video : false,
                image : false,
                url : url
            };

            if(url.toLowerCase().match(/\.(mp4|m4a|m4v|f4v|f4a|m4b|m4r|f4b|mov|3gp|3gp2|3g2|3gpp|3gpp2|ogg|oga|ogv|ogx|wmv|wma|asf|webm|flv|avi|hdv)$/) != null){
                result.video = true;
            } else if(url.toLowerCase().match(/\.(jpeg|jpg|gif|png)$/) != null){
                result.image = true;
            }

            return result;
        }

    </script>
@endsection
