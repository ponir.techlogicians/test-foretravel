@extends('layouts.app')
@section('content')
<div class="header-sections" style="background-image: url({{asset(__t('service_department_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{__t('service_department_header_title')}}</h1>
    </div>
</div>

{{-- page body --}}
<div class="page-body">
    <!-- service department -->
    <section class="section service-department">
    <h1 class="section-headline">{{__t('service_department_service_department_title')}}</h1>
    <p class="section-info">{{__t('service_department_service_department_info')}}</p>
        <div class="container team-text">
            <div class="row">
                <div class="col-md-7 image">
                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('service_department_service_department_team_image'))}}" class="team-image lazy">
                </div>
                <div class="col-md-5 info">
                    <div class="details">
                        <p class="body">{{__t('service_department_service_department_team_info')}}</p>
                        <div class="header">
                            {{ __t('service_department_service_department_team_appointment') }}
                        </div>
                        {{--<a href="javascript:;" class="footer">--}}
                            {{--<img src="{{ url('img/icon/facebook.svg') }}">--}}
                            {{--<div class="text">Join Foretravel Service on Facebook</div>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>

    </section>

    {{-- special services --}}

    <section class="section service-specials">
        <h1 class="section-headline">{{__t('service_department_service_specials_title')}}</h1>
        <p class="section-info">{{__t('service_department_service_specials_info')}}</p>

        <div class="container">
            <div class="row service-special-list">
                <div class="col-12">
                    <div class="service-special">

                        @foreach($serviceSpecials as $special)
                        <div class="service">
                            <div class="image">
                                <a href="{{ url($special->image) }}" data-rel="spe_image" class="service-special-image-show">
                                    <img src="{{ url($special->image) }}" data-src="{{ url($special->image) }}" class="lazy" title="">
                                </a>
                            </div>
                            <div class="info">
                                <h3 class="name">{{ $special->description }}</h3>

                            </div>
                        </div>
                        @endforeach
                        {{--<div class="service">--}}
                            {{--<div class="image">--}}
                                {{--<a href="img/no-image.png" data-rel="spe_image" class="service-special-image-show">--}}
                                    {{--<img src="img/no-image.png" data-src="img/no-image.png" class="lazy" title="">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="info">--}}
                                {{--<h3 class="name">Get up to $100 back by mail on 4 bridgestone tires.</h3>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="service">--}}
                            {{--<div class="image">--}}
                                {{--<a href="img/no-image.png" data-rel="spe_image" class="service-special-image-show">--}}
                                    {{--<img src="img/no-image.png" data-src="img/no-image.png" class="lazy" title="">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="info">--}}
                                {{--<h3 class="name">Get up to $100 back by mail on 4 bridgestone tires.</h3>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="service">--}}
                            {{--<div class="image">--}}
                                {{--<a href="img/no-image.png" data-rel="spe_image" class="service-special-image-show">--}}
                                    {{--<img src="img/no-image.png" data-src="img/no-image.png" class="lazy" title="">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="info">--}}
                                {{--<h3 class="name">Get up to $100 back by mail on 4 bridgestone tires.</h3>--}}

                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>

        @if(count($serviceSpecials))
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <button class="slick-slider-prev"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-left" class="svg-inline--fa fa-chevron-circle-left fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zM142.1 273l135.5 135.5c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L226.9 256l101.6-101.6c9.4-9.4 9.4-24.6 0-33.9l-17-17c-9.4-9.4-24.6-9.4-33.9 0L142.1 239c-9.4 9.4-9.4 24.6 0 34z"/></svg></button>
                    <button class="slick-slider-next"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"/></svg></button>
                </div>
            </div>
        </div>
        @endif

    </section>


    <!-- our team -->
    <section class="section our-special-team">
        <h1 class="section-headline">Our Team</h1>
        <p class="section-info">Our mission is to provide exceptional service by keeping our customers interest at heart so they will enjoy their motorcoach for many years to come. Put your coach in our hands. After all.... we built it. Since 1967 we’ve been here to take care of your needs one knows your coach better than us. From oil & generator service to appliance & component repair on all makes and models, you can count on Foretravel of Texas.</p>

        <div class="container team-slider">
            <div class="row">
                <div class="col-12">
                </div>
                <div class="col-12">
                    <div class="team-members">
                        @foreach($serviceDepartment as $member)
                            <div class="member">
                                <div class="image">
                                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(isset($member->photo)? $member->photo : 'assets/img/icon/user.png') }}" class="lazy">

                                </div>
                                <div class="info">
                                    <h3 class="name">{{ $member->name }}</h3>
                                    <span class="position">{{ $member->designation }}</span><br>
                                    <span class="position">{{ $member->fax }} Ext {{ $member->ext }}</span><br>

                                    <div class="email">
                                        <a style="color: #003470;text-decoration:none;font-size:10px" href="mailto:{{ $member->email }}?Subject=Hello%20again" target="_top">
                                            {{ $member->email }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <button class="slick-slider-prev"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-left" class="svg-inline--fa fa-chevron-circle-left fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zM142.1 273l135.5 135.5c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L226.9 256l101.6-101.6c9.4-9.4 9.4-24.6 0-33.9l-17-17c-9.4-9.4-24.6-9.4-33.9 0L142.1 239c-9.4 9.4-9.4 24.6 0 34z"/></svg></button>
                    <button class="slick-slider-next"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"/></svg></button>
                </div>
            </div>
        </div>
    </section>

    <!-- appoinment -->
    <section class="section appoinment">
    <h1 class="section-headline">{{__t('service_department_service_department_apoinment_title')}}</h1>
    <p class="section-info">{{__t('service_department_service_department_apoinment_info')}}</p>
    <div class="container">
        <div class="row contact-person-list">
            @foreach($appointmentMembers as $member)
            <div class="col-lg-3 col-sm-6 contact-person">
                <div class="image">
                    <div class="email">
                        <a  href="mailto:{{$member->email}}?Subject=Hello%20again" target="_top">
                            <img src="{{ asset(isset($member->photo)? $member->photo : 'assets/img/icon/user.png') }}" class="lazy">
                        </a>
                    </div>
                    {{-- <img src="{{ asset(isset($member->photo)? $member->photo : 'assets/img/icon/user.png') }}" class="lazy"> --}}
                </div>
                <div class="info">
                    <div class="name">{{ $member->name }}</div>
                    <div class="username">@ ext {{ $member->ext }}</div>
                    <div class="email">
                        <a href="mailto:{{$member->email}}?Subject=Hello%20again" target="_top">
                            {{ $member->email }}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </section>
</div>
@endsection
