@extends('layouts.app')
@section('content')
<div class="header-sections" style="background-image: url({{asset(__t('parts_department_header_image'))}})!important;">
    <div class="header-section-layer">
    <h1 class="d-flex justify-content-center">{{__t('parts_department_header_title')}}</h1>
    </div>
</div>


{{-- page body --}}
<div class="page-body">
    <!-- parts department -->
    <section class="section parts-department">
    <h1 class="section-headline">{{__t('parts_department_parts_department_title')}}</h1>
    <p class="section-info">{{__t('parts_department_parts_department_info')}}</p>
        <div class="container team-text">
            <div class="row">
                <div class="col-md-7 image">
                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(__t('parts_department_team_image'))}}" class="team-image lazy">
                </div>
                <div class="col-md-5 info">
                    <div class="details">
                        <h3 class="header">{{ __t('parts_department_team_info_header') }}</h3>
                        <p class="body">{{ __t('parts_department_team_info_body') }}</p>
                        <div class="header">
                            {{ __t('parts_department_team_info_appointment') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container team-slider">
            <div class="row">
                <div class="col-12">
                    <div class="team-members">
                        @foreach($partsDepartment as $member)
                            <div class="member">
                                <div class="image">
                                    <img src="{{url('/img/loading.svg')}}?v=1" data-src="{{ asset(isset($member->photo)? $member->photo : '') }}" class="lazy">
                                </div>
                                <div class="info">
                                    <h3 class="name">{{ $member->name }}</h3>
                                    <div class="position">{{ $member->designation }}</div>
                                    <div><div class="parts-email">
                                        <a style="color:#003470" href="mailto:{{$member->email}}?Subject=Hello%20again" target="_top">
                                            {{ $member->email }}
                                        </a>
                                    </div></div>
                                    <div class="fax">{!!$member->job_description!!}</div>
                                    <div class="fax">{{$member->fax}}</div>
                                    
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <button class="slick-slider-prev"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-left" class="svg-inline--fa fa-chevron-circle-left fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zM142.1 273l135.5 135.5c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L226.9 256l101.6-101.6c9.4-9.4 9.4-24.6 0-33.9l-17-17c-9.4-9.4-24.6-9.4-33.9 0L142.1 239c-9.4 9.4-9.4 24.6 0 34z"/></svg></button>
                    <button class="slick-slider-next"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-right" class="svg-inline--fa fa-chevron-circle-right fa-w-16" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm113.9 231L234.4 103.5c-9.4-9.4-24.6-9.4-33.9 0l-17 17c-9.4 9.4-9.4 24.6 0 33.9L285.1 256 183.5 357.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L369.9 273c9.4-9.4 9.4-24.6 0-34z"/></svg></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
