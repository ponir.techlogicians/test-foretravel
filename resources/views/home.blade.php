@extends('layouts.app')

@section('content')

{{--    {{ print_r($contents) }}--}}

    <!--slider section-->
    <div class="slider">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
               {{--  @foreach($videoSlides as $key => $slide)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="@if($loop->first) active @endif"></li>
                @endforeach --}}
            </ol>
            <div class="carousel-inner">
                {{-- @foreach($videoSlides as $slide)
                <div class="carousel-item @if($loop->first) active @endif">
                    <div class="overlay">
                        <div class="text-info">
                            <h2>{{ $slide->title }}</h2>
                             <a class="btn btn-inform"  href="javascript:;">{{ __t('main_button') }}</a>
                        </div>
                    </div>
                    <video autoplay muted loop name="{{ $slide->title }}" src="{{ asset($slide->file_path) }}"></video>
                </div>
                @endforeach --}}
                {{--<div class="carousel-item">--}}
                    {{--<div class="overlay">--}}
                        {{--<div class="text-info">--}}
                            {{--<h2>House of FORETRAVEL</h2>--}}
                            {{--<a class="btn btn-inform"  href="javascript:;">{{ $contents['main_button'] }}</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<video autoplay muted loop name="Video Name" src="{{ asset('video/REALM-FS6.mov') }}"></video>--}}
                {{--</div>--}}
                {{--<div class="carousel-item">--}}
                    {{--<div class="overlay">--}}
                        {{--<div class="text-info">--}}
                            {{--<h2>House of FORETRAVEL</h2>--}}
                            {{--<a class="btn btn-inform"  href="javascript:;">Keep me inform</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<video autoplay muted loop name="Video Name" src="{{ asset('video/REALM-FS6.mov') }}"></video>--}}
                {{--</div>--}}
            </div>
            <!--                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">-->
            <!--                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
            <!--                    <span class="sr-only">Previous</span>-->
            <!--                </a>-->
            <!--                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">-->
            <!--                    <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
            <!--                    <span class="sr-only">Next</span>-->
            <!--                </a>-->
        </div>
    </div>

    <div class="home-wrapper bg-layer">
        <div class="section luxury-coach">
            <h1 class="section-headline">{{ __t('coach_luxury_title') }}</h1>
            <p class="section-info">{{ __t('coach_luxury_motto') }}</p>
            <div class="coach-slider">
                @foreach($luxuryCoaches->slide as $slide)
                    <img src="{{ asset($slide->file_path) }}">
                @endforeach

            </div>
        </div>
        <div class="section featured-coach">
            <h1 class="section-headline">{{ __t('featured_coach_title') }}</h1>
            <p class="section-info">{{ __t('featured_coach_description') }}</p>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 coach-wrapper">
                        <div class="coach">
                            <div class="coach-image"><img src="{{ asset(__t('featured_coach_model_image_left')) }}"></div>
                            <div class="coach-title">{{ __t('featured_coach_model_image_left_title') }}</div>
                        </div>
                    </div>
                    <div class="col-md-6 coach-wrapper">
                        <div class="coach mirror">
                            <div class="coach-image"><img src="{{ asset(__t('featured_coach_model_image_left')) }}"></div>
                            <div class="coach-title">{{ __t('featured_coach_model_image_right_title') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section contact-us">
            <h1 class="section-headline">{{ __t('contact_us_title') }}</h1>
            <p class="section-info">{{ __t('contact_us_address') }}</p>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="map">
                            {{--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d395398.3605075727!2d90.39151019708369!3d22.969212893344615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1573476066601!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>--}}
                            <div id ="map" ></div>
                            <input type="hidden" id="mapCoordinates" value="{{ $contents['contact_us_map_coordinates'] }}">
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-info-text">{{ __t('contact_us_description') }}</div>
                        <ul class="contact-info-list">
                            <li class="contact-info">
                                    <span class="icon">
                                        <img src="{{ asset('img/icon/gmail.svg') }}">
                                    </span>
                                <span class="text">{{ __t('contact_us_email') }}</span>
                            </li>
                            <li class="contact-info">
                                    <span class="icon">
                                        <img src="{{ asset('img/icon/call-answer.svg') }}">
                                    </span>
                                <span class="text">{{ __t('contact_us_phone') }}</span>
                            </li>
                            <li class="contact-info">
                                    <span class="icon">
                                        <img src="{{ asset('img/icon/website.svg') }}">
                                    </span>
                                <span class="text">{{ __t('contact_us_website') }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <form class="contact-form" id="contactFrom" method="post" action="{{ url('contact') }}">

                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Name">

                                @error('name')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror

                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email">

                                @error('email')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="interested_in" class="form-control" placeholder="Which unit are you interested in?">

                                @error('interested_in')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="Message" rows="3"></textarea>

                                @error('message')
                                <div  class="help-block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group d-flex justify-content-center">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
