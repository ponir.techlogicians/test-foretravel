@extends('layouts.app')

@section('content')

    <!-- header section -->
    <div class="header-sections">
        <div class="header-section-layer">
            {{--<h1 class="d-flex justify-content-center">{{ __t('header_title') }}</h1>--}}
            <h1 class="d-flex justify-content-center"> @text(header_title) </h1>

        </div>
    </div>

    <!-- after header parent section -->
    <div class="after-header-parents-section">

        <!-- history of foretravel sections -->
        <div class="history-of-foretravel d-flex">
            <div class="history-image d-flex justify-content-start">
                @img(history_image)
            </div>
            <div class="history-text">
                <div class="text-header">
                    <h1>{{ __t('history_title') }}</h1>
                </div>
                <div class="text-body">
                    <h1>{{ __t('history_year') }}</h1>
                    {{ __t('history_description') }}
                </div>
                <div class="read-more">
                    <a href="">Read More</a>
                </div>
                <div class="history-option d-flex">
                    <div class="models d-flex section-gap">
                        <div class="icon inner-flex-section-gap">
                            <img src="{{ asset('img/icon/bus.png') }}" alt="">
                        </div>
                        <div class="text">
                            <div class="text-header">All Models</div>

                            <div class="span-text">Loren ispump</div>
                        </div>
                    </div>
                    <div class="support d-flex section-gap">
                        <div class="icon inner-flex-section-gap">
                            <img src="{{ asset('img/icon/24-hours-support.png') }}" alt="">
                        </div>
                        <div class="text">
                            <div class="text-header">Free Support</div>

                            <div class="span-text">Loren ispump</div>
                        </div>
                    </div>
                    <div class="appliance d-flex section-gap">
                        <div class="icon inner-flex-section-gap">
                            <img src="{{ asset('img/icon/appliase.png') }}" alt="">
                        </div>
                        <div class="text">
                            <div class="text-header">Free Support</div>

                            <div class="span-text">Loren ispump</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- why choose us section start -->
        <!-- <div class="why-choose-us-sections">


            <div class="left-side">
                <div class="fst-row">
                    <div class="stock left-side-card-size-one">
                        <div class="doted-border">
                            <div class="outer-border-icon">
                                <div class="icon d-flex justify-content-center">
                                    <img src="{{ asset('img/icon/bus_why_choose_us.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="text d-flex justify-content-center">
                            RV'S in Stock
                        </div>
                        <div class="number d-flex justify-content-center">
                            75
                        </div>
                    </div>
                    <div class="review left-side-card-size-three">
                        <div class="doted-border">
                            <div class="outer-border-icon">
                                <div class="icon d-flex justify-content-center">
                                    <img src="{{ asset('img/icon/bus_why_choose_us.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="text d-flex justify-content-center">
                            Dealer Reviews
                        </div>
                        <div class="number d-flex justify-content-center">
                            18
                        </div>
                    </div>
                </div>
                <div class="snd-row">
                    <div class="customers left-side-card-size-one">
                        <div class="doted-border">
                            <div class="outer-border-icon">
                                <div class="icon d-flex justify-content-center">
                                    <img src="{{ asset('img/icon/bus_why_choose_us.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="text d-flex justify-content-center">
                            Happy Customers
                        </div>
                        <div class="number d-flex justify-content-center">
                            87
                        </div>
                    </div>
                    <div class="awards left-side-card-size-two">
                        <div class="doted-border">
                            <div class="outer-border-icon">
                                <div class="icon d-flex justify-content-center">
                                    <img src="{{ asset('img/icon/bus_why_choose_us.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="text d-flex justify-content-center">
                            Our Awards
                        </div>
                        <div class="number d-flex justify-content-center">
                            5
                        </div>
                    </div>
                </div>
            </div>

            <div class="right-side">
                <div class="right-side-header">
                    Why Choose Us
                </div>
                <div class="right-side-sub-header">
                    From that modest beginning in 1967 with the 29’ “Speedy Marie” motor home produced in the backyard
                    of C.M. & Marie Fore, Foretravel continued to set the standard. Weathering the oil embargos of the
                    early 1970’s Fore travel introduced the first diesel-powered motorhome in 1974.

                    In addition to the numerous conveniences of a Foretravel, i.e., VCR’s, central vacuum cleaners,
                    icemakers, trash compactors, Foretravel was among the first to use fiberglass instead of aluminum,
                    real hardwood, and a full air bag suspension.

                    With the aerodynamic structure, the Fores wanted even more for their customers, so together they
                    developed the first unibodied chassis in 1986 and the Unihome was introduced. The new Unihome
                    offered uncomparable drivability and handling, along with a sleek new design.
                </div>
                <div class="right-side-bus-image">
                    <img src="{{ asset('img/about/why-choose-us-bus.png') }}') }}' }}" alt="">
                </div>
            </div>
        </div> -->

        <!-- foretravel location section -->
        <div class="foretravel-location-sections d-flex">
            <div class="left-side">
                <div class="header">
                    {{ __t('location_title') }}
                </div>
                <div class="map">
                    <div class="icon">
                        <img src="{{ asset('img/icon/placeholder-on-map-paper-in-perspective.png') }}" alt="">
                    </div>
                    <div class="text">
                        Map
                    </div>
                </div>
                <div class="body">
                    {{ __t('location_description') }}
                </div>
                <div class="read-more">
                    <a href="">Read More</a>
                </div>
            </div>
            <div class="right-side">
                <div class="right-side-image">
                    <img src="{{ asset(__t('location_image')) }}" alt="">
                </div>
            </div>
        </div>

        <!-- foretravel factory tour sections -->
        <div class="foretravel-factory-tour-section d-flex">
            <div class="left">
                <div class="factory-tour-left-image">
                    <img src="{{ asset(__t('factory_tour_image_left')) }}" alt="">
                </div>
            </div>

            <div class="middle">
                <div class="descriptions">
                    <div class="header">
                        {{ __t('factory_tour_title') }}
                    </div>
                    <div class="body">
                        {{ __t('factory_tour_description') }}
                    </div>
                </div>

            </div>

            <div class="right">
                <div class="factory-tour-right-image">
                    <img src="{{  asset(__t('factory_tour_image_right')) }}" alt="">
                </div>
            </div>
        </div>

        <!-- join invitation sections -->
        <div class="join-team-sections d-flex">
            <div class="left">
                <div class="header">
                    {{ __t('marketing_left_panel_title') }}
                </div>
                <div class="body">
                    {{ __t('marketing_left_panel_sub_title') }}
                </div>
                <div class="button">
                    <a href=""><img src="{{ asset('img/icon/magnifying-glass.png') }}" alt=""></a>
                </div>
            </div>
            <div class="right">
                <div class="header">
                    {{ __t('marketing_right_panel_title') }}
                </div>
                <div class="body">
                    {{ __t('marketing_right_panel_sub_title') }}
                </div>
                <div class="button">
                    <a href="#">{{ __t('marketing_right_panel_button_text') }}</a>
                </div>
            </div>
        </div>

        <!-- Testimonial sections -->
        <div class="testimonial-sections">
            <div class="main-test-header d-flex justify-content-center">
                {{ __t('testimonials_title') }}
            </div>
            <div class="sub-test-header d-flex justify-content-center">
                {{ __t('testimonials_sub_title') }}
            </div>

            <div class="slider-testimonial">
                <div class="variable-width slick-track">

                    @foreach($testimonials as $item)
                    <div class="slick-slide d-flex">
                        <div class="left-side-slick">
                            <img src="{{ asset($item->background_image) }}" alt="">
                        </div>
                        <div class="right-side-slick">
                            <div class="small-image-profile">
                                <div class="bg-small-image">
                                    <img src="{{ asset($item->photo) }}" alt="">
                                </div>
                            </div>
                            <div class="testimonial">
                                <div class="header">
                                    <div class="main-header">
                                        {{ $item->name }}
                                    </div>
                                    <div class="sub-header">
                                        {{ $item->designation }}
                                    </div>
                                    <div class="s-sub-header">
                                        {{ $item->company }}
                                    </div>
                                </div>
                                <div class="body">
                                    {{ $item->content }}
                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{--<div class="slick-slide d-flex">--}}
                        {{--<div class="left-side-slick">--}}
                            {{--<img src="{{ asset('img/about/slick-slider-left-image.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="right-side-slick">--}}
                            {{--<div class="small-image-profile">--}}
                                {{--<div class="bg-small-image">--}}
                                    {{--<img src="{{ asset('img/about/testimonial-profile-image.png') }}" alt="">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="testimonial">--}}
                                {{--<div class="header">--}}
                                    {{--<div class="main-header">--}}
                                        {{--PHIL BENNETT--}}
                                    {{--</div>--}}
                                    {{--<div class="sub-header">--}}
                                        {{--PRESIDENT--}}
                                    {{--</div>--}}
                                    {{--<div class="s-sub-header">--}}
                                        {{--SILVERDALE BUSINESS ASSOCIATION.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="body">--}}
                                    {{--Diligence, honesty and perseverance – Kim and Michelle capture the essential--}}
                                    {{--traits--}}
                                    {{--of dedicated agents who work consistently to gain the acknowledgement and--}}
                                    {{--gratitude--}}
                                    {{--of those around them. Similarly Kim and Michelle’s honesty and dependability is--}}
                                    {{--precisely conveyed through their strict and organised work ethic as well as--}}
                                    {{--their--}}
                                    {{--reliability as Agents on multiple occasions.--}}

                                    {{--Being a client with Kim and Michelle I can confidently affirm their admirable--}}
                                    {{--personalities and it is a pleasure to have known them throughout this occasion."--}}

                                    {{--- Mary Yao--}}

                                    {{--Retail Development Landlord, Albany, Auckland--}}


                                    {{--"Recently Michelle Kennedy and Kim Diack acted as Agents to sell our property at--}}
                                    {{--108--}}
                                    {{--Commercial Rd, Helensville. The property is an old BNZ building. I am delighted--}}
                                    {{--to--}}
                                    {{--say both Michelle and Kim really worked hard to sell our property. I remember--}}
                                    {{--Michelle saying finding the buyers was like *finding the needle in a haystack*.--}}
                                    {{--They--}}
                                    {{--persisted for over a year and now the property is sold. At all times they were--}}
                                    {{--optimistic, cheerful and always professional. They kept in contact throughout--}}
                                    {{--the--}}
                                    {{--entire time our place was on the market and were much more positive than us. I--}}
                                    {{--am--}}
                                    {{--happy that we made contact with Michelle and Kim and that they were the Agents--}}
                                    {{--who--}}
                                    {{--sold the property. This letter is to let you know you have two gems working for--}}
                                    {{--you."--}}

                                    {{--- Lauren Lysaght--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="slick-slide d-flex">--}}
                        {{--<div class="left-side-slick">--}}
                            {{--<img src="{{ asset('img/about/slick-slider-left-image.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="right-side-slick">--}}
                            {{--<div class="small-image-profile">--}}
                                {{--<div class="bg-small-image">--}}
                                    {{--<img src="{{ asset('img/about/testimonial-profile-image.png') }}" alt="">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="testimonial">--}}
                                {{--<div class="header">--}}
                                    {{--<div class="main-header">--}}
                                        {{--PHIL BENNETT--}}
                                    {{--</div>--}}
                                    {{--<div class="sub-header">--}}
                                        {{--PRESIDENT--}}
                                    {{--</div>--}}
                                    {{--<div class="s-sub-header">--}}
                                        {{--SILVERDALE BUSINESS ASSOCIATION.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="body">--}}
                                    {{--Diligence, honesty and perseverance – Kim and Michelle capture the essential--}}
                                    {{--traits--}}
                                    {{--of dedicated agents who work consistently to gain the acknowledgement and--}}
                                    {{--gratitude--}}
                                    {{--of those around them. Similarly Kim and Michelle’s honesty and dependability is--}}
                                    {{--precisely conveyed through their strict and organised work ethic as well as--}}
                                    {{--their--}}
                                    {{--reliability as Agents on multiple occasions.--}}

                                    {{--Being a client with Kim and Michelle I can confidently affirm their admirable--}}
                                    {{--personalities and it is a pleasure to have known them throughout this occasion."--}}

                                    {{--- Mary Yao--}}

                                    {{--Retail Development Landlord, Albany, Auckland--}}


                                    {{--"Recently Michelle Kennedy and Kim Diack acted as Agents to sell our property at--}}
                                    {{--108--}}
                                    {{--Commercial Rd, Helensville. The property is an old BNZ building. I am delighted--}}
                                    {{--to--}}
                                    {{--say both Michelle and Kim really worked hard to sell our property. I remember--}}
                                    {{--Michelle saying finding the buyers was like *finding the needle in a haystack*.--}}
                                    {{--They--}}
                                    {{--persisted for over a year and now the property is sold. At all times they were--}}
                                    {{--optimistic, cheerful and always professional. They kept in contact throughout--}}
                                    {{--the--}}
                                    {{--entire time our place was on the market and were much more positive than us. I--}}
                                    {{--am--}}
                                    {{--happy that we made contact with Michelle and Kim and that they were the Agents--}}
                                    {{--who--}}
                                    {{--sold the property. This letter is to let you know you have two gems working for--}}
                                    {{--you."--}}

                                    {{--- Lauren Lysaght--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="slick-slide d-flex">--}}
                        {{--<div class="left-side-slick">--}}
                            {{--<img src="{{ asset('img/about/slick-slider-left-image.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="right-side-slick d-flex">--}}
                            {{--<div class="small-image-profile">--}}
                                {{--<div class="bg-small-image">--}}
                                    {{--<img src="{{ asset('img/about/testimonial-profile-image.png') }}" alt="">--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            {{--<div class="testimonial">--}}
                                {{--<div class="header">--}}
                                    {{--<div class="main-header">--}}
                                        {{--PHIL BENNETT--}}
                                    {{--</div>--}}
                                    {{--<div class="sub-header">--}}
                                        {{--PRESIDENT--}}
                                    {{--</div>--}}
                                    {{--<div class="s-sub-header">--}}
                                        {{--SILVERDALE BUSINESS ASSOCIATION.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="body">--}}
                                    {{--Diligence, honesty and perseverance – Kim and Michelle capture the essential--}}
                                    {{--traits--}}
                                    {{--of dedicated agents who work consistently to gain the acknowledgement and--}}
                                    {{--gratitude--}}
                                    {{--of those around them. Similarly Kim and Michelle’s honesty and dependability is--}}
                                    {{--precisely conveyed through their strict and organised work ethic as well as--}}
                                    {{--their--}}
                                    {{--reliability as Agents on multiple occasions.--}}

                                    {{--Being a client with Kim and Michelle I can confidently affirm their admirable--}}
                                    {{--personalities and it is a pleasure to have known them throughout this occasion."--}}

                                    {{--- Mary Yao--}}

                                    {{--Retail Development Landlord, Albany, Auckland--}}


                                    {{--"Recently Michelle Kennedy and Kim Diack acted as Agents to sell our property at--}}
                                    {{--108--}}
                                    {{--Commercial Rd, Helensville. The property is an old BNZ building. I am delighted--}}
                                    {{--to--}}
                                    {{--say both Michelle and Kim really worked hard to sell our property. I remember--}}
                                    {{--Michelle saying finding the buyers was like *finding the needle in a haystack*.--}}
                                    {{--They--}}
                                    {{--persisted for over a year and now the property is sold. At all times they were--}}
                                    {{--optimistic, cheerful and always professional. They kept in contact throughout--}}
                                    {{--the--}}
                                    {{--entire time our place was on the market and were much more positive than us. I--}}
                                    {{--am--}}
                                    {{--happy that we made contact with Michelle and Kim and that they were the Agents--}}
                                    {{--who--}}
                                    {{--sold the property. This letter is to let you know you have two gems working for--}}
                                    {{--you."--}}

                                    {{--- Lauren Lysaght--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>

            </div>

        </div>

    </div>

@endsection
