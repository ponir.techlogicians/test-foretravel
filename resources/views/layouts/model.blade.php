@extends('layouts.app')

@section('content')

    <!-- Cover -->
    <section class="cover" style="background-image: url({{ __t('model_header_background_image') }});">
        <span class="title">@text(model_header_text)</span>
        @img(model_header_image)
    </section>


    <!--wrapper -->
    <section class="wrapper bg-layer">

        <div class="scroll-area">

            <!-- sticky sub menu -->
            <div class="menu">
                <div class="container">
                    <nav id="model_page">
                        <ul class="menu-list">
                            <li class="nav-item">
                                <a class="nav-link active" href="#floorplans"><span>Floorplans</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#color_options"><span>Color Options</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#key_features"><span>Key Features</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#specifications"><span>Specifications</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#gallery"><span>Gallery</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#videos"><span>Videos</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:;"><span>Brochure</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Floorplan -->
            <section class="section floorplans" id="floorplans">
                <h1 class="section-headline">@text(model_floorplan_title)</h1>
                <p class="section-info">@text(model_floorplan_description)</p>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="slider">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <!--  <ol class="carousel-indicators">
                                         <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                         <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                         <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                     </ol> -->
                                    <div class="carousel-inner">

                                        @foreach($floorplan as $item)
                                            <div class="carousel-item @if($loop->first) active @endif floorplan">
                                                <div class="row">
                                                    <div class="col-md-6 one">
                                                        <img src="{{ url($item->image) }}" class="floorplan-image lazy">
                                                    </div>
                                                    <div class="col-md-6 two">
                                                        <h3 class="floorplan-title">{{ $item->title }}</h3>
                                                        @if(count($item->floorPlanItems))
                                                        <ul class="floorplan-spec">
                                                            @foreach($item->floorPlanItems as $i)
                                                            <li>{{ $i->text }}</li>
                                                            @endforeach
                                                        </ul>
                                                        <a href="javascript:;" class="more">More</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Color Options -->
            <section class="section color-options" id="color_options">
                <h1 class="section-headline">@text(model_color_options_title)</h1>
                <p class="section-info">@text(model_color_options_description)</p>
                <div class="container">
                    <div class="row">
                        @if(isset($colorOptions->slides[0]))
                        <div class="col-md-6 one">
                            <img src="{{ asset($colorOptions->slides[0]->file_path) }}" class="main-image lazy">
                        </div>
                        <div class="col-md-6 two">
                            <ul class="model-images">
                                @foreach($colorOptions->slides as $slide)
                                <li class="model-image">
                                    <div class="image-wrapper">
                                        <img src="{{ asset($slide->file_path) }}" class="lazy">
                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </section>


            <!-- Key Features -->
            <section class="section key-features" id="key_features">
                <h1 class="section-headline">@text(model_key_features_title)</h1>
                <p class="section-info">@text(model_key_features_description)</p>
                <div class="container">
                    <div class="row">
                        <div class="col-6 one">
                            {{--<ul class="key-features-list">--}}
                                {{--<li>12 volt accessory receptacle</li>--}}
                                {{--<li>Dual 1 amp USB charge receptacle</li>--}}
                                {{--<li>Battery boost switch / auto-charge</li>--}}
                                {{--<li>Dual 1 amp USB charge receptacle</li>--}}
                            {{--</ul>--}}
                            {{--<a href="javascript:;" class="more">More</a>--}}
                            @text(model_key_features_items)
                        </div>
                        <div class="col-6 two">
                            <div class="images">
                                <img src="{{ asset(__t('model_key_features_image')) }}" class="lazy">
{{--                                <img src="{{ asset('img/model/collage.png') }}">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <!-- Specifications -->
            <section class="section specifications" id="specifications">
                <h1 class="section-headline">@text(model_specifications_title)</h1>
                <p class="section-info">@text(model_specifications_description)</p>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 specifications-details">@text(model_specifications_column_description)</div>
                    </div>
                </div>
            </section>

            <!-- Gallery -->
            <section class="section gallery" id="gallery">
                <h1 class="section-headline">@text(model_gallery_title)</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 interior">
                            <h2 class="sub-headline">@text(model_gallery_type_left)</h2>
                            <!-- 4,4,4,8,4,3,3,6 -->
                            <div class="gallery-images row">
                                @foreach($interiorGallery->slides as $gallery)
                                <a href="{{ asset($gallery->file_path) }}" data-rel="interior" class="col-4">
                                    <img src="{{ asset($gallery->file_path) }}" class="lazy">
                                </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6 exterior">
                            <h2 class="sub-headline">@text(model_gallery_type_right)</h2>
                            <!-- 4,4,4,8,4,3,3,6 -->
                            <div class="gallery-images row">
                                @foreach($exteriorGallery->slides as $gallery)
                                    <a href="{{ asset($gallery->file_path) }}" data-rel="exterior" class="col-4">
                                        <img src="{{ asset($gallery->file_path) }}" class="lazy">
                                    </a>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Video -->
            <section class="section videos" id="videos">
                <h1 class="section-headline">@text(model_video_title)</h1>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 video-wrapper">
                            <video name="Video Name" loop src="{{ __t('model_video_path') }}" id="model_video">
                            </video>
                            <div class="control pause">
                                <img src="{{ asset('img/icon/002-youtube.svg') }}" class="play-icon" onclick="modelVideoControl()">
                                <img src="{{ asset('img/icon/youtube-white.png') }}" class="pause-icon" onclick="modelVideoControl()">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- Model -->
        <section class="section models" id="models">
            <h1 class="section-headline">@text(model_title)</h1>
            <p class="section-info">
                <span>@text(model_short_description)</span>
                {{--<a class="btn btn-inform"  href="javascript:;">Keep me inform</a>--}}
            </p>
            <div class="container">
                <div class="row">
                    <div class="col-6 model-wrapper">
                        <div class="model">
                            <div class="model-title">@text(model_coach_left_title)</div>
                            <div class="model-image">@img(model_coach_left_image)</div>
                        </div>
                    </div>
                    <div class="col-6 model-wrapper">
                        <div class="model mirror">
                            <div class="model-title">@text(model_coach_right_title)</div>
                            <div class="model-image">@img(model_coach_right_image)</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 model-description">
                           <span>@text(model_long_description)</span>
                    </div>
                </div>
            </div>
        </section>
    </section>

@endsection
