<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>FORETRAVEL</title>
    <link rel="shortcut icon" href="{{ asset('img/icon/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/slick/slick-theme.css?v=0.51') }}">
    {{-- <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('vendor/simplelightbox/dist/simplelightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css?v=0.65') }}">
    <script src="{{asset('vendor/modernizr/modernizr.js')}}"></script>
    {{-- <script src="{{ asset('vendor/parse-address/parse-address.min.js') }}"></script> --}}
    {{-- <script type="text/javascript" src="{{'https://unpkg.com/@fragaria/address-formatter@1.0.0'}}"></script> --}}
    <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />

    <style>
        /* Set the size of the div element that contains the map */
         #map {
            height: calc(100vh - 280px);  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>
</head>
<body>
@include('partials.sidebar')
<div class="main {{(Request::route()->getName() == 'home')?'pad-0':''}}">

    @include('partials.menu')

    <!-- footer sections -->
    @yield('content')

    <!-- footer sections -->
    <div class="footer-section d-flex">
        <div class="footer-left d-flex">
             <div class="twitter logo-padding">
                <a href="https://www.instagram.com/foretravel_motorcoach"><img src="{{ asset('img/icon/instagram.svg') }}" alt=""></a>
            </div>
            <div class="facebook logo-padding">
                <a href="https://www.facebook.com/FOTservice/"><img src="{{ asset('img/icon/facebook.svg') }}" alt=""></a>
            </div>
            {{--<div class="instagram logo-padding">--}}
                {{--<a href="https://twitter.com/ForetravelCoach"><img src="{{ asset('img/icon/005-twitter.svg') }}" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="youtube logo-padding">--}}
                {{--<a href=""><img src="{{ asset('img/icon/002-youtube.svg') }}" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="gmail logo-padding">--}}
                {{--<a href=""><img src="{{ asset('img/icon/gmail-1.svg') }}" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="finger-print logo-padding">--}}
                {{--<a href=""><img src="{{ asset('img/icon/24px.svg') }}" alt=""></a>--}}
            {{--</div>--}}
        </div>
        <div class="footer-middle d-flex">
            <a href="javascript:;" onclick="toggleSidebar()" class="footer-nav footer-nav-padding sidebar-toggler">
                MODEL
            </a>
            <a href="{{ route('about') }}" class="footer-nav footer-nav-padding">
                ABOUT
            </a>
            <a href="{{route('service-department')}}" class="footer-nav footer-nav-padding">
                SERVICES
            </a>
            <a href="{{ route('inventory') }}" class="footer-nav footer-nav-padding">
                Dealer Inventory
            </a>
        </div>
        <div class="footer-right d-flex">
            {{-- <a href="javascript:;">Privacy Policy</a> --}}
        </div>
    </div>

</div>
@include('partials.popup')
@yield('extra-content')
<script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<script  src="{{ asset('js/html5gallery.js')}}" type="text/javascript"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
{{-- <script src="{{ asset('vendor/malihu-custom-scrollbar/jquery.mCustomScrollbar.js') }}"></script> --}}
<script src="{{ asset('vendor/simplelightbox/dist/simple-lightbox.min.js?v=0.51') }}"></script>
<script src="{{ asset('dashboard/js/jquery.lazy.min.js') }}"></script>
<script src="{{ asset('js/jquery.prettyPhoto.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('dashboard/js/utils.js') }}"></script>
<script src="{{ asset('js/script.js?v=0.57') }}"></script>





<!-- Jquery  -->
<script>
    $(document).ready(function () {

        $('.coach-slider').slick({
            centerMode: true,
            centerPadding: '300px',
            slidesToShow: 1,
            autoplay: true,
            dots: true,
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        centerPadding: '100px',
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0px',
                        slidesToShow: 1,
                        dots: true
                    }
                },
            ]
        }).on('afterChange', function(event, slick, currentSlide, nextSlide){
            $('.coach-slider .lazy').lazy({
                onError: function(element) {
                    $(element).attr('src','/img/no-image.png');
                    $(element).addClass('not-loaded');
                }
            });
        });

        $('.variable-width').slick({
            centerMode: true,
            centerPadding: '100px',
            slidesToShow: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        slidesToShow: 1
                    }
                }
            ]
        }).on('afterChange', function(event, slick, currentSlide, nextSlide){
            $('.variable-width .lazy').lazy({
                onError: function(element) {
                    $(element).attr('src','/img/no-image.png');
                    $(element).addClass('not-loaded');
                }
            });
        });

        $("li[rel^='prettyPhoto']").prettyPhoto();


        // validate contact form
        // form validation
        $('#contactFrom').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email:true
                },
                interested_in: {
                    required: true,
                    minlength: 1
                },
                message: {
                    required: true,
                    minlength: 5,
                },
                name: {
                    required: true
                }
            },

            messages: {
                email: {
                    required: "Please provide a valid email.",
                    email: "Please provide a valid email."
                },
                message: {
                    required: "Please state your message",
                },
                name: "Please write your name",
                interested_in: "Please state your interested area",
            },


            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });

        $('#technicalSupportSearchForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                category: {
                    required: true
                }
            },

            messages: {
                category: {
                    required: "Please Select a valid category."
                }
            },


            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form) {
            }
        });

        // init map
        // InitializeMap();
        // $('.model-list .lazy').lazy();

        // technical support category dependant section loading
        $(document).on('change','form#technicalSupportSearchForm select',function () {

            var attributeAsDependant = $(this).data('depending-by');
            var selectedId = $(this).val();

            console.log(attributeAsDependant);

            if (attributeAsDependant)
                $.ajax({
                url: '{{ url('reference-data-by-selection' ) }}/' + selectedId,
                type: 'get',
                data: {},
                dataType: 'json',
                success: function (categories) {
                    console.log(categories);

                    if (categories.length){

                        $("#" + attributeAsDependant).html('<option value="">Select A '+ ucfirst(attributeAsDependant) + '</option>');

                        $.each(categories,function (key, item) {
                            $("#" + attributeAsDependant).append(
                                '<option value="' + item.id + '">' + item.label + '</option>'
                            )
                        });

                        $("#" + attributeAsDependant).prop( "disabled", null );
                    }
                    else {

                        $("#" + attributeAsDependant).html('<option value="">Select a '+ ucfirst(attributeAsDependant) + '</option>');

                    }
                },
                error: function (request, status, error) {
                    console.log("error");
                }
            });
        });

    });

    /** map start **/
        // initialize the function for Gmap
        function initMap() {


        }
    /** map end **/

</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWi2AlHLV5erUJZp8wgvD08GbVjxeIieU&callback=initMap"></script>


@yield('script')

</body>
</html>
