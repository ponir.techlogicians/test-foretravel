<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Dashboard - </title>
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="{{ asset('img/icon/favicon.ico') }}">

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/bootstrap.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('dashboard/font-awesome/4.5.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/multiselect/bootstrap-multiselect.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/multiselect/select2.min.css')}}">
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/jquery-ui.custom.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('dashboard/css/jquery.gritter.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('dashboard/css/colorbox.min.css') }}" />
    <link rel="stylesheet" href="{{asset('dashboard/css/jquery-ui.min.css')}}">


    <!-- text fonts -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/fonts.googleapis.com.css') }}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{ asset('dashboard/css/ace-part2.min.css') }}" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('dashboard/css/ace-skins.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('dashboard/css/ace-rtl.min.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{ asset('dashboard/css/ace-ie.min.css') }}" />
    <![endif]-->


    <!-- custom styles -->
    <link rel="stylesheet" href="{{ asset('dashboard//css/custom.css') }}" />


    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="{{ asset('dashboard/js/ace-extra.min.js') }}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="{{ asset('dashboard/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/respond.min.js') }}"></script>
    <![endif]-->


</head>

<body class="no-skin">

@auth
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="index.html" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    {{ isset($set['company_name']) ? $set['company_name'] : 'Foretravel' }}
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        {{--<img class="nav-user-photo" src="{{ asset('dashboard/images/avatars/user.jpg') }}" alt="Jason's Photo" />--}}
                        <span class="user-info">
									<small>Welcome,</small>
									{{ Auth::user()->name }}
								</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<i class="ace-icon fa fa-cog"></i>--}}
                                {{--Settings--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<a href="profile.html">--}}
                                {{--<i class="ace-icon fa fa-user"></i>--}}
                                {{--Profile--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li class="divider"></li>--}}

                        <li>
                            {{--<a href="#">--}}
                                {{--<i class="ace-icon fa fa-power-off"></i>--}}
                                {{--Logout--}}
                            {{--</a>--}}

                            <a class="" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="ace-icon fa fa-power-off"></i>
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>
@endauth

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    @auth
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
            try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        {{--<div class="sidebar-shortcuts" id="sidebar-shortcuts">--}}
            {{--<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">--}}
                {{--<button class="btn btn-success">--}}
                    {{--<i class="ace-icon fa fa-signal"></i>--}}
                {{--</button>--}}

                {{--<button class="btn btn-info">--}}
                    {{--<i class="ace-icon fa fa-pencil"></i>--}}
                {{--</button>--}}

                {{--<button class="btn btn-warning">--}}
                    {{--<i class="ace-icon fa fa-users"></i>--}}
                {{--</button>--}}

                {{--<button class="btn btn-danger">--}}
                    {{--<i class="ace-icon fa fa-cogs"></i>--}}
                {{--</button>--}}
            {{--</div>--}}

            {{--<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">--}}
                {{--<span class="btn btn-success"></span>--}}

                {{--<span class="btn btn-info"></span>--}}

                {{--<span class="btn btn-warning"></span>--}}

                {{--<span class="btn btn-danger"></span>--}}
            {{--</div>--}}
        {{--</div><!-- /.sidebar-shortcuts -->--}}

        @include('layouts.admin.sidebar')

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    @endauth

    <div class="main-content">
        <div class="main-content-inner">


            <div class="page-content" style="">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->


                @yield('content')

            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->



    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">ForeTravel</span>
							Application &copy; 2019
						</span>

            &nbsp; &nbsp;
            {{--<span class="action-buttons">--}}
            {{--<a href="#">--}}
            {{--<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>--}}
            {{--</a>--}}

            {{--<a href="#">--}}
            {{--<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>--}}
            {{--</a>--}}

            {{--<a href="#">--}}
            {{--<i class="ace-icon fa fa-rss-square orange bigger-150"></i>--}}
            {{--</a>--}}
            {{--</span>--}}
        </div>
    </div>
</div>

<!-- dynamic image form -->
<form id="dynamicImageDeleteForm" action="" method="POST">
    @csrf
</form>
<!-- end dynamic image form -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{ asset('dashboard/js/jquery-2.1.4.min.js') }}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="{{ asset('dashboard/js/jquery-1.11.3.min.js') }}"></script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('dashboard/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
</script>
<script src="{{ asset('dashboard/js/bootstrap.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="{{ asset('dashboard/js/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{asset('dashboard/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('dashboard/js/jquery-ui.custom.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.ui.touch-punch.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.sparkline.index.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.flot.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.colorbox.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.hotkeys.index.min.js') }}"></script>
<script src="{{ asset('dashboard/js/bootstrap-wysiwyg.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.lazy.min.js') }}"></script>


<script src="{{ asset('dashboard/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('dashboard/js/jquery.dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashboard/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashboard/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashboard/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashboard/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('dashboard/js/dataTables.select.min.js') }}"></script>
<script src="{{asset('vendor/bootstrap/multiselect/multiselect.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/multiselect/select2.min.js')}}"></script>


<!-- tinymce -->
<script src="https://cdn.tiny.cloud/1/iuxgjc6s5i8qq6z9d3esm7imcvmw8b2rhqi1s09jmkcg1hc0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- ace scripts -->
<script src="{{ asset('dashboard/js/ace-elements.min.js') }}"></script>
<script src="{{ asset('dashboard/js/ace.min.js') }}"></script>
<script src="{{ asset('dashboard/js/script.js')}}"></script>
<script src="{{ asset('dashboard/js/utils.js')}}"></script>


<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function($) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        setTimeout(function () {
            $(".alert").fadeOut(500);
        },3000);


        $( ".datepicker" ).datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'yy-mm-dd'
        });



        // form validation
        $('#luxury_coach_1').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                // email: {
                //     required: true,
                //     email:true
                // },
                // password: {
                //     required: true,
                //     minlength: 5
                // },
                // password2: {
                //     required: true,
                //     minlength: 5,
                //     equalTo: "#password"
                // },
                name: {
                    required: true
                },
                // phone: {
                //     required: true,
                //     phone: 'required'
                // },
                // url: {
                //     required: true,
                //     url: true
                // },
                // comment: {
                //     required: true
                // },
                // state: {
                //     required: true
                // },
                // platform: {
                //     required: true
                // },
                // subscription: {
                //     required: true
                // },
                // gender: {
                //     required: true,
                // },
                // agree: {
                //     required: true,
                // }
            },

            messages: {
                // email: {
                //     required: "Please provide a valid email.",
                //     email: "Please provide a valid email."
                // },
                // password: {
                //     required: "Please specify a password.",
                //     minlength: "Please specify a secure password."
                // },
                // state: "Please choose state",
                // subscription: "Please choose at least one option",
                // gender: "Please choose gender",
                // agree: "Please accept our policy"
            },


            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });

        // datatables
        var myTable =
            $('#dynamic-table')
            // .wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                .DataTable( {
                    // bAutoWidth: false,
                    // "aoColumns": [
                    //     { "bSortable": false },
                    //     null, null,null, null, null,
                    //     { "bSortable": false }
                    // ],
                    // "aaSorting": [],


                    //"bProcessing": true,
                    //"bServerSide": true,
                    //"sAjaxSource": "http://127.0.0.1/table.php"	,

                    //,
                    //"sScrollY": "200px",
                    //"bPaginate": false,

                    //"sScrollX": "100%",
                    //"sScrollXInner": "120%",
                    //"bScrollCollapse": true,
                    //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                    //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                    //"iDisplayLength": 50


                    // select: {
                    //     style: 'multi'
                    // }
                } );


        // colorbox script
        var colorbox_params = {
            rel: 'colorbox',
            reposition:true,
            scalePhotos:true,
            scrolling:false,
            previous:'<i class="ace-icon fa fa-arrow-left"></i>',
            next:'<i class="ace-icon fa fa-arrow-right"></i>',
            close:'&times;',
            current:'{current} of {total}',
            maxWidth:'100%',
            maxHeight:'100%',
            onOpen:function(){
                $overflow = document.body.style.overflow;
                document.body.style.overflow = 'hidden';
            },
            onClosed:function(){
                document.body.style.overflow = $overflow;
            },
            onComplete:function(){
                $.colorbox.resize();
            }
        };

        $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);

        $(document).on('click', '#myTab3 li > a',function () {

            Cookies.set('latestTab', $(this).data('tab_key'));

        });


        // rich editor

        function showErrorAlert (reason, detail) {
            var msg='';
            if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
            else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
        }

        // $('.wysiwyg').ace_wysiwyg();
        // {
        $('.wysiwyg').ace_wysiwyg({
            toolbar:
                [
                    'font',
                    null,
                    'fontSize',
                    null,
                    {name:'bold', className:'btn-info'},
                    {name:'italic', className:'btn-info'},
                    {name:'strikethrough', className:'btn-info'},
                    {name:'underline', className:'btn-info'},
                    null,
                    {name:'insertunorderedlist', className:'btn-success'},
                    {name:'insertorderedlist', className:'btn-success'},
                    {name:'outdent', className:'btn-purple'},
                    {name:'indent', className:'btn-purple'},
                    null,
                    {name:'justifyleft', className:'btn-primary'},
                    {name:'justifycenter', className:'btn-primary'},
                    {name:'justifyright', className:'btn-primary'},
                    {name:'justifyfull', className:'btn-inverse'},
                    null,
                    {name:'createLink', className:'btn-pink'},
                    {name:'unlink', className:'btn-pink'},
                    null,
                    {name:'insertImage', className:'btn-success'},
                    null,
                    'foreColor',
                    null,
                    {name:'undo', className:'btn-grey'},
                    {name:'redo', className:'btn-grey'}
                ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        // tinymce

        tinymce.init({
            selector: 'textarea.full-tiny',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tiny.cloud/css/codepen.min.css'
            ],
            link_list: [
                { title: 'My page 1', value: 'http://www.tinymce.com' },
                { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_list: [
                { title: 'My page 1', value: 'http://www.tinymce.com' },
                { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
                { title: 'None', value: '' },
                { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
                }
            },
            templates: [
                { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
                { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
                { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
            ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: "mceNonEditable",
            toolbar_drawer: 'sliding',
            contextmenu: "link image imagetools table",
        });

// select tow

        $('.select2').css('width','200px').select2({allowClear:true})
				$('#select2-multiple-style .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('.select2').addClass('tag-input-style');
					 else $('.select2').removeClass('tag-input-style');
		});
        $('.multiselect').multiselect({
                enableFiltering: true,
                enableHTML: true,
                buttonClass: 'btn btn-white btn-primary',
                templates: {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
                }
            });



        // lazy loading image
        $('.lazy').Lazy();




        // switch logic and reset box
        $(document).on('click', '.toolbar a[data-target]', function(e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $(target).addClass('visible');//show target
        });




        // default scripts.........

        $('.dialogs,.comments').ace_scroll({
            size: 300
        });

        $('.sortable').sortable({
                opacity:0.8,
                revert:true,
                forceHelperSize:true,
                placeholder: 'draggable-placeholder',
                forcePlaceholderSize:true,
                tolerance:'pointer',
                stop: function( event, ui ) {
                    //just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                    $(ui.item).css('z-index', 'auto');
                    console.log('this is awesome');

                    // it became dirty now. show the update button
                    $("#sortButton").show();
                    $("#cancelSortButton").show();

                }
            }
        );
        // $('.tasks').disableSelection();
        // $('.tasks input:checkbox').removeAttr('checked').on('click', function(){
        //     if(this.checked) $(this).closest('li').addClass('selected');
        //     else $(this).closest('li').removeClass('selected');
        // });


        //show the dropdowns on top or bottom depending on window height and menu position
        $('#task-tab .dropdown-hover').on('mouseenter', function(e) {
            var offset = $(this).offset();

            var $w = $(window)
            if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
                $(this).addClass('dropup');
            else $(this).removeClass('dropup');
        });


        // fetching dependant value for selectbox
        $(document).on('change','form#validation-form select',function () {

            var attributeAsDependant = $(this).data('depending-by');
            var selectedId = $(this).val();

            console.log(attributeAsDependant);

            if (attributeAsDependant){

                // if has reference table to fetch
                let tableReferencedBy   = $(this).data('table-referenced-by');
                let foreignKey          = $(this).data('table-foreign-key');
                console.log(tableReferencedBy);

                $.ajax({
                    url: '{{ url('admin/reference-data-by-selection' ) }}/' + selectedId + '/' + tableReferencedBy + '/' + foreignKey,
                    type: 'get',
                    data: {},
                    dataType: 'json',
                    success: function (categories) {
                        console.log(categories);

                        if (categories.length){

                            $("#" + attributeAsDependant).html('<option value="">Select '+ ucfirst(attributeAsDependant) + '</option>');

                            $.each(categories,function (key, item) {
                                $("#" + attributeAsDependant).append(
                                    '<option value="' + item.id + '">' + item.label + '</option>'
                                )
                            });

                            $("#" + attributeAsDependant).prop( "disabled", null );
                        }
                        else {

                            $("#" + attributeAsDependant).html('<option value="">Select '+ ucfirst(attributeAsDependant) + '</option>');

                        }
                    },
                    error: function (request, status, error) {
                        console.log("error");
                    }
                });
            }
        });

    })


    $(document).ready(function () {
        $("#myTab3 li > a[href='#" +  Cookies.get('latestTab') + "']").click();
    })

    function updateSortOrder() {

        // items variable
        let sortedItems = [];
        let perPage = $("select[name='dynamic-table_length']").val();
        let pageNo  = $(".pagination li.active a").text();

        // gathering items id serial by
        $.each($(".items"), function(key,el){

            // loggin
            console.log($(el).data("item_id"));

            // pushing in an array to send over api
            sortedItems.push(parseInt($(el).data("item_id")));
        });

        console.log(sortedItems);

        // return false;

        // call ajax to update the sort column
        $.ajax({
            url: '{{ url('admin/update-sorting-order' ) }}',
            type: 'post',
            data: {
                'items'     : sortedItems,
                'per_page'  : perPage,
                'page_no'   : pageNo,
                'section'     : $("#sortButton").data('model')
            },
            dataType: 'json',
            success: function (response) {

                console.log(response);

                popupMessage('success', 'Success','Rearrange Done!!')

                // hide the update button
                $("#sortButton").hide();
                $("#cancelSortButton").hide();

            },
            error: function (request, status, error) {
                console.log("error");
            }
        });

    }

    function cancelSortOrder() {
        location.reload();
    }



</script>
</body>
</html>

@yield('script')
