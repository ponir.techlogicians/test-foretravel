<ul class="nav nav-list">
    <li class="">
        <a href="{{ url('admin') }}">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
    </li>



    <li class="@if(_isInPage(['page','fabrications'])) open @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-desktop"></i>
            <span class="menu-text">
								Pages
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">

            <li class="@if(_isInPage('page','home')) active @endif">
                <a href="{{ url('admin/page/home') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Home Page
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('page','about')) active @endif">
                <a href="{{ url('admin/page/about') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    About Page
                </a>

                <b class="arrow"></b>
            </li>

            {{--<li class="@if(_isInPage('page','model')) active @endif">--}}
                {{--<a href="{{ url('admin/page/model') }}">--}}
                    {{--<i class="menu-icon fa fa-caret-right"></i>--}}
                    {{--Model Page--}}
                {{--</a>--}}

                {{--<b class="arrow"></b>--}}
            {{--</li>--}}

            <li class="@if(_isInPage('page','model')) open @endif">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-caret-right"></i>

                    Models
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                    @foreach(\App\Models\CoachModel::where('is_active',1)->get()->sortBy('sort') as $model)
                        <li class="@if(_isInPage('page','model',$model->slug)) active @endif">
                            <a href="{{ url('admin/page/model/' . $model->slug) }}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{ $model->title }}
                            </a>

                            <b class="arrow"></b>
                        </li>
                    @endforeach
                </ul>
            </li>

            <li class="@if(_isInPage(['page','fabrications'],'alter')) open @endif">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-caret-right"></i>

                    Fabrication
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">

                    <li class="@if(_isInPage(['page','fabrications'],'only')) active @endif" style="background-color: lightblue; font-weight: bold">
                        <a href="{{ url('admin/fabrications') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            List Pages
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="@if(_isInPage(['page','fabrications'],'create')) active @endif" style="background-color: lightblue; font-weight: bold">
                        <a href="{{ url('admin/fabrications/create') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            New Page
                        </a>

                        <b class="arrow"></b>
                    </li>

                    @foreach(\App\Models\CustomReferenceCategory::where('type','fabrications')->orderBy('sort')->get() as $fabrication)
                        <li class="@if(_isInPage('page','fabrications',$fabrication->slug)) active @endif" style="background-color: #bec1a1; font-weight: bold">
                            <a href="{{ url('admin/page/fabrications/' . $fabrication->slug) }}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{ makeLabelBySlug($fabrication->label) }}
                            </a>

                            <b class="arrow"></b>
                        </li>
                    @endforeach
                </ul>
            </li>

            <li class="@if(_isInPage('page','inventory')) active @endif">
                <a href="{{ url('admin/page/inventory') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Dealer Inventory
                </a>

                <b class="arrow"></b>

            </li>
            <li class="@if(_isInPage('page','owner-service')) active @endif">
                <a href="{{ url('admin/page/owner-service') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Owner Service
                </a>

                <b class="arrow"></b>

            </li>


            <li class="@if(_isInPage('page','technical-support')) active @endif">
                <a href="{{ url('admin/page/technical-support') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Technical Support
                </a>

                <b class="arrow"></b>
            </li>
            <li class="@if(_isInPage('page','information')) active @endif">
                <a href="{{ url('admin/page/information') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Resources
                </a>
                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('page','motorcade-club')) open @endif">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-caret-right"></i>

                    Motorcade Club
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="@if(_isInPage('page','motorcade-club','general')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/general') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            General
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage('page','motorcade-club','club')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/club') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tab - Club
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage('page','motorcade-club','chapter')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/chapter') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tab - Chapter
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage('page','motorcade-club','officers')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/officers') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tab - Officers
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage('page','motorcade-club','magazine')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/magazine') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tab - Magazine
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="@if(_isInPage('page','motorcade-club','news-and-events')) active @endif">
                        <a href="{{ url('admin/page/motorcade-club/news-and-events') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Tab - News & Events
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li class="@if(_isInPage('page','service-department')) active @endif">
                <a href="{{ url('admin/page/service-department') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Service Department
                </a>

                <b class="arrow"></b>

            </li>
            <li class="@if(_isInPage('page','remodel-department')) active @endif">
                <a href="{{ url('admin/page/remodel-department') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Remodel Department
                </a>

                <b class="arrow"></b>

            <li class="@if(_isInPage('page','parts-department')) active @endif">
                <a href="{{ url('admin/page/parts-department') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Parts Department
                </a>

                <b class="arrow"></b>

            <li class="@if(_isInPage('page','recall-information')) active @endif">
                <a href="{{ url('admin/page/recall-information') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Recall Information
                </a>

                <b class="arrow"></b>

            </li>

        </ul>
    </li>

    @if(Auth::user()->role == 'user')



        @foreach(\App\Models\CustomPost::orderBy('sort')->get() as $post)
            @if(in_array($post->slug,['members']))
            <li class="@if(_isInPage($post->slug)) open @endif">
                <a href="" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> {{ $post->name }} </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="@if(_isInPage($post->slug,'only')) active @endif">
                        <a href="{{url('admin/'. $post->slug)}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            List
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage($post->slug,'create')) active @endif">
                        <a href="{{url('admin/'. $post->slug .'/create')}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            New
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            @endif
        @endforeach

    @endif

    @if(Auth::user()->role == 'superadmin')
        @foreach(\App\Models\CustomPost::orderBy('sort')->get() as $post)
            <li class="@if(_isInPage($post->slug)) open @endif">
                <a href="" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> {{ $post->name }} </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="@if(_isInPage($post->slug,'only')) active @endif">
                        <a href="{{url('admin/'.$post->slug)}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            List
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="@if(_isInPage($post->slug,'create')) active @endif">
                        <a href="{{url('admin/'.$post->slug .'/create')}}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            New
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
        @endforeach

        <li class="@if(_isInPage('testimonials')) open @endif">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Testimonials </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="@if(_isInPage('testimonials','only')) active @endif">
                    <a href="{{route('admin.testimonials.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="@if(_isInPage('testimonials','create')) active @endif">
                    <a href="{{route('admin.testimonials.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        New
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li class="@if(_isInPage('floorplans')) open @endif">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text"> Floor Plans </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="@if(_isInPage('floorplans','only')) active @endif">
                    <a href="{{route('admin.floorplans.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="@if(_isInPage('floorplans','create')) active @endif">
                    <a href="{{route('admin.floorplans.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        New
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text">Custom Posts</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{route('admin.customposts.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="{{route('admin.customposts.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        New
                    </a>

                    <b class="arrow"></b>
                </li>
                
            </ul>
        </li>

        <li class="@if(_isInPage('sliders')) open @endif">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text"> Sliders </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="@if(_isInPage('sliders','only')) active @endif">
                    <a href="{{route('admin.sliders.index')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="@if(_isInPage('sliders','create')) active @endif">
                    <a href="{{route('admin.sliders.create')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        New
                    </a>

                    <b class="arrow"></b>
                </li>

                {{--<li class="">--}}
                {{--<a href="form-wizard.html">--}}
                {{--<i class="menu-icon fa fa-caret-right"></i>--}}
                {{--Wizard &amp; Validation--}}
                {{--</a>--}}

                {{--<b class="arrow"></b>--}}
                {{--</li>--}}

                {{--<li class="">--}}
                {{--<a href="wysiwyg.html">--}}
                {{--<i class="menu-icon fa fa-caret-right"></i>--}}
                {{--Wysiwyg &amp; Markdown--}}
                {{--</a>--}}

                {{--<b class="arrow"></b>--}}
                {{--</li>--}}

                {{--<li class="">--}}
                {{--<a href="dropzone.html">--}}
                {{--<i class="menu-icon fa fa-caret-right"></i>--}}
                {{--Dropzone File Upload--}}
                {{--</a>--}}

                {{--<b class="arrow"></b>--}}
                {{--</li>--}}
            </ul>
        </li>


    @endif

    <li class="@if(_isInPage('settings')) active @endif">
        <a href="{{route('admin.settings.edit')}}" >
            <i class="menu-icon fa fa-pencil-square-o"></i>
            <span class="menu-text"> Settings </span>
        </a>
    </li>

    @if(in_array(Auth::user()->role ,['superadmin','admin']))
    <li class="@if(_isInPage('users')) open @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Users </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(_isInPage('users','only')) active @endif">
                <a href="{{ url('admin/users') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    List
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('users','create')) active @endif">
                <a href="{{ url('admin/users/create') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    New
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    @endif

    <li class="@if(_isInPage('members')) open @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Members </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(_isInPage('members','only')) active @endif">
                <a href="{{ url('admin/members') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    List
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('members','create')) active @endif">
                <a href="{{ url('admin/members/create') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    New
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <li class="@if(_isInPage('tour-event')) open @endif">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text">Custom Events </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(_isInPage('tour-event','only')) active @endif">
                <a href="{{ url('admin/tour-event') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    List
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('tour-event','create')) active @endif">
                <a href="{{ url('admin/tour-event/create') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    New
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(_isInPage('tour-event','payments')) active @endif">
                <a href="{{ url('admin/tour-event/payments') }}">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Payment History
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <li class="@if(_isInPage('contacts')) active @endif">
        <a href="{{route('admin.contacts.index')}}" class="">
            <i class="menu-icon fa fa-pencil-square-o"></i>
            <span class="menu-text"> Contacts </span>
        </a>
    </li>

</ul><!-- /.nav-list -->
